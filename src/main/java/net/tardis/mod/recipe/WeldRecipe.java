package net.tardis.mod.recipe;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class WeldRecipe {
	
	private Item output;
	private List<Item> inputs;
	private boolean repair = false;
	
	/**
	 * 
	 * @param stack - If isRepair is true, this is the item to repair, else, this is the item to create
	 * @param isRepair - If this recipe is a repair recipe or a creation one
	 * @param inputs - Items required for this recipe
	 */
	public WeldRecipe(Item stack, boolean isRepair, Item... inputs) {
		this.output = stack;
		this.inputs = Lists.newArrayList(inputs);
		this.repair = isRepair;
	}
	
	public Item getOutput() {
		return output;
	}
	
	public List<Item> getInputs() {
		return this.inputs;
	}
	
	public boolean isRepair() {
		return this.repair;
	}
	
	/**
	 * 
	 * @param repair - Item in the repair slot, or {@link ItemStack#EMPTY} if not a repair recipe
	 * @param recipe - Items to check this recipe against
	 * @return - If it matches this recipe
	 */
	public boolean matches(ItemStack repair, ItemStack... recipe) {
		List<Item> rec = new ArrayList<>();
		
		if(repair.getItem() != this.output && this.repair)
			return false;
		
		for(ItemStack s : recipe) {
			if(!s.isEmpty())
				rec.add(s.getItem());
		}
		if(rec.size() != inputs.size())
			return false;
		
		for(Item s : inputs) {
			if(!rec.contains(s))
				return false;
		}
		
		for(Item s : rec) {
			if(!inputs.contains(s))
				return false;
		}
		
		return true;
	}

}
