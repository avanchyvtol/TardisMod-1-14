package net.tardis.mod.recipe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.Blocks;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.compat.jei.AlembicRecipe;
import net.tardis.mod.items.TItems;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class Recipes {
	
	public static IRecipeType<AlembicRecipe> ALEMBIC = new IRecipeType<AlembicRecipe>() {};
	public static List<WeldRecipe> WELD_RECIPE = new ArrayList<WeldRecipe>();
	
	@SubscribeEvent
	public static void register(FMLCommonSetupEvent event) {
		WELD_RECIPE.add(new WeldRecipe(TItems.DEMAT_CIRCUIT, true, Items.ENDER_PEARL));
		WELD_RECIPE.add(new WeldRecipe(TItems.FLUID_LINK, false, Items.IRON_INGOT, Items.IRON_INGOT, TItems.CIRCUITS, TItems.MERCURY_BOTTLE));
		WELD_RECIPE.add(new WeldRecipe(TItems.FLUID_LINK, true, TItems.MERCURY_BOTTLE));
        WELD_RECIPE.add(new WeldRecipe(TItems.CHAMELEON_CIRCUIT, true, TItems.CIRCUITS));
		WELD_RECIPE.add(new WeldRecipe(TBlocks.ars_egg.asItem(), false, TItems.CIRCUITS, Items.EGG, TBlocks.xion_crystal.asItem()));
		WELD_RECIPE.add(new WeldRecipe(TItems.ATRIUM_UPGRADE, false, TItems.BLANK_UPGRADE, Items.REDSTONE, Items.ENDER_PEARL, Items.GOLD_INGOT));
		WELD_RECIPE.add(new WeldRecipe(TItems.POCKET_WATCH, false, TItems.CIRCUITS, TBlocks.xion_crystal.asItem(), Items.CLOCK));
        WELD_RECIPE.add(new WeldRecipe(TItems.CHAMELEON_CIRCUIT, false, TBlocks.xion_crystal.asItem(), TItems.CIRCUITS, Items.RED_DYE, Items.GREEN_DYE, Items.BLUE_DYE));
        WELD_RECIPE.add(new WeldRecipe(TItems.INTERSTITAL_ANTENNA,false,TBlocks.xion_crystal.asItem(),TItems.CIRCUITS,Items.GLASS_PANE,Items.IRON_INGOT,Items.REDSTONE_BLOCK));
        WELD_RECIPE.add(new WeldRecipe(TItems.INTERSTITAL_ANTENNA,true,Items.REDSTONE,TItems.CIRCUITS));
		WELD_RECIPE.add(new WeldRecipe(TItems.STATTENHEIM_REMOTE,false,TItems.INTERSTITAL_ANTENNA,Items.IRON_INGOT,Items.STONE_BUTTON));
        
		WELD_RECIPE.add(new WeldRecipe(TItems.SONIC_ACTIVATOR, false, TBlocks.xion_crystal.asItem(), Items.IRON_NUGGET, Items.IRON_NUGGET, Items.REDSTONE, Blocks.STONE_BUTTON.asItem()));
		WELD_RECIPE.add(new WeldRecipe(TItems.SONIC_EMITTER, false, TBlocks.xion_crystal.asItem(), Items.REDSTONE, Items.REDSTONE, Items.IRON_NUGGET, Items.IRON_NUGGET));
		WELD_RECIPE.add(new WeldRecipe(TItems.SONIC_END, false, Items.IRON_NUGGET, Items.IRON_NUGGET, Items.IRON_NUGGET));
		WELD_RECIPE.add(new WeldRecipe(TItems.SONIC_HANDLE, false, Items.IRON_NUGGET, Items.IRON_NUGGET, TItems.CIRCUITS, Items.IRON_NUGGET, Items.IRON_NUGGET));
		WELD_RECIPE.add(new WeldRecipe(TItems.SONIC, false, TItems.SONIC_ACTIVATOR, TItems.SONIC_EMITTER, TItems.SONIC_HANDLE, TItems.SONIC_END));
		
		
		
	}

}
