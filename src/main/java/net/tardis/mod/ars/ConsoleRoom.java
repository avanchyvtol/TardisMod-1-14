package net.tardis.mod.ars;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ArmorStandEntity;
import net.minecraft.entity.item.ItemFrameEntity;
import net.minecraft.entity.item.LeashKnotEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ReclamationTile;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ConsoleRoom {

	public static HashMap<ResourceLocation, ConsoleRoom> REGISTRY = new HashMap<ResourceLocation, ConsoleRoom>();
	public static ConsoleRoom TOYOTA;
	public static ConsoleRoom STEAM;
	public static ConsoleRoom JADE;
	public static ConsoleRoom NAUTILUS;
	public static ConsoleRoom OMEGA;
	public static ConsoleRoom ALABASTER;
	public static ConsoleRoom CORAL;
	
	//Broken exteriors
	public static ConsoleRoom BROKEN_STEAM;

	private ResourceLocation registryName;
	private ResourceLocation texture;
	private BlockPos offset;
	private ResourceLocation file;
	private TranslationTextComponent displayName;
	private boolean unlocked = true;
	
	public ConsoleRoom(BlockPos offset, boolean isDefault, ResourceLocation file, ResourceLocation texture, String name) {
		this.offset = offset;
		this.file = file;
		this.texture = texture;
		this.displayName = new TranslationTextComponent(name);
		this.unlocked = isDefault;
	}
	
	public ConsoleRoom(BlockPos offset, boolean unlocked, String file, String texture) {
		this(offset, unlocked,
				new ResourceLocation(Tardis.MODID, "tardis/structures/" + file),
				new ResourceLocation(Tardis.MODID, "textures/gui/interiors/" + texture + ".png"), 
				"interiors.tardis." + file);
	}
	
	
	public ConsoleRoom setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}
	
	public ResourceLocation getRegistryName() {
		return registryName;
	}
	
	public ResourceLocation getTexture() {
		return this.texture;
	}
	
	public TranslationTextComponent getDisplayName() {
		return this.displayName;
	}
	
	public boolean isDefault() {
		return this.unlocked;
	}

	@SubscribeEvent
	public static void registerConsoleRooms(FMLCommonSetupEvent event) {
		ConsoleRoom.STEAM = register(new ConsoleRoom(new BlockPos(13, 9, 12), true, "interior_steam", "steam"), "interior_steam");
		ConsoleRoom.JADE = register(new ConsoleRoom(new BlockPos(7, 11, 9), true, "interior_jade", "jade"), "interior_jade");
		ConsoleRoom.NAUTILUS = register(new ConsoleRoom(new BlockPos(15, 13, 11), false, "interior_nautilus", "nautilus"), "nautilus");
		ConsoleRoom.OMEGA = register(new ConsoleRoom(new BlockPos(15, 16, 15), false, "interior_omega", "omega"), "omega");
		ConsoleRoom.ALABASTER = register(new ConsoleRoom(new BlockPos(15, 8, 15), true, "interior_alabaster", "alabaster"), "alabaster");
		ConsoleRoom.CORAL = register(new ConsoleRoom(new BlockPos(12, 7, 12), false, "interior_coral", "coral"), "coral");
		
		ConsoleRoom.BROKEN_STEAM = register(new ConsoleRoom(new BlockPos(13, 8, 11), false, "interior_broken_steam", "steam"), "broken_steam");
	}

	public static ConsoleRoom register(ConsoleRoom room, ResourceLocation registryName) {
		REGISTRY.put(registryName, room.setRegistryName(registryName));
		return room;
	}

	public static ConsoleRoom register(ConsoleRoom room, String registryName) {
		return ConsoleRoom.register(room, new ResourceLocation(Tardis.MODID, registryName));
	}
    
	public void spawnConsoleRoom(ServerWorld world) {
		ConsoleTile console = null;
		CompoundNBT consoleData = null;
		BlockState consoleState = null;

		//Save the console
		if(world.getTileEntity(TardisHelper.TARDIS_POS) instanceof ConsoleTile) {
			console = (ConsoleTile)world.getTileEntity(TardisHelper.TARDIS_POS);
			consoleData = console.serializeNBT();
			consoleState = world.getBlockState(TardisHelper.TARDIS_POS);
		}

		List<ItemStack> allInvs = new ArrayList<ItemStack>();

		//Delete all old blocks
		BlockPos clearRadius = new BlockPos(20, 20, 20);
		BlockPos.getAllInBox(TardisHelper.TARDIS_POS.subtract(clearRadius), TardisHelper.TARDIS_POS.add(clearRadius)).forEach((pos) -> {
			if(world.getTileEntity(pos) instanceof IInventory) {
				IInventory inv = (IInventory)world.getTileEntity(pos);
				for(int i = 0; i < inv.getSizeInventory(); ++ i) {
					ItemStack stack = inv.getStackInSlot(i);
					if(!stack.isEmpty())
						allInvs.add(stack.copy());
				}
			}
			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
		});

		//Kill all non-living entities
		AxisAlignedBB killBox = new AxisAlignedBB(-20, -20, -20, 20, 20, 20).offset(TardisHelper.TARDIS_POS);
		BlockPos tpPos = TardisHelper.TARDIS_POS.offset(Direction.NORTH);
		for(Entity entity : world.getEntitiesWithinAABB(Entity.class, killBox)){
			if(!(entity instanceof LivingEntity))
				entity.remove();
			if(entity instanceof PlayerEntity)
				entity.setPositionAndUpdate(tpPos.getX() + 0.5, tpPos.getY() + 0.5, tpPos.getZ() + 0.5);
			if(entity instanceof ArmorStandEntity) {
				for (ItemStack armor : entity.getArmorInventoryList())
					allInvs.add(armor.copy());
				entity.remove();
			}
			if (entity instanceof ItemFrameEntity) {
				allInvs.add(((ItemFrameEntity)entity).getDisplayedItem());
				entity.remove();
			}
			if (entity instanceof LeashKnotEntity) {
				allInvs.add(new ItemStack(Items.LEAD));
				entity.remove();
			}
		}

		//Spawn console room
		Template temp = world.getStructureTemplateManager().getTemplate(file);
		temp.addBlocksToWorld(world, TardisHelper.TARDIS_POS.subtract(this.offset), new PlacementSettings().setIgnoreEntities(false));

		//Re-add console
		if(console != null) {
			world.setBlockState(TardisHelper.TARDIS_POS, consoleState);
			world.getTileEntity(TardisHelper.TARDIS_POS).deserializeNBT(consoleData);
		}

		//Spawn Reclamation Unit
		world.setBlockState(TardisHelper.TARDIS_POS.south(2), TBlocks.reclamation_unit.getDefaultState());
		TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS.south(2));
		if(te instanceof ReclamationTile) {
			ReclamationTile unit = (ReclamationTile)te;
			for(ItemStack stack : allInvs) {
				unit.addItem(stack);
			}
		}

		//Update lighting
		ChunkPos startPos = world.getChunk(TardisHelper.TARDIS_POS.subtract(temp.getSize())).getPos();
		ChunkPos endPos = world.getChunk(TardisHelper.TARDIS_POS.add(temp.getSize())).getPos();

		for(int x = startPos.x; x < endPos.x; ++x) {
			for(int z = startPos.z; z < endPos.z; ++z) {
				world.getChunkProvider().getLightManager().lightChunk(world.getChunk(x, z), true);
			}
		}

	}
}
