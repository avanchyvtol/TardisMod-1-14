package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.blocks.ReclamationBlock;

public class ReclamationTile extends TileEntity implements IInventory, ITickableTileEntity{
	protected int numPlayersUsing;
	private List<ItemStack> inv = new ArrayList<ItemStack>();
	
	public ReclamationTile() {
		super(TTiles.RECLAMATION_UNIT);
	}

	@Override
	public void clear() {
		inv.clear();
	}

	@Override
	public int getSizeInventory() {
		return 36; //set to chest
	}

	@Override
	public boolean isEmpty() {
		return inv.isEmpty();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return index < inv.size() ? inv.get(index) : ItemStack.EMPTY;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		this.resort();
		return this.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		if(index < inv.size())
			inv.remove(index);
		this.resort();
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if(index < this.inv.size())
			inv.set(index, stack);
		else this.inv.add(stack);
		this.resort();
		this.markDirty();
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}
	
	public void addItem(ItemStack stack) {
		this.inv.add(stack);
		this.resort();
		this.markDirty();
	}
	
	public void resort() {
		Iterator<ItemStack> items = inv.iterator();
		while(items.hasNext()) {
			if(items.next().isEmpty())
				items.remove();
		}
		
		this.markDirty();
	}

	@Override
	public void tick() {
		if(!world.isRemote && this.isEmpty())
			world.setBlockState(this.getPos(), Blocks.AIR.getDefaultState());
	}
	
	protected void onOpenOrClose() {
	      Block block = this.getBlockState().getBlock();
	      if (block instanceof ReclamationBlock) {
	         this.world.addBlockEvent(this.pos, block, 1, this.numPlayersUsing);
	         this.world.notifyNeighborsOfStateChange(this.pos, block);
      }

   }
	

	@Override
	public void openInventory(PlayerEntity player) {
      if (!player.isSpectator()) {
          if (this.numPlayersUsing < 0) {
             this.numPlayersUsing = 0;
          }

          ++this.numPlayersUsing;
          this.onOpenOrClose();
       }
	}

	@Override
	public void closeInventory(PlayerEntity player) {
		if (!player.isSpectator()) {
	         --this.numPlayersUsing;
	         this.onOpenOrClose();
      	}
	}
	
	 public static int getPlayersUsing(IBlockReader reader, BlockPos posIn) {
	      BlockState blockstate = reader.getBlockState(posIn);
	      if (blockstate.hasTileEntity()) {
	         TileEntity tileentity = reader.getTileEntity(posIn);
	         if (tileentity instanceof ReclamationTile) {
	            return ((ReclamationTile)tileentity).numPlayersUsing;
	         }
	      }

	      return 0;
   }
	 
	 

	@Override
	public boolean receiveClientEvent(int id, int type) {
		if (id == 1) {
	         this.numPlayersUsing = type;
	         return true;
	      } else {
	         return super.receiveClientEvent(id, type);
	      }
	}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		return IInventory.super.isItemValidForSlot(index, stack);
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		ListNBT items = compound.getList("items", Constants.NBT.TAG_COMPOUND);
		for(INBT i : items) {
			this.inv.add(ItemStack.read((CompoundNBT)i));
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT items = new ListNBT();
		for(ItemStack stack : this.inv) {
			if(!stack.isEmpty())
				items.add(stack.serializeNBT());
		}
		compound.put("items", items);
		return super.write(compound);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.write(new CompoundNBT()));
	}
	
	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}
	
	
}
