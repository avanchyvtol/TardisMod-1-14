package net.tardis.mod.tileentities;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ChestTileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.schematics.Schematic;

public class ShipComputerTile extends ChestTileEntity{

	private Schematic schematic;
	
	public ShipComputerTile() {
		super(TTiles.SHIP_COMPUTER);
	}
	
	public Schematic getSchematic() {
		return this.schematic;
	}
	
	public void setSchematic(Schematic sche) {
		this.schematic = sche;
		this.markDirty();
	}
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		if(compound.contains("schematic"))
			this.schematic = TardisRegistries.SCHEMATICS.getValue(new ResourceLocation(compound.getString("schematic")));
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.schematic != null)
			compound.putString("schematic", this.schematic.getRegistryName().toString());
		return super.write(compound);
	}
	
	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
	}
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.write(new CompoundNBT()));
	}
	
	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}
	

}
