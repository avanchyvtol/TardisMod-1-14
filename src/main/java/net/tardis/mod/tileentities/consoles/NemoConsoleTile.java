package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class NemoConsoleTile extends ConsoleTile{

	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -1, -2, 2, 2.5, 2);
	
	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

	public NemoConsoleTile(TileEntityType<?> type) {
		super(type);
	}
	
	public NemoConsoleTile() {
		this(TTiles.CONSOLE_NEMO);
	}

}
