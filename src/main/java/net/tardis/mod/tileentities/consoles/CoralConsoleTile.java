package net.tardis.mod.tileentities.consoles;

import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

import static net.tardis.mod.tileentities.TTiles.CONSOLE_CORAL;

/**
 * Created by Swirtzly
 * on 01/05/2020 @ 12:18
 */
public class CoralConsoleTile extends ConsoleTile {
    public CoralConsoleTile() {
        super(CONSOLE_CORAL);
        this.registerControlEntry(ControlRegistry.MONITOR);

    }


    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return new AxisAlignedBB(this.getPos()).expand(2, 4, 2);
    }


}
