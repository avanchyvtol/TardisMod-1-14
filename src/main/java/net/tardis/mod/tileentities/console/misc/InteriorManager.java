package net.tardis.mod.tileentities.console.misc;

import java.util.List;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.ChunkPos;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.StopHumMessage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class InteriorManager implements INBTSerializable<CompoundNBT>, ITickable{
	
	public static ResourceLocation KEY = new ResourceLocation(Tardis.MODID, "interior_manager");
	private ConsoleTile console;
	
	private int light = 0;
	private boolean alarm = false;
	private int interiorChangeTime = 0;
	private boolean humChanged = false;
	private InteriorHum hum;


	public InteriorManager(ConsoleTile console) {
		this.console = console;
		console.registerDataHandler(KEY, this);
		console.registerTicker(this);
		this.hum = TardisRegistries.HUM_REGISTRY.getValue(new ResourceLocation(Tardis.MODID, "toyota"));
	}
	
	public int getLight() {
		return light;
	}
	
	public void setLight(int light) {
		this.light = light;
		
		if(!this.console.getWorld().isRemote) {
			for(int x = -10; x < 10; ++x) {
				for(int z = -10; z < 10; ++z) {
					ChunkPos start = this.console.getWorld().getChunk(this.console.getPos()).getPos();
					ChunkPos pos = new ChunkPos(start.x + x, start.z + z);
					ILightCap cap = this.console.getWorld().getChunk(pos.x, pos.z).getCapability(Capabilities.LIGHT).orElse(null);
					if(cap != null) {
						cap.setLight(light);
					}
				}
			}
			
			ExteriorTile ext = console.getExterior().getExterior(console);
			if(ext != null) {
				ext.setLightLevel(light / 15.0F);
			}
			
		}
	}

	public void setAlarmOn(boolean alarm) {
		this.alarm = alarm;
		console.updateClient();
	}
	
	public boolean isAlarmOn() {
		return this.alarm;
	}
	
	public boolean canChangeInterior(){
		return this.interiorChangeTime == 0;
	}
	
	public void setInteriorChangeTime(int ticks) {
		this.interiorChangeTime = ticks;
	}
	
	public int getInteriorChangeTime() {
		return this.interiorChangeTime;
	}

	public void setHum(InteriorHum hum) {
		this.hum = hum;
	}

	public InteriorHum getHum() {
		return this.hum;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("light", light);
		tag.putBoolean("alarm", alarm);
		tag.putInt("interior_change_time", this.interiorChangeTime);
		tag.putString("hum", this.hum.getRegistryName().toString());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.light = nbt.getInt("light");
		this.alarm = nbt.getBoolean("alarm");
		this.interiorChangeTime = nbt.getInt("interior_change_time");
		if(nbt.contains("hum"))
			this.hum = TardisRegistries.HUM_REGISTRY.getValue(new ResourceLocation(nbt.getString("hum")));
	}

	@Override
	public void tick(ConsoleTile console) {
		if(this.interiorChangeTime > 0)
			--this.interiorChangeTime;

		//Hums
		if (hum.getEvent() != null && !console.getWorld().isRemote && (humChanged || console.getWorld().getGameTime() % hum.getDurationInTicks() == 0))
			console.getWorld().playSound(null, console.getPos(), hum.getEvent(), SoundCategory.AMBIENT, 0.25F, 1F);

	}

	public void toggleHum(InteriorHum newHum) {
		if (!console.getWorld().isRemote) {
			if (hum != null) {
				String oldHumPath = TardisRegistries.HUM_REGISTRY.getKeyFromValue(hum).getPath();
				hum = newHum;
				List<ServerPlayerEntity> players = ServerLifecycleHooks.getCurrentServer().getPlayerList().getPlayers();
				players.stream().filter(p -> p.getPosition().withinDistance(console.getPos(), 30)).forEach(p -> Network.sendTo(new StopHumMessage(oldHumPath), p));
			}

			console.updateClient();
			humChanged = true;
		}

	}

}
