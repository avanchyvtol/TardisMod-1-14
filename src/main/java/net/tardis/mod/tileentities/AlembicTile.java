package net.tardis.mod.tileentities;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisItemTags;

public class AlembicTile extends TileEntity implements ITickableTileEntity, IInventory{

	public static final int MAX_TIME = 200;
	private NonNullList<ItemStack> inv = NonNullList.withSize(6, ItemStack.EMPTY);
	private int progress = 0;
	private int burnTime = 0;
	private int maxBurnTime = 1;
	private int mercury = 0;
	private int water = 0;
	protected NonNullList<ItemStack> items = NonNullList.withSize(3, ItemStack.EMPTY);
	
	public AlembicTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public AlembicTile() {
		super(TTiles.ALEMBIC);
	}

	@Override
	public void tick() {
		
		if(this.burnTime > 0) {
			--this.burnTime;
		}
		
		//Consume Fuel
		if(this.burnTime <= 0 && !this.getStackInSlot(2).isEmpty() && isFuel(this.getStackInSlot(3))) {
			this.burnTime = this.maxBurnTime = ForgeHooks.getBurnTime(this.getStackInSlot(3));
			this.getStackInSlot(3).shrink(1);
			this.markDirty();
		}
		
		//If has fuel and has something to process
		int emptySpace = 1000 - this.mercury;
		if(this.burnTime > 0 && this.getStackInSlot(2).getItem().isIn(TardisItemTags.CINNABAR) && this.water > 0 && emptySpace > 0) {
			++progress;
			//If it's done
			if(this.progress >= MAX_TIME) {
				this.progress = 0;
				this.getStackInSlot(2).shrink(1);
				
				int amt = water > emptySpace ? emptySpace : water;
				this.water -= amt;
				this.mercury += amt;
				this.markDirty();
			}
		}
		else this.progress = 0;
		
		if(this.mercury >= 333 && this.getStackInSlot(4).getItem() == Items.GLASS_BOTTLE) {
			this.mercury -= 333;
			this.getStackInSlot(4).shrink(1);
			if(this.getStackInSlot(5).isEmpty())
				this.setInventorySlotContents(5, new ItemStack(TItems.MERCURY_BOTTLE));
			else if(this.getStackInSlot(5).getItem() == TItems.MERCURY_BOTTLE)
				this.getStackInSlot(5).grow(1);
		}
		
		this.getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).ifPresent(cap -> {
			FluidStack fluid = cap.getFluidInTank(0);
			if(fluid.getFluid() == Fluids.WATER) {
				int amtToTake = 1000 - water;
				water += cap.drain(amtToTake, FluidAction.EXECUTE).getAmount();
			}
			ItemStack stack = cap.getContainer();
			
			if(!ItemStack.areItemsEqual(this.getStackInSlot(1), stack) && !this.getStackInSlot(1).isEmpty())
				return;
			
			if(ItemStack.areItemsEqual(this.getStackInSlot(1), stack))
				this.getStackInSlot(1).grow(stack.getCount());
			else if(this.getStackInSlot(1).isEmpty())
				this.setInventorySlotContents(1, stack);
			if(this.getStackInSlot(0).getCount() == 1)
				this.setInventorySlotContents(0, ItemStack.EMPTY);
			else this.getStackInSlot(0).shrink(1);
		});
		
	}

	@Override
	public void clear() {
		inv.clear();
		this.markDirty();
	}

	@Override
	public int getSizeInventory() {
		return inv.size();
	}

	@Override
	public boolean isEmpty() {
		return inv.isEmpty();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return index < inv.size() ? inv.get(index) : ItemStack.EMPTY;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		return this.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		if(index < this.inv.size()) {
			ItemStack stack = inv.get(index);
			inv.set(index, ItemStack.EMPTY);
			return stack;
		}
		return ItemStack.EMPTY;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if(index < inv.size())
			inv.set(index, stack);
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}
	
	public int getWater() {
		return this.water;
	}
	
	public int getMercury() {
		return this.mercury;
	}
	
	public int getBurnTime() {
		return this.burnTime;
	}
	
	public int getMaxBurnTime() {
		return this.maxBurnTime;
	}
	
	public float getPercent() {
		return this.progress / 200.0F;
	}
	
	public static boolean isFuel(ItemStack stack) {
	      return ForgeHooks.getBurnTime(stack) > 0;
	   }
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		this.burnTime = compound.getInt("burn_time");
		this.maxBurnTime = compound.getInt("max_burn_time");
		this.progress = compound.getInt("progress");
		this.water = compound.getInt("water");
		this.mercury = compound.getInt("mercury");
		
		ListNBT items = compound.getList("items", Constants.NBT.TAG_COMPOUND);
		int index = 0;
		for(INBT nbt : items) {
			this.inv.set(index, ItemStack.read((CompoundNBT)nbt));
			++index;
		}
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		compound.putInt("burn_time", this.burnTime);
		compound.putInt("max_burn_time", this.maxBurnTime);
		compound.putInt("progress", this.progress);
		compound.putInt("water", this.water);
		compound.putInt("mercury", this.mercury);
		
		ListNBT items = new ListNBT();
		for(ItemStack stack : inv) {
			if(!stack.isEmpty())
				items.add(stack.serializeNBT());
		}
		compound.put("items", items);
		return super.write(compound);
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.serializeNBT());
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.write(new CompoundNBT());
	}
	
	
}
