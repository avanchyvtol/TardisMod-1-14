package net.tardis.mod.tileentities;

import javax.annotation.Nullable;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.containers.QuantiscopeSonicContainer;
import net.tardis.mod.containers.QuantiscopeWeldContainer;
import net.tardis.mod.recipe.Recipes;
import net.tardis.mod.recipe.WeldRecipe;

public class QuantiscopeTile extends TileEntity implements IInventory, ITickableTileEntity{

	private NonNullList<ItemStack> inv = NonNullList.withSize(7, ItemStack.EMPTY);
	private EnumMode mode = EnumMode.SONIC;
	private WeldRecipe weldRecipe;
	private int progress = 0;
	
	public QuantiscopeTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public QuantiscopeTile() {
		super(TTiles.QUANTISCOPE);
	}
	
	@Override
	public void tick() {
		if(mode == EnumMode.WELD) {
			if(this.weldRecipe == null) {
				this.weldRecipe = this.getRecipe();
			}
			else if(this.shouldWeld()) {
				
				//If first progress tick, update client
				if(this.progress == 0)
					if(!world.isRemote)
						this.world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
				
				++progress;
				if(progress >= this.mode.workTime) {
					finish();
				}
				
				if(world.isRemote && world.getGameTime() % 20 == 0)
					world.addParticle(ParticleTypes.LAVA, this.getPos().getX() + 0.5, this.getPos().getY(), this.getPos().getZ() + 0.5, 0, 0, 0);
			}
			else {
				this.weldRecipe = null;
				this.progress = 0;
				if(!world.isRemote)
					this.world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
			}
				
		}
	}
	
	private boolean shouldWeld() {
		if(this.weldRecipe != null) {
			return this.doesMatrixMatchRecipe(this.weldRecipe) && this.getStackInSlot(6).isEmpty();
		}
		return false;
	}
	
	private void finish() {
		ItemStack result;
		
		if(this.weldRecipe.isRepair()) {
			result = this.getStackInSlot(5).copy();
			result.setDamage(0);
		}
		else {
			result = new ItemStack(this.weldRecipe.getOutput());
		}
		
		this.setInventorySlotContents(6, result);
		for(int i = 0; i < 6; ++i)
			this.getStackInSlot(i).shrink(1);
		this.progress = 0;
		this.weldRecipe = null;
		if(!world.isRemote)
			this.world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
	}
	
	private boolean doesMatrixMatchRecipe(WeldRecipe recipe) {
		ItemStack repair = this.getStackInSlot(5);
		return recipe.matches(repair, this.getStackInSlot(0), this.getStackInSlot(1), this.getStackInSlot(2), this.getStackInSlot(3), this.getStackInSlot(4));
	}
	
	@Nullable
	private WeldRecipe getRecipe() {
		for(WeldRecipe rec : Recipes.WELD_RECIPE) {
			if(this.doesMatrixMatchRecipe(rec))
				return rec;
		}
		return null;
	}
	
	public float getProgress() {
		return this.progress / 200.0F;
	}
	
	public void setMode(EnumMode mode) {
		this.mode = mode;
		this.markDirty();
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 2);
	}
	@Override
	public void clear() {
		inv.clear();
	}

	@Override
	public int getSizeInventory() {
		return inv.size();
	}

	@Override
	public boolean isEmpty() {
		return inv.isEmpty();
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return index < inv.size() ? inv.get(index) : ItemStack.EMPTY;
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		this.markDirty();
		return this.getStackInSlot(index).split(count);
	}

	@Override
	public ItemStack removeStackFromSlot(int index) {
		ItemStack stack = this.getStackInSlot(index);
		if(index < inv.size())
			inv.set(index, ItemStack.EMPTY);
		this.markDirty();
		return stack;
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		if(index < inv.size())
			inv.set(index, stack);
		this.markDirty();
	}

	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		ListNBT items = compound.getList("items", Constants.NBT.TAG_COMPOUND);
		int index = 0;
		for(INBT nbt : items) {
			this.inv.set(index, ItemStack.read((CompoundNBT)nbt));
			++index;
		}
		this.mode = EnumMode.values()[compound.getInt("mode")];
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		ListNBT items = new ListNBT();
		for(ItemStack stack : inv) {
			items.add(stack.serializeNBT());
		}
		compound.put("items", items);
		compound.putInt("mode", this.mode.ordinal());
		return super.write(compound);
	}

	@Override
	public boolean isUsableByPlayer(PlayerEntity player) {
		return true;
	}
	
	public Container createContainer(int id, PlayerInventory inv) {
		return mode.fact.create(id, inv, this);
	}
	
	
	
	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), -1, this.serializeNBT());
	}



	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.deserializeNBT(pkt.getNbtCompound());
	}



	public static enum EnumMode{
		SONIC(QuantiscopeSonicContainer::new),
		WELD(QuantiscopeWeldContainer::new);
		
		IFactory<Container> fact;
		int workTime = 200;
		
		EnumMode(IFactory<Container> fact){
			this.fact = fact;
		}
		
		EnumMode(IFactory<Container> fact, int workTime){
			this.fact = fact;
			this.workTime = workTime;
		}
	}
	
	public static interface IFactory<T extends Container>{
		T create(int id, PlayerInventory inv, QuantiscopeTile tile);
	}

}
