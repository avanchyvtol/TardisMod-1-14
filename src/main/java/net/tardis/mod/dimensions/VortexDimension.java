package net.tardis.mod.dimensions;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.biome.provider.SingleBiomeProvider;
import net.minecraft.world.biome.provider.SingleBiomeProviderSettings;
import net.minecraft.world.border.WorldBorder;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.dimension.Dimension;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.IRenderHandler;
import net.tardis.mod.client.ModelRegistry;

public class VortexDimension extends Dimension{

	public static SingleBiomeProvider PROVIDER = new SingleBiomeProvider(new SingleBiomeProviderSettings().setBiome(Biomes.THE_VOID));

	public VortexDimension(World worldIn, DimensionType typeIn) {
		super(worldIn, typeIn);
	}

	@Override
	public ChunkGenerator<?> createChunkGenerator() {
		TardisChunkGenerator gen = new TardisChunkGenerator(this.world, PROVIDER, new TardisChunkGenerator.GeneratorSettingsTardis());
		return gen;
	}

	@Override
	public WorldBorder createWorldBorder() {
		WorldBorder bord = new WorldBorder();
		bord.setCenter(0, 0);
		bord.setSize(10);
		return bord;
	}

	@Override
	public boolean canDoRainSnowIce(Chunk chunk) {
		return false;
	}

	@Override
	public boolean canMineBlock(PlayerEntity player, BlockPos pos) {
		return false;
	}

	@Override
	public double getHorizon() {
		return 0;
	}

	@Override
	public boolean shouldMapSpin(String entity, double x, double z, double rotation) {
		return true;
	}

	@Override
	public BlockPos findSpawn(ChunkPos chunkPosIn, boolean checkValid) {
		return BlockPos.ZERO;
	}

	@Override
	public BlockPos findSpawn(int posX, int posZ, boolean checkValid) {
		return BlockPos.ZERO;
	}

	@Override
	public float calculateCelestialAngle(long worldTime, float partialTicks) {
		return 0F;
	}

	@Override
	public boolean isSurfaceWorld() {
		return false;
	}

	@Override
	public Vec3d getFogColor(float celestialAngle, float partialTicks) {
		return new Vec3d(0, 0, 0);
	}

	@Override
	public boolean canRespawnHere() {
		return false;
	}

	@Override
	public boolean doesXZShowFog(int x, int z) {
		return false;
	}

	@Override
	public boolean isNether() {
		return false;
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public IRenderHandler getSkyRenderer() {
		return ModelRegistry.VORTEX_RENDER;
	}

}
