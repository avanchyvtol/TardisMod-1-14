package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.BotiWorld;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.boti.WorldShell;

public class BOTIMessage {

	WorldShell shell;
	BlockPos pos;
	
	public BOTIMessage(BlockPos pos, WorldShell shell) {
		this.pos = pos;
		this.shell = shell;
	}
	
	public static void encode(BOTIMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		mes.shell.writeToBuffer(buf);
	}
	
	public static BOTIMessage decode(PacketBuffer buf) {
		return new BOTIMessage(buf.readBlockPos(), WorldShell.readFromBuffer(buf));
	}
	
	public static void handle(BOTIMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TileEntity te = Minecraft.getInstance().world.getTileEntity(mes.pos);
			if(te instanceof IBotiEnabled) {
				IBotiEnabled boti = (IBotiEnabled)te;
				boti.setBotiWorld(mes.shell);
				boti.getBotiWorld().setupTEs();
				boti.getBotiWorld().setNeedsUpdate(true);
			}
		});
		cont.get().setPacketHandled(true);
	}
}
