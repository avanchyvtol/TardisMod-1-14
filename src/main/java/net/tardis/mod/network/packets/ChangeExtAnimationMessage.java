package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.animation.IExteriorAnimation.ExteriorAnimationEntry;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.function.Supplier;

public class ChangeExtAnimationMessage {

    ResourceLocation name;

    public ChangeExtAnimationMessage(ResourceLocation name) {
        this.name = name;
    }

    public static void encode(ChangeExtAnimationMessage mes, PacketBuffer buf) {
        buf.writeResourceLocation(mes.name);
    }

    public static ChangeExtAnimationMessage decode(PacketBuffer buf) {
        return new ChangeExtAnimationMessage(buf.readResourceLocation());
    }

    public static void handle(ChangeExtAnimationMessage mes, Supplier<NetworkEvent.Context> cont) {
        cont.get().enqueueWork(() -> {
            TileEntity te = cont.get().getSender().world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                ConsoleTile console = (ConsoleTile) te;
                ExteriorAnimationEntry<?> entry = TardisRegistries.EXTERIOR_ANIMATIONS.getValue(mes.name);
                if (entry != null) {
                    console.getExteriorManager().setExteriorAnimation(entry.getRegistryName());
                }
            }
        });
        cont.get().setPacketHandled(true);
    }
}
