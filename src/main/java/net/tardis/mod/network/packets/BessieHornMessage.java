package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.entity.BessieEntity;
import net.tardis.mod.sounds.TSounds;

import java.util.function.Supplier;

/**
 * Created by Swirtzly
 * on 11/04/2020 @ 23:29
 */
public class BessieHornMessage {

    public BessieHornMessage() {
    }

    public static void encode(BessieHornMessage mes, PacketBuffer buffer) {
    }

    public static BessieHornMessage decode(PacketBuffer buffer) {
        return new BessieHornMessage();
    }

    public static void handle(BessieHornMessage mes, Supplier<NetworkEvent.Context> context) {
        context.get().enqueueWork(() -> {
            if (context.get().getSender().getRidingEntity() instanceof BessieEntity) {
                BessieEntity bessieEntity = (BessieEntity) context.get().getSender().getRidingEntity();
                if (bessieEntity.getLastHorn() == 0 || System.currentTimeMillis() - bessieEntity.getLastHorn() >= 2000) {
                    bessieEntity.setLastHorn(System.currentTimeMillis());
                    bessieEntity.playSound(TSounds.BESSIE_HORN, 1, 1);
                }
            }
        });
        context.get().setPacketHandled(true);
    }

}