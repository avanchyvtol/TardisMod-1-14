package net.tardis.mod.network.packets;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;

import java.util.function.Supplier;


public class VMTeleportMessage {

	public BlockPos pos;
	public int value;
	public boolean teleportPrecise;
	
	public VMTeleportMessage(BlockPos pos, int value, boolean teleportPrecise) {
		this.pos = pos;
		this.value = value;
		this.teleportPrecise = teleportPrecise;
	}


	public static void encode(VMTeleportMessage mes,PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.value);
		buf.writeBoolean(mes.teleportPrecise);
	}
	
	public static VMTeleportMessage decode(PacketBuffer buf) {
		return new VMTeleportMessage(buf.readBlockPos(),buf.readInt(),buf.readBoolean());
	}
	
        public static void handle(VMTeleportMessage mes,  Supplier<NetworkEvent.Context> ctx)
        {
        	ctx.get().enqueueWork(()->{
        		 	ServerPlayerEntity sender = ctx.get().getSender();
        		 	if (!LandingSystem.isPosBelowOrAboveWorld(sender.world.getDimension().getType(),mes.pos.getY()) 
        		 			&& LandingSystem.isBlockBehindWorldBorder(sender.getServerWorld(), mes.pos.getX(),mes.pos.getY(),mes.pos.getZ())) {
        		 		mes.pos = mes.teleportPrecise ? mes.pos : LandingSystem.getTopBlock(sender.world, mes.pos); //Ensures the vm will tp you to a safe spot
        		 		SpaceTimeCoord coord = new SpaceTimeCoord(sender.dimension, mes.pos);
                    	sender.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> cap.setDestination(coord));
                    	sender.getServerWorld().playSound(null, sender.getPosition(), TSounds.VM_TELEPORT, SoundCategory.BLOCKS, 0.25F, 1F);
                     	sender.teleport(sender.world.getServer().getWorld(TDimensions.VORTEX_TYPE), 0, 500, 0, 0, 90);
        		 	}
        		 	else {
        		 		sender.sendStatusMessage(new TranslationTextComponent("message.vm.invalidPos"), false);
        		 	}
        		 			
        	});
        	ctx.get().setPacketHandled(true);
        }


}
