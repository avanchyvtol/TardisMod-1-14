package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.sonic.capability.SonicCapability;

/**
 * Created by Swirtzly
 * on 23/03/2020 @ 00:06
 */
public class SyncSonicMessage {

    public CompoundNBT data;
    public Hand hand;

    public SyncSonicMessage(Hand hand, CompoundNBT data) {
        this.hand = hand;
        this.data = data;
    }

    public static void encode(SyncSonicMessage mes, PacketBuffer buffer) {
        buffer.writeInt(mes.hand.ordinal());
        buffer.writeCompoundTag(mes.data);
    }

    public static SyncSonicMessage decode(PacketBuffer buffer) {
        return new SyncSonicMessage(Hand.values()[buffer.readInt()], buffer.readCompoundTag());
    }

    public static void handle(SyncSonicMessage mes, Supplier<NetworkEvent.Context> ctx) {
        ctx.get().enqueueWork(() -> SonicCapability.getForStack(Minecraft.getInstance().player.getHeldItem(mes.hand)).ifPresent((data) -> {
            data.deserializeNBT(mes.data);
        }));
        ctx.get().setPacketHandled(true);
    }

}
