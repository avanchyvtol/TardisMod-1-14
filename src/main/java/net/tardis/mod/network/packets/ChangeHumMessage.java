package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;

public class ChangeHumMessage {
    
	private ResourceLocation name;

    public ChangeHumMessage() {
    }

    public ChangeHumMessage(ResourceLocation name) {
        this.name = name;
    }

    public static void encode(ChangeHumMessage mes, PacketBuffer buf) {
        buf.writeResourceLocation(mes.name);
    }

    public static ChangeHumMessage decode(PacketBuffer buf) {
        return new ChangeHumMessage(buf.readResourceLocation());
    }

    public static void handle(ChangeHumMessage mes, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
        		ConsoleTile console = TardisHelper.getConsole(ctx.get().getSender().getServer(), ctx.get().getSender().getUniqueID());
                if(console != null)
                	console.getInteriorManager().toggleHum(TardisRegistries.HUM_REGISTRY.getValue(mes.name));
        });
        ctx.get().setPacketHandled(true);
    }
}

