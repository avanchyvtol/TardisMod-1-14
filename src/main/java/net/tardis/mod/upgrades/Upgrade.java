package net.tardis.mod.upgrades;

import javax.annotation.Nullable;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public abstract class Upgrade{
	
	private UpgradeEntry<?> entry;
	private ConsoleTile console;
	
	private ItemStack stack;
	private Class<? extends Subsystem> parent;
	
	/**
	 * See {@link UpgradeEntry} for params
	 */
	protected Upgrade(UpgradeEntry<?> entry, ConsoleTile tile, Class<? extends Subsystem> clazz) {
		this.entry = entry;
		this.console = tile;
		this.parent = clazz;
	}
	
	/**
	 * Verifies the chached console and returns it if it's fine
	 * Otherwise tries to get the new console from the world
	 * @return Console
	 */
	public ConsoleTile getConsole() {
		if(console != null && !console.isRemoved())
			return console;
		if(console != null && console.getWorld() != null) {
			TileEntity te = console.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
			if(te instanceof ConsoleTile)
				return (ConsoleTile)te;
		}
		return null;
	}
	
	public UpgradeEntry<?> getEntry(){
		return this.entry;
	}
	
	//Default functions
	
	/**
	 * 
	 * @return The ItemStack reperesenting this upgrade
	 */
	public ItemStack getStack() {
		if(this.stack == null || stack.isEmpty())
			return (stack = this.getStackFromWorld(console.getWorld()));
		return stack;
	}
	
	/**
	 * INTERNAL USE ONLY! USE {@link #getStack()} instead!
	 * @param world
	 * @return
	 */
	private ItemStack getStackFromWorld(World world) {
		ITardisWorldData data = world.getCapability(Capabilities.TARDIS_DATA).orElse(null);
		if(data != null) {
			PanelInventory inv = data.getEngineInventoryForSide(Direction.SOUTH);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				if(inv.getStackInSlot(i).getStack().getItem() == this.getEntry().getItem()) {
					return inv.getStackInSlot(i);
				}
			}
		}
		return ItemStack.EMPTY;
	}
	
	/**
	 * 
	 * @return If this upgrade can do it's function
	 */
	public boolean isUsable() {
		return !this.getStack().isEmpty();
	}
	/**
	 * 
	 * @param amount - Amount of damage to do
	 * @param type - The type of damage, see #{@link DamageType} for what they do
	 * @param entity - The ServerPlayer damaging it, can be null
	 */
	public void damage(int amount, DamageType type, @Nullable ServerPlayerEntity entity) {
		
		if(type == DamageType.ITEM || type == DamageType.BOTH)
			this.getStack().attemptDamageItem(amount, this.getConsole().getWorld().rand, entity);
		
		if(type == DamageType.PARENT || type == DamageType.BOTH) {
			if(this.parent != null)
				console.getSubsystem(this.parent).ifPresent(sys -> sys.damage(entity, amount));
		}
	}
	
	/**
	 * Called when the TARDIS lands
	 */
	public abstract void onLand();
	/**
	 * Called when the TARDIS takes off
	 */
	public abstract void onTakeoff();

	/**
	 * Called once a second in flight
	 */
    public abstract void onFlightSecond();
    
    /**
     * PARENT damages *only* the linked subsystem's health
     * ITEM damages this Upgrades's Item in the engine
     * BOTH does what it says on the tin
     */
    public static enum DamageType{
    	BOTH,
    	PARENT,
    	ITEM
    }

}
