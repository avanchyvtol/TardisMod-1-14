package net.tardis.mod.items;

import java.util.List;


import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;

public class StatRemoteItem extends Item{

	public StatRemoteItem(Properties properties) {
		super(properties);
	}
	
	@Override
    public ActionResultType onItemUse(ItemUseContext context) {
		if (!context.getWorld().isRemote && !Helper.isDimensionBlocked(context.getWorld().getDimension().getType())) {
			context.getItem().getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> {
				cap.onClick(context.getWorld(), context.getPlayer(), context.getPos());
			});
			return ActionResultType.SUCCESS;
		}
		else if (Helper.isDimensionBlocked(context.getWorld().getDimension().getType())){
			context.getPlayer().sendStatusMessage(Constants.Translations.CANT_USE_IN_TARDIS, true);
			return ActionResultType.FAIL;
		}
		return ActionResultType.SUCCESS;
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.stat_remote.use"));
		stack.getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> {
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.tardis_owner", cap.getOwner()));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.exterior_dim", Helper.formatDimName(cap.getExteriorDim())));
			tooltip.add(new TranslationTextComponent("tooltip.stat_remote.exterior_pos", Helper.formatBlockPos(cap.getExteriorPos())));
            tooltip.add(new TranslationTextComponent("tooltip.stat_remote.time_left", cap.getTimeToArrival()));

		});
	}

	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		stack.getCapability(Capabilities.REMOTE_CAP).ifPresent(cap -> {
			if(!worldIn.isRemote) {
				if (cap.getOwner() == null) {
					cap.setOwner(entityIn.getUniqueID());
				}
				else if (cap.getOwner() !=null){
					cap.tick(worldIn, entityIn);
				}
			}
			
		});
		super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);
	}

}
