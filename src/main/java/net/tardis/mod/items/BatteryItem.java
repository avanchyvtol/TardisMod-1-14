package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.artron.IArtronBattery;

/**
 * Created by 50ap5ud5
 * on 24 Mar 2020 @ 9:29:34 am
 */

//Used for providing charge to items
//Also used to power machines
public class BatteryItem extends TardisPartItem implements IArtronBattery{
	
	private static final String CHARGE = "artron";
	private final float chargeRateMultiplier;
	private final float dischargeRateMultiplier;
	
	public BatteryItem(float chargeRateMultiplier, float dischargeRateMultiplier) {
		this.chargeRateMultiplier = chargeRateMultiplier;
		this.dischargeRateMultiplier = dischargeRateMultiplier;
	}
	
	//Returns how much was charged
	//Also writes the changes in data to nbt
	@Override
	public float charge(ItemStack stack, float amount) {
		amount = amount * this.chargeRateMultiplier;
		float charge = stack.getOrCreateTag().getFloat(CHARGE);
		float maxCharge = this.getMaxCharge(stack);
		//If adding more will go over the max charge, return however much is needed to reach max.
		if (charge + amount >= maxCharge) {
			float chargeToAdd = maxCharge - charge;
			this.writeCharge(stack, charge + chargeToAdd); //We need to write to nbt everytime we want to save data
			return chargeToAdd;
		}
		else {
			this.writeCharge(stack, charge + amount);
			return amount;
		}
	}
	

	@Override
	public float discharge(ItemStack stack, float amount) {
		float current = stack.getOrCreateTag().getFloat(CHARGE);
		float dischargeRate = amount * this.dischargeRateMultiplier;
		float chargeToTake = current - dischargeRate;
		if(amount <= current && chargeToTake > 0) {
			writeCharge(stack, chargeToTake);
			return amount;
		}
		writeCharge(stack, 0);
		return current;
	}

	@Override
	public float getMaxCharge(ItemStack stack) {
		return 250;
	}

	@Override
	public float getCharge(ItemStack stack) {
		return readCharge(stack);
	}
	
	public void writeCharge(ItemStack stack, float charge) {
		stack.getOrCreateTag().putFloat(CHARGE, charge);
	}
	
	public float readCharge(ItemStack stack) {
		return stack.getOrCreateTag().getFloat(CHARGE);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.artron_battery.max_charge", this.getMaxCharge(stack)));
		tooltip.add(new TranslationTextComponent("tooltip.artron_battery.charge", this.getCharge(stack)));
	}
	
	
	
}
