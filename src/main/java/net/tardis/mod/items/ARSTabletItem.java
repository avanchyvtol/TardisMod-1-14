package net.tardis.mod.items;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.GuiItemContext;
import net.tardis.mod.properties.Prop;

public class ARSTabletItem extends Item {

	public ARSTabletItem() {
		super(Prop.Items.ONE);
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(worldIn.isRemote)
			Tardis.proxy.openGUI(Constants.Gui.ARS_TABLET, new GuiItemContext(playerIn.getHeldItem(handIn)));
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

}
