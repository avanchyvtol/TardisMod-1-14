package net.tardis.mod.items;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.tileentities.SquarenessChamelonTile;

import java.util.Iterator;

/**
 * Created by Swirtzly
 * on 15/04/2020 @ 10:34
 */
public class SquarenessGunItem extends Item {
    public SquarenessGunItem() {
        super(new Item.Properties().maxStackSize(1).group(TItemGroups.MAIN));
    }

    public static AxisAlignedBB getSelection(BlockPos clickedBlock, Direction direction) {
        AxisAlignedBB box = null;
        switch (direction) {
            case EAST:
            case WEST:
                box = new AxisAlignedBB(clickedBlock.up().north(), clickedBlock.down().south());
                break;
            case NORTH:
            case SOUTH:
                box = new AxisAlignedBB(clickedBlock.up().west(), clickedBlock.down().east());
                break;
            case DOWN:
            case UP:
                box = new AxisAlignedBB(clickedBlock.north().west(), clickedBlock.south().east());
                break;
            default:
                break;
        }
        return box;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand handIn) {

        RayTraceResult result = SonicItem.getPosLookingAt(player, 5);
        if (result != null && result.getType() == RayTraceResult.Type.BLOCK) {
            BlockRayTraceResult blockRayTraceResult = (BlockRayTraceResult) result;
            BlockPos clickedBlock = blockRayTraceResult.getPos();
            Direction direction = Direction.getFacingFromVector((float) (player.posX - clickedBlock.getX()), (float) (player.posY - clickedBlock.getY()), (float) (player.posZ - clickedBlock.getZ()));

            AxisAlignedBB box = getSelection(clickedBlock, direction);

            if (box == null) {
                return super.onItemRightClick(world, player, handIn);
            }

            for (Iterator<BlockPos> iterator = BlockPos.getAllInBox(new BlockPos(box.maxX, box.maxY, box.maxZ), new BlockPos(box.minX, box.minY, box.minZ)).iterator(); iterator.hasNext(); ) {
                BlockPos pos = iterator.next();
                BlockState blockState = world.getBlockState(pos);
                if (!(blockState.getBlock() instanceof IDontBreak) && blockState.getBlock() != Blocks.BEDROCK && !blockState.hasTileEntity()) {
                    world.setBlockState(pos, TBlocks.squareness.getDefaultState());
                    if(world.getTileEntity(pos) instanceof SquarenessChamelonTile){
                        SquarenessChamelonTile chamelonTile = (SquarenessChamelonTile) world.getTileEntity(pos);
                        if (chamelonTile != null) {
                            chamelonTile.setState(blockState);
                        }
                    }
                }
            }

        }
        return super.onItemRightClick(world, player, handIn);
    }

}
