package net.tardis.mod.items;

import net.minecraft.item.Item;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.properties.Prop;

/**
 * Created by Swirtzly
 * on 22/08/2019 @ 20:15
 */
public class TardisPartItem extends Item{

    public TardisPartItem() {
        super(Prop.Items.ONE.group(TItemGroups.MAINTENANCE).maxDamage(250));
    }
    
    public TardisPartItem(Item.Properties prop) {
    	super(prop);
    }
}
