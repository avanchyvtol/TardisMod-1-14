package net.tardis.mod.commands;

import com.mojang.brigadier.CommandDispatcher;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.tardis.mod.Tardis;
import net.tardis.mod.commands.subcommands.*;

public class TardisCommand {
    public static void register(CommandDispatcher<CommandSource> dispatcher){
        dispatcher.register(
                Commands.literal(Tardis.MODID)
                        .then(CreateCommand.register(dispatcher))
                        .then(InteriorCommand.register(dispatcher))
                        .then(RefuelCommand.register(dispatcher))
        );
    }
}
