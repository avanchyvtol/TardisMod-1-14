package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.block.BlockState;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.blocks.BrokenExteriorBlock;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class CreateCommand extends TCommand {
    private static final CreateCommand CMD = new CreateCommand();

    public CreateCommand() {
        super(PermissionEnum.CREATE);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("create")
                .executes(CMD);
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        CommandSource source = context.getSource();

        if (canExecute(source)) {

            ServerPlayerEntity player = source.asPlayer();
            DimensionType interior = TardisHelper.setupPlayersTARDIS(player);
            BlockPos pos = player.getPosition().up().offset(player.getHorizontalFacing(), 2);
            player.world.setBlockState(pos, TBlocks.broken_exterior.getDefaultState());
            TileEntity te = player.world.getTileEntity(pos);

            if (te instanceof BrokenExteriorTile) {
                BrokenExteriorTile brokeBoi = (BrokenExteriorTile) te;
                brokeBoi.setConsoleDimension(interior);
                brokeBoi.increaseLoyalty(101);

                BlockState state = player.world.getBlockState(pos);
                ((BrokenExteriorBlock) state.getBlock()).replace(player.world, pos, interior, state.get(BlockStateProperties.HORIZONTAL_FACING));
            }
        } else {
            source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
        }

        return Command.SINGLE_SUCCESS;
    }
}
