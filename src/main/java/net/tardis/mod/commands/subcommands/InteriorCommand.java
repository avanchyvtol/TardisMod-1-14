package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.exceptions.NoDimensionFoundException;
import net.tardis.mod.exceptions.NoPlayerFoundException;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TeleportUtil;

public class InteriorCommand extends TCommand {
    private static final InteriorCommand CMD = new InteriorCommand();

    public InteriorCommand() {
        super(PermissionEnum.INTERIOR);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("interior")
                .then(Commands.argument("username", StringArgumentType.string())
                        .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                        .executes(CMD));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        CommandSource source = context.getSource();
        String username = context.getArgument("username", String.class);

        if (canExecute(source)) {
            try {
                ServerPlayerEntity sourcePlayer = source.asPlayer();
                DimensionType consoleDimension = TardisHelper.getTardisDimension(username);
                TeleportUtil.teleportEntity(sourcePlayer, consoleDimension, TardisHelper.TARDIS_POS.getX() + 2, TardisHelper.TARDIS_POS.getY(), TardisHelper.TARDIS_POS.getZ());
            } catch (NoPlayerFoundException | NoDimensionFoundException e) {
                source.sendErrorMessage(new StringTextComponent(e.getMessage()));
            }
        } else {
            source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
        }

        return Command.SINGLE_SUCCESS;
    }

}
