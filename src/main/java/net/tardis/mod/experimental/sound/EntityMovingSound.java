package net.tardis.mod.experimental.sound;

import net.minecraft.client.audio.TickableSound;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.entity.BessieEntity;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.AntiGravityTile;

@OnlyIn(Dist.CLIENT)
public class EntityMovingSound extends TickableSound {

    private final Entity entity;
    private final SoundEvent refSound;

    public EntityMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory) {
        super(soundEvent, soundCategory);
        this.entity = entity;
        refSound = soundEvent;
        this.repeat = false;
        this.repeatDelay = 0;
        this.volume = 0.0F;
        this.x = (float) entity.posX;
        this.y = (float) entity.posY;
        this.z = (float) entity.posZ;

    }

    public EntityMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory, float vol) {
        this(entity, soundEvent, soundCategory);
        this.repeat = false;
        this.volume = vol;
        this.x = (float) entity.posX;
        this.y = (float) entity.posY;
        this.z = (float) entity.posZ;
    }

    public EntityMovingSound(Entity entity, SoundEvent soundEvent, SoundCategory soundCategory, float vol, boolean repeat) {
        this(entity, soundEvent, soundCategory);
        this.volume = vol;
        this.x = (float) entity.posX;
        this.y = (float) entity.posY;
        this.z = (float) entity.posZ;
        this.repeat = repeat;
    }

    @Override
    public boolean canBeSilent() {
        return true;
    }

    @Override
    public void tick() {
        if (this.entity.removed) {
            this.donePlaying = true;
        } else {
            this.x = (float) this.entity.posX;
            this.y = (float) this.entity.posY;
            this.z = (float) this.entity.posZ;

            float motion = MathHelper.sqrt(Entity.horizontalMag(this.entity.getMotion()));

            // ===== Dalek =====
            if (refSound == TSounds.DALEK_MOVES) {
                if (entity instanceof DalekEntity) {
                    DalekEntity dalek = (DalekEntity) entity;
                    if (dalek.isAirBorne) {
                        silence();
                    } else {
                        this.volume = 0.5F;
                    }
                }
            }

            //Dalek Flying
            if (refSound == TSounds.DALEK_HOVER) {
                if (!entity.onGround || entity.world.getBlockState(entity.getPosition().down()).isAir()) {
                    this.volume = 0.5F;
                } else {
                    silence();
                }
            }
            //Dalek Flying End

            //Player
            if (entity instanceof AbstractClientPlayerEntity) {
                if (refSound.getRegistryName().equals(TSounds.SHIELD_HUM.getRegistryName())) {
                    if (AntiGravityTile.isInAntiGrav(entity.getBoundingBox().grow(5, entity.world.getHeight(), 5), entity.world)) {
                        volume = 0.2f;
                    } else {
                        silence();
                    }
                }
            }


            // ===== Bessie =====
            if (entity instanceof BessieEntity) {
                if ((double) motion >= 0.01D) {
                    this.volume = MathHelper.lerp(MathHelper.clamp(motion, 0.0F, 1), 0.0F, 1);
                } else {
                    silence();
                }
            }

        }
    }

    public void silence() {
        volume = 0F;
    }

}