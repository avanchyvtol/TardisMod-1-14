package net.tardis.mod.world.feature.structures;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.tardis.mod.Tardis;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.tileentities.ShipComputerTile;
import net.tardis.mod.world.WorldGen;

public class CrashedStructure extends Feature<NoFeatureConfig> {

	public static final ResourceLocation CRASHED_SHIP = new ResourceLocation(Tardis.MODID, "tardis/structures/worldgen/crashed_shuttle");
	
	
	public CrashedStructure(Function<Dynamic<?>, ? extends NoFeatureConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, NoFeatureConfig config) {
		if(worldIn.getDimension().getType() != DimensionType.OVERWORLD)
			return false;
		
		if(worldIn instanceof WorldGenRegion) {
			WorldGenRegion reg = (WorldGenRegion)worldIn;
			Template temp = reg.getWorld().getStructureTemplateManager().getTemplate(CRASHED_SHIP);
			if(temp != null) {
				pos = worldIn.getHeight(Type.WORLD_SURFACE_WG, pos).down(7);
				
				PlacementSettings set = new PlacementSettings();
				temp.addBlocksToWorld(reg, pos, set);
				WorldGen.BROKEN_EXTERIORS.add(pos);
				System.out.println("Crashed ships " + pos);
				
				List<BlockInfo> infos = temp.func_215381_a(pos, set, Blocks.STRUCTURE_BLOCK);
				for(BlockInfo info : infos) {
					if(info.nbt.contains("metadata") && info.nbt.getString("metadata").contentEquals("computer")) {
						TileEntity te = reg.getTileEntity(info.pos.down());
						if(te instanceof ShipComputerTile) {
							((ShipComputerTile)te).setSchematic(Schematics.POLICE_BOX);
						}
						reg.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 3);
					}
				}
				
				return true;
			}
		}
		return false;
	}

}
