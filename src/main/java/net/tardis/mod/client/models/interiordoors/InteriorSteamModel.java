package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

public class InteriorSteamModel extends Model implements IInteriorDoorRenderer{
	private final RendererModel glow_front_window;
	private final RendererModel door;
	private final RendererModel hinge;
	private final RendererModel inner_frame;
	private final RendererModel floorboard;
	private final RendererModel roof_front;
	private final RendererModel peak;
	private final RendererModel wroughtiron_front;
	private final RendererModel panel_front_top;
	private final RendererModel outer_frame;
	private final RendererModel boti;

	public InteriorSteamModel() {
		textureWidth = 1024;
		textureHeight = 1024;

		glow_front_window = new RendererModel(this);
		glow_front_window.setRotationPoint(0.0F, 23.0556F, 0.4167F);
		glow_front_window.cubeList.add(new ModelBox(glow_front_window, 516, 150, 6.0F, -179.0556F, -18.1667F, 12, 12, 7, 0.0F, false));
		glow_front_window.cubeList.add(new ModelBox(glow_front_window, 470, 146, -18.0F, -179.0556F, -18.1667F, 12, 12, 7, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(26.5F, -5.0F, -14.5F);
		door.cubeList.add(new ModelBox(door, 163, 372, -50.5F, -3.0F, -2.25F, 48, 20, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 258, 27, -50.5F, -123.0F, -1.25F, 48, 140, 0, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 150, 269, -50.5F, -123.0F, -2.25F, 48, 8, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 150, 269, -10.5F, -115.0F, -2.25F, 8, 112, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 150, 269, -50.5F, -115.0F, -2.25F, 8, 112, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 150, 269, -40.5F, -113.0F, -2.25F, 28, 4, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 151, 370, -40.5F, -13.0F, -2.25F, 28, 8, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 148, 283, -40.5F, -109.0F, -2.25F, 4, 96, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 171, 289, -16.5F, -109.0F, -2.25F, 4, 96, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 253, 291, -34.5F, -107.0F, -2.25F, 16, 92, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 489, 45, -48.5F, -1.0F, -3.25F, 44, 16, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 498, 41, -48.5F, -68.0F, -4.25F, 4, 16, 8, 0.0F, false));

		hinge = new RendererModel(this);
		hinge.setRotationPoint(-0.6532F, -56.6667F, 1.1099F);
		setRotationAngle(hinge, 0.0F, 0.7854F, 0.0F);
		door.addChild(hinge);
		hinge.cubeList.add(new ModelBox(hinge, 521, 45, -2.0F, -59.3333F, -2.0F, 4, 12, 4, 0.0F, false));
		hinge.cubeList.add(new ModelBox(hinge, 521, 45, -2.0F, -11.3333F, -2.0F, 4, 12, 4, 0.0F, false));
		hinge.cubeList.add(new ModelBox(hinge, 521, 45, -2.0F, 52.6667F, -2.0F, 4, 12, 4, 0.0F, false));

		inner_frame = new RendererModel(this);
		inner_frame.setRotationPoint(0.0F, 23.0556F, 0.4167F);
		inner_frame.cubeList.add(new ModelBox(inner_frame, 144, 391, -32.0F, -11.0556F, -18.1667F, 64, 8, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 143, 207, -32.0F, -183.0556F, -18.1667F, 8, 172, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 234, 212, 24.0F, -183.0556F, -18.1667F, 8, 172, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 146, 265, -32.0F, -191.0556F, -18.1667F, 64, 8, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 150, 273, -24.0F, -163.0556F, -18.1667F, 48, 12, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 151, 282, -32.0F, -191.0556F, -20.1667F, 64, 4, 8, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 459, 41, -20.0F, -161.0556F, -17.1667F, 40, 8, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 185, 215, -29.0F, -187.0556F, -17.1667F, 4, 184, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 269, 215, 25.0F, -187.0556F, -17.1667F, 4, 184, 4, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 144, 211, -32.0F, -187.0556F, -20.1667F, 4, 184, 8, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 279, 213, 28.0F, -187.0556F, -20.1667F, 4, 184, 8, 0.0F, false));
		inner_frame.cubeList.add(new ModelBox(inner_frame, 143, 280, -28.0F, -159.0556F, -20.1667F, 56, 4, 4, 0.0F, false));

		floorboard = new RendererModel(this);
		floorboard.setRotationPoint(0.0F, 0.0F, 0.0F);
		inner_frame.addChild(floorboard);
		floorboard.cubeList.add(new ModelBox(floorboard, 238, 389, -32.0F, -3.0556F, -25.1667F, 64, 4, 16, 0.0F, false));

		roof_front = new RendererModel(this);
		roof_front.setRotationPoint(0.0F, -47.0F, -12.0F);
		inner_frame.addChild(roof_front);

		peak = new RendererModel(this);
		peak.setRotationPoint(0.0F, 0.0F, 4.0F);
		roof_front.addChild(peak);

		wroughtiron_front = new RendererModel(this);
		wroughtiron_front.setRotationPoint(0.0F, 47.9444F, 11.5833F);
		roof_front.addChild(wroughtiron_front);

		panel_front_top = new RendererModel(this);
		panel_front_top.setRotationPoint(0.0F, 0.0F, 0.0F);
		inner_frame.addChild(panel_front_top);
		panel_front_top.cubeList.add(new ModelBox(panel_front_top, 78, 223, -24.0F, -183.0556F, -17.1667F, 48, 20, 4, 0.0F, false));

		outer_frame = new RendererModel(this);
		outer_frame.setRotationPoint(0.0F, 24.0F, 0.0F);
		outer_frame.cubeList.add(new ModelBox(outer_frame, 64, 205, -40.0F, -192.0F, -17.75F, 8, 192, 12, 0.0F, false));
		outer_frame.cubeList.add(new ModelBox(outer_frame, 45, 205, 32.0F, -192.0F, -17.75F, 8, 192, 12, 0.0F, false));
		outer_frame.cubeList.add(new ModelBox(outer_frame, 91, 256, -40.0F, -200.0F, -17.75F, 80, 8, 12, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 12, 14, -36.0F, -164.0F, -23.75F, 72, 160, 8, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity ent) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.translated(0, 1, -0.2);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.STEAM.getRotationForState(ent.getOpenState()));
		
		door.render(0.0625F);
		inner_frame.render(0.0625F);
		outer_frame.render(0.0625F);
		boti.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1F, glow_front_window);
		
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		return SteamExteriorRenderer.TEXTURE;
	}
}