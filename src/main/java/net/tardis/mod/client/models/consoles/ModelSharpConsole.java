package net.tardis.mod.client.models.consoles;// Made with Blockbench
// Paste this code into your mod.
// Make sure to generate all required imports

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.tileentities.ConsoleTile;

public class ModelSharpConsole extends Model implements IConsoleModel {

    private final RendererModel SharpUnit;
    private final RendererModel MainFrame1;
    private final RendererModel MainFrameAngler1;
    private final RendererModel MainFrameAngler2;
    private final RendererModel MainFrameAngler3;
    private final RendererModel MainFrameAngler4;
    private final RendererModel MainFrame2;
    private final RendererModel MainFrameAngler5;
    private final RendererModel MainFrameAngler6;
    private final RendererModel MainFrameAngler7;
    private final RendererModel MainFrameAngler8;
    private final RendererModel MainFrame3;
    private final RendererModel MainFrameAngler9;
    private final RendererModel MainFrameAngler10;
    private final RendererModel MainFrameAngler11;
    private final RendererModel MainFrameAngler12;
    private final RendererModel BottomPart1;
    private final RendererModel bone;
    private final RendererModel bone2;
    private final RendererModel BottomPart2;
    private final RendererModel bone3;
    private final RendererModel bone4;
    private final RendererModel BottomPart3;
    private final RendererModel bone5;
    private final RendererModel bone6;
    private final RendererModel ConsoleCollar1;
    private final RendererModel ConsoleCollar2;
    private final RendererModel ConsoleCollar3;
    private final RendererModel ConsoleTable1;
    private final RendererModel ConsolePanelAngler1;
    private final RendererModel ConsoleTable2;
    private final RendererModel ConsolePanelAngler2;
    private final RendererModel ConsoleTable3;
    private final RendererModel ConsolePanelAngler3;
    private final RendererModel ConsoleTable4;
    private final RendererModel ConsolePanelAngler4;
    private final RendererModel ConsoleTable5;
    private final RendererModel ConsolePanelAngler5;
    private final RendererModel ConsoleTable6;
    private final RendererModel ConsolePanelAngler6;
    private final RendererModel bone7;
    private final RendererModel Rotor;
    private final RendererModel RotorGoobler1;
    private final RendererModel RotorGoobler2;
    private final RendererModel RotorGoobler3;
    private final RendererModel RotorDoobler1;
    private final RendererModel RotorDoobler2;
    private final RendererModel RotorDoobler3;
    private final RendererModel RotorCenter;
    private final RendererModel RotorPipe1;
    private final RendererModel RotorPipe2;
    private final RendererModel RotorPipe3;
    private final RendererModel RotorOuter1;
    private final RendererModel RotorOuter2;
    private final RendererModel RotorOuter3;
    private final RendererModel Panel1;
    private final RendererModel Panel1Angler;
    private final RendererModel Panel1Switch1;
    private final RendererModel Panel1Switch2;
    private final RendererModel Panel1Switch3;
    private final RendererModel Panel1Lever1;
    private final RendererModel Panel1Knob1;
    private final RendererModel Panel1Knob4;
    private final RendererModel Panel1Knob2;
    private final RendererModel Panel1Knob3;
    private final RendererModel Panel2;
    private final RendererModel Panel2Angler;
    private final RendererModel Panel2TopSwitch;
    private final RendererModel Panel2BlackSwitch1;
    private final RendererModel Panel2BlackSwitch2;
    private final RendererModel Panel2Switchies1;
    private final RendererModel Panel2Switchies2;
    private final RendererModel Panel2BigKnob;
    private final RendererModel Panel2BigKnob2;
    private final RendererModel Panel3;
    private final RendererModel Panel3Angler;
    private final RendererModel Panel3TopSwitch;
    private final RendererModel Panel3BigKnobL;
    private final RendererModel Panel3BigKnobR;
    private final RendererModel Panel3Switches;
    private final RendererModel Panel3Switches2;
    private final RendererModel Panel3LightAnglerLeft;
    private final RendererModel Panel3LightAnglerLeft2;
    private final RendererModel Panel4;
    private final RendererModel Panel4Angler;
    private final RendererModel Panel4SwitchesLeft;
    private final RendererModel Panel4SwitchesRight;
    private final RendererModel Panel4RightLights;
    private final RendererModel Panel4LeftLights;
    private final RendererModel Panel4BigSwitch;
    private final RendererModel Panel5;
    private final RendererModel Panel5Angler;
    private final RendererModel Panel5BigSwitch1;
    private final RendererModel Panel5BigSwitch2;
    private final RendererModel Panel5SwitchesLeft;
    private final RendererModel Panel5SwitchesRight;
    private final RendererModel Panel6;
    private final RendererModel Panel6Angler;
    private final RendererModel Panel6Lever1;
    private final RendererModel Panel6Lever2;
    private final RendererModel Panel6LinesRight;
    private final RendererModel Panel6LinesLeft;

    public ModelSharpConsole() {
        textureWidth = 256;
        textureHeight = 256;

        SharpUnit = new RendererModel(this);
        SharpUnit.setRotationPoint(0.0F, 24.0F, 0.0F);

        MainFrame1 = new RendererModel(this);
        MainFrame1.setRotationPoint(0.0F, 0.0F, 0.0F);
        SharpUnit.addChild(MainFrame1);
        MainFrame1.cubeList.add(new ModelBox(MainFrame1, 66, 49, -9.5F, -13.0F, -0.5F, 19, 13, 1, 0.0F, false));
        MainFrame1.cubeList.add(new ModelBox(MainFrame1, 0, 0, -18.5F, -14.0F, -0.5F, 37, 1, 1, 0.0F, false));
        MainFrame1.cubeList.add(new ModelBox(MainFrame1, 8, 156, 5.0F, -18.0F, -0.5F, 1, 2, 1, 0.0F, false));
        MainFrame1.cubeList.add(new ModelBox(MainFrame1, 8, 156, -6.0F, -18.0F, -0.5F, 1, 2, 1, 0.0F, false));

        MainFrameAngler1 = new RendererModel(this);
        MainFrameAngler1.setRotationPoint(-6.0F, -18.0F, 0.0F);
        setRotationAngle(MainFrameAngler1, 0.0F, 0.0F, -0.3142F);
        MainFrame1.addChild(MainFrameAngler1);
        MainFrameAngler1.cubeList.add(new ModelBox(MainFrameAngler1, 8, 156, -13.0F, 0.0F, -0.5F, 13, 1, 1, 0.0F, false));

        MainFrameAngler2 = new RendererModel(this);
        MainFrameAngler2.setRotationPoint(6.0F, -18.0F, 0.0F);
        setRotationAngle(MainFrameAngler2, 0.0F, 0.0F, 0.3142F);
        MainFrame1.addChild(MainFrameAngler2);
        MainFrameAngler2.cubeList.add(new ModelBox(MainFrameAngler2, 8, 156, 0.0F, 0.0F, -0.5F, 13, 1, 1, 0.0F, false));

        MainFrameAngler3 = new RendererModel(this);
        MainFrameAngler3.setRotationPoint(18.5F, -13.0F, 0.0F);
        setRotationAngle(MainFrameAngler3, 0.0F, 0.0F, -0.2187F);
        MainFrame1.addChild(MainFrameAngler3);
        MainFrameAngler3.cubeList.add(new ModelBox(MainFrameAngler3, 8, 159, -4.0F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
        MainFrameAngler3.cubeList.add(new ModelBox(MainFrameAngler3, 8, 159, -9.22F, -2.0F, -0.5F, 6, 2, 1, 0.0F, false));

        MainFrameAngler4 = new RendererModel(this);
        MainFrameAngler4.setRotationPoint(-18.5F, -13.0F, 0.0F);
        setRotationAngle(MainFrameAngler4, 0.0F, 0.0F, 0.2187F);
        MainFrame1.addChild(MainFrameAngler4);
        MainFrameAngler4.cubeList.add(new ModelBox(MainFrameAngler4, 8, 159, 0.0F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
        MainFrameAngler4.cubeList.add(new ModelBox(MainFrameAngler4, 8, 159, 3.22F, -2.0F, -0.5F, 6, 2, 1, 0.0F, false));

        MainFrame2 = new RendererModel(this);
        MainFrame2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(MainFrame2, 0.0F, -1.0472F, 0.0F);
        SharpUnit.addChild(MainFrame2);
        MainFrame2.cubeList.add(new ModelBox(MainFrame2, 66, 49, -9.5F, -13.0F, -0.5F, 19, 13, 1, 0.0F, false));
        MainFrame2.cubeList.add(new ModelBox(MainFrame2, 0, 0, -18.5F, -14.0F, -0.5F, 37, 1, 1, 0.0F, false));
        MainFrame2.cubeList.add(new ModelBox(MainFrame2, 8, 156, 5.0F, -18.0F, -0.5F, 1, 2, 1, 0.0F, false));
        MainFrame2.cubeList.add(new ModelBox(MainFrame2, 8, 156, -6.0F, -18.0F, -0.5F, 1, 2, 1, 0.0F, false));

        MainFrameAngler5 = new RendererModel(this);
        MainFrameAngler5.setRotationPoint(-6.0F, -18.0F, 0.0F);
        setRotationAngle(MainFrameAngler5, 0.0F, 0.0F, -0.3142F);
        MainFrame2.addChild(MainFrameAngler5);
        MainFrameAngler5.cubeList.add(new ModelBox(MainFrameAngler5, 8, 156, -13.0F, 0.0F, -0.5F, 13, 1, 1, 0.0F, false));

        MainFrameAngler6 = new RendererModel(this);
        MainFrameAngler6.setRotationPoint(6.0F, -18.0F, 0.0F);
        setRotationAngle(MainFrameAngler6, 0.0F, 0.0F, 0.3142F);
        MainFrame2.addChild(MainFrameAngler6);
        MainFrameAngler6.cubeList.add(new ModelBox(MainFrameAngler6, 8, 156, 0.0F, 0.0F, -0.5F, 13, 1, 1, 0.0F, false));

        MainFrameAngler7 = new RendererModel(this);
        MainFrameAngler7.setRotationPoint(18.5F, -13.0F, 0.0F);
        setRotationAngle(MainFrameAngler7, 0.0F, 0.0F, -0.2187F);
        MainFrame2.addChild(MainFrameAngler7);
        MainFrameAngler7.cubeList.add(new ModelBox(MainFrameAngler7, 8, 159, -4.0F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
        MainFrameAngler7.cubeList.add(new ModelBox(MainFrameAngler7, 8, 159, -9.22F, -2.0F, -0.5F, 6, 2, 1, 0.0F, false));

        MainFrameAngler8 = new RendererModel(this);
        MainFrameAngler8.setRotationPoint(-18.5F, -13.0F, 0.0F);
        setRotationAngle(MainFrameAngler8, 0.0F, 0.0F, 0.2187F);
        MainFrame2.addChild(MainFrameAngler8);
        MainFrameAngler8.cubeList.add(new ModelBox(MainFrameAngler8, 8, 159, 0.0F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
        MainFrameAngler8.cubeList.add(new ModelBox(MainFrameAngler8, 8, 159, 3.22F, -2.0F, -0.5F, 6, 2, 1, 0.0F, false));

        MainFrame3 = new RendererModel(this);
        MainFrame3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(MainFrame3, 0.0F, -2.0944F, 0.0F);
        SharpUnit.addChild(MainFrame3);
        MainFrame3.cubeList.add(new ModelBox(MainFrame3, 66, 49, -9.5F, -13.0F, -0.5F, 19, 13, 1, 0.0F, false));
        MainFrame3.cubeList.add(new ModelBox(MainFrame3, 0, 0, -18.5F, -14.0F, -0.5F, 37, 1, 1, 0.0F, false));
        MainFrame3.cubeList.add(new ModelBox(MainFrame3, 8, 156, 5.0F, -18.0F, -0.5F, 1, 2, 1, 0.0F, false));
        MainFrame3.cubeList.add(new ModelBox(MainFrame3, 8, 156, -6.0F, -18.0F, -0.5F, 1, 2, 1, 0.0F, false));

        MainFrameAngler9 = new RendererModel(this);
        MainFrameAngler9.setRotationPoint(-6.0F, -18.0F, 0.0F);
        setRotationAngle(MainFrameAngler9, 0.0F, 0.0F, -0.3142F);
        MainFrame3.addChild(MainFrameAngler9);
        MainFrameAngler9.cubeList.add(new ModelBox(MainFrameAngler9, 8, 156, -13.0F, 0.0F, -0.5F, 13, 1, 1, 0.0F, false));

        MainFrameAngler10 = new RendererModel(this);
        MainFrameAngler10.setRotationPoint(6.0F, -18.0F, 0.0F);
        setRotationAngle(MainFrameAngler10, 0.0F, 0.0F, 0.3142F);
        MainFrame3.addChild(MainFrameAngler10);
        MainFrameAngler10.cubeList.add(new ModelBox(MainFrameAngler10, 8, 156, 0.0F, 0.0F, -0.5F, 13, 1, 1, 0.0F, false));

        MainFrameAngler11 = new RendererModel(this);
        MainFrameAngler11.setRotationPoint(18.5F, -13.0F, 0.0F);
        setRotationAngle(MainFrameAngler11, 0.0F, 0.0F, -0.2187F);
        MainFrame3.addChild(MainFrameAngler11);
        MainFrameAngler11.cubeList.add(new ModelBox(MainFrameAngler11, 8, 159, -4.0F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
        MainFrameAngler11.cubeList.add(new ModelBox(MainFrameAngler11, 8, 159, -9.22F, -2.0F, -0.5F, 6, 2, 1, 0.0F, false));

        MainFrameAngler12 = new RendererModel(this);
        MainFrameAngler12.setRotationPoint(-18.5F, -13.0F, 0.0F);
        setRotationAngle(MainFrameAngler12, 0.0F, 0.0F, 0.2187F);
        MainFrame3.addChild(MainFrameAngler12);
        MainFrameAngler12.cubeList.add(new ModelBox(MainFrameAngler12, 8, 159, 0.0F, -1.0F, -0.5F, 4, 1, 1, 0.0F, false));
        MainFrameAngler12.cubeList.add(new ModelBox(MainFrameAngler12, 8, 159, 3.22F, -2.0F, -0.5F, 6, 2, 1, 0.0F, false));

        BottomPart1 = new RendererModel(this);
        BottomPart1.setRotationPoint(0.0F, 0.0F, 0.0F);
        SharpUnit.addChild(BottomPart1);
        BottomPart1.cubeList.add(new ModelBox(BottomPart1, 85, 106, -0.683F, -1.0F, -8.4772F, 5, 1, 9, 0.0F, false));
        BottomPart1.cubeList.add(new ModelBox(BottomPart1, 85, 106, -0.683F, -1.0F, -0.5228F, 5, 1, 9, 0.0F, false));
        BottomPart1.cubeList.add(new ModelBox(BottomPart1, 85, 106, -4.317F, -1.0F, -8.4772F, 5, 1, 9, 0.0F, false));
        BottomPart1.cubeList.add(new ModelBox(BottomPart1, 85, 106, -4.317F, -1.0F, -0.5228F, 5, 1, 9, 0.0F, false));
        BottomPart1.cubeList.add(new ModelBox(BottomPart1, 76, 82, -3.5F, -13.0F, -6.0F, 7, 12, 12, 0.0F, false));

        bone = new RendererModel(this);
        bone.setRotationPoint(0.0F, -1.0F, -8.25F);
        setRotationAngle(bone, 0.7854F, 0.0F, 0.0F);
        BottomPart1.addChild(bone);
        bone.cubeList.add(new ModelBox(bone, 8, 159, -4.5F, 0.0F, 0.0F, 9, 1, 1, 0.0F, false));
        bone.cubeList.add(new ModelBox(bone, 8, 159, -4.0F, 0.0F, 1.0F, 8, 1, 1, 0.0F, false));
        bone.cubeList.add(new ModelBox(bone, 8, 159, -3.5F, 0.0F, 2.0F, 7, 1, 2, 0.0F, false));

        bone2 = new RendererModel(this);
        bone2.setRotationPoint(0.0F, -1.0F, 8.25F);
        setRotationAngle(bone2, -0.7854F, 0.0F, 0.0F);
        BottomPart1.addChild(bone2);
        bone2.cubeList.add(new ModelBox(bone2, 8, 159, -4.5F, 0.0F, -1.0F, 9, 1, 1, 0.0F, false));
        bone2.cubeList.add(new ModelBox(bone2, 8, 159, -4.0F, 0.0F, -2.0F, 8, 1, 1, 0.0F, false));
        bone2.cubeList.add(new ModelBox(bone2, 8, 159, -3.5F, 0.0F, -4.0F, 7, 1, 2, 0.0F, false));

        BottomPart2 = new RendererModel(this);
        BottomPart2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(BottomPart2, 0.0F, -1.0472F, 0.0F);
        SharpUnit.addChild(BottomPart2);
        BottomPart2.cubeList.add(new ModelBox(BottomPart2, 85, 106, -0.683F, -1.0F, -8.4772F, 5, 1, 9, 0.0F, false));
        BottomPart2.cubeList.add(new ModelBox(BottomPart2, 85, 106, -0.683F, -1.0F, -0.5228F, 5, 1, 9, 0.0F, false));
        BottomPart2.cubeList.add(new ModelBox(BottomPart2, 85, 106, -4.317F, -1.0F, -8.4772F, 5, 1, 9, 0.0F, false));
        BottomPart2.cubeList.add(new ModelBox(BottomPart2, 85, 106, -4.317F, -1.0F, -0.5228F, 5, 1, 9, 0.0F, false));
        BottomPart2.cubeList.add(new ModelBox(BottomPart2, 76, 82, -3.5F, -13.0F, -6.0F, 7, 12, 12, 0.0F, false));

        bone3 = new RendererModel(this);
        bone3.setRotationPoint(0.0F, -1.0F, -8.25F);
        setRotationAngle(bone3, 0.7854F, 0.0F, 0.0F);
        BottomPart2.addChild(bone3);
        bone3.cubeList.add(new ModelBox(bone3, 8, 159, -4.5F, 0.0F, 0.0F, 9, 1, 1, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 8, 159, -4.0F, 0.0F, 1.0F, 8, 1, 1, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 8, 159, -3.5F, 0.0F, 2.0F, 7, 1, 2, 0.0F, false));

        bone4 = new RendererModel(this);
        bone4.setRotationPoint(0.0F, -1.0F, 8.25F);
        setRotationAngle(bone4, -0.7854F, 0.0F, 0.0F);
        BottomPart2.addChild(bone4);
        bone4.cubeList.add(new ModelBox(bone4, 8, 159, -4.5F, 0.0F, -1.0F, 9, 1, 1, 0.0F, false));
        bone4.cubeList.add(new ModelBox(bone4, 8, 159, -4.0F, 0.0F, -2.0F, 8, 1, 1, 0.0F, false));
        bone4.cubeList.add(new ModelBox(bone4, 8, 159, -3.5F, 0.0F, -4.0F, 7, 1, 2, 0.0F, false));

        BottomPart3 = new RendererModel(this);
        BottomPart3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(BottomPart3, 0.0F, -2.0944F, 0.0F);
        SharpUnit.addChild(BottomPart3);
        BottomPart3.cubeList.add(new ModelBox(BottomPart3, 85, 106, -0.683F, -1.0F, -8.4772F, 5, 1, 9, 0.0F, false));
        BottomPart3.cubeList.add(new ModelBox(BottomPart3, 85, 106, -0.683F, -1.0F, -0.5228F, 5, 1, 9, 0.0F, false));
        BottomPart3.cubeList.add(new ModelBox(BottomPart3, 85, 106, -4.317F, -1.0F, -8.4772F, 5, 1, 9, 0.0F, false));
        BottomPart3.cubeList.add(new ModelBox(BottomPart3, 85, 106, -4.317F, -1.0F, -0.5228F, 5, 1, 9, 0.0F, false));
        BottomPart3.cubeList.add(new ModelBox(BottomPart3, 76, 82, -3.5F, -13.0F, -6.0F, 7, 12, 12, 0.0F, false));

        bone5 = new RendererModel(this);
        bone5.setRotationPoint(0.0F, -1.0F, -8.25F);
        setRotationAngle(bone5, 0.7854F, 0.0F, 0.0F);
        BottomPart3.addChild(bone5);
        bone5.cubeList.add(new ModelBox(bone5, 8, 159, -4.5F, 0.0F, 0.0F, 9, 1, 1, 0.0F, false));
        bone5.cubeList.add(new ModelBox(bone5, 8, 159, -4.0F, 0.0F, 1.0F, 8, 1, 1, 0.0F, false));
        bone5.cubeList.add(new ModelBox(bone5, 8, 159, -3.5F, 0.0F, 2.0F, 7, 1, 2, 0.0F, false));

        bone6 = new RendererModel(this);
        bone6.setRotationPoint(0.0F, -1.0F, 8.25F);
        setRotationAngle(bone6, -0.7854F, 0.0F, 0.0F);
        BottomPart3.addChild(bone6);
        bone6.cubeList.add(new ModelBox(bone6, 8, 159, -4.5F, 0.0F, -1.0F, 9, 1, 1, 0.0F, false));
        bone6.cubeList.add(new ModelBox(bone6, 8, 159, -4.0F, 0.0F, -2.0F, 8, 1, 1, 0.0F, false));
        bone6.cubeList.add(new ModelBox(bone6, 8, 159, -3.5F, 0.0F, -4.0F, 7, 1, 2, 0.0F, false));

        ConsoleCollar1 = new RendererModel(this);
        ConsoleCollar1.setRotationPoint(0.0F, 0.0F, 0.0F);
        SharpUnit.addChild(ConsoleCollar1);
        ConsoleCollar1.cubeList.add(new ModelBox(ConsoleCollar1, 8, 156, -0.433F, -18.0F, -5.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar1.cubeList.add(new ModelBox(ConsoleCollar1, 8, 156, -0.433F, -18.0F, 4.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar1.cubeList.add(new ModelBox(ConsoleCollar1, 8, 156, -2.567F, -18.0F, -5.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar1.cubeList.add(new ModelBox(ConsoleCollar1, 8, 156, -2.567F, -18.0F, 4.4462F, 3, 2, 1, 0.0F, false));

        ConsoleCollar2 = new RendererModel(this);
        ConsoleCollar2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleCollar2, 0.0F, -1.0472F, 0.0F);
        SharpUnit.addChild(ConsoleCollar2);
        ConsoleCollar2.cubeList.add(new ModelBox(ConsoleCollar2, 8, 156, -0.433F, -18.0F, -5.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar2.cubeList.add(new ModelBox(ConsoleCollar2, 8, 156, -0.433F, -18.0F, 4.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar2.cubeList.add(new ModelBox(ConsoleCollar2, 8, 156, -2.567F, -18.0F, -5.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar2.cubeList.add(new ModelBox(ConsoleCollar2, 8, 156, -2.567F, -18.0F, 4.4462F, 3, 2, 1, 0.0F, false));

        ConsoleCollar3 = new RendererModel(this);
        ConsoleCollar3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleCollar3, 0.0F, -2.0944F, 0.0F);
        SharpUnit.addChild(ConsoleCollar3);
        ConsoleCollar3.cubeList.add(new ModelBox(ConsoleCollar3, 8, 156, -0.433F, -18.0F, -5.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar3.cubeList.add(new ModelBox(ConsoleCollar3, 8, 156, -0.433F, -18.0F, 4.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar3.cubeList.add(new ModelBox(ConsoleCollar3, 8, 156, -2.567F, -18.0F, -5.4462F, 3, 2, 1, 0.0F, false));
        ConsoleCollar3.cubeList.add(new ModelBox(ConsoleCollar3, 8, 156, -2.567F, -18.0F, 4.4462F, 3, 2, 1, 0.0F, false));

        ConsoleTable1 = new RendererModel(this);
        ConsoleTable1.setRotationPoint(0.0F, 0.0F, 0.0F);
        SharpUnit.addChild(ConsoleTable1);
        ConsoleTable1.cubeList.add(new ModelBox(ConsoleTable1, 33, 7, -0.183F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));
        ConsoleTable1.cubeList.add(new ModelBox(ConsoleTable1, 33, 7, -8.817F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));

        ConsolePanelAngler1 = new RendererModel(this);
        ConsolePanelAngler1.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(ConsolePanelAngler1, 0.2967F, 0.0F, 0.0F);
        ConsoleTable1.addChild(ConsolePanelAngler1);
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -8.5F, 0.0F, 0.0F, 17, 1, 2, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -7.5F, 0.0F, 2.0F, 15, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -7.0F, 0.0F, 3.0F, 14, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -6.5F, 0.0F, 4.0F, 13, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -6.0F, 0.0F, 5.0F, 12, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -5.5F, 0.0F, 6.0F, 11, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -5.0F, 0.0F, 7.0F, 10, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -4.5F, 0.0F, 8.0F, 9, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -4.0F, 0.0F, 9.0F, 8, 1, 1, 0.0F, false));
        ConsolePanelAngler1.cubeList.add(new ModelBox(ConsolePanelAngler1, 6, 146, -3.0F, 0.0F, 10.0F, 6, 1, 1, 0.0F, false));

        ConsoleTable2 = new RendererModel(this);
        ConsoleTable2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleTable2, 0.0F, -1.0472F, 0.0F);
        SharpUnit.addChild(ConsoleTable2);
        ConsoleTable2.cubeList.add(new ModelBox(ConsoleTable2, 33, 7, -0.183F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));
        ConsoleTable2.cubeList.add(new ModelBox(ConsoleTable2, 33, 7, -8.817F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));

        ConsolePanelAngler2 = new RendererModel(this);
        ConsolePanelAngler2.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(ConsolePanelAngler2, 0.2967F, 0.0F, 0.0F);
        ConsoleTable2.addChild(ConsolePanelAngler2);
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -8.5F, 0.0F, 0.0F, 17, 1, 2, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -7.5F, 0.0F, 2.0F, 15, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -7.0F, 0.0F, 3.0F, 14, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -6.5F, 0.0F, 4.0F, 13, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -6.0F, 0.0F, 5.0F, 12, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -5.5F, 0.0F, 6.0F, 11, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -5.0F, 0.0F, 7.0F, 10, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -4.5F, 0.0F, 8.0F, 9, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -4.0F, 0.0F, 9.0F, 8, 1, 1, 0.0F, false));
        ConsolePanelAngler2.cubeList.add(new ModelBox(ConsolePanelAngler2, 6, 146, -3.0F, 0.0F, 10.0F, 6, 1, 1, 0.0F, false));

        ConsoleTable3 = new RendererModel(this);
        ConsoleTable3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleTable3, 0.0F, -2.0944F, 0.0F);
        SharpUnit.addChild(ConsoleTable3);
        ConsoleTable3.cubeList.add(new ModelBox(ConsoleTable3, 33, 7, -0.183F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));
        ConsoleTable3.cubeList.add(new ModelBox(ConsoleTable3, 33, 7, -8.817F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));

        ConsolePanelAngler3 = new RendererModel(this);
        ConsolePanelAngler3.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(ConsolePanelAngler3, 0.2967F, 0.0F, 0.0F);
        ConsoleTable3.addChild(ConsolePanelAngler3);
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -8.5F, 0.0F, 0.0F, 17, 1, 2, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -7.5F, 0.0F, 2.0F, 15, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -7.0F, 0.0F, 3.0F, 14, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -6.5F, 0.0F, 4.0F, 13, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -6.0F, 0.0F, 5.0F, 12, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -5.5F, 0.0F, 6.0F, 11, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -5.0F, 0.0F, 7.0F, 10, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -4.5F, 0.0F, 8.0F, 9, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -4.0F, 0.0F, 9.0F, 8, 1, 1, 0.0F, false));
        ConsolePanelAngler3.cubeList.add(new ModelBox(ConsolePanelAngler3, 6, 146, -3.0F, 0.0F, 10.0F, 6, 1, 1, 0.0F, false));

        ConsoleTable4 = new RendererModel(this);
        ConsoleTable4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleTable4, 0.0F, 3.1416F, 0.0F);
        SharpUnit.addChild(ConsoleTable4);
        ConsoleTable4.cubeList.add(new ModelBox(ConsoleTable4, 33, 7, -0.183F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));
        ConsoleTable4.cubeList.add(new ModelBox(ConsoleTable4, 33, 7, -8.817F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));

        ConsolePanelAngler4 = new RendererModel(this);
        ConsolePanelAngler4.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(ConsolePanelAngler4, 0.2967F, 0.0F, 0.0F);
        ConsoleTable4.addChild(ConsolePanelAngler4);
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -8.5F, 0.0F, 0.0F, 17, 1, 2, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -7.5F, 0.0F, 2.0F, 15, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -7.0F, 0.0F, 3.0F, 14, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -6.5F, 0.0F, 4.0F, 13, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -6.0F, 0.0F, 5.0F, 12, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -5.5F, 0.0F, 6.0F, 11, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -5.0F, 0.0F, 7.0F, 10, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -4.5F, 0.0F, 8.0F, 9, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -4.0F, 0.0F, 9.0F, 8, 1, 1, 0.0F, false));
        ConsolePanelAngler4.cubeList.add(new ModelBox(ConsolePanelAngler4, 6, 146, -3.0F, 0.0F, 10.0F, 6, 1, 1, 0.0F, false));

        ConsoleTable5 = new RendererModel(this);
        ConsoleTable5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleTable5, 0.0F, 2.0944F, 0.0F);
        SharpUnit.addChild(ConsoleTable5);
        ConsoleTable5.cubeList.add(new ModelBox(ConsoleTable5, 33, 7, -0.183F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));
        ConsoleTable5.cubeList.add(new ModelBox(ConsoleTable5, 33, 7, -8.817F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));

        ConsolePanelAngler5 = new RendererModel(this);
        ConsolePanelAngler5.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(ConsolePanelAngler5, 0.2967F, 0.0F, 0.0F);
        ConsoleTable5.addChild(ConsolePanelAngler5);
        ConsolePanelAngler5.cubeList.add(new ModelBox(ConsolePanelAngler5, 6, 146, -8.5F, 0.0F, 0.0F, 17, 1, 2, 0.0F, false));
        ConsolePanelAngler5.cubeList.add(new ModelBox(ConsolePanelAngler5, 6, 146, -7.5F, 0.0F, 2.0F, 15, 1, 1, 0.0F, false));
        ConsolePanelAngler5.cubeList.add(new ModelBox(ConsolePanelAngler5, 6, 146, -5.0F, 0.0F, 7.0F, 10, 1, 1, 0.0F, false));
        ConsolePanelAngler5.cubeList.add(new ModelBox(ConsolePanelAngler5, 6, 146, -4.5F, 0.0F, 8.0F, 9, 1, 1, 0.0F, false));
        ConsolePanelAngler5.cubeList.add(new ModelBox(ConsolePanelAngler5, 6, 146, -4.0F, 0.0F, 9.0F, 8, 1, 1, 0.0F, false));
        ConsolePanelAngler5.cubeList.add(new ModelBox(ConsolePanelAngler5, 6, 146, -3.0F, 0.0F, 10.0F, 6, 1, 1, 0.0F, false));

        ConsoleTable6 = new RendererModel(this);
        ConsoleTable6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(ConsoleTable6, 0.0F, 1.0472F, 0.0F);
        SharpUnit.addChild(ConsoleTable6);
        ConsoleTable6.cubeList.add(new ModelBox(ConsoleTable6, 33, 7, -0.183F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));
        ConsoleTable6.cubeList.add(new ModelBox(ConsoleTable6, 33, 7, -8.817F, -14.0F, -16.2715F, 9, 1, 15, 0.0F, false));

        ConsolePanelAngler6 = new RendererModel(this);
        ConsolePanelAngler6.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(ConsolePanelAngler6, 0.2967F, 0.0F, 0.0F);
        ConsoleTable6.addChild(ConsolePanelAngler6);
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -8.5F, 0.0F, 0.0F, 17, 1, 2, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -7.5F, 0.0F, 2.0F, 15, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -7.0F, 0.0F, 3.0F, 14, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -6.5F, 0.0F, 4.0F, 13, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -6.0F, 0.0F, 5.0F, 12, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -5.5F, 0.0F, 6.0F, 11, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -5.0F, 0.0F, 7.0F, 10, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -4.5F, 0.0F, 8.0F, 9, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -4.0F, 0.0F, 9.0F, 8, 1, 1, 0.0F, false));
        ConsolePanelAngler6.cubeList.add(new ModelBox(ConsolePanelAngler6, 6, 146, -3.0F, 0.0F, 10.0F, 6, 1, 1, 0.0F, false));

        bone7 = new RendererModel(this);
        bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
        ConsolePanelAngler6.addChild(bone7);

        Rotor = new RendererModel(this);
        Rotor.setRotationPoint(0.0F, 0.0F, 0.0F);
        SharpUnit.addChild(Rotor);

        RotorGoobler1 = new RendererModel(this);
        RotorGoobler1.setRotationPoint(0.0F, 0.0F, 0.0F);
        Rotor.addChild(RotorGoobler1);
        RotorGoobler1.cubeList.add(new ModelBox(RotorGoobler1, 36, 124, -3.5F, -20.0F, -0.5F, 7, 3, 1, 0.0F, false));

        RotorGoobler2 = new RendererModel(this);
        RotorGoobler2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorGoobler2, 0.0F, -1.0472F, 0.0F);
        Rotor.addChild(RotorGoobler2);
        RotorGoobler2.cubeList.add(new ModelBox(RotorGoobler2, 36, 124, -3.5F, -20.0F, -0.5F, 7, 3, 1, 0.0F, false));

        RotorGoobler3 = new RendererModel(this);
        RotorGoobler3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorGoobler3, 0.0F, -2.0944F, 0.0F);
        Rotor.addChild(RotorGoobler3);
        RotorGoobler3.cubeList.add(new ModelBox(RotorGoobler3, 36, 124, -3.5F, -20.0F, -0.5F, 7, 3, 1, 0.0F, false));

        RotorDoobler1 = new RendererModel(this);
        RotorDoobler1.setRotationPoint(0.0F, 0.0F, 0.0F);
        Rotor.addChild(RotorDoobler1);
        RotorDoobler1.cubeList.add(new ModelBox(RotorDoobler1, 0, 61, -0.683F, -20.0F, -3.2811F, 2, 3, 4, 0.0F, false));
        RotorDoobler1.cubeList.add(new ModelBox(RotorDoobler1, 0, 61, -0.683F, -20.0F, -0.7189F, 2, 3, 4, 0.0F, false));
        RotorDoobler1.cubeList.add(new ModelBox(RotorDoobler1, 0, 61, -1.317F, -20.0F, -3.2811F, 2, 3, 4, 0.0F, false));
        RotorDoobler1.cubeList.add(new ModelBox(RotorDoobler1, 0, 61, -1.317F, -20.0F, -0.7189F, 2, 3, 4, 0.0F, false));

        RotorDoobler2 = new RendererModel(this);
        RotorDoobler2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorDoobler2, 0.0F, -1.0472F, 0.0F);
        Rotor.addChild(RotorDoobler2);
        RotorDoobler2.cubeList.add(new ModelBox(RotorDoobler2, 0, 61, -0.683F, -20.0F, -3.2811F, 2, 3, 4, 0.0F, false));
        RotorDoobler2.cubeList.add(new ModelBox(RotorDoobler2, 0, 61, -0.683F, -20.0F, -0.7189F, 2, 3, 4, 0.0F, false));
        RotorDoobler2.cubeList.add(new ModelBox(RotorDoobler2, 0, 61, -1.317F, -20.0F, -3.2811F, 2, 3, 4, 0.0F, false));
        RotorDoobler2.cubeList.add(new ModelBox(RotorDoobler2, 0, 61, -1.317F, -20.0F, -0.7189F, 2, 3, 4, 0.0F, false));

        RotorDoobler3 = new RendererModel(this);
        RotorDoobler3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorDoobler3, 0.0F, -2.0944F, 0.0F);
        Rotor.addChild(RotorDoobler3);
        RotorDoobler3.cubeList.add(new ModelBox(RotorDoobler3, 0, 61, -0.683F, -20.0F, -3.2811F, 2, 3, 4, 0.0F, false));
        RotorDoobler3.cubeList.add(new ModelBox(RotorDoobler3, 0, 61, -0.683F, -20.0F, -0.7189F, 2, 3, 4, 0.0F, false));
        RotorDoobler3.cubeList.add(new ModelBox(RotorDoobler3, 0, 61, -1.317F, -20.0F, -3.2811F, 2, 3, 4, 0.0F, false));
        RotorDoobler3.cubeList.add(new ModelBox(RotorDoobler3, 0, 61, -1.317F, -20.0F, -0.7189F, 2, 3, 4, 0.0F, false));

        RotorCenter = new RendererModel(this);
        RotorCenter.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorCenter, 0.0F, -0.7854F, 0.0F);
        Rotor.addChild(RotorCenter);
        RotorCenter.cubeList.add(new ModelBox(RotorCenter, 76, 0, -0.5F, -28.0F, -0.5F, 1, 8, 1, 0.0F, false));

        RotorPipe1 = new RendererModel(this);
        RotorPipe1.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorPipe1, 0.0F, -1.0472F, 0.0F);
        Rotor.addChild(RotorPipe1);
        RotorPipe1.cubeList.add(new ModelBox(RotorPipe1, 0, 100, -0.5F, -28.0F, -2.5F, 1, 8, 1, 0.0F, false));

        RotorPipe2 = new RendererModel(this);
        RotorPipe2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorPipe2, 0.0F, 1.0472F, 0.0F);
        Rotor.addChild(RotorPipe2);
        RotorPipe2.cubeList.add(new ModelBox(RotorPipe2, 0, 100, -0.5F, -28.0F, -2.5F, 1, 8, 1, 0.0F, false));

        RotorPipe3 = new RendererModel(this);
        RotorPipe3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorPipe3, 0.0F, 3.1416F, 0.0F);
        Rotor.addChild(RotorPipe3);
        RotorPipe3.cubeList.add(new ModelBox(RotorPipe3, 0, 100, -0.5F, -28.0F, -2.5F, 1, 8, 1, 0.0F, false));

        RotorOuter1 = new RendererModel(this);
        RotorOuter1.setRotationPoint(0.0F, 0.0F, 0.0F);
        Rotor.addChild(RotorOuter1);
        RotorOuter1.cubeList.add(new ModelBox(RotorOuter1, 99, 35, -1.5F, -28.0F, -2.5F, 3, 8, 0, 0.0F, false));

        RotorOuter2 = new RendererModel(this);
        RotorOuter2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorOuter2, 0.0F, -2.0944F, 0.0F);
        Rotor.addChild(RotorOuter2);
        RotorOuter2.cubeList.add(new ModelBox(RotorOuter2, 99, 35, -1.5F, -28.0F, -2.5F, 3, 8, 0, 0.0F, false));

        RotorOuter3 = new RendererModel(this);
        RotorOuter3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(RotorOuter3, 0.0F, 2.0944F, 0.0F);
        Rotor.addChild(RotorOuter3);
        RotorOuter3.cubeList.add(new ModelBox(RotorOuter3, 99, 35, -1.5F, -28.0F, -2.5F, 3, 8, 0, 0.0F, false));

        Panel1 = new RendererModel(this);
        Panel1.setRotationPoint(0.0F, 0.0F, 0.0F);
        SharpUnit.addChild(Panel1);

        Panel1Angler = new RendererModel(this);
        Panel1Angler.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(Panel1Angler, 0.2967F, 0.0F, 0.0F);
        Panel1.addChild(Panel1Angler);
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 38, 105, 4.5F, -0.375F, 0.5F, 2, 1, 2, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 104, 106, -1.5F, -0.25F, 8.0F, 3, 1, 2, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 8, 13, -2.0F, -0.5F, 5.5F, 1, 1, 2, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 0, 63, -3.5F, -0.5F, 6.0F, 1, 1, 1, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 8, 13, -2.0F, -0.5F, 3.0F, 1, 1, 2, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 0, 63, -3.5F, -0.5F, 3.5F, 1, 1, 1, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 8, 13, -2.0F, -0.5F, 0.5F, 1, 1, 2, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 0, 63, -3.5F, -0.5F, 1.0F, 1, 1, 1, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 48, 128, 1.5F, -0.5F, 1.0F, 1, 1, 1, 0.0F, false));
        Panel1Angler.cubeList.add(new ModelBox(Panel1Angler, 74, 32, -1.0F, -0.375F, 8.5F, 2, 1, 1, 0.0F, false));

        Panel1Switch1 = new RendererModel(this);
        Panel1Switch1.setRotationPoint(-1.5F, -0.5F, 1.5F);
        setRotationAngle(Panel1Switch1, -0.2967F, 0.0F, 0.0F);
        Panel1Angler.addChild(Panel1Switch1);
        Panel1Switch1.cubeList.add(new ModelBox(Panel1Switch1, 66, 32, -0.5F, -2.0F, -0.5F, 1, 1, 1, 0.0F, false));
        Panel1Switch1.cubeList.add(new ModelBox(Panel1Switch1, 8, 5, 0.0F, -1.5F, -0.5F, 0, 2, 1, 0.0F, false));

        Panel1Switch2 = new RendererModel(this);
        Panel1Switch2.setRotationPoint(-1.5F, -0.5F, 4.0F);
        setRotationAngle(Panel1Switch2, -0.2967F, 0.0F, 0.0F);
        Panel1Angler.addChild(Panel1Switch2);
        Panel1Switch2.cubeList.add(new ModelBox(Panel1Switch2, 66, 32, -0.5F, -2.0F, -0.5F, 1, 1, 1, 0.0F, false));
        Panel1Switch2.cubeList.add(new ModelBox(Panel1Switch2, 8, 5, 0.0F, -1.5F, -0.5F, 0, 2, 1, 0.0F, false));

        Panel1Switch3 = new RendererModel(this);
        Panel1Switch3.setRotationPoint(-1.5F, -0.5F, 6.5F);
        setRotationAngle(Panel1Switch3, -0.2967F, 0.0F, 0.0F);
        Panel1Angler.addChild(Panel1Switch3);
        Panel1Switch3.cubeList.add(new ModelBox(Panel1Switch3, 66, 32, -0.5F, -2.0F, -0.5F, 1, 1, 1, 0.0F, false));
        Panel1Switch3.cubeList.add(new ModelBox(Panel1Switch3, 8, 5, 0.0F, -1.5F, -0.5F, 0, 2, 1, 0.0F, false));

        Panel1Lever1 = new RendererModel(this);
        Panel1Lever1.setRotationPoint(1.0F, 0.0F, 7.0F);
        Panel1Angler.addChild(Panel1Lever1);
        Panel1Lever1.cubeList.add(new ModelBox(Panel1Lever1, 8, 13, -0.5F, -0.5F, -1.5F, 1, 1, 2, 0.0F, false));

        Panel1Knob1 = new RendererModel(this);
        Panel1Knob1.setRotationPoint(3.0F, 0.0F, 7.0F);
        setRotationAngle(Panel1Knob1, 0.0F, -0.7854F, 0.0F);
        Panel1Angler.addChild(Panel1Knob1);
        Panel1Knob1.cubeList.add(new ModelBox(Panel1Knob1, 66, 32, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel1Knob4 = new RendererModel(this);
        Panel1Knob4.setRotationPoint(3.0F, 0.0F, 4.0F);
        setRotationAngle(Panel1Knob4, 0.0F, -0.7854F, 0.0F);
        Panel1Angler.addChild(Panel1Knob4);
        Panel1Knob4.cubeList.add(new ModelBox(Panel1Knob4, 48, 128, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel1Knob2 = new RendererModel(this);
        Panel1Knob2.setRotationPoint(-5.0F, 0.0F, 1.5F);
        setRotationAngle(Panel1Knob2, 0.0F, -0.7854F, 0.0F);
        Panel1Angler.addChild(Panel1Knob2);
        Panel1Knob2.cubeList.add(new ModelBox(Panel1Knob2, 48, 128, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel1Knob3 = new RendererModel(this);
        Panel1Knob3.setRotationPoint(-5.0F, 0.0F, 3.5F);
        setRotationAngle(Panel1Knob3, 0.0F, -0.7854F, 0.0F);
        Panel1Angler.addChild(Panel1Knob3);
        Panel1Knob3.cubeList.add(new ModelBox(Panel1Knob3, 63, 129, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel2 = new RendererModel(this);
        Panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Panel2, 0.0F, 1.0472F, 0.0F);
        SharpUnit.addChild(Panel2);

        Panel2Angler = new RendererModel(this);
        Panel2Angler.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(Panel2Angler, 0.2967F, 0.0F, 0.0F);
        Panel2.addChild(Panel2Angler);
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 104, 106, -1.5F, -0.25F, 8.0F, 3, 1, 2, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 74, 32, -1.0F, -0.375F, 8.5F, 2, 1, 1, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 57, 111, 2.0F, -0.5F, 5.0F, 2, 1, 2, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 57, 111, -4.0F, -0.5F, 5.0F, 2, 1, 2, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 104, 106, -1.5F, -0.5F, 5.0F, 3, 1, 2, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 0, 63, -0.5F, -0.5F, 3.0F, 1, 1, 1, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 113, 119, -4.0F, -0.25F, 0.5F, 8, 1, 2, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 0, 63, 1.0F, -0.5F, 3.0F, 1, 1, 1, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 0, 63, -2.0F, -0.5F, 3.0F, 1, 1, 1, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 0, 63, 2.5F, -0.5F, 3.0F, 1, 1, 1, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 0, 63, -3.5F, -0.5F, 3.0F, 1, 1, 1, 0.0F, false));
        Panel2Angler.cubeList.add(new ModelBox(Panel2Angler, 19, 117, -1.0F, -0.75F, 5.5F, 2, 1, 1, 0.0F, false));

        Panel2TopSwitch = new RendererModel(this);
        Panel2TopSwitch.setRotationPoint(-2.5F, 0.0F, 8.75F);
        setRotationAngle(Panel2TopSwitch, 0.0F, -0.7854F, 0.0F);
        Panel2Angler.addChild(Panel2TopSwitch);
        Panel2TopSwitch.cubeList.add(new ModelBox(Panel2TopSwitch, 63, 129, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel2BlackSwitch1 = new RendererModel(this);
        Panel2BlackSwitch1.setRotationPoint(-3.0F, -0.25F, 1.5F);
        setRotationAngle(Panel2BlackSwitch1, 0.0F, -0.7854F, 0.0F);
        Panel2Angler.addChild(Panel2BlackSwitch1);
        Panel2BlackSwitch1.cubeList.add(new ModelBox(Panel2BlackSwitch1, 66, 32, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel2BlackSwitch2 = new RendererModel(this);
        Panel2BlackSwitch2.setRotationPoint(3.0F, -0.25F, 1.5F);
        setRotationAngle(Panel2BlackSwitch2, 0.0F, 0.7854F, 0.0F);
        Panel2Angler.addChild(Panel2BlackSwitch2);
        Panel2BlackSwitch2.cubeList.add(new ModelBox(Panel2BlackSwitch2, 66, 32, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel2Switchies1 = new RendererModel(this);
        Panel2Switchies1.setRotationPoint(0.0F, -0.25F, 1.5F);
        setRotationAngle(Panel2Switchies1, -0.2618F, 0.0F, 0.0F);
        Panel2Angler.addChild(Panel2Switchies1);
        Panel2Switchies1.cubeList.add(new ModelBox(Panel2Switchies1, 109, 45, -2.0F, -0.5F, -0.5F, 2, 1, 1, 0.0F, false));

        Panel2Switchies2 = new RendererModel(this);
        Panel2Switchies2.setRotationPoint(0.0F, -0.25F, 1.5F);
        setRotationAngle(Panel2Switchies2, 0.2618F, 0.0F, 0.0F);
        Panel2Angler.addChild(Panel2Switchies2);
        Panel2Switchies2.cubeList.add(new ModelBox(Panel2Switchies2, 109, 45, 0.0F, -0.5F, -0.5F, 2, 1, 1, 0.0F, false));

        Panel2BigKnob = new RendererModel(this);
        Panel2BigKnob.setRotationPoint(5.5F, 0.0F, 1.5F);
        setRotationAngle(Panel2BigKnob, 0.0F, -0.7854F, 0.0F);
        Panel2Angler.addChild(Panel2BigKnob);
        Panel2BigKnob.cubeList.add(new ModelBox(Panel2BigKnob, 48, 128, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel2BigKnob2 = new RendererModel(this);
        Panel2BigKnob2.setRotationPoint(-5.5F, 0.0F, 1.5F);
        setRotationAngle(Panel2BigKnob2, 0.0F, 0.7854F, 0.0F);
        Panel2Angler.addChild(Panel2BigKnob2);
        Panel2BigKnob2.cubeList.add(new ModelBox(Panel2BigKnob2, 48, 128, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel3 = new RendererModel(this);
        Panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Panel3, 0.0F, 2.0944F, 0.0F);
        SharpUnit.addChild(Panel3);

        Panel3Angler = new RendererModel(this);
        Panel3Angler.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(Panel3Angler, 0.2967F, 0.0F, 0.0F);
        Panel3.addChild(Panel3Angler);
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 104, 106, -1.5F, -0.25F, 8.0F, 3, 1, 2, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 74, 32, -1.0F, -0.375F, 8.5F, 2, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 7, 174, -5.0F, -0.125F, 6.5F, 10, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 7, 174, -5.5F, -0.125F, 5.5F, 11, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 7, 174, -6.5F, -0.125F, 4.5F, 13, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 7, 174, -7.0F, -0.125F, 3.5F, 14, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 7, 174, -7.5F, -0.125F, 2.5F, 15, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 7, 174, -3.0F, -0.125F, 1.5F, 11, 1, 1, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 64, 142, -0.5F, -0.25F, 2.5F, 1, 1, 2, 0.0F, false));
        Panel3Angler.cubeList.add(new ModelBox(Panel3Angler, 19, 111, -5.5F, -0.5F, 0.0F, 2, 1, 2, 0.0F, false));

        Panel3TopSwitch = new RendererModel(this);
        Panel3TopSwitch.setRotationPoint(2.5F, 0.0F, 9.0F);
        setRotationAngle(Panel3TopSwitch, 0.0F, -0.7854F, 0.0F);
        Panel3Angler.addChild(Panel3TopSwitch);
        Panel3TopSwitch.cubeList.add(new ModelBox(Panel3TopSwitch, 48, 128, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel3BigKnobL = new RendererModel(this);
        Panel3BigKnobL.setRotationPoint(-3.0F, -0.25F, 4.5F);
        setRotationAngle(Panel3BigKnobL, 0.0F, -0.7854F, 0.0F);
        Panel3Angler.addChild(Panel3BigKnobL);
        Panel3BigKnobL.cubeList.add(new ModelBox(Panel3BigKnobL, 0, 110, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

        Panel3BigKnobR = new RendererModel(this);
        Panel3BigKnobR.setRotationPoint(3.0F, -0.25F, 4.5F);
        setRotationAngle(Panel3BigKnobR, 0.0F, 0.7854F, 0.0F);
        Panel3Angler.addChild(Panel3BigKnobR);
        Panel3BigKnobR.cubeList.add(new ModelBox(Panel3BigKnobR, 57, 108, -1.0F, -0.5F, -1.0F, 2, 1, 2, 0.0F, false));

        Panel3Switches = new RendererModel(this);
        Panel3Switches.setRotationPoint(0.0F, -0.125F, 2.5F);
        setRotationAngle(Panel3Switches, 0.2967F, 0.0F, 0.0F);
        Panel3Angler.addChild(Panel3Switches);
        Panel3Switches.cubeList.add(new ModelBox(Panel3Switches, 8, 45, -1.0F, -1.0F, -0.5F, 2, 2, 1, 0.0F, false));

        Panel3Switches2 = new RendererModel(this);
        Panel3Switches2.setRotationPoint(0.0F, -0.125F, 6.5F);
        setRotationAngle(Panel3Switches2, -0.2967F, 0.0F, 0.0F);
        Panel3Angler.addChild(Panel3Switches2);
        Panel3Switches2.cubeList.add(new ModelBox(Panel3Switches2, 0, 13, -0.5F, -1.0F, -0.5F, 1, 2, 1, 0.0F, false));

        Panel3LightAnglerLeft = new RendererModel(this);
        Panel3LightAnglerLeft.setRotationPoint(-0.5F, -0.25F, 4.5F);
        setRotationAngle(Panel3LightAnglerLeft, 0.0F, 0.7854F, 0.0F);
        Panel3Angler.addChild(Panel3LightAnglerLeft);
        Panel3LightAnglerLeft.cubeList.add(new ModelBox(Panel3LightAnglerLeft, 64, 142, 0.0F, 0.0F, 0.0F, 1, 1, 3, 0.0F, false));
        Panel3LightAnglerLeft.cubeList.add(new ModelBox(Panel3LightAnglerLeft, 103, 66, 0.0F, -0.25F, 3.0F, 1, 1, 1, 0.0F, false));

        Panel3LightAnglerLeft2 = new RendererModel(this);
        Panel3LightAnglerLeft2.setRotationPoint(0.5F, -0.25F, 4.5F);
        setRotationAngle(Panel3LightAnglerLeft2, 0.0F, -0.7854F, 0.0F);
        Panel3Angler.addChild(Panel3LightAnglerLeft2);
        Panel3LightAnglerLeft2.cubeList.add(new ModelBox(Panel3LightAnglerLeft2, 64, 142, -1.0F, 0.0F, 0.0F, 1, 1, 3, 0.0F, false));
        Panel3LightAnglerLeft2.cubeList.add(new ModelBox(Panel3LightAnglerLeft2, 24, 100, -1.0F, -0.25F, 3.0F, 1, 1, 1, 0.0F, false));

        Panel4 = new RendererModel(this);
        Panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Panel4, 0.0F, 3.1416F, 0.0F);
        SharpUnit.addChild(Panel4);

        Panel4Angler = new RendererModel(this);
        Panel4Angler.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(Panel4Angler, 0.2967F, 0.0F, 0.0F);
        Panel4.addChild(Panel4Angler);
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 104, 106, -1.5F, -0.25F, 8.0F, 3, 1, 2, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 0, 70, -2.0F, -0.5F, 6.0F, 4, 1, 2, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 66, 32, -1.0F, -0.5F, 0.25F, 2, 1, 4, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 65, 29, -3.0F, -0.125F, 1.75F, 6, 1, 1, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 19, 111, 2.5F, -0.5F, 1.25F, 2, 1, 2, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 19, 111, -4.5F, -0.5F, 1.25F, 2, 1, 2, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 64, 142, -4.5F, -0.125F, 4.5F, 9, 1, 1, 0.0F, false));
        Panel4Angler.cubeList.add(new ModelBox(Panel4Angler, 74, 32, -1.0F, -0.375F, 8.5F, 2, 1, 1, 0.0F, false));

        Panel4SwitchesLeft = new RendererModel(this);
        Panel4SwitchesLeft.setRotationPoint(0.0F, -0.5F, 7.0F);
        setRotationAngle(Panel4SwitchesLeft, -0.2967F, 0.0F, 0.0F);
        Panel4Angler.addChild(Panel4SwitchesLeft);
        Panel4SwitchesLeft.cubeList.add(new ModelBox(Panel4SwitchesLeft, 46, 89, -1.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel4SwitchesRight = new RendererModel(this);
        Panel4SwitchesRight.setRotationPoint(0.0F, -0.5F, 7.0F);
        setRotationAngle(Panel4SwitchesRight, 0.2967F, 0.0F, 0.0F);
        Panel4Angler.addChild(Panel4SwitchesRight);
        Panel4SwitchesRight.cubeList.add(new ModelBox(Panel4SwitchesRight, 33, 54, 0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel4RightLights = new RendererModel(this);
        Panel4RightLights.setRotationPoint(7.0F, 0.0F, 0.0F);
        setRotationAngle(Panel4RightLights, 0.0F, -0.5236F, 0.0F);
        Panel4Angler.addChild(Panel4RightLights);
        Panel4RightLights.cubeList.add(new ModelBox(Panel4RightLights, 0, 63, 0.0F, -0.5F, 2.0F, 1, 1, 1, 0.0F, false));
        Panel4RightLights.cubeList.add(new ModelBox(Panel4RightLights, 64, 142, 0.0F, -0.125F, 3.0F, 1, 1, 2, 0.0F, false));
        Panel4RightLights.cubeList.add(new ModelBox(Panel4RightLights, 64, 142, 0.0F, -0.125F, 6.0F, 1, 1, 2, 0.0F, false));
        Panel4RightLights.cubeList.add(new ModelBox(Panel4RightLights, 0, 63, 0.0F, -0.5F, 5.0F, 1, 1, 1, 0.0F, false));
        Panel4RightLights.cubeList.add(new ModelBox(Panel4RightLights, 0, 63, 0.0F, -0.5F, 8.0F, 1, 1, 1, 0.0F, false));

        Panel4LeftLights = new RendererModel(this);
        Panel4LeftLights.setRotationPoint(-7.0F, 0.0F, 0.0F);
        setRotationAngle(Panel4LeftLights, 0.0F, 0.5236F, 0.0F);
        Panel4Angler.addChild(Panel4LeftLights);
        Panel4LeftLights.cubeList.add(new ModelBox(Panel4LeftLights, 0, 63, -1.0F, -0.5F, 2.0F, 1, 1, 1, 0.0F, false));
        Panel4LeftLights.cubeList.add(new ModelBox(Panel4LeftLights, 64, 142, -1.0F, -0.125F, 3.0F, 1, 1, 2, 0.0F, false));
        Panel4LeftLights.cubeList.add(new ModelBox(Panel4LeftLights, 64, 142, -1.0F, -0.125F, 6.0F, 1, 1, 2, 0.0F, false));
        Panel4LeftLights.cubeList.add(new ModelBox(Panel4LeftLights, 0, 63, -1.0F, -0.5F, 5.0F, 1, 1, 1, 0.0F, false));
        Panel4LeftLights.cubeList.add(new ModelBox(Panel4LeftLights, 0, 63, -1.0F, -0.5F, 8.0F, 1, 1, 1, 0.0F, false));

        Panel4BigSwitch = new RendererModel(this);
        Panel4BigSwitch.setRotationPoint(0.0F, 1.5F, 2.25F);
        Panel4Angler.addChild(Panel4BigSwitch);
        Panel4BigSwitch.cubeList.add(new ModelBox(Panel4BigSwitch, 66, 32, -0.5F, -2.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel5 = new RendererModel(this);
        Panel5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Panel5, 0.0F, -2.0944F, 0.0F);
        SharpUnit.addChild(Panel5);

        Panel5Angler = new RendererModel(this);
        Panel5Angler.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(Panel5Angler, 0.2967F, 0.0F, 0.0F);
        Panel5.addChild(Panel5Angler);
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 104, 106, -1.5F, -0.25F, 8.0F, 3, 1, 2, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 73, 69, -1.5F, -0.25F, 5.0F, 3, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 0, 63, -0.5F, -0.5F, 6.5F, 1, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 0, 63, 1.5F, -0.5F, 6.5F, 1, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 0, 63, -2.5F, -0.5F, 6.5F, 1, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 64, 142, -1.5F, -0.125F, 6.5F, 1, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 64, 142, 0.5F, -0.125F, 6.5F, 1, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 64, 142, -3.5F, -0.125F, 2.5F, 1, 1, 5, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 64, 142, 2.5F, -0.125F, 5.5F, 1, 1, 2, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 0, 63, 2.5F, -0.5F, 4.5F, 1, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 74, 32, -1.0F, -0.375F, 8.5F, 2, 1, 1, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 66, 32, -4.0F, -0.5F, 0.5F, 2, 1, 4, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 38, 105, -6.5F, -0.5F, 0.75F, 2, 1, 2, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 66, 32, -1.5F, -0.5F, 0.5F, 2, 1, 4, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 0, 70, 1.0F, -0.5F, 2.0F, 4, 1, 2, 0.0F, false));
        Panel5Angler.cubeList.add(new ModelBox(Panel5Angler, 69, 24, 3.0F, -0.375F, 0.0F, 4, 1, 2, 0.0F, false));

        Panel5BigSwitch1 = new RendererModel(this);
        Panel5BigSwitch1.setRotationPoint(-3.0F, 1.5F, 2.5F);
        Panel5Angler.addChild(Panel5BigSwitch1);
        Panel5BigSwitch1.cubeList.add(new ModelBox(Panel5BigSwitch1, 66, 32, -0.5F, -2.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel5BigSwitch2 = new RendererModel(this);
        Panel5BigSwitch2.setRotationPoint(-0.5F, 1.5F, 2.5F);
        Panel5Angler.addChild(Panel5BigSwitch2);
        Panel5BigSwitch2.cubeList.add(new ModelBox(Panel5BigSwitch2, 66, 32, -0.5F, -2.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel5SwitchesLeft = new RendererModel(this);
        Panel5SwitchesLeft.setRotationPoint(3.0F, -0.5F, 3.0F);
        setRotationAngle(Panel5SwitchesLeft, -0.2967F, 0.0F, 0.0F);
        Panel5Angler.addChild(Panel5SwitchesLeft);
        Panel5SwitchesLeft.cubeList.add(new ModelBox(Panel5SwitchesLeft, 46, 89, -1.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel5SwitchesRight = new RendererModel(this);
        Panel5SwitchesRight.setRotationPoint(3.0F, -0.5F, 3.0F);
        setRotationAngle(Panel5SwitchesRight, 0.2967F, 0.0F, 0.0F);
        Panel5Angler.addChild(Panel5SwitchesRight);
        Panel5SwitchesRight.cubeList.add(new ModelBox(Panel5SwitchesRight, 33, 54, 0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

        Panel6 = new RendererModel(this);
        Panel6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Panel6, 0.0F, -1.0472F, 0.0F);
        SharpUnit.addChild(Panel6);

        Panel6Angler = new RendererModel(this);
        Panel6Angler.setRotationPoint(0.0F, -14.0F, -15.75F);
        setRotationAngle(Panel6Angler, 0.2967F, 0.0F, 0.0F);
        Panel6.addChild(Panel6Angler);
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 0, 63, 2.5F, -0.5F, 4.0F, 1, 1, 1, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 0, 63, -0.5F, -0.5F, 4.0F, 1, 1, 1, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 0, 63, -3.5F, -0.5F, 4.0F, 1, 1, 1, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 104, 106, -1.5F, -0.25F, 8.0F, 3, 1, 2, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 64, 142, -4.5F, -0.125F, 3.0F, 9, 1, 1, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 19, 111, -1.0F, -0.5F, 0.5F, 2, 1, 2, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 66, 74, 3.0F, -0.375F, 0.5F, 3, 1, 2, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 64, 142, 3.5F, -0.125F, 4.0F, 1, 1, 1, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 64, 142, -4.5F, -0.125F, 4.0F, 1, 1, 1, 0.0F, false));
        Panel6Angler.cubeList.add(new ModelBox(Panel6Angler, 74, 32, -1.0F, -0.375F, 8.5F, 2, 1, 1, 0.0F, false));

        Panel6Lever1 = new RendererModel(this);
        Panel6Lever1.setRotationPoint(0.0F, 0.0F, 7.0F);
        Panel6Angler.addChild(Panel6Lever1);
        Panel6Lever1.cubeList.add(new ModelBox(Panel6Lever1, 8, 13, -0.5F, -0.5F, -1.5F, 1, 1, 2, 0.0F, false));

        Panel6Lever2 = new RendererModel(this);
        Panel6Lever2.setRotationPoint(-4.0F, 0.0F, 2.0F);
        Panel6Angler.addChild(Panel6Lever2);
        Panel6Lever2.cubeList.add(new ModelBox(Panel6Lever2, 8, 13, -0.5F, -0.5F, -1.5F, 1, 1, 2, 0.0F, false));

        Panel6LinesRight = new RendererModel(this);
        Panel6LinesRight.setRotationPoint(4.5F, -0.125F, 5.0F);
        setRotationAngle(Panel6LinesRight, 0.0F, -0.5236F, 0.0F);
        Panel6Angler.addChild(Panel6LinesRight);
        Panel6LinesRight.cubeList.add(new ModelBox(Panel6LinesRight, 64, 142, -1.0F, 0.0F, 0.0F, 1, 1, 3, 0.0F, false));
        Panel6LinesRight.cubeList.add(new ModelBox(Panel6LinesRight, 0, 63, -2.0F, -0.375F, 2.0F, 1, 1, 1, 0.0F, false));

        Panel6LinesLeft = new RendererModel(this);
        Panel6LinesLeft.setRotationPoint(-4.5F, -0.125F, 5.0F);
        setRotationAngle(Panel6LinesLeft, 0.0F, 0.5236F, 0.0F);
        Panel6Angler.addChild(Panel6LinesLeft);
        Panel6LinesLeft.cubeList.add(new ModelBox(Panel6LinesLeft, 0, 6, 0.0F, -0.875F, 3.0F, 1, 2, 1, 0.0F, false));
        Panel6LinesLeft.cubeList.add(new ModelBox(Panel6LinesLeft, 64, 142, 0.0F, 0.0F, 0.0F, 1, 1, 3, 0.0F, false));
        Panel6LinesLeft.cubeList.add(new ModelBox(Panel6LinesLeft, 0, 63, 1.0F, -0.375F, 2.0F, 1, 1, 1, 0.0F, false));
    }

    public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
        RendererModel.rotateAngleX = x;
        RendererModel.rotateAngleY = y;
        RendererModel.rotateAngleZ = z;
    }

    @Override
    public void render(ConsoleTile console, float scale) {
        SharpUnit.render(scale);

        float rotorAnim = (float) (Math.cos(console.flightTicks * 0.1) * 0.5F);
        this.Rotor.rotateAngleY = (float) Math.toRadians((console.flightTicks - Minecraft.getInstance().getRenderPartialTicks()) % 360);
        this.Rotor.offsetY = rotorAnim / 10 + 0.009F;

    }
}