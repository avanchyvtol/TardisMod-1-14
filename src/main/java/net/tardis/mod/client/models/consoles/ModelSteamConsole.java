package net.tardis.mod.client.models.consoles;
//Made with Blockbench
//Paste this code into your mod.


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.Direction;
import net.tardis.mod.controls.CommunicatorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class ModelSteamConsole extends Model implements IConsoleModel{
	private final RendererModel glow;
	private final RendererModel glow_meterglass_a1;
	private final RendererModel glow_lamp_a1;
	private final RendererModel glow_lamp_a2;
	private final RendererModel glow_glass_b1;
	private final RendererModel glow_glass_b2;
	private final RendererModel glow_lamp_c1;
	private final RendererModel glow_lamp_c2;
	private final RendererModel glow_panel_e;
	private final RendererModel glow_glass_e2;
	private final RendererModel glow_glass_e3;
	private final RendererModel glow_glass_e1;
	private final RendererModel redlamp_e3;
	private final RendererModel redlamp_e4;
	private final RendererModel glow_core;
	private final RendererModel glow_radio;
	private final RendererModel rotor;
	private final RendererModel core;
	private final RendererModel underbit_1;
	private final RendererModel underbit_2;
	private final RendererModel underbit_3;
	private final RendererModel spinner_slide_y;
	private final RendererModel hourflip_rotate_x;
	private final RendererModel glass;
	private final RendererModel center;
	private final RendererModel top_ring;
	private final RendererModel under_ring;
	private final RendererModel strut_1;
	private final RendererModel strut_2;
	private final RendererModel controls;
	private final RendererModel contolset_a;
	private final RendererModel door_switch;
	private final RendererModel door_crank_rotate_y;
	private final RendererModel sonic_port;
	private final RendererModel meter_a1;
	private final RendererModel needle_a1_rotate_y;
	private final RendererModel button_set_a1;
	private final RendererModel baseplate_a1;
	private final RendererModel baseplate_a2;
	private final RendererModel baseplate_a3;
	private final RendererModel controlset_b;
	private final RendererModel throttle;
	private final RendererModel leaver_b1_rotate_z;
	private final RendererModel leaver_b1_jig;
	private final RendererModel x_switch;
	private final RendererModel y_switch;
	private final RendererModel z_switch;
	private final RendererModel cordselect_inc;
	private final RendererModel cord_slider_slide_x;
	private final RendererModel nixietube_b1;
	private final RendererModel nixietube_b2;
	private final RendererModel baseplate_b1;
	private final RendererModel baseplate_b2;
	private final RendererModel baseplate_b3;
	private final RendererModel readout_b1;
	private final RendererModel baseplate_b4;
	private final RendererModel readout_b2;
	private final RendererModel controlset_c;
	private final RendererModel communications;
	private final RendererModel radio;
	private final RendererModel radio_dial;
	private final RendererModel radio_dial_sub;
	private final RendererModel radio_needle;
	private final RendererModel speakers;
	private final RendererModel radio_body;
	private final RendererModel radio_dial_base;
	private final RendererModel refuler;
	private final RendererModel baseplate_c1;
	private final RendererModel baseplate_c2;
	private final RendererModel slider_c1;
	private final RendererModel sliderknob_c1_slide_x;
	private final RendererModel slider_track_c1;
	private final RendererModel slider_c2;
	private final RendererModel sliderknob_c2_slide_x;
	private final RendererModel slider_track_c2;
	private final RendererModel slider_c3;
	private final RendererModel sliderknob_c3_slide_x;
	private final RendererModel slider_track_c3;
	private final RendererModel switch_c1;
	private final RendererModel toggle_c1;
	private final RendererModel switch_c2;
	private final RendererModel toggle_c2;
	private final RendererModel switch_c3;
	private final RendererModel toggle_c3;
	private final RendererModel switch_c4;
	private final RendererModel toggle_c4;
	private final RendererModel controlset_d;
	private final RendererModel waypoint_selector;
	private final RendererModel type_arm_rotate_x;
	private final RendererModel keys;
	private final RendererModel type_body;
	private final RendererModel tumbler;
	private final RendererModel dimention_selector;
	private final RendererModel tilt_d1;
	private final RendererModel baseplate_d1;
	private final RendererModel baseplate_d2;
	private final RendererModel baseplate_d3;
	private final RendererModel controlset_e;
	private final RendererModel telepathic_circuits;
	private final RendererModel talking_board;
	private final RendererModel talkingboard_text;
	private final RendererModel talkingboard_corners;
	private final RendererModel scrying_glass;
	private final RendererModel frame;
	private final RendererModel scrying_glass_frame_e1;
	private final RendererModel scrying_glass_frame_e3;
	private final RendererModel scrying_glass_frame_e2;
	private final RendererModel post_e1;
	private final RendererModel nixietube_e1;
	private final RendererModel nixietube_e2;
	private final RendererModel nixietube_e3;
	private final RendererModel baseplate_e1;
	private final RendererModel baseplate_e2;
	private final RendererModel switch_e1;
	private final RendererModel toggle_e1;
	private final RendererModel switch_e2;
	private final RendererModel toggle_e2;
	private final RendererModel controlset_f;
	private final RendererModel baseplate_f1;
	private final RendererModel switch_f1;
	private final RendererModel toggle_f1;
	private final RendererModel switch_f2;
	private final RendererModel toggle_f2;
	private final RendererModel switch_f3;
	private final RendererModel toggle_f3;
	private final RendererModel switch_f4;
	private final RendererModel toggle_f4;
	private final RendererModel switch_f5;
	private final RendererModel toggle_f5;
	private final RendererModel switch_f6;
	private final RendererModel toggle_f6;
	private final RendererModel handbreak;
	private final RendererModel lever_f1_rotate_z;
	private final RendererModel barrel;
	private final RendererModel randomize_cords;
	private final RendererModel globe_rotate_y;
	private final RendererModel globe_mount;
	private final RendererModel rotation_selector;
	private final RendererModel rotation_crank_rotate_y;
	private final RendererModel structure;
	private final RendererModel side_001;
	private final RendererModel skin;
	private final RendererModel floor_skin;
	private final RendererModel floorboards;
	private final RendererModel leg_skin;
	private final RendererModel knee_skin;
	private final RendererModel thigh_skin;
	private final RendererModel belly_skin;
	private final RendererModel rimtop_skin;
	private final RendererModel panel_skin;
	private final RendererModel skelly;
	private final RendererModel rib;
	private final RendererModel rib_bolts_1;
	private final RendererModel foot;
	private final RendererModel leg;
	private final RendererModel thigh;
	private final RendererModel floorpipes;
	private final RendererModel front_rail;
	private final RendererModel front_railbolt;
	private final RendererModel rail_topbevel;
	private final RendererModel rail_underbevel;
	private final RendererModel rotor_gasket;
	private final RendererModel floor_trim;
	private final RendererModel side_002;
	private final RendererModel skin2;
	private final RendererModel floor_skin2;
	private final RendererModel floorboards2;
	private final RendererModel leg_skin2;
	private final RendererModel knee_skin2;
	private final RendererModel thigh_skin2;
	private final RendererModel belly_skin2;
	private final RendererModel rimtop_skin2;
	private final RendererModel panel_skin2;
	private final RendererModel skelly2;
	private final RendererModel rib2;
	private final RendererModel rib_bolts_2;
	private final RendererModel foot2;
	private final RendererModel leg2;
	private final RendererModel thigh2;
	private final RendererModel floorpipes2;
	private final RendererModel front_rail2;
	private final RendererModel front_railbolt2;
	private final RendererModel rail_topbevel2;
	private final RendererModel rail_underbevel2;
	private final RendererModel rotor_gasket2;
	private final RendererModel floor_trim2;
	private final RendererModel side_003;
	private final RendererModel skin3;
	private final RendererModel floor_skin3;
	private final RendererModel floorboards3;
	private final RendererModel leg_skin3;
	private final RendererModel knee_skin3;
	private final RendererModel thigh_skin3;
	private final RendererModel belly_skin3;
	private final RendererModel rimtop_skin3;
	private final RendererModel panel_skin3;
	private final RendererModel skelly3;
	private final RendererModel rib3;
	private final RendererModel rib_bolts_3;
	private final RendererModel foot3;
	private final RendererModel leg3;
	private final RendererModel thigh3;
	private final RendererModel floorpipes3;
	private final RendererModel front_rail3;
	private final RendererModel front_railbolt3;
	private final RendererModel rail_topbevel3;
	private final RendererModel rail_underbevel3;
	private final RendererModel rotor_gasket3;
	private final RendererModel floor_trim3;
	private final RendererModel side_004;
	private final RendererModel skin4;
	private final RendererModel floor_skin4;
	private final RendererModel floorboards4;
	private final RendererModel leg_skin4;
	private final RendererModel knee_skin4;
	private final RendererModel thigh_skin4;
	private final RendererModel belly_skin4;
	private final RendererModel rimtop_skin4;
	private final RendererModel panel_skin4;
	private final RendererModel skelly4;
	private final RendererModel rib4;
	private final RendererModel rib_bolts_4;
	private final RendererModel foot4;
	private final RendererModel leg4;
	private final RendererModel thigh4;
	private final RendererModel floorpipes4;
	private final RendererModel front_rail4;
	private final RendererModel front_railbolt4;
	private final RendererModel rail_topbevel4;
	private final RendererModel rail_underbevel4;
	private final RendererModel rotor_gasket4;
	private final RendererModel floor_trim4;
	private final RendererModel side_005;
	private final RendererModel skin5;
	private final RendererModel floor_skin5;
	private final RendererModel floorboards5;
	private final RendererModel leg_skin5;
	private final RendererModel knee_skin5;
	private final RendererModel thigh_skin5;
	private final RendererModel belly_skin5;
	private final RendererModel rimtop_skin5;
	private final RendererModel panel_skin5;
	private final RendererModel skelly5;
	private final RendererModel rib5;
	private final RendererModel rib_bolts_5;
	private final RendererModel foot5;
	private final RendererModel leg5;
	private final RendererModel thigh5;
	private final RendererModel floorpipes5;
	private final RendererModel front_rail5;
	private final RendererModel front_railbolt5;
	private final RendererModel rail_topbevel5;
	private final RendererModel rail_underbevel5;
	private final RendererModel rotor_gasket5;
	private final RendererModel floor_trim5;
	private final RendererModel side_006;
	private final RendererModel skin6;
	private final RendererModel floor_skin6;
	private final RendererModel floorboards6;
	private final RendererModel leg_skin6;
	private final RendererModel knee_skin6;
	private final RendererModel thigh_skin6;
	private final RendererModel belly_skin6;
	private final RendererModel rimtop_skin6;
	private final RendererModel panel_skin6;
	private final RendererModel skelly6;
	private final RendererModel rib6;
	private final RendererModel rib_bolts_6;
	private final RendererModel foot6;
	private final RendererModel leg6;
	private final RendererModel thigh6;
	private final RendererModel floorpipes6;
	private final RendererModel front_rail6;
	private final RendererModel front_railbolt6;
	private final RendererModel rail_topbevel6;
	private final RendererModel rail_underbevel6;
	private final RendererModel rotor_gasket6;
	private final RendererModel floor_trim6;

	public ModelSteamConsole() {
		textureWidth = 512;
		textureHeight = 512;

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(glow, 0.0F, 1.0472F, 0.0F);

		glow_meterglass_a1 = new RendererModel(this);
		glow_meterglass_a1.setRotationPoint(0.0F, -58.0F, 26.0F);
		setRotationAngle(glow_meterglass_a1, -0.3491F, 0.0F, 0.0F);
		glow.addChild(glow_meterglass_a1);
		glow_meterglass_a1.cubeList.add(new ModelBox(glow_meterglass_a1, 426, 330, -3.0F, -2.4F, -2.6F, 6, 4, 6, 0.0F, false));

		glow_lamp_a1 = new RendererModel(this);
		glow_lamp_a1.setRotationPoint(-11.0F, -58.8F, 38.0F);
		setRotationAngle(glow_lamp_a1, -0.3491F, 0.0F, 0.0F);
		glow.addChild(glow_lamp_a1);
		glow_lamp_a1.cubeList.add(new ModelBox(glow_lamp_a1, 490, 450, -2.0F, -3.0F, -2.0F, 4, 6, 4, 0.0F, false));

		glow_lamp_a2 = new RendererModel(this);
		glow_lamp_a2.setRotationPoint(11.0F, -59.0F, 37.0F);
		setRotationAngle(glow_lamp_a2, -0.3491F, 0.0F, 0.0F);
		glow.addChild(glow_lamp_a2);
		glow_lamp_a2.cubeList.add(new ModelBox(glow_lamp_a2, 490, 450, -2.0F, -3.0F, -1.0F, 4, 6, 4, 0.0F, false));

		glow_glass_b1 = new RendererModel(this);
		glow_glass_b1.setRotationPoint(17.0667F, -64.0F, 14.5333F);
		setRotationAngle(glow_glass_b1, -0.0873F, -0.5236F, 0.1745F);
		glow.addChild(glow_glass_b1);
		glow_glass_b1.cubeList.add(new ModelBox(glow_glass_b1, 240, 460, -0.0667F, 1.6F, -0.5333F, 1, 1, 1, 0.0F, false));
		glow_glass_b1.cubeList.add(new ModelBox(glow_glass_b1, 240, 460, -0.6667F, -2.4F, -0.9333F, 2, 4, 2, 0.0F, false));
		glow_glass_b1.cubeList.add(new ModelBox(glow_glass_b1, 240, 460, -0.0667F, -3.4F, -0.5333F, 1, 1, 1, 0.0F, false));

		glow_glass_b2 = new RendererModel(this);
		glow_glass_b2.setRotationPoint(22.0667F, -64.0F, 6.9333F);
		setRotationAngle(glow_glass_b2, 0.1745F, -2.0944F, 0.0F);
		glow.addChild(glow_glass_b2);
		glow_glass_b2.cubeList.add(new ModelBox(glow_glass_b2, 240, 460, -0.4667F, 2.0F, -0.5333F, 1, 1, 1, 0.0F, false));
		glow_glass_b2.cubeList.add(new ModelBox(glow_glass_b2, 240, 460, -1.0667F, -2.0F, -0.9333F, 2, 4, 2, 0.0F, false));
		glow_glass_b2.cubeList.add(new ModelBox(glow_glass_b2, 240, 460, -0.4667F, -3.0F, -0.5333F, 1, 1, 1, 0.0F, false));

		glow_lamp_c1 = new RendererModel(this);
		glow_lamp_c1.setRotationPoint(40.0F, -60.4F, -9.8F);
		setRotationAngle(glow_lamp_c1, 0.2618F, -1.0472F, 0.0F);
		glow.addChild(glow_lamp_c1);
		glow_lamp_c1.cubeList.add(new ModelBox(glow_lamp_c1, 280, 420, -2.6F, -3.8F, -2.2F, 1, 8, 2, 0.0F, false));
		glow_lamp_c1.cubeList.add(new ModelBox(glow_lamp_c1, 280, 420, -1.6F, -3.8F, -3.2F, 2, 8, 1, 0.0F, false));
		glow_lamp_c1.cubeList.add(new ModelBox(glow_lamp_c1, 280, 420, -1.6F, -3.8F, -0.2F, 2, 8, 1, 0.0F, false));
		glow_lamp_c1.cubeList.add(new ModelBox(glow_lamp_c1, 280, 420, -1.6F, -4.8F, -2.2F, 2, 8, 2, 0.0F, false));
		glow_lamp_c1.cubeList.add(new ModelBox(glow_lamp_c1, 280, 420, 0.2F, -3.8F, -2.2F, 1, 8, 2, 0.0F, false));

		glow_lamp_c2 = new RendererModel(this);
		glow_lamp_c2.setRotationPoint(29.0F, -60.4F, -29.0F);
		setRotationAngle(glow_lamp_c2, 0.2618F, -1.0472F, 0.0F);
		glow.addChild(glow_lamp_c2);
		glow_lamp_c2.cubeList.add(new ModelBox(glow_lamp_c2, 280, 420, 0.6F, -3.8F, -1.8F, 1, 8, 2, 0.0F, false));
		glow_lamp_c2.cubeList.add(new ModelBox(glow_lamp_c2, 280, 420, -1.4F, -3.8F, -2.8F, 2, 8, 1, 0.0F, false));
		glow_lamp_c2.cubeList.add(new ModelBox(glow_lamp_c2, 280, 420, -1.4F, -3.8F, 0.2F, 2, 8, 1, 0.0F, false));
		glow_lamp_c2.cubeList.add(new ModelBox(glow_lamp_c2, 280, 420, -1.4F, -4.8F, -1.8F, 2, 8, 2, 0.0F, false));
		glow_lamp_c2.cubeList.add(new ModelBox(glow_lamp_c2, 280, 420, -2.4F, -3.8F, -1.8F, 1, 8, 2, 0.0F, false));

		glow_panel_e = new RendererModel(this);
		glow_panel_e.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(glow_panel_e, 0.0F, -0.5236F, 0.0F);
		glow.addChild(glow_panel_e);

		glow_glass_e2 = new RendererModel(this);
		glow_glass_e2.setRotationPoint(34.0F, -58.0F, -2.0F);
		glow_panel_e.addChild(glow_glass_e2);
		glow_glass_e2.cubeList.add(new ModelBox(glow_glass_e2, 240, 460, -57.8F, -5.2F, -4.0F, 1, 1, 1, 0.0F, false));
		glow_glass_e2.cubeList.add(new ModelBox(glow_glass_e2, 240, 460, -58.4F, -9.2F, -4.4F, 2, 4, 2, 0.0F, false));
		glow_glass_e2.cubeList.add(new ModelBox(glow_glass_e2, 240, 460, -57.8F, -10.2F, -4.0F, 1, 1, 1, 0.0F, false));

		glow_glass_e3 = new RendererModel(this);
		glow_glass_e3.setRotationPoint(34.0F, -58.0F, -2.0F);
		glow_panel_e.addChild(glow_glass_e3);
		glow_glass_e3.cubeList.add(new ModelBox(glow_glass_e3, 240, 460, -55.4F, -5.2F, 1.6F, 1, 1, 1, 0.0F, false));
		glow_glass_e3.cubeList.add(new ModelBox(glow_glass_e3, 240, 460, -56.0F, -9.2F, 1.2F, 2, 4, 2, 0.0F, false));
		glow_glass_e3.cubeList.add(new ModelBox(glow_glass_e3, 240, 460, -55.4F, -10.2F, 1.6F, 1, 1, 1, 0.0F, false));

		glow_glass_e1 = new RendererModel(this);
		glow_glass_e1.setRotationPoint(34.0F, -58.0F, -2.0F);
		glow_panel_e.addChild(glow_glass_e1);
		glow_glass_e1.cubeList.add(new ModelBox(glow_glass_e1, 240, 460, -57.8F, -5.2F, 6.8F, 1, 1, 1, 0.0F, false));
		glow_glass_e1.cubeList.add(new ModelBox(glow_glass_e1, 240, 460, -58.4F, -9.2F, 6.4F, 2, 4, 2, 0.0F, false));
		glow_glass_e1.cubeList.add(new ModelBox(glow_glass_e1, 240, 460, -57.8F, -10.2F, 6.8F, 1, 1, 1, 0.0F, false));

		redlamp_e3 = new RendererModel(this);
		redlamp_e3.setRotationPoint(-52.0F, -54.0F, -19.0F);
		setRotationAngle(redlamp_e3, 0.0F, 0.0F, -0.3491F);
		glow_panel_e.addChild(redlamp_e3);
		redlamp_e3.cubeList.add(new ModelBox(redlamp_e3, 390, 440, -2.0F, 1.6F, -1.8F, 4, 4, 4, 0.0F, false));

		redlamp_e4 = new RendererModel(this);
		redlamp_e4.setRotationPoint(-52.0F, -54.0F, -19.0F);
		setRotationAngle(redlamp_e4, 0.0F, 0.0F, -0.3491F);
		glow_panel_e.addChild(redlamp_e4);
		redlamp_e4.cubeList.add(new ModelBox(redlamp_e4, 390, 440, -2.0F, 1.6F, 35.2F, 4, 4, 4, 0.0F, false));

		glow_core = new RendererModel(this);
		glow_core.setRotationPoint(0.0F, 4.0F, 0.0F);
		glow.addChild(glow_core);
		glow_core.cubeList.add(new ModelBox(glow_core, 266, 327, -16.4F, -54.0F, -12.0F, 32, 16, 24, 0.0F, false));

		glow_radio = new RendererModel(this);
		glow_radio.setRotationPoint(25.4F, -58.0F, -14.0F);
		setRotationAngle(glow_radio, -0.1047F, 0.5236F, -0.2094F);
		glow.addChild(glow_radio);
		glow_radio.cubeList.add(new ModelBox(glow_radio, 420, 340, -1.0F, -2.8F, -8.52F, 4, 4, 16, 0.0F, false));

		rotor = new RendererModel(this);
		rotor.setRotationPoint(0.0F, 28.0F, 0.0F);

		core = new RendererModel(this);
		core.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(core, 0.0F, 0.5236F, 0.0F);
		rotor.addChild(core);

		underbit_1 = new RendererModel(this);
		underbit_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		core.addChild(underbit_1);
		underbit_1.cubeList.add(new ModelBox(underbit_1, 80, 83, -20.4F, -9.0F, -12.0F, 40, 2, 24, 0.0F, false));

		underbit_2 = new RendererModel(this);
		underbit_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underbit_2, 0.0F, 1.0472F, 0.0F);
		core.addChild(underbit_2);
		underbit_2.cubeList.add(new ModelBox(underbit_2, 80, 83, -20.4F, -10.0F, -12.0F, 40, 4, 24, 0.0F, false));

		underbit_3 = new RendererModel(this);
		underbit_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underbit_3, 0.0F, 2.0944F, 0.0F);
		core.addChild(underbit_3);
		underbit_3.cubeList.add(new ModelBox(underbit_3, 80, 83, -20.4F, -10.4F, -12.0F, 40, 4, 24, 0.0F, false));

		spinner_slide_y = new RendererModel(this);
		spinner_slide_y.setRotationPoint(0.0F, -101.4F, 0.0F);
		rotor.addChild(spinner_slide_y);

		hourflip_rotate_x = new RendererModel(this);
		hourflip_rotate_x.setRotationPoint(0.0F, 0.15F, -0.25F);
		spinner_slide_y.addChild(hourflip_rotate_x);

		glass = new RendererModel(this);
		glass.setRotationPoint(0.0F, 101.25F, 0.25F);
		hourflip_rotate_x.addChild(glass);
		glass.cubeList.add(new ModelBox(glass, 296, 280, -6.0F, -119.0F, -6.0F, 12, 12, 12, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 295, 330, -4.0F, -109.0F, -4.0F, 8, 4, 8, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 295, 330, -3.0F, -107.0F, -3.0F, 6, 4, 6, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 295, 330, -2.0F, -104.0F, -2.0F, 4, 4, 4, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 295, 330, -3.0F, -100.0F, -3.0F, 6, 4, 6, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 295, 330, -4.0F, -98.0F, -4.0F, 8, 4, 8, 0.0F, false));
		glass.cubeList.add(new ModelBox(glass, 291, 362, -6.0F, -96.0F, -6.0F, 12, 12, 12, 0.0F, false));

		center = new RendererModel(this);
		center.setRotationPoint(0.0F, 101.25F, 0.25F);
		hourflip_rotate_x.addChild(center);
		center.cubeList.add(new ModelBox(center, 377, 108, 6.25F, -111.575F, -1.0F, 1, 9, 2, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 375, 86, 6.25F, -100.6F, -1.0F, 1, 9, 2, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 302, 78, 6.0F, -102.6F, -1.0F, 6, 2, 2, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 348, 78, -7.25F, -111.6F, -1.0F, 1, 9, 2, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 317, 77, -7.25F, -100.6F, -1.0F, 1, 9, 2, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 302, 78, -12.0F, -102.6F, -1.0F, 6, 2, 2, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 140, 420, 7.4F, -103.6F, -2.0F, 2, 4, 4, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 140, 420, -9.4F, -103.6F, -2.0F, 2, 4, 4, 0.0F, false));

		top_ring = new RendererModel(this);
		top_ring.setRotationPoint(0.0F, 101.25F, 0.25F);
		hourflip_rotate_x.addChild(top_ring);
		top_ring.cubeList.add(new ModelBox(top_ring, 303, 170, -7.0F, -118.0F, -7.0F, 14, 2, 14, 0.0F, false));
		top_ring.cubeList.add(new ModelBox(top_ring, 303, 84, 6.0F, -112.2F, -6.0F, 1, 2, 12, 0.0F, false));
		top_ring.cubeList.add(new ModelBox(top_ring, 318, 79, -7.0F, -112.2F, -6.0F, 1, 2, 12, 0.0F, false));
		top_ring.cubeList.add(new ModelBox(top_ring, 331, 81, -6.0F, -112.2F, 6.0F, 12, 2, 1, 0.0F, false));
		top_ring.cubeList.add(new ModelBox(top_ring, 306, 78, -6.0F, -112.2F, -7.0F, 12, 2, 1, 0.0F, false));

		under_ring = new RendererModel(this);
		under_ring.setRotationPoint(0.0F, 101.25F, 0.25F);
		hourflip_rotate_x.addChild(under_ring);
		under_ring.cubeList.add(new ModelBox(under_ring, 306, 176, -7.0F, -87.0F, -7.0F, 14, 2, 14, 0.0F, false));
		under_ring.cubeList.add(new ModelBox(under_ring, 351, 98, 6.0F, -92.8F, -6.0F, 1, 2, 12, 0.0F, false));
		under_ring.cubeList.add(new ModelBox(under_ring, 346, 78, -7.0F, -92.8F, -6.0F, 1, 2, 12, 0.0F, false));
		under_ring.cubeList.add(new ModelBox(under_ring, 359, 90, -6.0F, -92.8F, 6.0F, 12, 2, 1, 0.0F, false));
		under_ring.cubeList.add(new ModelBox(under_ring, 334, 89, -6.0F, -92.8F, -7.0F, 12, 2, 1, 0.0F, false));

		strut_1 = new RendererModel(this);
		strut_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor.addChild(strut_1);
		strut_1.cubeList.add(new ModelBox(strut_1, 306, 37, -9.5F, -104.0F, -4.0F, 2, 50, 2, 0.0F, false));
		strut_1.cubeList.add(new ModelBox(strut_1, 306, 37, -9.5F, -104.0F, 2.0F, 2, 50, 2, 0.0F, false));
		strut_1.cubeList.add(new ModelBox(strut_1, 304, 43, -9.5F, -106.0F, -4.0F, 2, 2, 8, 0.0F, false));

		strut_2 = new RendererModel(this);
		strut_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor.addChild(strut_2);
		strut_2.cubeList.add(new ModelBox(strut_2, 304, 44, 7.5F, -104.0F, -4.0F, 2, 50, 2, 0.0F, false));
		strut_2.cubeList.add(new ModelBox(strut_2, 304, 44, 7.5F, -104.0F, 2.0F, 2, 50, 2, 0.0F, false));
		strut_2.cubeList.add(new ModelBox(strut_2, 299, 46, 7.5F, -106.0F, -4.0F, 2, 2, 8, 0.0F, false));

		controls = new RendererModel(this);
		controls.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(controls, 0.0F, -0.5236F, 0.0F);

		contolset_a = new RendererModel(this);
		contolset_a.setRotationPoint(50.0F, -50.0F, 14.0F);
		setRotationAngle(contolset_a, 0.0F, 0.0F, -2.7925F);
		controls.addChild(contolset_a);

		door_switch = new RendererModel(this);
		door_switch.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(door_switch, 0.0F, 0.0F, 0.5236F);
		contolset_a.addChild(door_switch);
		door_switch.cubeList.add(new ModelBox(door_switch, 379, 60, -2.0F, -2.0F, 2.0F, 4, 4, 4, 0.0F, false));

		door_crank_rotate_y = new RendererModel(this);
		door_crank_rotate_y.setRotationPoint(0.0F, 6.1F, 4.0F);
		door_switch.addChild(door_crank_rotate_y);
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 149, 423, -1.0F, -7.1F, -1.0F, 2, 8, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, -1.0F, -2.1F, -3.0F, 2, 2, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, -1.0F, -2.1F, 1.0F, 2, 2, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, -3.0F, -2.1F, -1.0F, 2, 2, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, 1.0F, -2.1F, -1.0F, 2, 2, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, 3.0F, -2.1F, -3.0F, 2, 2, 6, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, -5.0F, -2.1F, -3.0F, 2, 2, 6, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, -3.0F, -2.1F, 3.0F, 6, 2, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 379, 60, -3.0F, -2.1F, -5.0F, 6, 2, 2, 0.0F, false));
		door_crank_rotate_y.cubeList.add(new ModelBox(door_crank_rotate_y, 86, 427, 3.0F, -0.1F, -1.0F, 2, 4, 2, 0.0F, false));

		sonic_port = new RendererModel(this);
		sonic_port.setRotationPoint(0.0F, 0.0F, 0.0F);
		contolset_a.addChild(sonic_port);
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 4.0F, -2.0F, -18.0F, 8, 4, 2, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 4.0F, -2.0F, -12.0F, 8, 4, 2, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 78, 441, 6.0F, -3.0F, -16.0F, 4, 4, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 10.0F, -2.0F, -16.0F, 2, 4, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 4.0F, -2.0F, -16.0F, 2, 4, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 2.0F, -3.0F, -16.0F, 2, 4, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 12.0F, -3.0F, -16.0F, 2, 4, 4, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 6.0F, -3.0F, -10.0F, 4, 4, 2, 0.0F, false));
		sonic_port.cubeList.add(new ModelBox(sonic_port, 379, 60, 6.0F, -3.0F, -20.0F, 4, 4, 2, 0.0F, false));

		meter_a1 = new RendererModel(this);
		meter_a1.setRotationPoint(0.0F, 16.3333F, -14.0F);
		contolset_a.addChild(meter_a1);
		meter_a1.cubeList.add(new ModelBox(meter_a1, 69, 431, 21.0F, -19.3333F, -4.0F, 8, 4, 8, 0.0F, false));

		needle_a1_rotate_y = new RendererModel(this);
		needle_a1_rotate_y.setRotationPoint(23.2F, -15.3333F, -0.1F);
		setRotationAngle(needle_a1_rotate_y, 0.0F, 0.5236F, 0.0F);
		meter_a1.addChild(needle_a1_rotate_y);
		needle_a1_rotate_y.cubeList.add(new ModelBox(needle_a1_rotate_y, 82, 433, -0.2F, -2.0F, -0.5F, 4, 4, 1, 0.0F, false));

		button_set_a1 = new RendererModel(this);
		button_set_a1.setRotationPoint(0.0F, 0.0F, 0.0F);
		contolset_a.addChild(button_set_a1);
		button_set_a1.cubeList.add(new ModelBox(button_set_a1, 70, 420, 17.0F, -3.0F, -19.0F, 2, 4, 2, 0.0F, false));
		button_set_a1.cubeList.add(new ModelBox(button_set_a1, 70, 420, 17.0F, -3.0F, -11.0F, 2, 4, 2, 0.0F, false));
		button_set_a1.cubeList.add(new ModelBox(button_set_a1, 70, 420, 17.0F, -3.0F, -15.0F, 2, 4, 2, 0.0F, false));

		baseplate_a1 = new RendererModel(this);
		baseplate_a1.setRotationPoint(0.0F, 0.0F, 0.0F);
		contolset_a.addChild(baseplate_a1);
		baseplate_a1.cubeList.add(new ModelBox(baseplate_a1, 379, 60, 30.0F, -4.0F, -20.0F, 2, 4, 12, 0.0F, false));
		baseplate_a1.cubeList.add(new ModelBox(baseplate_a1, 379, 60, 18.0F, -4.0F, -22.0F, 12, 4, 16, 0.0F, false));
		baseplate_a1.cubeList.add(new ModelBox(baseplate_a1, 379, 60, 16.0F, -4.0F, -20.0F, 2, 4, 12, 0.0F, false));

		baseplate_a2 = new RendererModel(this);
		baseplate_a2.setRotationPoint(24.0F, -2.0F, -14.0F);
		contolset_a.addChild(baseplate_a2);
		baseplate_a2.cubeList.add(new ModelBox(baseplate_a2, 379, 60, -13.0F, 0.0F, 8.0F, 6, 4, 6, 0.0F, false));

		baseplate_a3 = new RendererModel(this);
		baseplate_a3.setRotationPoint(24.0F, -2.0F, -14.0F);
		contolset_a.addChild(baseplate_a3);
		baseplate_a3.cubeList.add(new ModelBox(baseplate_a3, 379, 60, -13.0F, 0.0F, -14.0F, 6, 4, 6, 0.0F, false));
		baseplate_a3.cubeList.add(new ModelBox(baseplate_a3, 379, 60, -25.0F, -1.0F, -12.0F, 12, 4, 2, 0.0F, false));
		baseplate_a3.cubeList.add(new ModelBox(baseplate_a3, 379, 60, -25.0F, -1.0F, -32.0F, 2, 4, 20, 0.0F, false));
		baseplate_a3.cubeList.add(new ModelBox(baseplate_a3, 276, 93, -26.0F, -4.0F, -13.0F, 4, 8, 4, 0.0F, false));

		controlset_b = new RendererModel(this);
		controlset_b.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controlset_b, 0.0F, 1.0472F, 0.0F);
		controls.addChild(controlset_b);

		throttle = new RendererModel(this);
		throttle.setRotationPoint(52.0F, -62.0F, 0.0F);
		setRotationAngle(throttle, 0.0F, 0.0F, 0.1745F);
		controlset_b.addChild(throttle);
		throttle.cubeList.add(new ModelBox(throttle, 379, 60, -10.0F, 10.0F, 10.0F, 16, 4, 8, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 139, 427, -8.0F, 6.0F, 11.0F, 13, 8, 6, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 489, 415, -5.0F, 5.0F, 13.0F, 2, 2, 2, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 400, 411, 1.0F, 5.0F, 13.0F, 2, 2, 2, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 450, 422, -2.0F, 5.0F, 13.0F, 2, 2, 2, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 379, 60, -0.8F, 11.6F, 18.0F, 2, 4, 8, 0.0F, false));

		leaver_b1_rotate_z = new RendererModel(this);
		leaver_b1_rotate_z.setRotationPoint(-2.0F, 8.0F, 14.0F);
		setRotationAngle(leaver_b1_rotate_z, 0.0F, 0.0F, 0.5236F);
		throttle.addChild(leaver_b1_rotate_z);
		leaver_b1_rotate_z.cubeList.add(new ModelBox(leaver_b1_rotate_z, 379, 60, -1.0F, -1.0F, -5.0F, 2, 2, 10, 0.0F, false));
		leaver_b1_rotate_z.cubeList.add(new ModelBox(leaver_b1_rotate_z, 379, 60, 0.0F, -6.0F, 3.0F, 2, 8, 1, 0.0F, false));
		leaver_b1_rotate_z.cubeList.add(new ModelBox(leaver_b1_rotate_z, 145, 426, 0.0F, -11.0F, -1.0F, 2, 2, 8, 0.0F, false));
		leaver_b1_rotate_z.cubeList.add(new ModelBox(leaver_b1_rotate_z, 379, 60, 0.0F, -10.6F, 7.0F, 2, 4, 1, 0.0F, false));

		leaver_b1_jig = new RendererModel(this);
		leaver_b1_jig.setRotationPoint(1.0F, -6.0F, 4.5F);
		setRotationAngle(leaver_b1_jig, -1.1345F, 0.0F, 0.0F);
		leaver_b1_rotate_z.addChild(leaver_b1_jig);
		leaver_b1_jig.cubeList.add(new ModelBox(leaver_b1_jig, 379, 60, -1.0F, -3.0F, -0.5F, 2, 4, 1, 0.0F, false));

		x_switch = new RendererModel(this);
		x_switch.setRotationPoint(31.0F, -58.0F, -5.0F);
		setRotationAngle(x_switch, 0.0F, 0.0F, 0.7854F);
		controlset_b.addChild(x_switch);
		x_switch.cubeList.add(new ModelBox(x_switch, 87, 436, -1.0F, -2.0F, -1.0F, 2, 2, 2, 0.0F, false));
		x_switch.cubeList.add(new ModelBox(x_switch, 379, 60, -2.0F, -1.0F, -2.0F, 4, 4, 4, 0.0F, false));

		y_switch = new RendererModel(this);
		y_switch.setRotationPoint(31.0F, -58.0F, 0.0F);
		setRotationAngle(y_switch, 0.0F, 0.0F, 0.7854F);
		controlset_b.addChild(y_switch);
		y_switch.cubeList.add(new ModelBox(y_switch, 86, 436, -1.0F, -2.0F, -1.0F, 2, 2, 2, 0.0F, false));
		y_switch.cubeList.add(new ModelBox(y_switch, 363, 59, -2.0F, -1.0F, -2.0F, 4, 4, 4, 0.0F, false));

		z_switch = new RendererModel(this);
		z_switch.setRotationPoint(31.0F, -58.0F, 5.0F);
		setRotationAngle(z_switch, 0.0F, 0.0F, 0.7854F);
		controlset_b.addChild(z_switch);
		z_switch.cubeList.add(new ModelBox(z_switch, 95, 437, -1.0F, -2.0F, -1.0F, 2, 2, 2, 0.0F, false));
		z_switch.cubeList.add(new ModelBox(z_switch, 379, 60, -2.0F, -1.0F, -2.0F, 4, 4, 4, 0.0F, false));

		cordselect_inc = new RendererModel(this);
		cordselect_inc.setRotationPoint(34.0F, -58.0F, -2.0F);
		setRotationAngle(cordselect_inc, 0.0F, 0.0F, 0.2618F);
		controlset_b.addChild(cordselect_inc);
		cordselect_inc.cubeList.add(new ModelBox(cordselect_inc, 83, 431, 11.0F, 2.0F, -12.0F, 8, 4, 1, 0.0F, false));
		cordselect_inc.cubeList.add(new ModelBox(cordselect_inc, 379, 60, 11.0F, 2.0F, -14.0F, 8, 4, 2, 0.0F, false));
		cordselect_inc.cubeList.add(new ModelBox(cordselect_inc, 379, 60, 11.0F, 2.0F, -11.0F, 8, 4, 2, 0.0F, false));
		cordselect_inc.cubeList.add(new ModelBox(cordselect_inc, 379, 60, 19.0F, 2.0F, -14.0F, 2, 4, 5, 0.0F, false));
		cordselect_inc.cubeList.add(new ModelBox(cordselect_inc, 379, 60, 9.0F, 2.0F, -14.0F, 2, 4, 5, 0.0F, false));

		cord_slider_slide_x = new RendererModel(this);
		cord_slider_slide_x.setRotationPoint(0.0F, 0.0F, 0.0F);
		cordselect_inc.addChild(cord_slider_slide_x);
		cord_slider_slide_x.cubeList.add(new ModelBox(cord_slider_slide_x, 155, 438, 18.0F, 1.0F, -13.6F, 1, 1, 4, 0.0F, false));

		nixietube_b1 = new RendererModel(this);
		nixietube_b1.setRotationPoint(34.0F, -58.0F, -2.0F);
		controlset_b.addChild(nixietube_b1);
		nixietube_b1.cubeList.add(new ModelBox(nixietube_b1, 77, 428, -13.0F, -3.0F, 5.0F, 2, 2, 2, 0.0F, false));

		nixietube_b2 = new RendererModel(this);
		nixietube_b2.setRotationPoint(34.0F, -58.0F, -2.0F);
		controlset_b.addChild(nixietube_b2);
		nixietube_b2.cubeList.add(new ModelBox(nixietube_b2, 78, 435, -13.0F, -3.0F, -4.0F, 2, 2, 2, 0.0F, false));

		baseplate_b1 = new RendererModel(this);
		baseplate_b1.setRotationPoint(34.0F, -58.0F, -2.0F);
		setRotationAngle(baseplate_b1, 0.0F, 0.0F, 0.2618F);
		controlset_b.addChild(baseplate_b1);
		baseplate_b1.cubeList.add(new ModelBox(baseplate_b1, 379, 60, -5.0F, 1.0F, -8.0F, 6, 4, 2, 0.0F, false));
		baseplate_b1.cubeList.add(new ModelBox(baseplate_b1, 379, 60, -5.0F, 1.0F, 10.0F, 6, 4, 2, 0.0F, false));
		baseplate_b1.cubeList.add(new ModelBox(baseplate_b1, 379, 60, -6.0F, 1.0F, -6.0F, 8, 4, 16, 0.0F, false));

		baseplate_b2 = new RendererModel(this);
		baseplate_b2.setRotationPoint(34.0F, -58.0F, -2.0F);
		setRotationAngle(baseplate_b2, 0.0F, 0.0F, 0.2618F);
		controlset_b.addChild(baseplate_b2);
		baseplate_b2.cubeList.add(new ModelBox(baseplate_b2, 379, 60, -12.6F, 1.0F, -1.0F, 1, 4, 6, 0.0F, false));
		baseplate_b2.cubeList.add(new ModelBox(baseplate_b2, 379, 60, -14.0F, 1.0F, -5.0F, 4, 4, 4, 0.0F, false));
		baseplate_b2.cubeList.add(new ModelBox(baseplate_b2, 379, 60, -14.0F, 1.0F, 4.0F, 4, 4, 4, 0.0F, false));

		baseplate_b3 = new RendererModel(this);
		baseplate_b3.setRotationPoint(34.0F, -58.0F, -2.0F);
		setRotationAngle(baseplate_b3, 0.0F, 0.0F, 0.2618F);
		controlset_b.addChild(baseplate_b3);
		baseplate_b3.cubeList.add(new ModelBox(baseplate_b3, 379, 60, 7.0F, 2.0F, -3.0F, 5, 4, 10, 0.0F, false));
		baseplate_b3.cubeList.add(new ModelBox(baseplate_b3, 379, 60, 7.0F, 1.0F, -4.0F, 5, 4, 1, 0.0F, false));
		baseplate_b3.cubeList.add(new ModelBox(baseplate_b3, 379, 60, 7.0F, 1.0F, 7.0F, 5, 4, 1, 0.0F, false));
		baseplate_b3.cubeList.add(new ModelBox(baseplate_b3, 379, 60, 8.0F, 2.0F, 8.0F, 2, 4, 1, 0.0F, false));
		baseplate_b3.cubeList.add(new ModelBox(baseplate_b3, 379, 60, 8.0F, 2.0F, -5.0F, 2, 4, 1, 0.0F, false));

		readout_b1 = new RendererModel(this);
		readout_b1.setRotationPoint(9.0F, 2.0F, 2.0F);
		setRotationAngle(readout_b1, 0.0F, 0.0F, 0.6109F);
		baseplate_b3.addChild(readout_b1);
		readout_b1.cubeList.add(new ModelBox(readout_b1, 73, 435, -1.0F, -0.8F, -5.0F, 4, 4, 10, 0.0F, false));
		readout_b1.cubeList.add(new ModelBox(readout_b1, 214, 438, -0.6F, -1.0F, -5.0F, 2, 2, 4, 0.0F, false));
		readout_b1.cubeList.add(new ModelBox(readout_b1, 214, 438, -0.6F, -1.0F, 1.0F, 2, 2, 4, 0.0F, false));
		readout_b1.cubeList.add(new ModelBox(readout_b1, 214, 438, -0.6F, -1.0F, -0.52F, 2, 2, 1, 0.0F, false));

		baseplate_b4 = new RendererModel(this);
		baseplate_b4.setRotationPoint(40.0F, -56.0F, -2.0F);
		setRotationAngle(baseplate_b4, 0.0F, 0.0F, 0.2618F);
		controlset_b.addChild(baseplate_b4);
		baseplate_b4.cubeList.add(new ModelBox(baseplate_b4, 379, 60, 7.0F, 2.0F, -3.0F, 5, 4, 10, 0.0F, false));
		baseplate_b4.cubeList.add(new ModelBox(baseplate_b4, 379, 60, 7.0F, 1.0F, -4.0F, 5, 4, 1, 0.0F, false));
		baseplate_b4.cubeList.add(new ModelBox(baseplate_b4, 379, 60, 7.0F, 1.0F, 7.0F, 5, 4, 1, 0.0F, false));
		baseplate_b4.cubeList.add(new ModelBox(baseplate_b4, 379, 60, 8.0F, 2.0F, 8.0F, 2, 4, 1, 0.0F, false));
		baseplate_b4.cubeList.add(new ModelBox(baseplate_b4, 379, 60, 8.0F, 2.0F, -5.0F, 2, 4, 1, 0.0F, false));

		readout_b2 = new RendererModel(this);
		readout_b2.setRotationPoint(9.0F, 2.0F, 2.0F);
		setRotationAngle(readout_b2, 0.0F, 0.0F, 0.6109F);
		baseplate_b4.addChild(readout_b2);
		readout_b2.cubeList.add(new ModelBox(readout_b2, 75, 440, -1.0F, -0.8F, -5.0F, 4, 4, 10, 0.0F, false));
		readout_b2.cubeList.add(new ModelBox(readout_b2, 208, 435, -0.6F, -1.0F, -5.0F, 2, 2, 4, 0.0F, false));
		readout_b2.cubeList.add(new ModelBox(readout_b2, 208, 435, -0.6F, -1.0F, 1.0F, 2, 2, 4, 0.0F, false));
		readout_b2.cubeList.add(new ModelBox(readout_b2, 208, 435, -0.6F, -1.0F, -0.52F, 2, 2, 1, 0.0F, false));

		controlset_c = new RendererModel(this);
		controlset_c.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controlset_c, 0.0F, 2.0944F, 0.0F);
		controls.addChild(controlset_c);

		communications = new RendererModel(this);
		communications.setRotationPoint(0.0F, 0.0F, 0.0F);
		controlset_c.addChild(communications);

		radio = new RendererModel(this);
		radio.setRotationPoint(25.0F, -64.0F, 0.0F);
		setRotationAngle(radio, 0.0F, 0.0F, -0.2618F);
		communications.addChild(radio);

		radio_dial = new RendererModel(this);
		radio_dial.setRotationPoint(13.0F, 15.0F, 13.0F);
		setRotationAngle(radio_dial, 0.0F, 0.0F, 0.6109F);
		radio.addChild(radio_dial);
		radio_dial.cubeList.add(new ModelBox(radio_dial, 78, 430, -7.0F, -3.2F, -14.0F, 2, 4, 2, 0.0F, false));
		radio_dial.cubeList.add(new ModelBox(radio_dial, 78, 430, -8.0F, -2.2F, -15.0F, 4, 4, 4, 0.0F, false));

		radio_dial_sub = new RendererModel(this);
		radio_dial_sub.setRotationPoint(-6.0F, -3.0F, -13.0F);
		setRotationAngle(radio_dial_sub, 0.0F, -0.7854F, 0.0F);
		radio_dial.addChild(radio_dial_sub);
		radio_dial_sub.cubeList.add(new ModelBox(radio_dial_sub, 78, 430, -2.0F, 1.0F, -2.0F, 4, 4, 4, 0.0F, false));

		radio_needle = new RendererModel(this);
		radio_needle.setRotationPoint(0.0F, 0.0F, 0.0F);
		radio.addChild(radio_needle);
		radio_needle.cubeList.add(new ModelBox(radio_needle, 391, 411, -2.0F, 5.0F, -7.0F, 8, 3, 1, 0.0F, false));

		speakers = new RendererModel(this);
		speakers.setRotationPoint(0.0F, 0.0F, 0.0F);
		radio.addChild(speakers);
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, -4.0F, -2.0F, 8, 6, 4, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, -7.0F, -2.0F, 8, 2, 4, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, 0.0F, -5.0F, 8, 2, 2, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, -7.0F, -5.0F, 8, 2, 2, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, -7.0F, 3.0F, 8, 2, 2, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, -4.0F, 3.0F, 8, 3, 2, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, 0.0F, 3.0F, 8, 2, 2, 0.0F, false));
		speakers.cubeList.add(new ModelBox(speakers, 77, 431, -2.6F, -4.0F, -5.0F, 8, 3, 2, 0.0F, false));

		radio_body = new RendererModel(this);
		radio_body.setRotationPoint(0.0F, 0.0F, 0.0F);
		radio.addChild(radio_body);
		radio_body.cubeList.add(new ModelBox(radio_body, 161, 329, -5.0F, -3.0F, -10.0F, 10, 12, 20, 0.0F, false));
		radio_body.cubeList.add(new ModelBox(radio_body, 161, 329, -5.0F, -7.0F, -8.0F, 10, 4, 16, 0.0F, false));
		radio_body.cubeList.add(new ModelBox(radio_body, 161, 329, -5.0F, -9.0F, -6.0F, 10, 2, 12, 0.0F, false));
		radio_body.cubeList.add(new ModelBox(radio_body, 71, 430, -2.2F, 6.0F, -7.6F, 8, 1, 15, 0.0F, false));

		radio_dial_base = new RendererModel(this);
		radio_dial_base.setRotationPoint(13.0F, 15.0F, 3.0F);
		setRotationAngle(radio_dial_base, 0.0F, 0.0F, 0.6109F);
		radio.addChild(radio_dial_base);
		radio_dial_base.cubeList.add(new ModelBox(radio_dial_base, 379, 60, -9.0F, -1.0F, -6.0F, 6, 4, 6, 0.0F, false));

		refuler = new RendererModel(this);
		refuler.setRotationPoint(47.0F, -55.0F, -19.0F);
		controlset_c.addChild(refuler);
		refuler.cubeList.add(new ModelBox(refuler, 390, 410, 6.0F, 2.0F, -3.0F, 2, 4, 10, 0.0F, false));
		refuler.cubeList.add(new ModelBox(refuler, 379, 60, 5.0F, 4.0F, -4.0F, 4, 4, 12, 0.0F, false));

		baseplate_c1 = new RendererModel(this);
		baseplate_c1.setRotationPoint(38.0F, -52.5F, -10.5F);
		setRotationAngle(baseplate_c1, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(baseplate_c1);
		baseplate_c1.cubeList.add(new ModelBox(baseplate_c1, 379, 60, -3.0F, -3.5F, -3.5F, 6, 4, 6, 0.0F, false));
		baseplate_c1.cubeList.add(new ModelBox(baseplate_c1, 84, 431, -2.6F, -4.5F, -3.1F, 5, 1, 5, 0.0F, false));

		baseplate_c2 = new RendererModel(this);
		baseplate_c2.setRotationPoint(38.0F, -52.5F, 11.5F);
		setRotationAngle(baseplate_c2, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(baseplate_c2);
		baseplate_c2.cubeList.add(new ModelBox(baseplate_c2, 379, 60, -3.0F, -3.5F, -3.5F, 6, 4, 6, 0.0F, false));
		baseplate_c2.cubeList.add(new ModelBox(baseplate_c2, 85, 440, -2.6F, -4.5F, -3.1F, 5, 1, 5, 0.0F, false));

		slider_c1 = new RendererModel(this);
		slider_c1.setRotationPoint(50.0F, -52.5F, -2.5F);
		setRotationAngle(slider_c1, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(slider_c1);

		sliderknob_c1_slide_x = new RendererModel(this);
		sliderknob_c1_slide_x.setRotationPoint(0.0F, 0.0F, 0.0F);
		slider_c1.addChild(sliderknob_c1_slide_x);
		sliderknob_c1_slide_x.cubeList.add(new ModelBox(sliderknob_c1_slide_x, 214, 105, 1.0F, -0.5F, -2.7F, 2, 2, 1, 0.0F, false));
		sliderknob_c1_slide_x.cubeList.add(new ModelBox(sliderknob_c1_slide_x, 70, 420, 1.4F, -0.9F, -3.7F, 1, 1, 3, 0.0F, false));

		slider_track_c1 = new RendererModel(this);
		slider_track_c1.setRotationPoint(0.0F, 0.0F, 0.0F);
		slider_c1.addChild(slider_track_c1);
		slider_track_c1.cubeList.add(new ModelBox(slider_track_c1, 379, 60, -7.0F, 0.5F, -3.7F, 1, 4, 3, 0.0F, false));
		slider_track_c1.cubeList.add(new ModelBox(slider_track_c1, 379, 60, -6.0F, 0.5F, -1.7F, 9, 4, 1, 0.0F, false));
		slider_track_c1.cubeList.add(new ModelBox(slider_track_c1, 379, 60, -6.0F, 0.5F, -3.7F, 9, 4, 1, 0.0F, false));
		slider_track_c1.cubeList.add(new ModelBox(slider_track_c1, 78, 431, -6.0F, 1.5F, -2.7F, 9, 4, 1, 0.0F, false));
		slider_track_c1.cubeList.add(new ModelBox(slider_track_c1, 379, 60, 3.0F, 0.5F, -3.7F, 1, 4, 3, 0.0F, false));

		slider_c2 = new RendererModel(this);
		slider_c2.setRotationPoint(50.0F, -52.5F, -2.5F);
		setRotationAngle(slider_c2, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(slider_c2);

		sliderknob_c2_slide_x = new RendererModel(this);
		sliderknob_c2_slide_x.setRotationPoint(0.0F, 0.0F, 0.0F);
		slider_c2.addChild(sliderknob_c2_slide_x);
		sliderknob_c2_slide_x.cubeList.add(new ModelBox(sliderknob_c2_slide_x, 203, 98, 1.0F, -0.5F, 2.1F, 2, 2, 1, 0.0F, false));
		sliderknob_c2_slide_x.cubeList.add(new ModelBox(sliderknob_c2_slide_x, 70, 420, 1.4F, -0.9F, 1.1F, 1, 1, 3, 0.0F, false));

		slider_track_c2 = new RendererModel(this);
		slider_track_c2.setRotationPoint(0.0F, 0.0F, 0.0F);
		slider_c2.addChild(slider_track_c2);
		slider_track_c2.cubeList.add(new ModelBox(slider_track_c2, 379, 60, -7.0F, 0.5F, 1.1F, 1, 4, 3, 0.0F, false));
		slider_track_c2.cubeList.add(new ModelBox(slider_track_c2, 379, 60, -6.0F, 0.5F, 3.1F, 9, 4, 1, 0.0F, false));
		slider_track_c2.cubeList.add(new ModelBox(slider_track_c2, 379, 60, -6.0F, 0.5F, 1.1F, 9, 4, 1, 0.0F, false));
		slider_track_c2.cubeList.add(new ModelBox(slider_track_c2, 79, 431, -6.0F, 1.5F, 2.1F, 9, 4, 1, 0.0F, false));
		slider_track_c2.cubeList.add(new ModelBox(slider_track_c2, 379, 60, 3.0F, 0.5F, 1.1F, 1, 4, 3, 0.0F, false));

		slider_c3 = new RendererModel(this);
		slider_c3.setRotationPoint(50.0F, -52.5F, -2.5F);
		setRotationAngle(slider_c3, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(slider_c3);

		sliderknob_c3_slide_x = new RendererModel(this);
		sliderknob_c3_slide_x.setRotationPoint(0.0F, 0.0F, 0.0F);
		slider_c3.addChild(sliderknob_c3_slide_x);
		sliderknob_c3_slide_x.cubeList.add(new ModelBox(sliderknob_c3_slide_x, 208, 106, 1.0F, -0.5F, 6.9F, 2, 2, 1, 0.0F, false));
		sliderknob_c3_slide_x.cubeList.add(new ModelBox(sliderknob_c3_slide_x, 70, 420, 1.4F, -0.9F, 5.9F, 1, 1, 3, 0.0F, false));

		slider_track_c3 = new RendererModel(this);
		slider_track_c3.setRotationPoint(0.0F, 0.0F, 0.0F);
		slider_c3.addChild(slider_track_c3);
		slider_track_c3.cubeList.add(new ModelBox(slider_track_c3, 379, 60, -7.0F, 0.5F, 5.9F, 1, 4, 3, 0.0F, false));
		slider_track_c3.cubeList.add(new ModelBox(slider_track_c3, 379, 60, -6.0F, 0.5F, 7.9F, 9, 4, 1, 0.0F, false));
		slider_track_c3.cubeList.add(new ModelBox(slider_track_c3, 379, 60, -6.0F, 0.5F, 5.9F, 9, 4, 1, 0.0F, false));
		slider_track_c3.cubeList.add(new ModelBox(slider_track_c3, 73, 443, -6.0F, 1.5F, 6.9F, 9, 4, 1, 0.0F, false));
		slider_track_c3.cubeList.add(new ModelBox(slider_track_c3, 379, 60, 3.0F, 0.5F, 5.9F, 1, 4, 3, 0.0F, false));

		switch_c1 = new RendererModel(this);
		switch_c1.setRotationPoint(50.0F, -52.5F, -2.5F);
		setRotationAngle(switch_c1, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(switch_c1);
		switch_c1.cubeList.add(new ModelBox(switch_c1, 379, 60, 0.0F, 0.5F, 11.5F, 3, 4, 3, 0.0F, false));

		toggle_c1 = new RendererModel(this);
		toggle_c1.setRotationPoint(1.7F, 0.7F, 12.8F);
		setRotationAngle(toggle_c1, 0.0F, 0.0F, -0.6109F);
		switch_c1.addChild(toggle_c1);
		toggle_c1.cubeList.add(new ModelBox(toggle_c1, 400, 300, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_c1.cubeList.add(new ModelBox(toggle_c1, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_c2 = new RendererModel(this);
		switch_c2.setRotationPoint(50.0F, -52.5F, 1.5F);
		setRotationAngle(switch_c2, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(switch_c2);
		switch_c2.cubeList.add(new ModelBox(switch_c2, 379, 60, 0.0F, 0.5F, 11.5F, 3, 4, 3, 0.0F, false));

		toggle_c2 = new RendererModel(this);
		toggle_c2.setRotationPoint(1.7F, 0.7F, 12.8F);
		setRotationAngle(toggle_c2, 0.0F, 0.0F, -0.6109F);
		switch_c2.addChild(toggle_c2);
		toggle_c2.cubeList.add(new ModelBox(toggle_c2, 400, 300, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_c2.cubeList.add(new ModelBox(toggle_c2, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_c3 = new RendererModel(this);
		switch_c3.setRotationPoint(50.0F, -52.5F, 5.5F);
		setRotationAngle(switch_c3, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(switch_c3);
		switch_c3.cubeList.add(new ModelBox(switch_c3, 379, 60, 0.0F, 0.5F, 11.5F, 3, 4, 3, 0.0F, false));

		toggle_c3 = new RendererModel(this);
		toggle_c3.setRotationPoint(1.7F, 0.7F, 12.8F);
		setRotationAngle(toggle_c3, 0.0F, 0.0F, -0.6109F);
		switch_c3.addChild(toggle_c3);
		toggle_c3.cubeList.add(new ModelBox(toggle_c3, 400, 300, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_c3.cubeList.add(new ModelBox(toggle_c3, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_c4 = new RendererModel(this);
		switch_c4.setRotationPoint(50.0F, -52.5F, 9.5F);
		setRotationAngle(switch_c4, 0.0F, 0.0F, 0.3491F);
		controlset_c.addChild(switch_c4);
		switch_c4.cubeList.add(new ModelBox(switch_c4, 379, 60, 0.0F, 0.5F, 11.5F, 3, 4, 3, 0.0F, false));

		toggle_c4 = new RendererModel(this);
		toggle_c4.setRotationPoint(1.7F, 0.7F, 12.8F);
		setRotationAngle(toggle_c4, 0.0F, 0.0F, -0.6109F);
		switch_c4.addChild(toggle_c4);
		toggle_c4.cubeList.add(new ModelBox(toggle_c4, 400, 300, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_c4.cubeList.add(new ModelBox(toggle_c4, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		controlset_d = new RendererModel(this);
		controlset_d.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controlset_d);

		waypoint_selector = new RendererModel(this);
		waypoint_selector.setRotationPoint(-38.0F, -53.0F, 0.0F);
		setRotationAngle(waypoint_selector, 0.0F, 0.0262F, -0.2618F);
		controlset_d.addChild(waypoint_selector);

		type_arm_rotate_x = new RendererModel(this);
		type_arm_rotate_x.setRotationPoint(-15.0F, -10.0F, 12.0F);
		setRotationAngle(type_arm_rotate_x, 0.6109F, 0.0F, 1.6581F);
		waypoint_selector.addChild(type_arm_rotate_x);
		type_arm_rotate_x.cubeList.add(new ModelBox(type_arm_rotate_x, 379, 60, -0.2F, -0.4F, -0.8F, 1, 12, 1, 0.0F, false));
		type_arm_rotate_x.cubeList.add(new ModelBox(type_arm_rotate_x, 88, 439, -0.8F, 8.6F, -0.4F, 2, 4, 1, 0.0F, false));

		keys = new RendererModel(this);
		keys.setRotationPoint(-19.6225F, -6.0F, -0.1575F);
		setRotationAngle(keys, 0.0F, 0.0F, -0.2618F);
		waypoint_selector.addChild(keys);
		keys.cubeList.add(new ModelBox(keys, 75, 429, -11.3775F, 1.0F, -7.8425F, 2, 2, 16, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -11.3775F, 1.0F, -10.4425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, -9.2425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, -6.8425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, -2.0425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, -4.4425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, 2.7575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, 0.3575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, 5.1575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, 9.9575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.3775F, 1.0F, 7.5575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -8.7775F, 1.0F, -12.4425F, 5, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, -10.0425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, -7.6425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, -2.8425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, -5.2425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, 1.9575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, -0.4425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, 4.3575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, 9.1575F, 2, 2, 3, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -5.7775F, 1.0F, 6.7575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, -11.6425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, -9.2425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, -6.8425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, -2.0425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, -4.4425F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, 2.7575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, 0.3575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, 5.1575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, 9.9575F, 2, 2, 2, 0.0F, false));
		keys.cubeList.add(new ModelBox(keys, 75, 429, -3.3775F, 1.0F, 7.5575F, 2, 2, 2, 0.0F, false));

		type_body = new RendererModel(this);
		type_body.setRotationPoint(0.0F, 0.0F, 0.0F);
		waypoint_selector.addChild(type_body);
		type_body.cubeList.add(new ModelBox(type_body, 154, 315, -32.0F, -2.0F, -14.0F, 12, 2, 28, 0.0F, false));
		type_body.cubeList.add(new ModelBox(type_body, 154, 315, -20.0F, -8.0F, -14.0F, 2, 8, 28, 0.0F, false));
		type_body.cubeList.add(new ModelBox(type_body, 154, 315, -18.0F, -8.0F, 10.0F, 8, 8, 4, 0.0F, false));
		type_body.cubeList.add(new ModelBox(type_body, 154, 315, -18.0F, -8.0F, -14.0F, 8, 8, 4, 0.0F, false));
		type_body.cubeList.add(new ModelBox(type_body, 154, 315, -12.0F, -8.0F, -10.2F, 2, 8, 20, 0.0F, false));

		tumbler = new RendererModel(this);
		tumbler.setRotationPoint(-7.95F, -6.95F, 0.0F);
		setRotationAngle(tumbler, 0.0F, 0.0F, -0.7854F);
		waypoint_selector.addChild(tumbler);
		tumbler.cubeList.add(new ModelBox(tumbler, 379, 60, -5.05F, -9.05F, 10.0F, 4, 4, 2, 0.0F, false));
		tumbler.cubeList.add(new ModelBox(tumbler, 198, 433, -6.05F, -8.05F, -10.0F, 4, 4, 20, 0.0F, false));
		tumbler.cubeList.add(new ModelBox(tumbler, 379, 60, -6.05F, -7.05F, -11.0F, 3, 3, 22, 0.0F, false));
		tumbler.cubeList.add(new ModelBox(tumbler, 194, 428, -2.05F, -4.05F, -10.0F, 7, 0, 20, 0.0F, false));

		dimention_selector = new RendererModel(this);
		dimention_selector.setRotationPoint(-36.0F, -54.0F, 0.0F);
		setRotationAngle(dimention_selector, 0.0F, 0.0F, -0.3491F);
		controlset_d.addChild(dimention_selector);
		dimention_selector.cubeList.add(new ModelBox(dimention_selector, 379, 60, 11.0F, -2.0F, -8.0F, 10, 4, 16, 0.0F, false));

		tilt_d1 = new RendererModel(this);
		tilt_d1.setRotationPoint(14.5F, -4.0F, -0.1F);
		setRotationAngle(tilt_d1, 0.0F, 0.0F, -0.6109F);
		dimention_selector.addChild(tilt_d1);
		tilt_d1.cubeList.add(new ModelBox(tilt_d1, 65, 428, -5.5F, 1.0F, -7.5F, 11, 5, 15, 0.0F, false));
		tilt_d1.cubeList.add(new ModelBox(tilt_d1, 430, 360, -0.5F, -0.2F, -5.7F, 5, 3, 11, 0.0F, false));

		baseplate_d1 = new RendererModel(this);
		baseplate_d1.setRotationPoint(-36.0F, -54.0F, 0.0F);
		setRotationAngle(baseplate_d1, 0.0F, 0.0F, -0.3491F);
		controlset_d.addChild(baseplate_d1);
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 3.6F, -2.0F, 10.0F, 2, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 0.6F, -2.0F, 8.0F, 8, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 0.6F, -2.0F, 6.0F, 2, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 0.6F, -2.0F, -8.0F, 2, 4, 16, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 6.6F, -2.0F, -8.0F, 2, 4, 16, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 0.6F, -2.0F, -10.0F, 8, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, 3.6F, -2.0F, -12.0F, 2, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 272, 282, 2.6F, -1.2F, -8.0F, 4, 4, 16, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, -15.4F, -2.0F, 1.0F, 16, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, -7.4F, -2.0F, -19.0F, 2, 4, 18, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, -11.4F, -2.0F, -19.0F, 4, 4, 2, 0.0F, false));
		baseplate_d1.cubeList.add(new ModelBox(baseplate_d1, 379, 60, -5.4F, -2.0F, -3.0F, 6, 4, 2, 0.0F, false));

		baseplate_d2 = new RendererModel(this);
		baseplate_d2.setRotationPoint(-36.0F, -54.0F, 0.0F);
		setRotationAngle(baseplate_d2, 0.0F, 0.0F, -0.3491F);
		controlset_d.addChild(baseplate_d2);
		baseplate_d2.cubeList.add(new ModelBox(baseplate_d2, 379, 60, -19.4F, -2.0F, -22.0F, 8, 4, 6, 0.0F, false));
		baseplate_d2.cubeList.add(new ModelBox(baseplate_d2, 146, 429, -18.4F, -4.0F, -21.0F, 6, 4, 4, 0.0F, false));

		baseplate_d3 = new RendererModel(this);
		baseplate_d3.setRotationPoint(-36.0F, -54.0F, 0.0F);
		setRotationAngle(baseplate_d3, 0.0F, 0.0F, -0.3491F);
		controlset_d.addChild(baseplate_d3);
		baseplate_d3.cubeList.add(new ModelBox(baseplate_d3, 379, 60, -19.4F, -2.0F, 16.0F, 8, 4, 6, 0.0F, false));
		baseplate_d3.cubeList.add(new ModelBox(baseplate_d3, 152, 433, -18.4F, -4.0F, 17.0F, 6, 4, 4, 0.0F, false));

		controlset_e = new RendererModel(this);
		controlset_e.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controlset_e, 0.0F, 1.0472F, 0.0F);
		controls.addChild(controlset_e);

		telepathic_circuits = new RendererModel(this);
		telepathic_circuits.setRotationPoint(-36.0F, -54.0F, 0.0F);
		setRotationAngle(telepathic_circuits, 0.0F, 0.0F, -0.3491F);
		controlset_e.addChild(telepathic_circuits);

		talking_board = new RendererModel(this);
		talking_board.setRotationPoint(0.0F, 0.0F, 0.0F);
		telepathic_circuits.addChild(talking_board);
		talking_board.cubeList.add(new ModelBox(talking_board, 37, 83, -15.6F, -2.0F, -14.0F, 19, 4, 27, 0.0F, false));
		talking_board.cubeList.add(new ModelBox(talking_board, 379, 60, 3.0F, -2.0F, 11.8F, 3, 2, 1, 0.0F, false));
		talking_board.cubeList.add(new ModelBox(talking_board, 379, 60, 6.0F, -2.0F, -0.2F, 1, 2, 13, 0.0F, false));
		talking_board.cubeList.add(new ModelBox(talking_board, 379, 60, 7.0F, -2.0F, -0.4F, 8, 2, 1, 0.0F, false));
		talking_board.cubeList.add(new ModelBox(talking_board, 281, 103, 5.6F, -2.5F, -0.8F, 2, 2, 2, 0.0F, false));
		talking_board.cubeList.add(new ModelBox(talking_board, 379, 60, 13.0F, -2.0F, -4.8F, 1, 2, 9, 0.0F, false));
		talking_board.cubeList.add(new ModelBox(talking_board, 280, 109, 12.6F, -2.6F, -0.8F, 2, 2, 2, 0.0F, false));

		talkingboard_text = new RendererModel(this);
		talkingboard_text.setRotationPoint(36.0F, 54.0F, 0.0F);
		talking_board.addChild(talkingboard_text);
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -50.8F, -56.8F, 8.8F, 4, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -50.8F, -56.8F, -13.6F, 4, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -36.4F, -56.8F, -13.6F, 4, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -36.4F, -56.8F, 8.8F, 4, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -43.6F, -56.8F, 6.4F, 2, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -43.6F, -56.8F, -11.6F, 2, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -36.4F, -56.8F, -5.6F, 3, 0, 10, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -47.4F, -56.8F, -7.6F, 2, 0, 14, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -50.4F, -56.8F, -7.6F, 2, 0, 14, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -39.4F, -56.8F, -8.6F, 2, 0, 16, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -40.8F, -56.8F, 7.2F, 2, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -40.8F, -56.8F, -12.4F, 2, 0, 4, 0.0F, false));
		talkingboard_text.cubeList.add(new ModelBox(talkingboard_text, 435, 332, -42.0F, -56.8F, -7.6F, 2, 0, 14, 0.0F, false));

		talkingboard_corners = new RendererModel(this);
		talkingboard_corners.setRotationPoint(36.0F, 54.0F, 0.0F);
		talking_board.addChild(talkingboard_corners);
		talkingboard_corners.cubeList.add(new ModelBox(talkingboard_corners, 449, 88, -35.0F, -57.0F, 10.8F, 3, 2, 3, 0.0F, false));
		talkingboard_corners.cubeList.add(new ModelBox(talkingboard_corners, 449, 88, -35.0F, -57.0F, -14.2F, 3, 2, 3, 0.0F, false));
		talkingboard_corners.cubeList.add(new ModelBox(talkingboard_corners, 449, 88, -52.0F, -57.0F, -14.2F, 3, 2, 3, 0.0F, false));
		talkingboard_corners.cubeList.add(new ModelBox(talkingboard_corners, 449, 88, -52.0F, -57.0F, 10.8F, 3, 2, 3, 0.0F, false));

		scrying_glass = new RendererModel(this);
		scrying_glass.setRotationPoint(-8.0F, -5.0F, 2.0F);
		setRotationAngle(scrying_glass, 0.0F, -1.309F, 0.0F);
		telepathic_circuits.addChild(scrying_glass);

		frame = new RendererModel(this);
		frame.setRotationPoint(44.0F, 59.0F, -7.0F);
		scrying_glass.addChild(frame);

		scrying_glass_frame_e1 = new RendererModel(this);
		scrying_glass_frame_e1.setRotationPoint(-43.6F, -58.9F, 9.4F);
		frame.addChild(scrying_glass_frame_e1);
		scrying_glass_frame_e1.cubeList.add(new ModelBox(scrying_glass_frame_e1, 379, 60, -3.0F, -0.5F, -1.0F, 6, 1, 2, 0.0F, false));

		scrying_glass_frame_e3 = new RendererModel(this);
		scrying_glass_frame_e3.setRotationPoint(-39.6F, -58.9F, 8.4F);
		setRotationAngle(scrying_glass_frame_e3, 0.0F, -1.0996F, 0.0F);
		frame.addChild(scrying_glass_frame_e3);
		scrying_glass_frame_e3.cubeList.add(new ModelBox(scrying_glass_frame_e3, 379, 60, -8.0F, -0.5F, -1.0F, 8, 1, 2, 0.0F, false));

		scrying_glass_frame_e2 = new RendererModel(this);
		scrying_glass_frame_e2.setRotationPoint(-47.6F, -58.9F, 8.4F);
		setRotationAngle(scrying_glass_frame_e2, 0.0F, 1.117F, 0.0F);
		frame.addChild(scrying_glass_frame_e2);
		scrying_glass_frame_e2.cubeList.add(new ModelBox(scrying_glass_frame_e2, 379, 60, 0.0F, -0.5F, -1.0F, 8, 1, 2, 0.0F, false));

		post_e1 = new RendererModel(this);
		post_e1.setRotationPoint(44.0F, 59.0F, -7.0F);
		scrying_glass.addChild(post_e1);
		post_e1.cubeList.add(new ModelBox(post_e1, 379, 60, -49.4F, -59.8F, 7.4F, 3, 4, 3, 0.0F, false));
		post_e1.cubeList.add(new ModelBox(post_e1, 379, 60, -40.4F, -59.8F, 7.4F, 3, 4, 3, 0.0F, false));
		post_e1.cubeList.add(new ModelBox(post_e1, 379, 60, -45.0F, -59.8F, -1.0F, 3, 4, 3, 0.0F, false));

		nixietube_e1 = new RendererModel(this);
		nixietube_e1.setRotationPoint(34.0F, -58.0F, -2.0F);
		controlset_e.addChild(nixietube_e1);
		nixietube_e1.cubeList.add(new ModelBox(nixietube_e1, 70, 420, -58.4F, -4.2F, 6.4F, 2, 2, 2, 0.0F, false));
		nixietube_e1.cubeList.add(new ModelBox(nixietube_e1, 379, 60, -59.0F, -3.4F, 5.8F, 3, 2, 3, 0.0F, false));

		nixietube_e2 = new RendererModel(this);
		nixietube_e2.setRotationPoint(34.0F, -58.0F, -2.0F);
		controlset_e.addChild(nixietube_e2);
		nixietube_e2.cubeList.add(new ModelBox(nixietube_e2, 70, 420, -58.4F, -4.2F, -4.4F, 2, 2, 2, 0.0F, false));
		nixietube_e2.cubeList.add(new ModelBox(nixietube_e2, 379, 60, -59.0F, -3.4F, -5.0F, 3, 2, 3, 0.0F, false));

		nixietube_e3 = new RendererModel(this);
		nixietube_e3.setRotationPoint(34.0F, -58.0F, -2.0F);
		controlset_e.addChild(nixietube_e3);
		nixietube_e3.cubeList.add(new ModelBox(nixietube_e3, 70, 420, -56.0F, -4.2F, 1.2F, 2, 2, 2, 0.0F, false));
		nixietube_e3.cubeList.add(new ModelBox(nixietube_e3, 379, 60, -56.6F, -3.4F, 0.6F, 3, 2, 3, 0.0F, false));

		baseplate_e1 = new RendererModel(this);
		baseplate_e1.setRotationPoint(-52.0F, -54.0F, -19.0F);
		setRotationAngle(baseplate_e1, 0.0F, 0.0F, -0.3491F);
		controlset_e.addChild(baseplate_e1);
		baseplate_e1.cubeList.add(new ModelBox(baseplate_e1, 379, 60, -2.6F, 3.0F, -2.4F, 5, 4, 5, 0.0F, false));

		baseplate_e2 = new RendererModel(this);
		baseplate_e2.setRotationPoint(-52.0F, -54.0F, -19.0F);
		setRotationAngle(baseplate_e2, 0.0F, 0.0F, -0.3491F);
		controlset_e.addChild(baseplate_e2);
		baseplate_e2.cubeList.add(new ModelBox(baseplate_e2, 379, 60, -2.6F, 3.0F, 34.6F, 5, 4, 5, 0.0F, false));

		switch_e1 = new RendererModel(this);
		switch_e1.setRotationPoint(-32.25F, -26.0F, -6.85F);
		setRotationAngle(switch_e1, 0.0F, 3.1416F, -0.3491F);
		controlset_e.addChild(switch_e1);
		switch_e1.cubeList.add(new ModelBox(switch_e1, 379, 60, -15.75F, -30.0F, -4.75F, 3, 4, 3, 0.0F, false));

		toggle_e1 = new RendererModel(this);
		toggle_e1.setRotationPoint(-14.05F, -29.8F, -3.45F);
		setRotationAngle(toggle_e1, 0.0F, 0.0F, -0.6109F);
		switch_e1.addChild(toggle_e1);
		toggle_e1.cubeList.add(new ModelBox(toggle_e1, 400, 300, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_e1.cubeList.add(new ModelBox(toggle_e1, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_e2 = new RendererModel(this);
		switch_e2.setRotationPoint(-32.25F, -26.0F, -10.85F);
		setRotationAngle(switch_e2, 0.0F, 3.1416F, -0.3491F);
		controlset_e.addChild(switch_e2);
		switch_e2.cubeList.add(new ModelBox(switch_e2, 379, 60, -15.75F, -30.0F, -4.75F, 3, 4, 3, 0.0F, false));

		toggle_e2 = new RendererModel(this);
		toggle_e2.setRotationPoint(-14.05F, -29.8F, -3.45F);
		setRotationAngle(toggle_e2, 0.0F, 0.0F, -0.6109F);
		switch_e2.addChild(toggle_e2);
		toggle_e2.cubeList.add(new ModelBox(toggle_e2, 400, 300, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_e2.cubeList.add(new ModelBox(toggle_e2, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		controlset_f = new RendererModel(this);
		controlset_f.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controlset_f, 0.0F, 2.0944F, 0.0F);
		controls.addChild(controlset_f);

		baseplate_f1 = new RendererModel(this);
		baseplate_f1.setRotationPoint(-44.8F, -54.0F, 0.0F);
		setRotationAngle(baseplate_f1, -0.0087F, 0.0262F, -0.6109F);
		controlset_f.addChild(baseplate_f1);
		baseplate_f1.cubeList.add(new ModelBox(baseplate_f1, 379, 60, -9.2F, 1.0F, -9.0F, 17, 4, 19, 0.0F, false));

		switch_f1 = new RendererModel(this);
		switch_f1.setRotationPoint(4.15F, 4.2F, -5.85F);
		setRotationAngle(switch_f1, 0.0F, 3.1416F, -0.3491F);
		baseplate_f1.addChild(switch_f1);
		switch_f1.cubeList.add(new ModelBox(switch_f1, 379, 60, 1.25F, -6.0F, -1.75F, 3, 4, 3, 0.0F, false));

		toggle_f1 = new RendererModel(this);
		toggle_f1.setRotationPoint(2.95F, -5.8F, -0.45F);
		setRotationAngle(toggle_f1, 0.0F, 0.0F, -0.8727F);
		switch_f1.addChild(toggle_f1);
		toggle_f1.cubeList.add(new ModelBox(toggle_f1, 464, 342, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_f1.cubeList.add(new ModelBox(toggle_f1, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_f2 = new RendererModel(this);
		switch_f2.setRotationPoint(3.95F, 4.2F, -0.05F);
		setRotationAngle(switch_f2, 0.0F, 3.1416F, -0.3491F);
		baseplate_f1.addChild(switch_f2);
		switch_f2.cubeList.add(new ModelBox(switch_f2, 379, 60, 1.25F, -6.0F, -1.75F, 3, 4, 3, 0.0F, false));

		toggle_f2 = new RendererModel(this);
		toggle_f2.setRotationPoint(2.95F, -5.8F, -0.45F);
		setRotationAngle(toggle_f2, 0.0F, 0.0F, -0.8727F);
		switch_f2.addChild(toggle_f2);
		toggle_f2.cubeList.add(new ModelBox(toggle_f2, 461, 320, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_f2.cubeList.add(new ModelBox(toggle_f2, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_f3 = new RendererModel(this);
		switch_f3.setRotationPoint(3.95F, 4.2F, 6.15F);
		setRotationAngle(switch_f3, 0.0F, 3.1416F, -0.3491F);
		baseplate_f1.addChild(switch_f3);
		switch_f3.cubeList.add(new ModelBox(switch_f3, 379, 60, 1.25F, -6.0F, -1.75F, 3, 4, 3, 0.0F, false));

		toggle_f3 = new RendererModel(this);
		toggle_f3.setRotationPoint(2.95F, -5.8F, -0.45F);
		setRotationAngle(toggle_f3, 0.0F, 0.0F, -0.8727F);
		switch_f3.addChild(toggle_f3);
		toggle_f3.cubeList.add(new ModelBox(toggle_f3, 440, 329, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_f3.cubeList.add(new ModelBox(toggle_f3, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_f4 = new RendererModel(this);
		switch_f4.setRotationPoint(8.55F, 4.6F, -5.85F);
		setRotationAngle(switch_f4, 0.0F, 3.1416F, -0.3491F);
		baseplate_f1.addChild(switch_f4);
		switch_f4.cubeList.add(new ModelBox(switch_f4, 379, 60, 1.25F, -6.0F, -1.75F, 3, 4, 3, 0.0F, false));

		toggle_f4 = new RendererModel(this);
		toggle_f4.setRotationPoint(2.95F, -5.8F, -0.45F);
		setRotationAngle(toggle_f4, 0.0F, 0.0F, -0.8727F);
		switch_f4.addChild(toggle_f4);
		toggle_f4.cubeList.add(new ModelBox(toggle_f4, 429, 318, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_f4.cubeList.add(new ModelBox(toggle_f4, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_f5 = new RendererModel(this);
		switch_f5.setRotationPoint(8.55F, 4.0F, -0.25F);
		setRotationAngle(switch_f5, 0.0F, 3.1416F, -0.3491F);
		baseplate_f1.addChild(switch_f5);
		switch_f5.cubeList.add(new ModelBox(switch_f5, 379, 60, 1.25F, -6.0F, -1.75F, 3, 4, 3, 0.0F, false));

		toggle_f5 = new RendererModel(this);
		toggle_f5.setRotationPoint(2.95F, -5.8F, -0.45F);
		setRotationAngle(toggle_f5, 0.0F, 0.0F, -0.8727F);
		switch_f5.addChild(toggle_f5);
		toggle_f5.cubeList.add(new ModelBox(toggle_f5, 439, 342, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_f5.cubeList.add(new ModelBox(toggle_f5, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		switch_f6 = new RendererModel(this);
		switch_f6.setRotationPoint(8.55F, 4.0F, 6.15F);
		setRotationAngle(switch_f6, 0.0F, 3.1416F, -0.3491F);
		baseplate_f1.addChild(switch_f6);
		switch_f6.cubeList.add(new ModelBox(switch_f6, 379, 60, 1.25F, -6.0F, -1.75F, 3, 4, 3, 0.0F, false));

		toggle_f6 = new RendererModel(this);
		toggle_f6.setRotationPoint(2.95F, -5.8F, -0.45F);
		setRotationAngle(toggle_f6, 0.0F, 0.0F, -0.8727F);
		switch_f6.addChild(toggle_f6);
		toggle_f6.cubeList.add(new ModelBox(toggle_f6, 457, 345, -0.7F, -3.2F, -0.38F, 1, 2, 1, 0.0F, false));
		toggle_f6.cubeList.add(new ModelBox(toggle_f6, 70, 420, -1.1F, -1.2F, -0.78F, 2, 2, 2, 0.0F, false));

		handbreak = new RendererModel(this);
		handbreak.setRotationPoint(-36.0F, -54.0F, 0.0F);
		setRotationAngle(handbreak, 0.0F, 0.0F, -0.3491F);
		controlset_f.addChild(handbreak);
		handbreak.cubeList.add(new ModelBox(handbreak, 379, 60, -15.4F, -2.0F, -20.0F, 7, 4, 8, 0.0F, false));

		lever_f1_rotate_z = new RendererModel(this);
		lever_f1_rotate_z.setRotationPoint(-11.9F, -3.2F, -17.0F);
		setRotationAngle(lever_f1_rotate_z, 0.0F, 0.0F, 3.0543F);
		handbreak.addChild(lever_f1_rotate_z);
		lever_f1_rotate_z.cubeList.add(new ModelBox(lever_f1_rotate_z, 379, 60, -1.5F, -0.6F, -3.4F, 2, 2, 9, 0.0F, false));
		lever_f1_rotate_z.cubeList.add(new ModelBox(lever_f1_rotate_z, 379, 60, -5.5F, -0.6F, 4.2F, 4, 2, 1, 0.0F, false));
		lever_f1_rotate_z.cubeList.add(new ModelBox(lever_f1_rotate_z, 379, 60, -5.5F, -0.6F, -3.2F, 4, 2, 1, 0.0F, false));
		lever_f1_rotate_z.cubeList.add(new ModelBox(lever_f1_rotate_z, 379, 60, -6.5F, -0.6F, -3.0F, 1, 2, 8, 0.0F, false));
		lever_f1_rotate_z.cubeList.add(new ModelBox(lever_f1_rotate_z, 379, 60, -7.5F, -0.6F, 0.0F, 1, 2, 2, 0.0F, false));
		lever_f1_rotate_z.cubeList.add(new ModelBox(lever_f1_rotate_z, 363, 193, -13.5F, -0.6F, 0.0F, 6, 2, 2, 0.0F, false));

		barrel = new RendererModel(this);
		barrel.setRotationPoint(-13.0F, -3.0F, -16.0F);
		handbreak.addChild(barrel);
		barrel.cubeList.add(new ModelBox(barrel, 150, 427, -1.2F, -3.0F, -3.0F, 4, 5, 6, 0.0F, false));

		randomize_cords = new RendererModel(this);
		randomize_cords.setRotationPoint(-36.0F, -56.0F, 0.0F);
		setRotationAngle(randomize_cords, 0.0F, 0.0349F, -0.0873F);
		controlset_f.addChild(randomize_cords);
		randomize_cords.cubeList.add(new ModelBox(randomize_cords, 379, 60, 4.0F, -2.6F, -6.0F, 12, 4, 12, 0.0F, false));

		globe_rotate_y = new RendererModel(this);
		globe_rotate_y.setRotationPoint(9.0F, -15.4F, 1.0F);
		setRotationAngle(globe_rotate_y, -1.5708F, 2.0944F, 0.0F);
		randomize_cords.addChild(globe_rotate_y);
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 264, 384, -3.6F, -3.6F, -0.4F, 8, 8, 8, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, 0.8F, -4.2F, -0.6F, 4, 4, 4, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, -1.0F, -4.2F, 1.8F, 3, 4, 2, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, -4.2F, -4.2F, 4.8F, 3, 4, 2, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, -3.8F, 2.8F, 3.0F, 3, 2, 5, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, -4.2F, -3.2F, 2.8F, 2, 2, 2, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, 0.8F, 2.8F, 1.8F, 1, 2, 1, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, 2.8F, 2.8F, 4.8F, 1, 2, 1, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, 2.2F, 3.2F, 4.2F, 1, 2, 1, 0.0F, false));
		globe_rotate_y.cubeList.add(new ModelBox(globe_rotate_y, 379, 60, -2.2F, -1.4F, -0.8F, 4, 6, 1, 0.0F, false));

		globe_mount = new RendererModel(this);
		globe_mount.setRotationPoint(0.0F, 0.0F, 0.0F);
		randomize_cords.addChild(globe_mount);
		globe_mount.cubeList.add(new ModelBox(globe_mount, 81, 431, 5.0F, -3.8F, -3.8F, 8, 2, 8, 0.0F, false));
		globe_mount.cubeList.add(new ModelBox(globe_mount, 379, 60, 9.0F, -19.0F, -8.0F, 1, 12, 2, 0.0F, false));
		globe_mount.cubeList.add(new ModelBox(globe_mount, 379, 60, 9.0F, -7.0F, -8.0F, 1, 2, 8, 0.0F, false));
		globe_mount.cubeList.add(new ModelBox(globe_mount, 379, 60, 9.0F, -19.0F, 0.0F, 1, 16, 1, 0.0F, false));
		globe_mount.cubeList.add(new ModelBox(globe_mount, 379, 60, 9.0F, -19.0F, -6.0F, 1, 2, 6, 0.0F, false));
		globe_mount.cubeList.add(new ModelBox(globe_mount, 190, 319, 5.6F, -4.4F, -3.4F, 7, 2, 7, 0.0F, false));

		rotation_selector = new RendererModel(this);
		rotation_selector.setRotationPoint(50.0F, -50.0F, 14.0F);
		controlset_f.addChild(rotation_selector);
		rotation_selector.cubeList.add(new ModelBox(rotation_selector, 379, 60, -103.0F, -1.0F, 2.0F, 4, 4, 4, 0.0F, false));

		rotation_crank_rotate_y = new RendererModel(this);
		rotation_crank_rotate_y.setRotationPoint(-101.0F, -4.9F, 3.8F);
		rotation_selector.addChild(rotation_crank_rotate_y);
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 146, 428, -1.0F, -2.1F, -0.8F, 2, 8, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, -1.0F, -1.1F, -2.8F, 2, 2, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, -1.0F, -1.1F, 1.2F, 2, 2, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, -3.0F, -1.1F, -0.8F, 2, 2, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, 1.0F, -1.1F, -0.8F, 2, 2, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, 3.0F, -1.1F, -2.8F, 2, 2, 6, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, -5.0F, -1.1F, -2.8F, 2, 2, 6, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, -3.0F, -1.1F, 3.2F, 6, 2, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 379, 60, -3.0F, -1.1F, -4.8F, 6, 2, 2, 0.0F, false));
		rotation_crank_rotate_y.cubeList.add(new ModelBox(rotation_crank_rotate_y, 90, 433, 3.0F, -5.1F, -0.8F, 2, 4, 2, 0.0F, false));

		structure = new RendererModel(this);
		structure.setRotationPoint(0.0F, 24.0F, 0.0F);

		side_001 = new RendererModel(this);
		side_001.setRotationPoint(0.0F, 0.0F, 0.0F);
		structure.addChild(side_001);

		skin = new RendererModel(this);
		skin.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(skin, 0.0F, 0.5236F, 0.0F);
		side_001.addChild(skin);

		floor_skin = new RendererModel(this);
		floor_skin.setRotationPoint(0.0F, 0.0F, 0.0F);
		skin.addChild(floor_skin);

		floorboards = new RendererModel(this);
		floorboards.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorboards, 3.1416F, 0.0F, 0.0F);
		floor_skin.addChild(floorboards);
		floorboards.cubeList.add(new ModelBox(floorboards, 125, 146, -74.0F, 0.25F, -40.0F, 10, 1, 78, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 150, 150, -64.0F, 0.25F, -34.0F, 10, 1, 68, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 176, 156, -54.0F, 0.25F, -28.0F, 10, 1, 56, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 214, 162, -44.0F, 0.25F, -23.0F, 10, 1, 45, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 318, 76, -34.0F, 0.5F, -17.0F, 10, 2, 34, 0.0F, false));
		floorboards.cubeList.add(new ModelBox(floorboards, 285, 73, -24.25F, 1.5F, -13.0F, 3, 4, 26, 0.0F, false));

		leg_skin = new RendererModel(this);
		leg_skin.setRotationPoint(-23.0F, -3.0F, 0.0F);
		setRotationAngle(leg_skin, 0.0F, 0.0F, -1.2217F);
		floor_skin.addChild(leg_skin);
		leg_skin.cubeList.add(new ModelBox(leg_skin, 319, 178, 0.0F, 0.0F, -12.0F, 20, 1, 24, 0.0F, false));

		knee_skin = new RendererModel(this);
		knee_skin.setRotationPoint(16.0F, -4.0F, 0.0F);
		setRotationAngle(knee_skin, 0.0F, 0.0F, 1.2217F);
		leg_skin.addChild(knee_skin);
		knee_skin.cubeList.add(new ModelBox(knee_skin, 329, 88, 1.0F, -16.0F, -9.0F, 4, 4, 20, 0.0F, false));
		knee_skin.cubeList.add(new ModelBox(knee_skin, 319, 178, 3.0F, -17.0F, -10.0F, 1, 20, 22, 0.0F, false));
		knee_skin.cubeList.add(new ModelBox(knee_skin, 320, 57, -2.0F, 12.0F, -13.0F, 4, 4, 27, 0.0F, false));

		thigh_skin = new RendererModel(this);
		thigh_skin.setRotationPoint(1.0F, -14.0F, 0.0F);
		setRotationAngle(thigh_skin, 0.0F, 0.0F, -0.6981F);
		knee_skin.addChild(thigh_skin);
		thigh_skin.cubeList.add(new ModelBox(thigh_skin, 35, 73, 0.75F, -13.0F, -13.0F, 1, 13, 26, 0.0F, false));

		belly_skin = new RendererModel(this);
		belly_skin.setRotationPoint(2.0F, -10.0F, 0.0F);
		setRotationAngle(belly_skin, 0.0F, 0.0F, -0.8727F);
		thigh_skin.addChild(belly_skin);
		belly_skin.cubeList.add(new ModelBox(belly_skin, 319, 178, 1.0F, -35.0F, -28.0F, 1, 19, 55, 0.0F, false));
		belly_skin.cubeList.add(new ModelBox(belly_skin, 305, 71, 0.5F, -16.0F, -20.0F, 2, 14, 40, 0.0F, false));

		rimtop_skin = new RendererModel(this);
		rimtop_skin.setRotationPoint(4.5F, -34.0F, 0.0F);
		setRotationAngle(rimtop_skin, 0.0F, 0.0F, 2.9671F);
		belly_skin.addChild(rimtop_skin);
		rimtop_skin.cubeList.add(new ModelBox(rimtop_skin, 319, 178, -1.5F, -8.0F, -34.0F, 1, 8, 68, 0.0F, false));

		panel_skin = new RendererModel(this);
		panel_skin.setRotationPoint(-6.5F, -4.0F, 1.0F);
		setRotationAngle(panel_skin, 0.0F, 0.0F, 3.0718F);
		rimtop_skin.addChild(panel_skin);
		panel_skin.cubeList.add(new ModelBox(panel_skin, 319, 188, -6.55F, 3.0F, -27.0F, 1, 14, 51, 0.0F, false));
		panel_skin.cubeList.add(new ModelBox(panel_skin, 319, 184, -6.55F, 17.0F, -19.0F, 1, 14, 36, 0.0F, false));
		panel_skin.cubeList.add(new ModelBox(panel_skin, 319, 178, -6.55F, 31.0F, -13.0F, 1, 12, 23, 0.0F, false));

		skelly = new RendererModel(this);
		skelly.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_001.addChild(skelly);

		rib = new RendererModel(this);
		rib.setRotationPoint(-68.0F, -49.0F, -2.0F);
		setRotationAngle(rib, 0.0F, 0.0F, -0.2618F);
		skelly.addChild(rib);
		rib.cubeList.add(new ModelBox(rib, 318, 238, 1.0F, -1.0F, -2.0F, 54, 5, 8, 0.0F, false));
		rib.cubeList.add(new ModelBox(rib, 325, 77, 0.0F, -3.0F, 1.0F, 55, 4, 2, 0.0F, false));

		rib_bolts_1 = new RendererModel(this);
		rib_bolts_1.setRotationPoint(68.0F, 49.0F, 2.0F);
		rib.addChild(rib_bolts_1);
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -59.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -19.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -39.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -29.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -49.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -59.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -19.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -39.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -29.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_1.cubeList.add(new ModelBox(rib_bolts_1, 117, 104, -49.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));

		foot = new RendererModel(this);
		foot.setRotationPoint(-8.0F, 0.0F, -2.0F);
		skelly.addChild(foot);
		foot.cubeList.add(new ModelBox(foot, 288, 89, -76.0F, -8.0F, 1.0F, 60, 4, 2, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 334, 144, -69.0F, -4.0F, -1.0F, 53, 4, 6, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 419, 78, -81.0F, -6.0F, -2.0F, 12, 6, 8, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 302, 0, -16.0F, -40.0F, -0.5F, 4, 33, 5, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 333, 84, -59.0F, -46.0F, -2.0F, 40, 4, 8, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 349, 84, -63.5F, -50.25F, -2.5F, 19, 8, 9, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 353, 72, -7.75F, -80.0F, -3.0F, 4, 20, 10, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 339, 74, -65.0F, -52.0F, 0.5F, 12, 12, 3, 0.0F, false));
		foot.cubeList.add(new ModelBox(foot, 160, 75, -9.0F, -77.4F, -4.0F, 4, 3, 12, 0.0F, false));

		leg = new RendererModel(this);
		leg.setRotationPoint(-21.0F, -4.0F, 0.0F);
		setRotationAngle(leg, 0.0F, 0.0F, 0.4363F);
		foot.addChild(leg);
		leg.cubeList.add(new ModelBox(leg, 430, 61, 0.0F, -12.0F, 0.0F, 4, 12, 4, 0.0F, false));
		leg.cubeList.add(new ModelBox(leg, 51, 93, -16.0F, -65.0F, 0.0F, 4, 10, 4, 0.0F, false));

		thigh = new RendererModel(this);
		thigh.setRotationPoint(-8.0F, -32.0F, 0.0F);
		setRotationAngle(thigh, 0.0F, 0.0F, -0.7854F);
		foot.addChild(thigh);
		thigh.cubeList.add(new ModelBox(thigh, 419, 56, -4.0F, -20.0F, 0.0F, 4, 18, 4, 0.0F, false));
		thigh.cubeList.add(new ModelBox(thigh, 406, 59, -11.0F, -26.0F, 1.0F, 4, 28, 2, 0.0F, false));

		floorpipes = new RendererModel(this);
		floorpipes.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorpipes, 0.0F, 0.5236F, 0.0F);
		side_001.addChild(floorpipes);
		floorpipes.cubeList.add(new ModelBox(floorpipes, 384, 57, -31.6F, -4.0F, 0.4F, 4, 2, 12, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 309, 70, -31.0F, -43.0F, -2.0F, 2, 20, 2, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 375, 69, -31.0F, -23.0F, -2.0F, 2, 2, 4, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 145, 435, -32.0F, -24.0F, 2.0F, 4, 4, 4, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 443, 91, -31.0F, -20.0F, 3.0F, 2, 20, 2, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 346, 42, -31.0F, -44.0F, 9.0F, 2, 40, 2, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 149, 430, -27.2F, -17.4F, -6.0F, 4, 7, 7, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 80, 429, -26.6F, -18.0F, -6.6F, 4, 8, 8, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 375, 82, -25.6F, -4.0F, -3.6F, 2, 2, 2, 0.0F, false));
		floorpipes.cubeList.add(new ModelBox(floorpipes, 345, 50, -25.0F, -10.0F, -3.0F, 1, 6, 1, 0.0F, false));

		front_rail = new RendererModel(this);
		front_rail.setRotationPoint(-88.0F, 0.0F, 2.0F);
		setRotationAngle(front_rail, 0.0F, 0.5236F, 0.0F);
		side_001.addChild(front_rail);
		front_rail.cubeList.add(new ModelBox(front_rail, 345, 32, 14.7F, -46.85F, 11.0F, 2, 4, 64, 0.0F, false));

		front_railbolt = new RendererModel(this);
		front_railbolt.setRotationPoint(88.0F, 0.0F, -2.0F);
		front_rail.addChild(front_railbolt);
		front_railbolt.cubeList.add(new ModelBox(front_railbolt, 137, 109, -73.55F, -45.7F, 69.5F, 2, 2, 2, 0.0F, false));
		front_railbolt.cubeList.add(new ModelBox(front_railbolt, 137, 109, -73.55F, -45.7F, 17.15F, 2, 2, 2, 0.0F, false));
		front_railbolt.cubeList.add(new ModelBox(front_railbolt, 137, 109, -73.55F, -45.7F, 44.9F, 2, 2, 2, 0.0F, false));
		front_railbolt.cubeList.add(new ModelBox(front_railbolt, 137, 109, -73.55F, -45.7F, 30.9F, 2, 2, 2, 0.0F, false));
		front_railbolt.cubeList.add(new ModelBox(front_railbolt, 137, 109, -73.55F, -45.7F, 57.9F, 2, 2, 2, 0.0F, false));

		rail_topbevel = new RendererModel(this);
		rail_topbevel.setRotationPoint(16.0F, -50.0F, 43.0F);
		setRotationAngle(rail_topbevel, 0.0F, 0.0F, 0.8727F);
		front_rail.addChild(rail_topbevel);
		rail_topbevel.cubeList.add(new ModelBox(rail_topbevel, 331, 39, 1.6F, -1.0F, -32.0F, 2, 4, 64, 0.0F, false));

		rail_underbevel = new RendererModel(this);
		rail_underbevel.setRotationPoint(16.0F, -42.0F, 43.0F);
		setRotationAngle(rail_underbevel, 0.0F, 0.0F, -0.9599F);
		front_rail.addChild(rail_underbevel);
		rail_underbevel.cubeList.add(new ModelBox(rail_underbevel, 328, 37, -0.075F, -1.55F, -35.0F, 2, 4, 69, 0.0F, false));

		rotor_gasket = new RendererModel(this);
		rotor_gasket.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rotor_gasket, 0.0F, 0.5236F, 0.0F);
		side_001.addChild(rotor_gasket);
		rotor_gasket.cubeList.add(new ModelBox(rotor_gasket, 304, 52, -15.0F, -70.0F, -4.0F, 2, 10, 8, 0.0F, false));
		rotor_gasket.cubeList.add(new ModelBox(rotor_gasket, 352, 63, -14.0F, -79.0F, -4.0F, 2, 6, 8, 0.0F, false));
		rotor_gasket.cubeList.add(new ModelBox(rotor_gasket, 136, 96, -17.0F, -76.9F, -3.0F, 2, 2, 6, 0.0F, false));
		rotor_gasket.cubeList.add(new ModelBox(rotor_gasket, 297, 77, -17.0F, -67.0F, -5.0F, 2, 10, 10, 0.0F, false));

		floor_trim = new RendererModel(this);
		floor_trim.setRotationPoint(-86.0F, -0.75F, 44.0F);
		setRotationAngle(floor_trim, 0.0F, 0.5236F, 0.0F);
		side_001.addChild(floor_trim);
		floor_trim.cubeList.add(new ModelBox(floor_trim, 292, 18, 20.0F, -2.0F, -36.0F, 4, 2, 84, 0.0F, false));

		side_002 = new RendererModel(this);
		side_002.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side_002, 0.0F, -1.0472F, 0.0F);
		structure.addChild(side_002);

		skin2 = new RendererModel(this);
		skin2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(skin2, 0.0F, 0.5236F, 0.0F);
		side_002.addChild(skin2);

		floor_skin2 = new RendererModel(this);
		floor_skin2.setRotationPoint(0.0F, 0.0F, 0.0F);
		skin2.addChild(floor_skin2);

		floorboards2 = new RendererModel(this);
		floorboards2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorboards2, 3.1416F, 0.0F, 0.0F);
		floor_skin2.addChild(floorboards2);
		floorboards2.cubeList.add(new ModelBox(floorboards2, 125, 146, -74.0F, 0.25F, -40.0F, 10, 1, 78, 0.0F, false));
		floorboards2.cubeList.add(new ModelBox(floorboards2, 150, 150, -64.0F, 0.25F, -34.0F, 10, 1, 68, 0.0F, false));
		floorboards2.cubeList.add(new ModelBox(floorboards2, 176, 156, -54.0F, 0.25F, -28.0F, 10, 1, 56, 0.0F, false));
		floorboards2.cubeList.add(new ModelBox(floorboards2, 214, 162, -44.0F, 0.25F, -23.0F, 10, 1, 45, 0.0F, false));
		floorboards2.cubeList.add(new ModelBox(floorboards2, 318, 76, -34.0F, 0.5F, -17.0F, 10, 2, 34, 0.0F, false));
		floorboards2.cubeList.add(new ModelBox(floorboards2, 285, 73, -24.25F, 1.5F, -13.0F, 3, 4, 26, 0.0F, false));

		leg_skin2 = new RendererModel(this);
		leg_skin2.setRotationPoint(-23.0F, -3.0F, 0.0F);
		setRotationAngle(leg_skin2, 0.0F, 0.0F, -1.2217F);
		floor_skin2.addChild(leg_skin2);
		leg_skin2.cubeList.add(new ModelBox(leg_skin2, 319, 178, 0.0F, 0.0F, -12.0F, 20, 1, 24, 0.0F, false));

		knee_skin2 = new RendererModel(this);
		knee_skin2.setRotationPoint(16.0F, -4.0F, 0.0F);
		setRotationAngle(knee_skin2, 0.0F, 0.0F, 1.2217F);
		leg_skin2.addChild(knee_skin2);
		knee_skin2.cubeList.add(new ModelBox(knee_skin2, 329, 88, 1.0F, -16.0F, -9.0F, 4, 4, 20, 0.0F, false));
		knee_skin2.cubeList.add(new ModelBox(knee_skin2, 319, 178, 3.0F, -17.0F, -10.0F, 1, 20, 22, 0.0F, false));
		knee_skin2.cubeList.add(new ModelBox(knee_skin2, 320, 57, -2.0F, 12.0F, -13.0F, 4, 4, 27, 0.0F, false));

		thigh_skin2 = new RendererModel(this);
		thigh_skin2.setRotationPoint(1.0F, -14.0F, 0.0F);
		setRotationAngle(thigh_skin2, 0.0F, 0.0F, -0.6981F);
		knee_skin2.addChild(thigh_skin2);
		thigh_skin2.cubeList.add(new ModelBox(thigh_skin2, 35, 73, 0.75F, -13.0F, -13.0F, 1, 13, 26, 0.0F, false));

		belly_skin2 = new RendererModel(this);
		belly_skin2.setRotationPoint(2.0F, -10.0F, 0.0F);
		setRotationAngle(belly_skin2, 0.0F, 0.0F, -0.8727F);
		thigh_skin2.addChild(belly_skin2);
		belly_skin2.cubeList.add(new ModelBox(belly_skin2, 319, 178, 1.0F, -35.0F, -28.0F, 1, 19, 55, 0.0F, false));
		belly_skin2.cubeList.add(new ModelBox(belly_skin2, 305, 71, 0.5F, -16.0F, -20.0F, 2, 14, 40, 0.0F, false));

		rimtop_skin2 = new RendererModel(this);
		rimtop_skin2.setRotationPoint(4.5F, -34.0F, 0.0F);
		setRotationAngle(rimtop_skin2, 0.0F, 0.0F, 2.9671F);
		belly_skin2.addChild(rimtop_skin2);
		rimtop_skin2.cubeList.add(new ModelBox(rimtop_skin2, 319, 178, -1.5F, -8.0F, -34.0F, 1, 8, 68, 0.0F, false));

		panel_skin2 = new RendererModel(this);
		panel_skin2.setRotationPoint(-6.5F, -4.0F, 1.0F);
		setRotationAngle(panel_skin2, 0.0F, 0.0F, 3.0718F);
		rimtop_skin2.addChild(panel_skin2);
		panel_skin2.cubeList.add(new ModelBox(panel_skin2, 319, 188, -6.55F, 3.0F, -27.0F, 1, 14, 51, 0.0F, false));
		panel_skin2.cubeList.add(new ModelBox(panel_skin2, 319, 184, -6.55F, 17.0F, -19.0F, 1, 14, 36, 0.0F, false));
		panel_skin2.cubeList.add(new ModelBox(panel_skin2, 319, 178, -6.55F, 31.0F, -13.0F, 1, 12, 23, 0.0F, false));

		skelly2 = new RendererModel(this);
		skelly2.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_002.addChild(skelly2);

		rib2 = new RendererModel(this);
		rib2.setRotationPoint(-68.0F, -49.0F, -2.0F);
		setRotationAngle(rib2, 0.0F, 0.0F, -0.2618F);
		skelly2.addChild(rib2);
		rib2.cubeList.add(new ModelBox(rib2, 318, 238, 1.0F, -1.0F, -2.0F, 54, 5, 8, 0.0F, false));
		rib2.cubeList.add(new ModelBox(rib2, 325, 77, 0.0F, -3.0F, 1.0F, 55, 4, 2, 0.0F, false));

		rib_bolts_2 = new RendererModel(this);
		rib_bolts_2.setRotationPoint(68.0F, 49.0F, 2.0F);
		rib2.addChild(rib_bolts_2);
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -59.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -19.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -39.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -29.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -49.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -59.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -19.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -39.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -29.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_2.cubeList.add(new ModelBox(rib_bolts_2, 117, 104, -49.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));

		foot2 = new RendererModel(this);
		foot2.setRotationPoint(-8.0F, 0.0F, -2.0F);
		skelly2.addChild(foot2);
		foot2.cubeList.add(new ModelBox(foot2, 288, 89, -76.0F, -8.0F, 1.0F, 60, 4, 2, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 334, 144, -69.0F, -4.0F, -1.0F, 53, 4, 6, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 419, 78, -81.0F, -6.0F, -2.0F, 12, 6, 8, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 302, 0, -16.0F, -40.0F, -0.5F, 4, 33, 5, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 333, 84, -59.0F, -46.0F, -2.0F, 40, 4, 8, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 349, 84, -63.5F, -50.25F, -2.5F, 19, 8, 9, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 353, 72, -7.75F, -80.0F, -3.0F, 4, 20, 10, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 339, 74, -65.0F, -52.0F, 0.5F, 12, 12, 3, 0.0F, false));
		foot2.cubeList.add(new ModelBox(foot2, 160, 75, -9.0F, -77.4F, -4.0F, 4, 3, 12, 0.0F, false));

		leg2 = new RendererModel(this);
		leg2.setRotationPoint(-21.0F, -4.0F, 0.0F);
		setRotationAngle(leg2, 0.0F, 0.0F, 0.4363F);
		foot2.addChild(leg2);
		leg2.cubeList.add(new ModelBox(leg2, 430, 61, 0.0F, -12.0F, 0.0F, 4, 12, 4, 0.0F, false));
		leg2.cubeList.add(new ModelBox(leg2, 51, 93, -16.0F, -65.0F, 0.0F, 4, 10, 4, 0.0F, false));

		thigh2 = new RendererModel(this);
		thigh2.setRotationPoint(-8.0F, -32.0F, 0.0F);
		setRotationAngle(thigh2, 0.0F, 0.0F, -0.7854F);
		foot2.addChild(thigh2);
		thigh2.cubeList.add(new ModelBox(thigh2, 419, 56, -4.0F, -20.0F, 0.0F, 4, 18, 4, 0.0F, false));
		thigh2.cubeList.add(new ModelBox(thigh2, 406, 59, -11.0F, -26.0F, 1.0F, 4, 28, 2, 0.0F, false));

		floorpipes2 = new RendererModel(this);
		floorpipes2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorpipes2, 0.0F, 0.5236F, 0.0F);
		side_002.addChild(floorpipes2);
		floorpipes2.cubeList.add(new ModelBox(floorpipes2, 384, 57, -31.6F, -4.0F, 0.4F, 4, 2, 12, 0.0F, false));
		floorpipes2.cubeList.add(new ModelBox(floorpipes2, 309, 70, -31.0F, -43.0F, -2.0F, 2, 20, 2, 0.0F, false));
		floorpipes2.cubeList.add(new ModelBox(floorpipes2, 375, 69, -31.0F, -23.0F, -2.0F, 2, 2, 4, 0.0F, false));
		floorpipes2.cubeList.add(new ModelBox(floorpipes2, 145, 435, -32.0F, -24.0F, 2.0F, 4, 4, 4, 0.0F, false));
		floorpipes2.cubeList.add(new ModelBox(floorpipes2, 443, 91, -31.0F, -20.0F, 3.0F, 2, 20, 2, 0.0F, false));
		floorpipes2.cubeList.add(new ModelBox(floorpipes2, 346, 42, -31.0F, -44.0F, 9.0F, 2, 40, 2, 0.0F, false));

		front_rail2 = new RendererModel(this);
		front_rail2.setRotationPoint(-88.0F, 0.0F, 2.0F);
		setRotationAngle(front_rail2, 0.0F, 0.5236F, 0.0F);
		side_002.addChild(front_rail2);
		front_rail2.cubeList.add(new ModelBox(front_rail2, 345, 32, 14.7F, -46.85F, 11.0F, 2, 4, 64, 0.0F, false));

		front_railbolt2 = new RendererModel(this);
		front_railbolt2.setRotationPoint(88.0F, 0.0F, -2.0F);
		front_rail2.addChild(front_railbolt2);
		front_railbolt2.cubeList.add(new ModelBox(front_railbolt2, 137, 109, -73.55F, -45.7F, 69.5F, 2, 2, 2, 0.0F, false));
		front_railbolt2.cubeList.add(new ModelBox(front_railbolt2, 137, 109, -73.55F, -45.7F, 17.15F, 2, 2, 2, 0.0F, false));
		front_railbolt2.cubeList.add(new ModelBox(front_railbolt2, 137, 109, -73.55F, -45.7F, 44.9F, 2, 2, 2, 0.0F, false));
		front_railbolt2.cubeList.add(new ModelBox(front_railbolt2, 137, 109, -73.55F, -45.7F, 30.9F, 2, 2, 2, 0.0F, false));
		front_railbolt2.cubeList.add(new ModelBox(front_railbolt2, 137, 109, -73.55F, -45.7F, 57.9F, 2, 2, 2, 0.0F, false));

		rail_topbevel2 = new RendererModel(this);
		rail_topbevel2.setRotationPoint(16.0F, -50.0F, 43.0F);
		setRotationAngle(rail_topbevel2, 0.0F, 0.0F, 0.8727F);
		front_rail2.addChild(rail_topbevel2);
		rail_topbevel2.cubeList.add(new ModelBox(rail_topbevel2, 331, 39, 1.6F, -1.0F, -32.0F, 2, 4, 64, 0.0F, false));

		rail_underbevel2 = new RendererModel(this);
		rail_underbevel2.setRotationPoint(16.0F, -42.0F, 43.0F);
		setRotationAngle(rail_underbevel2, 0.0F, 0.0F, -0.9599F);
		front_rail2.addChild(rail_underbevel2);
		rail_underbevel2.cubeList.add(new ModelBox(rail_underbevel2, 328, 37, -0.075F, -1.55F, -35.0F, 2, 4, 69, 0.0F, false));

		rotor_gasket2 = new RendererModel(this);
		rotor_gasket2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rotor_gasket2, 0.0F, 0.5236F, 0.0F);
		side_002.addChild(rotor_gasket2);
		rotor_gasket2.cubeList.add(new ModelBox(rotor_gasket2, 304, 52, -15.0F, -70.0F, -4.0F, 2, 10, 8, 0.0F, false));
		rotor_gasket2.cubeList.add(new ModelBox(rotor_gasket2, 352, 63, -14.0F, -79.0F, -4.0F, 2, 6, 8, 0.0F, false));
		rotor_gasket2.cubeList.add(new ModelBox(rotor_gasket2, 136, 96, -17.0F, -76.9F, -3.0F, 2, 2, 6, 0.0F, false));
		rotor_gasket2.cubeList.add(new ModelBox(rotor_gasket2, 297, 77, -17.0F, -67.0F, -5.0F, 2, 10, 10, 0.0F, false));

		floor_trim2 = new RendererModel(this);
		floor_trim2.setRotationPoint(-86.0F, -0.75F, 44.0F);
		setRotationAngle(floor_trim2, 0.0F, 0.5236F, 0.0F);
		side_002.addChild(floor_trim2);
		floor_trim2.cubeList.add(new ModelBox(floor_trim2, 292, 18, 20.0F, -2.0F, -36.0F, 4, 2, 84, 0.0F, false));

		side_003 = new RendererModel(this);
		side_003.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side_003, 0.0F, -2.0944F, 0.0F);
		structure.addChild(side_003);

		skin3 = new RendererModel(this);
		skin3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(skin3, 0.0F, 0.5236F, 0.0F);
		side_003.addChild(skin3);

		floor_skin3 = new RendererModel(this);
		floor_skin3.setRotationPoint(0.0F, 0.0F, 0.0F);
		skin3.addChild(floor_skin3);

		floorboards3 = new RendererModel(this);
		floorboards3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorboards3, 3.1416F, 0.0F, 0.0F);
		floor_skin3.addChild(floorboards3);
		floorboards3.cubeList.add(new ModelBox(floorboards3, 125, 146, -74.0F, 0.25F, -40.0F, 10, 1, 78, 0.0F, false));
		floorboards3.cubeList.add(new ModelBox(floorboards3, 150, 150, -64.0F, 0.25F, -34.0F, 10, 1, 68, 0.0F, false));
		floorboards3.cubeList.add(new ModelBox(floorboards3, 176, 156, -54.0F, 0.25F, -28.0F, 10, 1, 56, 0.0F, false));
		floorboards3.cubeList.add(new ModelBox(floorboards3, 214, 162, -44.0F, 0.25F, -23.0F, 10, 1, 45, 0.0F, false));
		floorboards3.cubeList.add(new ModelBox(floorboards3, 318, 76, -34.0F, 0.5F, -17.0F, 10, 2, 34, 0.0F, false));
		floorboards3.cubeList.add(new ModelBox(floorboards3, 285, 73, -24.25F, 1.5F, -13.0F, 3, 4, 26, 0.0F, false));

		leg_skin3 = new RendererModel(this);
		leg_skin3.setRotationPoint(-23.0F, -3.0F, 0.0F);
		setRotationAngle(leg_skin3, 0.0F, 0.0F, -1.2217F);
		floor_skin3.addChild(leg_skin3);
		leg_skin3.cubeList.add(new ModelBox(leg_skin3, 319, 178, 0.0F, 0.0F, -12.0F, 20, 1, 24, 0.0F, false));

		knee_skin3 = new RendererModel(this);
		knee_skin3.setRotationPoint(16.0F, -4.0F, 0.0F);
		setRotationAngle(knee_skin3, 0.0F, 0.0F, 1.2217F);
		leg_skin3.addChild(knee_skin3);
		knee_skin3.cubeList.add(new ModelBox(knee_skin3, 329, 88, 1.0F, -16.0F, -9.0F, 4, 4, 20, 0.0F, false));
		knee_skin3.cubeList.add(new ModelBox(knee_skin3, 319, 178, 3.0F, -17.0F, -10.0F, 1, 20, 22, 0.0F, false));
		knee_skin3.cubeList.add(new ModelBox(knee_skin3, 320, 57, -2.0F, 12.0F, -13.0F, 4, 4, 27, 0.0F, false));

		thigh_skin3 = new RendererModel(this);
		thigh_skin3.setRotationPoint(1.0F, -14.0F, 0.0F);
		setRotationAngle(thigh_skin3, 0.0F, 0.0F, -0.6981F);
		knee_skin3.addChild(thigh_skin3);
		thigh_skin3.cubeList.add(new ModelBox(thigh_skin3, 35, 73, 0.75F, -13.0F, -13.0F, 1, 13, 26, 0.0F, false));

		belly_skin3 = new RendererModel(this);
		belly_skin3.setRotationPoint(2.0F, -10.0F, 0.0F);
		setRotationAngle(belly_skin3, 0.0F, 0.0F, -0.8727F);
		thigh_skin3.addChild(belly_skin3);
		belly_skin3.cubeList.add(new ModelBox(belly_skin3, 319, 178, 1.0F, -35.0F, -28.0F, 1, 19, 55, 0.0F, false));
		belly_skin3.cubeList.add(new ModelBox(belly_skin3, 305, 71, 0.5F, -16.0F, -20.0F, 2, 14, 40, 0.0F, false));

		rimtop_skin3 = new RendererModel(this);
		rimtop_skin3.setRotationPoint(4.5F, -34.0F, 0.0F);
		setRotationAngle(rimtop_skin3, 0.0F, 0.0F, 2.9671F);
		belly_skin3.addChild(rimtop_skin3);
		rimtop_skin3.cubeList.add(new ModelBox(rimtop_skin3, 319, 178, -1.5F, -8.0F, -34.0F, 1, 8, 68, 0.0F, false));

		panel_skin3 = new RendererModel(this);
		panel_skin3.setRotationPoint(-6.5F, -4.0F, 1.0F);
		setRotationAngle(panel_skin3, 0.0F, 0.0F, 3.0718F);
		rimtop_skin3.addChild(panel_skin3);
		panel_skin3.cubeList.add(new ModelBox(panel_skin3, 319, 188, -6.55F, 3.0F, -27.0F, 1, 14, 51, 0.0F, false));
		panel_skin3.cubeList.add(new ModelBox(panel_skin3, 319, 184, -6.55F, 17.0F, -19.0F, 1, 14, 36, 0.0F, false));
		panel_skin3.cubeList.add(new ModelBox(panel_skin3, 319, 178, -6.55F, 31.0F, -13.0F, 1, 12, 23, 0.0F, false));

		skelly3 = new RendererModel(this);
		skelly3.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_003.addChild(skelly3);

		rib3 = new RendererModel(this);
		rib3.setRotationPoint(-68.0F, -49.0F, -2.0F);
		setRotationAngle(rib3, 0.0F, 0.0F, -0.2618F);
		skelly3.addChild(rib3);
		rib3.cubeList.add(new ModelBox(rib3, 318, 238, 1.0F, -1.0F, -2.0F, 54, 5, 8, 0.0F, false));
		rib3.cubeList.add(new ModelBox(rib3, 325, 77, 0.0F, -3.0F, 1.0F, 55, 4, 2, 0.0F, false));

		rib_bolts_3 = new RendererModel(this);
		rib_bolts_3.setRotationPoint(68.0F, 49.0F, 2.0F);
		rib3.addChild(rib_bolts_3);
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -59.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -19.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -39.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -29.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -49.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -59.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -19.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -39.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -29.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_3.cubeList.add(new ModelBox(rib_bolts_3, 117, 104, -49.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));

		foot3 = new RendererModel(this);
		foot3.setRotationPoint(-8.0F, 0.0F, -2.0F);
		skelly3.addChild(foot3);
		foot3.cubeList.add(new ModelBox(foot3, 288, 89, -76.0F, -8.0F, 1.0F, 60, 4, 2, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 334, 144, -69.0F, -4.0F, -1.0F, 53, 4, 6, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 419, 78, -81.0F, -6.0F, -2.0F, 12, 6, 8, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 302, 0, -16.0F, -40.0F, -0.5F, 4, 33, 5, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 333, 84, -59.0F, -46.0F, -2.0F, 40, 4, 8, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 349, 84, -63.5F, -50.25F, -2.5F, 19, 8, 9, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 353, 72, -7.75F, -80.0F, -3.0F, 4, 20, 10, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 339, 74, -65.0F, -52.0F, 0.5F, 12, 12, 3, 0.0F, false));
		foot3.cubeList.add(new ModelBox(foot3, 160, 75, -9.0F, -77.4F, -4.0F, 4, 3, 12, 0.0F, false));

		leg3 = new RendererModel(this);
		leg3.setRotationPoint(-21.0F, -4.0F, 0.0F);
		setRotationAngle(leg3, 0.0F, 0.0F, 0.4363F);
		foot3.addChild(leg3);
		leg3.cubeList.add(new ModelBox(leg3, 430, 61, 0.0F, -12.0F, 0.0F, 4, 12, 4, 0.0F, false));
		leg3.cubeList.add(new ModelBox(leg3, 51, 93, -16.0F, -65.0F, 0.0F, 4, 10, 4, 0.0F, false));

		thigh3 = new RendererModel(this);
		thigh3.setRotationPoint(-8.0F, -32.0F, 0.0F);
		setRotationAngle(thigh3, 0.0F, 0.0F, -0.7854F);
		foot3.addChild(thigh3);
		thigh3.cubeList.add(new ModelBox(thigh3, 419, 56, -4.0F, -20.0F, 0.0F, 4, 18, 4, 0.0F, false));
		thigh3.cubeList.add(new ModelBox(thigh3, 406, 59, -11.0F, -26.0F, 1.0F, 4, 28, 2, 0.0F, false));

		floorpipes3 = new RendererModel(this);
		floorpipes3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorpipes3, 0.0F, 0.5236F, 0.0F);
		side_003.addChild(floorpipes3);
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 384, 57, -31.6F, -4.0F, 0.4F, 4, 2, 12, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 309, 70, -31.0F, -43.0F, -2.0F, 2, 20, 2, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 375, 69, -31.0F, -23.0F, -2.0F, 2, 2, 4, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 145, 435, -32.0F, -24.0F, 2.0F, 4, 4, 4, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 443, 91, -31.0F, -20.0F, 3.0F, 2, 20, 2, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 346, 42, -31.0F, -44.0F, 9.0F, 2, 40, 2, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 149, 430, -27.2F, -17.4F, -6.0F, 4, 7, 7, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 80, 429, -26.6F, -18.0F, -6.6F, 4, 8, 8, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 375, 82, -25.6F, -4.0F, -3.6F, 2, 2, 2, 0.0F, false));
		floorpipes3.cubeList.add(new ModelBox(floorpipes3, 345, 50, -25.0F, -10.0F, -3.0F, 1, 6, 1, 0.0F, false));

		front_rail3 = new RendererModel(this);
		front_rail3.setRotationPoint(-88.0F, 0.0F, 2.0F);
		setRotationAngle(front_rail3, 0.0F, 0.5236F, 0.0F);
		side_003.addChild(front_rail3);
		front_rail3.cubeList.add(new ModelBox(front_rail3, 345, 32, 14.7F, -46.85F, 11.0F, 2, 4, 64, 0.0F, false));

		front_railbolt3 = new RendererModel(this);
		front_railbolt3.setRotationPoint(88.0F, 0.0F, -2.0F);
		front_rail3.addChild(front_railbolt3);
		front_railbolt3.cubeList.add(new ModelBox(front_railbolt3, 137, 109, -73.55F, -45.7F, 69.5F, 2, 2, 2, 0.0F, false));
		front_railbolt3.cubeList.add(new ModelBox(front_railbolt3, 137, 109, -73.55F, -45.7F, 17.15F, 2, 2, 2, 0.0F, false));
		front_railbolt3.cubeList.add(new ModelBox(front_railbolt3, 137, 109, -73.55F, -45.7F, 44.9F, 2, 2, 2, 0.0F, false));
		front_railbolt3.cubeList.add(new ModelBox(front_railbolt3, 137, 109, -73.55F, -45.7F, 30.9F, 2, 2, 2, 0.0F, false));
		front_railbolt3.cubeList.add(new ModelBox(front_railbolt3, 137, 109, -73.55F, -45.7F, 57.9F, 2, 2, 2, 0.0F, false));

		rail_topbevel3 = new RendererModel(this);
		rail_topbevel3.setRotationPoint(16.0F, -50.0F, 43.0F);
		setRotationAngle(rail_topbevel3, 0.0F, 0.0F, 0.8727F);
		front_rail3.addChild(rail_topbevel3);
		rail_topbevel3.cubeList.add(new ModelBox(rail_topbevel3, 331, 39, 1.6F, -1.0F, -32.0F, 2, 4, 64, 0.0F, false));

		rail_underbevel3 = new RendererModel(this);
		rail_underbevel3.setRotationPoint(16.0F, -42.0F, 43.0F);
		setRotationAngle(rail_underbevel3, 0.0F, 0.0F, -0.9599F);
		front_rail3.addChild(rail_underbevel3);
		rail_underbevel3.cubeList.add(new ModelBox(rail_underbevel3, 328, 37, -0.075F, -1.55F, -35.0F, 2, 4, 69, 0.0F, false));

		rotor_gasket3 = new RendererModel(this);
		rotor_gasket3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rotor_gasket3, 0.0F, 0.5236F, 0.0F);
		side_003.addChild(rotor_gasket3);
		rotor_gasket3.cubeList.add(new ModelBox(rotor_gasket3, 304, 52, -15.0F, -70.0F, -4.0F, 2, 10, 8, 0.0F, false));
		rotor_gasket3.cubeList.add(new ModelBox(rotor_gasket3, 352, 63, -14.0F, -79.0F, -4.0F, 2, 6, 8, 0.0F, false));
		rotor_gasket3.cubeList.add(new ModelBox(rotor_gasket3, 136, 96, -17.0F, -76.9F, -3.0F, 2, 2, 6, 0.0F, false));
		rotor_gasket3.cubeList.add(new ModelBox(rotor_gasket3, 297, 77, -17.0F, -67.0F, -5.0F, 2, 10, 10, 0.0F, false));

		floor_trim3 = new RendererModel(this);
		floor_trim3.setRotationPoint(-86.0F, -0.75F, 44.0F);
		setRotationAngle(floor_trim3, 0.0F, 0.5236F, 0.0F);
		side_003.addChild(floor_trim3);
		floor_trim3.cubeList.add(new ModelBox(floor_trim3, 292, 18, 20.0F, -2.0F, -36.0F, 4, 2, 84, 0.0F, false));

		side_004 = new RendererModel(this);
		side_004.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side_004, 0.0F, 3.1416F, 0.0F);
		structure.addChild(side_004);

		skin4 = new RendererModel(this);
		skin4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(skin4, 0.0F, 0.5236F, 0.0F);
		side_004.addChild(skin4);

		floor_skin4 = new RendererModel(this);
		floor_skin4.setRotationPoint(0.0F, 0.0F, 0.0F);
		skin4.addChild(floor_skin4);

		floorboards4 = new RendererModel(this);
		floorboards4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorboards4, 3.1416F, 0.0F, 0.0F);
		floor_skin4.addChild(floorboards4);
		floorboards4.cubeList.add(new ModelBox(floorboards4, 125, 146, -74.0F, 0.25F, -40.0F, 10, 1, 78, 0.0F, false));
		floorboards4.cubeList.add(new ModelBox(floorboards4, 150, 150, -64.0F, 0.25F, -34.0F, 10, 1, 68, 0.0F, false));
		floorboards4.cubeList.add(new ModelBox(floorboards4, 176, 156, -54.0F, 0.25F, -28.0F, 10, 1, 56, 0.0F, false));
		floorboards4.cubeList.add(new ModelBox(floorboards4, 214, 162, -44.0F, 0.25F, -23.0F, 10, 1, 45, 0.0F, false));
		floorboards4.cubeList.add(new ModelBox(floorboards4, 318, 76, -34.0F, 0.5F, -17.0F, 10, 2, 34, 0.0F, false));
		floorboards4.cubeList.add(new ModelBox(floorboards4, 285, 73, -24.25F, 1.5F, -13.0F, 3, 4, 26, 0.0F, false));

		leg_skin4 = new RendererModel(this);
		leg_skin4.setRotationPoint(-23.0F, -3.0F, 0.0F);
		setRotationAngle(leg_skin4, 0.0F, 0.0F, -1.2217F);
		floor_skin4.addChild(leg_skin4);
		leg_skin4.cubeList.add(new ModelBox(leg_skin4, 319, 178, 0.0F, 0.0F, -12.0F, 20, 1, 24, 0.0F, false));

		knee_skin4 = new RendererModel(this);
		knee_skin4.setRotationPoint(16.0F, -4.0F, 0.0F);
		setRotationAngle(knee_skin4, 0.0F, 0.0F, 1.2217F);
		leg_skin4.addChild(knee_skin4);
		knee_skin4.cubeList.add(new ModelBox(knee_skin4, 329, 88, 1.0F, -16.0F, -9.0F, 4, 4, 20, 0.0F, false));
		knee_skin4.cubeList.add(new ModelBox(knee_skin4, 319, 178, 3.0F, -17.0F, -10.0F, 1, 20, 22, 0.0F, false));
		knee_skin4.cubeList.add(new ModelBox(knee_skin4, 320, 57, -2.0F, 12.0F, -13.0F, 4, 4, 27, 0.0F, false));

		thigh_skin4 = new RendererModel(this);
		thigh_skin4.setRotationPoint(1.0F, -14.0F, 0.0F);
		setRotationAngle(thigh_skin4, 0.0F, 0.0F, -0.6981F);
		knee_skin4.addChild(thigh_skin4);
		thigh_skin4.cubeList.add(new ModelBox(thigh_skin4, 35, 73, 0.75F, -13.0F, -13.0F, 1, 13, 26, 0.0F, false));

		belly_skin4 = new RendererModel(this);
		belly_skin4.setRotationPoint(2.0F, -10.0F, 0.0F);
		setRotationAngle(belly_skin4, 0.0F, 0.0F, -0.8727F);
		thigh_skin4.addChild(belly_skin4);
		belly_skin4.cubeList.add(new ModelBox(belly_skin4, 319, 178, 1.0F, -35.0F, -28.0F, 1, 19, 55, 0.0F, false));
		belly_skin4.cubeList.add(new ModelBox(belly_skin4, 305, 71, 0.5F, -16.0F, -20.0F, 2, 14, 40, 0.0F, false));

		rimtop_skin4 = new RendererModel(this);
		rimtop_skin4.setRotationPoint(4.5F, -34.0F, 0.0F);
		setRotationAngle(rimtop_skin4, 0.0F, 0.0F, 2.9671F);
		belly_skin4.addChild(rimtop_skin4);
		rimtop_skin4.cubeList.add(new ModelBox(rimtop_skin4, 319, 178, -1.5F, -8.0F, -34.0F, 1, 8, 68, 0.0F, false));

		panel_skin4 = new RendererModel(this);
		panel_skin4.setRotationPoint(-6.5F, -4.0F, 1.0F);
		setRotationAngle(panel_skin4, 0.0F, 0.0F, 3.0718F);
		rimtop_skin4.addChild(panel_skin4);
		panel_skin4.cubeList.add(new ModelBox(panel_skin4, 319, 188, -6.55F, 3.0F, -27.0F, 1, 14, 51, 0.0F, false));
		panel_skin4.cubeList.add(new ModelBox(panel_skin4, 319, 184, -6.55F, 17.0F, -19.0F, 1, 14, 36, 0.0F, false));
		panel_skin4.cubeList.add(new ModelBox(panel_skin4, 319, 178, -6.55F, 31.0F, -13.0F, 1, 12, 23, 0.0F, false));

		skelly4 = new RendererModel(this);
		skelly4.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_004.addChild(skelly4);

		rib4 = new RendererModel(this);
		rib4.setRotationPoint(-68.0F, -49.0F, -2.0F);
		setRotationAngle(rib4, 0.0F, 0.0F, -0.2618F);
		skelly4.addChild(rib4);
		rib4.cubeList.add(new ModelBox(rib4, 318, 238, 1.0F, -1.0F, -2.0F, 54, 5, 8, 0.0F, false));
		rib4.cubeList.add(new ModelBox(rib4, 325, 77, 0.0F, -3.0F, 1.0F, 55, 4, 2, 0.0F, false));

		rib_bolts_4 = new RendererModel(this);
		rib_bolts_4.setRotationPoint(68.0F, 49.0F, 2.0F);
		rib4.addChild(rib_bolts_4);
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -59.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -19.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -39.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -29.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -49.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -59.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -19.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -39.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -29.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_4.cubeList.add(new ModelBox(rib_bolts_4, 117, 104, -49.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));

		foot4 = new RendererModel(this);
		foot4.setRotationPoint(-8.0F, 0.0F, -2.0F);
		skelly4.addChild(foot4);
		foot4.cubeList.add(new ModelBox(foot4, 288, 89, -76.0F, -8.0F, 1.0F, 60, 4, 2, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 334, 144, -69.0F, -4.0F, -1.0F, 53, 4, 6, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 419, 78, -81.0F, -6.0F, -2.0F, 12, 6, 8, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 302, 0, -16.0F, -40.0F, -0.5F, 4, 33, 5, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 333, 84, -59.0F, -46.0F, -2.0F, 40, 4, 8, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 349, 84, -63.5F, -50.25F, -2.5F, 19, 8, 9, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 353, 72, -7.75F, -80.0F, -3.0F, 4, 20, 10, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 339, 74, -65.0F, -52.0F, 0.5F, 12, 12, 3, 0.0F, false));
		foot4.cubeList.add(new ModelBox(foot4, 160, 75, -9.0F, -77.4F, -4.0F, 4, 3, 12, 0.0F, false));

		leg4 = new RendererModel(this);
		leg4.setRotationPoint(-21.0F, -4.0F, 0.0F);
		setRotationAngle(leg4, 0.0F, 0.0F, 0.4363F);
		foot4.addChild(leg4);
		leg4.cubeList.add(new ModelBox(leg4, 430, 61, 0.0F, -12.0F, 0.0F, 4, 12, 4, 0.0F, false));
		leg4.cubeList.add(new ModelBox(leg4, 51, 93, -16.0F, -65.0F, 0.0F, 4, 10, 4, 0.0F, false));

		thigh4 = new RendererModel(this);
		thigh4.setRotationPoint(-8.0F, -32.0F, 0.0F);
		setRotationAngle(thigh4, 0.0F, 0.0F, -0.7854F);
		foot4.addChild(thigh4);
		thigh4.cubeList.add(new ModelBox(thigh4, 419, 56, -4.0F, -20.0F, 0.0F, 4, 18, 4, 0.0F, false));
		thigh4.cubeList.add(new ModelBox(thigh4, 406, 59, -11.0F, -26.0F, 1.0F, 4, 28, 2, 0.0F, false));

		floorpipes4 = new RendererModel(this);
		floorpipes4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorpipes4, 0.0F, 0.5236F, 0.0F);
		side_004.addChild(floorpipes4);
		floorpipes4.cubeList.add(new ModelBox(floorpipes4, 384, 57, -31.6F, -4.0F, 0.4F, 4, 2, 12, 0.0F, false));
		floorpipes4.cubeList.add(new ModelBox(floorpipes4, 309, 70, -31.0F, -43.0F, -2.0F, 2, 20, 2, 0.0F, false));
		floorpipes4.cubeList.add(new ModelBox(floorpipes4, 375, 69, -31.0F, -23.0F, -2.0F, 2, 2, 4, 0.0F, false));
		floorpipes4.cubeList.add(new ModelBox(floorpipes4, 145, 435, -32.0F, -24.0F, 2.0F, 4, 4, 4, 0.0F, false));
		floorpipes4.cubeList.add(new ModelBox(floorpipes4, 443, 91, -31.0F, -20.0F, 3.0F, 2, 20, 2, 0.0F, false));
		floorpipes4.cubeList.add(new ModelBox(floorpipes4, 346, 42, -31.0F, -44.0F, 9.0F, 2, 40, 2, 0.0F, false));

		front_rail4 = new RendererModel(this);
		front_rail4.setRotationPoint(-88.0F, 0.0F, 2.0F);
		setRotationAngle(front_rail4, 0.0F, 0.5236F, 0.0F);
		side_004.addChild(front_rail4);
		front_rail4.cubeList.add(new ModelBox(front_rail4, 345, 32, 14.7F, -46.85F, 11.0F, 2, 4, 64, 0.0F, false));

		front_railbolt4 = new RendererModel(this);
		front_railbolt4.setRotationPoint(88.0F, 0.0F, -2.0F);
		front_rail4.addChild(front_railbolt4);
		front_railbolt4.cubeList.add(new ModelBox(front_railbolt4, 137, 109, -73.55F, -45.7F, 69.5F, 2, 2, 2, 0.0F, false));
		front_railbolt4.cubeList.add(new ModelBox(front_railbolt4, 137, 109, -73.55F, -45.7F, 17.15F, 2, 2, 2, 0.0F, false));
		front_railbolt4.cubeList.add(new ModelBox(front_railbolt4, 137, 109, -73.55F, -45.7F, 44.9F, 2, 2, 2, 0.0F, false));
		front_railbolt4.cubeList.add(new ModelBox(front_railbolt4, 137, 109, -73.55F, -45.7F, 30.9F, 2, 2, 2, 0.0F, false));
		front_railbolt4.cubeList.add(new ModelBox(front_railbolt4, 137, 109, -73.55F, -45.7F, 57.9F, 2, 2, 2, 0.0F, false));

		rail_topbevel4 = new RendererModel(this);
		rail_topbevel4.setRotationPoint(16.0F, -50.0F, 43.0F);
		setRotationAngle(rail_topbevel4, 0.0F, 0.0F, 0.8727F);
		front_rail4.addChild(rail_topbevel4);
		rail_topbevel4.cubeList.add(new ModelBox(rail_topbevel4, 331, 39, 1.6F, -1.0F, -32.0F, 2, 4, 64, 0.0F, false));

		rail_underbevel4 = new RendererModel(this);
		rail_underbevel4.setRotationPoint(16.0F, -42.0F, 43.0F);
		setRotationAngle(rail_underbevel4, 0.0F, 0.0F, -0.9599F);
		front_rail4.addChild(rail_underbevel4);
		rail_underbevel4.cubeList.add(new ModelBox(rail_underbevel4, 328, 37, -0.075F, -1.55F, -35.0F, 2, 4, 69, 0.0F, false));

		rotor_gasket4 = new RendererModel(this);
		rotor_gasket4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rotor_gasket4, 0.0F, 0.5236F, 0.0F);
		side_004.addChild(rotor_gasket4);
		rotor_gasket4.cubeList.add(new ModelBox(rotor_gasket4, 304, 52, -15.0F, -70.0F, -4.0F, 2, 10, 8, 0.0F, false));
		rotor_gasket4.cubeList.add(new ModelBox(rotor_gasket4, 352, 63, -14.0F, -79.0F, -4.0F, 2, 6, 8, 0.0F, false));
		rotor_gasket4.cubeList.add(new ModelBox(rotor_gasket4, 136, 96, -17.0F, -76.9F, -3.0F, 2, 2, 6, 0.0F, false));
		rotor_gasket4.cubeList.add(new ModelBox(rotor_gasket4, 297, 77, -17.0F, -67.0F, -5.0F, 2, 10, 10, 0.0F, false));

		floor_trim4 = new RendererModel(this);
		floor_trim4.setRotationPoint(-86.0F, -0.75F, 44.0F);
		setRotationAngle(floor_trim4, 0.0F, 0.5236F, 0.0F);
		side_004.addChild(floor_trim4);
		floor_trim4.cubeList.add(new ModelBox(floor_trim4, 292, 18, 20.0F, -2.0F, -36.0F, 4, 2, 84, 0.0F, false));

		side_005 = new RendererModel(this);
		side_005.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side_005, 0.0F, 2.0944F, 0.0F);
		structure.addChild(side_005);

		skin5 = new RendererModel(this);
		skin5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(skin5, 0.0F, 0.5236F, 0.0F);
		side_005.addChild(skin5);

		floor_skin5 = new RendererModel(this);
		floor_skin5.setRotationPoint(0.0F, 0.0F, 0.0F);
		skin5.addChild(floor_skin5);

		floorboards5 = new RendererModel(this);
		floorboards5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorboards5, 3.1416F, 0.0F, 0.0F);
		floor_skin5.addChild(floorboards5);
		floorboards5.cubeList.add(new ModelBox(floorboards5, 125, 146, -74.0F, 0.25F, -40.0F, 10, 1, 78, 0.0F, false));
		floorboards5.cubeList.add(new ModelBox(floorboards5, 150, 150, -64.0F, 0.25F, -34.0F, 10, 1, 68, 0.0F, false));
		floorboards5.cubeList.add(new ModelBox(floorboards5, 176, 156, -54.0F, 0.25F, -28.0F, 10, 1, 56, 0.0F, false));
		floorboards5.cubeList.add(new ModelBox(floorboards5, 214, 162, -44.0F, 0.25F, -23.0F, 10, 1, 45, 0.0F, false));
		floorboards5.cubeList.add(new ModelBox(floorboards5, 318, 76, -34.0F, 0.5F, -17.0F, 10, 2, 34, 0.0F, false));
		floorboards5.cubeList.add(new ModelBox(floorboards5, 285, 73, -24.25F, 1.5F, -13.0F, 3, 4, 26, 0.0F, false));

		leg_skin5 = new RendererModel(this);
		leg_skin5.setRotationPoint(-23.0F, -3.0F, 0.0F);
		setRotationAngle(leg_skin5, 0.0F, 0.0F, -1.2217F);
		floor_skin5.addChild(leg_skin5);
		leg_skin5.cubeList.add(new ModelBox(leg_skin5, 319, 178, 0.0F, 0.0F, -12.0F, 20, 1, 24, 0.0F, false));

		knee_skin5 = new RendererModel(this);
		knee_skin5.setRotationPoint(16.0F, -4.0F, 0.0F);
		setRotationAngle(knee_skin5, 0.0F, 0.0F, 1.2217F);
		leg_skin5.addChild(knee_skin5);
		knee_skin5.cubeList.add(new ModelBox(knee_skin5, 329, 88, 1.0F, -16.0F, -9.0F, 4, 4, 20, 0.0F, false));
		knee_skin5.cubeList.add(new ModelBox(knee_skin5, 319, 178, 3.0F, -17.0F, -10.0F, 1, 20, 22, 0.0F, false));
		knee_skin5.cubeList.add(new ModelBox(knee_skin5, 320, 57, -2.0F, 12.0F, -13.0F, 4, 4, 27, 0.0F, false));

		thigh_skin5 = new RendererModel(this);
		thigh_skin5.setRotationPoint(1.0F, -14.0F, 0.0F);
		setRotationAngle(thigh_skin5, 0.0F, 0.0F, -0.6981F);
		knee_skin5.addChild(thigh_skin5);
		thigh_skin5.cubeList.add(new ModelBox(thigh_skin5, 35, 73, 0.75F, -13.0F, -13.0F, 1, 13, 26, 0.0F, false));

		belly_skin5 = new RendererModel(this);
		belly_skin5.setRotationPoint(2.0F, -10.0F, 0.0F);
		setRotationAngle(belly_skin5, 0.0F, 0.0F, -0.8727F);
		thigh_skin5.addChild(belly_skin5);
		belly_skin5.cubeList.add(new ModelBox(belly_skin5, 319, 178, 1.0F, -35.0F, -28.0F, 1, 19, 55, 0.0F, false));
		belly_skin5.cubeList.add(new ModelBox(belly_skin5, 305, 71, 0.5F, -16.0F, -20.0F, 2, 14, 40, 0.0F, false));

		rimtop_skin5 = new RendererModel(this);
		rimtop_skin5.setRotationPoint(4.5F, -34.0F, 0.0F);
		setRotationAngle(rimtop_skin5, 0.0F, 0.0F, 2.9671F);
		belly_skin5.addChild(rimtop_skin5);
		rimtop_skin5.cubeList.add(new ModelBox(rimtop_skin5, 319, 178, -1.5F, -8.0F, -34.0F, 1, 8, 68, 0.0F, false));

		panel_skin5 = new RendererModel(this);
		panel_skin5.setRotationPoint(-6.5F, -4.0F, 1.0F);
		setRotationAngle(panel_skin5, 0.0F, 0.0F, 3.0718F);
		rimtop_skin5.addChild(panel_skin5);
		panel_skin5.cubeList.add(new ModelBox(panel_skin5, 319, 188, -6.55F, 3.0F, -27.0F, 1, 14, 51, 0.0F, false));
		panel_skin5.cubeList.add(new ModelBox(panel_skin5, 319, 184, -6.55F, 17.0F, -19.0F, 1, 14, 36, 0.0F, false));
		panel_skin5.cubeList.add(new ModelBox(panel_skin5, 319, 178, -6.55F, 31.0F, -13.0F, 1, 12, 23, 0.0F, false));

		skelly5 = new RendererModel(this);
		skelly5.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_005.addChild(skelly5);

		rib5 = new RendererModel(this);
		rib5.setRotationPoint(-68.0F, -49.0F, -2.0F);
		setRotationAngle(rib5, 0.0F, 0.0F, -0.2618F);
		skelly5.addChild(rib5);
		rib5.cubeList.add(new ModelBox(rib5, 318, 238, 1.0F, -1.0F, -2.0F, 54, 5, 8, 0.0F, false));
		rib5.cubeList.add(new ModelBox(rib5, 325, 77, 0.0F, -3.0F, 1.0F, 55, 4, 2, 0.0F, false));

		rib_bolts_5 = new RendererModel(this);
		rib_bolts_5.setRotationPoint(68.0F, 49.0F, 2.0F);
		rib5.addChild(rib_bolts_5);
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -59.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -19.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -39.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -29.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -49.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -59.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -19.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -39.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -29.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_5.cubeList.add(new ModelBox(rib_bolts_5, 117, 104, -49.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));

		foot5 = new RendererModel(this);
		foot5.setRotationPoint(-8.0F, 0.0F, -2.0F);
		skelly5.addChild(foot5);
		foot5.cubeList.add(new ModelBox(foot5, 288, 89, -76.0F, -8.0F, 1.0F, 60, 4, 2, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 334, 144, -69.0F, -4.0F, -1.0F, 53, 4, 6, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 419, 78, -81.0F, -6.0F, -2.0F, 12, 6, 8, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 302, 0, -16.0F, -40.0F, -0.5F, 4, 33, 5, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 333, 84, -59.0F, -46.0F, -2.0F, 40, 4, 8, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 349, 84, -63.5F, -50.25F, -2.5F, 19, 8, 9, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 353, 72, -7.75F, -80.0F, -3.0F, 4, 20, 10, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 339, 74, -65.0F, -52.0F, 0.5F, 12, 12, 3, 0.0F, false));
		foot5.cubeList.add(new ModelBox(foot5, 160, 75, -9.0F, -77.4F, -4.0F, 4, 3, 12, 0.0F, false));

		leg5 = new RendererModel(this);
		leg5.setRotationPoint(-21.0F, -4.0F, 0.0F);
		setRotationAngle(leg5, 0.0F, 0.0F, 0.4363F);
		foot5.addChild(leg5);
		leg5.cubeList.add(new ModelBox(leg5, 430, 61, 0.0F, -12.0F, 0.0F, 4, 12, 4, 0.0F, false));
		leg5.cubeList.add(new ModelBox(leg5, 51, 93, -16.0F, -65.0F, 0.0F, 4, 10, 4, 0.0F, false));

		thigh5 = new RendererModel(this);
		thigh5.setRotationPoint(-8.0F, -32.0F, 0.0F);
		setRotationAngle(thigh5, 0.0F, 0.0F, -0.7854F);
		foot5.addChild(thigh5);
		thigh5.cubeList.add(new ModelBox(thigh5, 419, 56, -4.0F, -20.0F, 0.0F, 4, 18, 4, 0.0F, false));
		thigh5.cubeList.add(new ModelBox(thigh5, 406, 59, -11.0F, -26.0F, 1.0F, 4, 28, 2, 0.0F, false));

		floorpipes5 = new RendererModel(this);
		floorpipes5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorpipes5, 0.0F, 0.5236F, 0.0F);
		side_005.addChild(floorpipes5);
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 384, 57, -31.6F, -4.0F, 0.4F, 4, 2, 12, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 309, 70, -31.0F, -43.0F, -2.0F, 2, 20, 2, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 375, 69, -31.0F, -23.0F, -2.0F, 2, 2, 4, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 145, 435, -32.0F, -24.0F, 2.0F, 4, 4, 4, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 443, 91, -31.0F, -20.0F, 3.0F, 2, 20, 2, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 346, 42, -31.0F, -44.0F, 9.0F, 2, 40, 2, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 149, 430, -27.2F, -17.4F, -6.0F, 4, 7, 7, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 80, 429, -26.6F, -18.0F, -6.6F, 4, 8, 8, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 375, 82, -25.6F, -4.0F, -3.6F, 2, 2, 2, 0.0F, false));
		floorpipes5.cubeList.add(new ModelBox(floorpipes5, 345, 50, -25.0F, -10.0F, -3.0F, 1, 6, 1, 0.0F, false));

		front_rail5 = new RendererModel(this);
		front_rail5.setRotationPoint(-88.0F, 0.0F, 2.0F);
		setRotationAngle(front_rail5, 0.0F, 0.5236F, 0.0F);
		side_005.addChild(front_rail5);
		front_rail5.cubeList.add(new ModelBox(front_rail5, 345, 32, 14.7F, -46.85F, 11.0F, 2, 4, 64, 0.0F, false));

		front_railbolt5 = new RendererModel(this);
		front_railbolt5.setRotationPoint(88.0F, 0.0F, -2.0F);
		front_rail5.addChild(front_railbolt5);
		front_railbolt5.cubeList.add(new ModelBox(front_railbolt5, 137, 109, -73.55F, -45.7F, 69.5F, 2, 2, 2, 0.0F, false));
		front_railbolt5.cubeList.add(new ModelBox(front_railbolt5, 137, 109, -73.55F, -45.7F, 17.15F, 2, 2, 2, 0.0F, false));
		front_railbolt5.cubeList.add(new ModelBox(front_railbolt5, 137, 109, -73.55F, -45.7F, 44.9F, 2, 2, 2, 0.0F, false));
		front_railbolt5.cubeList.add(new ModelBox(front_railbolt5, 137, 109, -73.55F, -45.7F, 30.9F, 2, 2, 2, 0.0F, false));
		front_railbolt5.cubeList.add(new ModelBox(front_railbolt5, 137, 109, -73.55F, -45.7F, 57.9F, 2, 2, 2, 0.0F, false));

		rail_topbevel5 = new RendererModel(this);
		rail_topbevel5.setRotationPoint(16.0F, -50.0F, 43.0F);
		setRotationAngle(rail_topbevel5, 0.0F, 0.0F, 0.8727F);
		front_rail5.addChild(rail_topbevel5);
		rail_topbevel5.cubeList.add(new ModelBox(rail_topbevel5, 331, 39, 1.6F, -1.0F, -32.0F, 2, 4, 64, 0.0F, false));

		rail_underbevel5 = new RendererModel(this);
		rail_underbevel5.setRotationPoint(16.0F, -42.0F, 43.0F);
		setRotationAngle(rail_underbevel5, 0.0F, 0.0F, -0.9599F);
		front_rail5.addChild(rail_underbevel5);
		rail_underbevel5.cubeList.add(new ModelBox(rail_underbevel5, 328, 37, -0.075F, -1.55F, -35.0F, 2, 4, 69, 0.0F, false));

		rotor_gasket5 = new RendererModel(this);
		rotor_gasket5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rotor_gasket5, 0.0F, 0.5236F, 0.0F);
		side_005.addChild(rotor_gasket5);
		rotor_gasket5.cubeList.add(new ModelBox(rotor_gasket5, 304, 52, -15.0F, -70.0F, -4.0F, 2, 10, 8, 0.0F, false));
		rotor_gasket5.cubeList.add(new ModelBox(rotor_gasket5, 352, 63, -14.0F, -79.0F, -4.0F, 2, 6, 8, 0.0F, false));
		rotor_gasket5.cubeList.add(new ModelBox(rotor_gasket5, 136, 96, -17.0F, -76.9F, -3.0F, 2, 2, 6, 0.0F, false));
		rotor_gasket5.cubeList.add(new ModelBox(rotor_gasket5, 297, 77, -17.0F, -67.0F, -5.0F, 2, 10, 10, 0.0F, false));

		floor_trim5 = new RendererModel(this);
		floor_trim5.setRotationPoint(-86.0F, -0.75F, 44.0F);
		setRotationAngle(floor_trim5, 0.0F, 0.5236F, 0.0F);
		side_005.addChild(floor_trim5);
		floor_trim5.cubeList.add(new ModelBox(floor_trim5, 292, 18, 20.0F, -2.0F, -36.0F, 4, 2, 84, 0.0F, false));

		side_006 = new RendererModel(this);
		side_006.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(side_006, 0.0F, 1.0472F, 0.0F);
		structure.addChild(side_006);

		skin6 = new RendererModel(this);
		skin6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(skin6, 0.0F, 0.5236F, 0.0F);
		side_006.addChild(skin6);

		floor_skin6 = new RendererModel(this);
		floor_skin6.setRotationPoint(0.0F, 0.0F, 0.0F);
		skin6.addChild(floor_skin6);

		floorboards6 = new RendererModel(this);
		floorboards6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorboards6, 3.1416F, 0.0F, 0.0F);
		floor_skin6.addChild(floorboards6);
		floorboards6.cubeList.add(new ModelBox(floorboards6, 125, 146, -74.0F, 0.25F, -40.0F, 10, 1, 78, 0.0F, false));
		floorboards6.cubeList.add(new ModelBox(floorboards6, 150, 150, -64.0F, 0.25F, -34.0F, 10, 1, 68, 0.0F, false));
		floorboards6.cubeList.add(new ModelBox(floorboards6, 176, 156, -54.0F, 0.25F, -28.0F, 10, 1, 56, 0.0F, false));
		floorboards6.cubeList.add(new ModelBox(floorboards6, 214, 162, -44.0F, 0.25F, -23.0F, 10, 1, 45, 0.0F, false));
		floorboards6.cubeList.add(new ModelBox(floorboards6, 318, 76, -34.0F, 0.5F, -17.0F, 10, 2, 34, 0.0F, false));
		floorboards6.cubeList.add(new ModelBox(floorboards6, 285, 73, -24.25F, 1.5F, -13.0F, 3, 4, 26, 0.0F, false));

		leg_skin6 = new RendererModel(this);
		leg_skin6.setRotationPoint(-23.0F, -3.0F, 0.0F);
		setRotationAngle(leg_skin6, 0.0F, 0.0F, -1.2217F);
		floor_skin6.addChild(leg_skin6);
		leg_skin6.cubeList.add(new ModelBox(leg_skin6, 319, 178, 0.0F, 0.0F, -12.0F, 20, 1, 24, 0.0F, false));

		knee_skin6 = new RendererModel(this);
		knee_skin6.setRotationPoint(16.0F, -4.0F, 0.0F);
		setRotationAngle(knee_skin6, 0.0F, 0.0F, 1.2217F);
		leg_skin6.addChild(knee_skin6);
		knee_skin6.cubeList.add(new ModelBox(knee_skin6, 329, 88, 1.0F, -16.0F, -9.0F, 4, 4, 20, 0.0F, false));
		knee_skin6.cubeList.add(new ModelBox(knee_skin6, 319, 178, 3.0F, -17.0F, -10.0F, 1, 20, 22, 0.0F, false));
		knee_skin6.cubeList.add(new ModelBox(knee_skin6, 320, 57, -2.0F, 12.0F, -13.0F, 4, 4, 27, 0.0F, false));

		thigh_skin6 = new RendererModel(this);
		thigh_skin6.setRotationPoint(1.0F, -14.0F, 0.0F);
		setRotationAngle(thigh_skin6, 0.0F, 0.0F, -0.6981F);
		knee_skin6.addChild(thigh_skin6);
		thigh_skin6.cubeList.add(new ModelBox(thigh_skin6, 35, 73, 0.75F, -13.0F, -13.0F, 1, 13, 26, 0.0F, false));

		belly_skin6 = new RendererModel(this);
		belly_skin6.setRotationPoint(2.0F, -10.0F, 0.0F);
		setRotationAngle(belly_skin6, 0.0F, 0.0F, -0.8727F);
		thigh_skin6.addChild(belly_skin6);
		belly_skin6.cubeList.add(new ModelBox(belly_skin6, 319, 178, 1.0F, -35.0F, -28.0F, 1, 19, 55, 0.0F, false));
		belly_skin6.cubeList.add(new ModelBox(belly_skin6, 305, 71, 0.5F, -16.0F, -20.0F, 2, 14, 40, 0.0F, false));

		rimtop_skin6 = new RendererModel(this);
		rimtop_skin6.setRotationPoint(4.5F, -34.0F, 0.0F);
		setRotationAngle(rimtop_skin6, 0.0F, 0.0F, 2.9671F);
		belly_skin6.addChild(rimtop_skin6);
		rimtop_skin6.cubeList.add(new ModelBox(rimtop_skin6, 319, 178, -1.5F, -8.0F, -34.0F, 1, 8, 68, 0.0F, false));

		panel_skin6 = new RendererModel(this);
		panel_skin6.setRotationPoint(-6.5F, -4.0F, 1.0F);
		setRotationAngle(panel_skin6, 0.0F, 0.0F, 3.0718F);
		rimtop_skin6.addChild(panel_skin6);
		panel_skin6.cubeList.add(new ModelBox(panel_skin6, 319, 188, -6.55F, 3.0F, -27.0F, 1, 14, 51, 0.0F, false));
		panel_skin6.cubeList.add(new ModelBox(panel_skin6, 319, 184, -6.55F, 17.0F, -19.0F, 1, 14, 36, 0.0F, false));
		panel_skin6.cubeList.add(new ModelBox(panel_skin6, 319, 178, -6.55F, 31.0F, -13.0F, 1, 12, 23, 0.0F, false));

		skelly6 = new RendererModel(this);
		skelly6.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_006.addChild(skelly6);

		rib6 = new RendererModel(this);
		rib6.setRotationPoint(-68.0F, -49.0F, -2.0F);
		setRotationAngle(rib6, 0.0F, 0.0F, -0.2618F);
		skelly6.addChild(rib6);
		rib6.cubeList.add(new ModelBox(rib6, 318, 238, 1.0F, -1.0F, -2.0F, 54, 5, 8, 0.0F, false));
		rib6.cubeList.add(new ModelBox(rib6, 325, 77, 0.0F, -3.0F, 1.0F, 55, 4, 2, 0.0F, false));

		rib_bolts_6 = new RendererModel(this);
		rib_bolts_6.setRotationPoint(68.0F, 49.0F, 2.0F);
		rib6.addChild(rib_bolts_6);
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -59.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -19.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -39.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -29.75F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -49.5F, -50.25F, 1.5F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -59.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -19.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -39.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -29.75F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));
		rib_bolts_6.cubeList.add(new ModelBox(rib_bolts_6, 117, 104, -49.5F, -50.25F, -3.6F, 2, 1, 2, 0.0F, false));

		foot6 = new RendererModel(this);
		foot6.setRotationPoint(-8.0F, 0.0F, -2.0F);
		skelly6.addChild(foot6);
		foot6.cubeList.add(new ModelBox(foot6, 288, 89, -76.0F, -8.0F, 1.0F, 60, 4, 2, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 334, 144, -69.0F, -4.0F, -1.0F, 53, 4, 6, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 419, 78, -81.0F, -6.0F, -2.0F, 12, 6, 8, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 302, 0, -16.0F, -40.0F, -0.5F, 4, 33, 5, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 333, 84, -59.0F, -46.0F, -2.0F, 40, 4, 8, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 349, 84, -63.5F, -50.25F, -2.5F, 19, 8, 9, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 353, 72, -7.75F, -80.0F, -3.0F, 4, 20, 10, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 339, 74, -65.0F, -52.0F, 0.5F, 12, 12, 3, 0.0F, false));
		foot6.cubeList.add(new ModelBox(foot6, 160, 75, -9.0F, -77.4F, -4.0F, 4, 3, 12, 0.0F, false));

		leg6 = new RendererModel(this);
		leg6.setRotationPoint(-21.0F, -4.0F, 0.0F);
		setRotationAngle(leg6, 0.0F, 0.0F, 0.4363F);
		foot6.addChild(leg6);
		leg6.cubeList.add(new ModelBox(leg6, 430, 61, 0.0F, -12.0F, 0.0F, 4, 12, 4, 0.0F, false));
		leg6.cubeList.add(new ModelBox(leg6, 51, 93, -16.0F, -65.0F, 0.0F, 4, 10, 4, 0.0F, false));

		thigh6 = new RendererModel(this);
		thigh6.setRotationPoint(-8.0F, -32.0F, 0.0F);
		setRotationAngle(thigh6, 0.0F, 0.0F, -0.7854F);
		foot6.addChild(thigh6);
		thigh6.cubeList.add(new ModelBox(thigh6, 419, 56, -4.0F, -20.0F, 0.0F, 4, 18, 4, 0.0F, false));
		thigh6.cubeList.add(new ModelBox(thigh6, 406, 59, -11.0F, -26.0F, 1.0F, 4, 28, 2, 0.0F, false));

		floorpipes6 = new RendererModel(this);
		floorpipes6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(floorpipes6, 0.0F, 0.5236F, 0.0F);
		side_006.addChild(floorpipes6);
		floorpipes6.cubeList.add(new ModelBox(floorpipes6, 384, 57, -31.6F, -4.0F, 0.4F, 4, 2, 12, 0.0F, false));
		floorpipes6.cubeList.add(new ModelBox(floorpipes6, 309, 70, -31.0F, -43.0F, -2.0F, 2, 20, 2, 0.0F, false));
		floorpipes6.cubeList.add(new ModelBox(floorpipes6, 375, 69, -31.0F, -23.0F, -2.0F, 2, 2, 4, 0.0F, false));
		floorpipes6.cubeList.add(new ModelBox(floorpipes6, 145, 435, -32.0F, -24.0F, 2.0F, 4, 4, 4, 0.0F, false));
		floorpipes6.cubeList.add(new ModelBox(floorpipes6, 443, 91, -31.0F, -20.0F, 3.0F, 2, 20, 2, 0.0F, false));
		floorpipes6.cubeList.add(new ModelBox(floorpipes6, 346, 42, -31.0F, -44.0F, 9.0F, 2, 40, 2, 0.0F, false));

		front_rail6 = new RendererModel(this);
		front_rail6.setRotationPoint(-88.0F, 0.0F, 2.0F);
		setRotationAngle(front_rail6, 0.0F, 0.5236F, 0.0F);
		side_006.addChild(front_rail6);
		front_rail6.cubeList.add(new ModelBox(front_rail6, 345, 32, 14.7F, -46.85F, 11.0F, 2, 4, 64, 0.0F, false));

		front_railbolt6 = new RendererModel(this);
		front_railbolt6.setRotationPoint(88.0F, 0.0F, -2.0F);
		front_rail6.addChild(front_railbolt6);
		front_railbolt6.cubeList.add(new ModelBox(front_railbolt6, 137, 109, -73.55F, -45.7F, 69.5F, 2, 2, 2, 0.0F, false));
		front_railbolt6.cubeList.add(new ModelBox(front_railbolt6, 137, 109, -73.55F, -45.7F, 17.15F, 2, 2, 2, 0.0F, false));
		front_railbolt6.cubeList.add(new ModelBox(front_railbolt6, 137, 109, -73.55F, -45.7F, 44.9F, 2, 2, 2, 0.0F, false));
		front_railbolt6.cubeList.add(new ModelBox(front_railbolt6, 137, 109, -73.55F, -45.7F, 30.9F, 2, 2, 2, 0.0F, false));
		front_railbolt6.cubeList.add(new ModelBox(front_railbolt6, 137, 109, -73.55F, -45.7F, 57.9F, 2, 2, 2, 0.0F, false));

		rail_topbevel6 = new RendererModel(this);
		rail_topbevel6.setRotationPoint(16.0F, -50.0F, 43.0F);
		setRotationAngle(rail_topbevel6, 0.0F, 0.0F, 0.8727F);
		front_rail6.addChild(rail_topbevel6);
		rail_topbevel6.cubeList.add(new ModelBox(rail_topbevel6, 331, 39, 1.6F, -1.0F, -32.0F, 2, 4, 64, 0.0F, false));

		rail_underbevel6 = new RendererModel(this);
		rail_underbevel6.setRotationPoint(16.0F, -42.0F, 43.0F);
		setRotationAngle(rail_underbevel6, 0.0F, 0.0F, -0.9599F);
		front_rail6.addChild(rail_underbevel6);
		rail_underbevel6.cubeList.add(new ModelBox(rail_underbevel6, 328, 37, -0.075F, -1.55F, -35.0F, 2, 4, 69, 0.0F, false));

		rotor_gasket6 = new RendererModel(this);
		rotor_gasket6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rotor_gasket6, 0.0F, 0.5236F, 0.0F);
		side_006.addChild(rotor_gasket6);
		rotor_gasket6.cubeList.add(new ModelBox(rotor_gasket6, 304, 52, -15.0F, -70.0F, -4.0F, 2, 10, 8, 0.0F, false));
		rotor_gasket6.cubeList.add(new ModelBox(rotor_gasket6, 352, 63, -14.0F, -79.0F, -4.0F, 2, 6, 8, 0.0F, false));
		rotor_gasket6.cubeList.add(new ModelBox(rotor_gasket6, 136, 96, -17.0F, -76.9F, -3.0F, 2, 2, 6, 0.0F, false));
		rotor_gasket6.cubeList.add(new ModelBox(rotor_gasket6, 297, 77, -17.0F, -67.0F, -5.0F, 2, 10, 10, 0.0F, false));

		floor_trim6 = new RendererModel(this);
		floor_trim6.setRotationPoint(-86.0F, -0.75F, 44.0F);
		setRotationAngle(floor_trim6, 0.0F, 0.5236F, 0.0F);
		side_006.addChild(floor_trim6);
		floor_trim6.cubeList.add(new ModelBox(floor_trim6, 292, 18, 20.0F, -2.0F, -36.0F, 4, 2, 84, 0.0F, false));
	}

	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}
	
	public void render(float f5) {
		structure.render(f5);
		controls.render(f5);
		rotor.render(f5);
		glow.render(f5);
	}
	
	@Override
	public void render(ConsoleTile console, float scale) {
		GlStateManager.pushMatrix();
		
		//Rotor animation
		float rotorAnim = (float)(Math.cos(console.flightTicks * 0.1) * 0.5F);
		this.rotor.rotateAngleY = (float) Math.toRadians((console.flightTicks - Minecraft.getInstance().getRenderPartialTicks()) % 360);
		this.rotor.offsetY = rotorAnim + 0.5F;
		
		//throttle animation
		ThrottleControl throttle = console.getControl(ThrottleControl.class);
		if(throttle != null)
			this.leaver_b1_rotate_z.rotateAngleZ = (float) Math.toRadians(90 - (float) 180 * throttle.getAmount());
		
		//Demat lever
		HandbrakeControl flight = console.getControl(HandbrakeControl.class);
		if(flight != null)
			this.lever_f1_rotate_z.rotateAngleZ = (float)Math.toRadians(flight.isFree() ? 10 : 170);
		
		//Randomizer control
		RandomiserControl randomizer = console.getControl(RandomiserControl.class);
		if(randomizer != null)
			this.globe_rotate_y.rotateAngleY = (float)Math.toRadians((randomizer.getAnimationTicks() * 36) + Minecraft.getInstance().getRenderPartialTicks() % 360.0);

		FacingControl facing = console.getControl(FacingControl.class);
		if(facing != null)
			this.rotation_crank_rotate_y.rotateAngleY = (float)Math.toRadians(
					facing.getDirection() == Direction.EAST ? 90 :
						(facing.getDirection() == Direction.SOUTH ? 180 :
						(facing.getDirection() == Direction.WEST ? 270 : 0)));
		
		IncModControl coord = console.getControl(IncModControl.class);
		if(coord != null) {
			this.cord_slider_slide_x.offsetX = (coord.index / (float)IncModControl.COORD_MODS.length) * -0.6F;
		}
		
		CommunicatorControl communicator = console.getControl(CommunicatorControl.class);
		if(communicator != null)
			this.radio_needle.offsetZ = (float) Math.sin(Math.toRadians(((20 - communicator.getAnimationTicks()) * 40) * 0.1)) * 0.75F;
			
		this.needle_a1_rotate_y.rotateAngleY = (float)Math.toRadians(45);
		
		structure.render(scale);
		controls.render(scale);
		rotor.render(scale);
		
		ModelHelper.renderPartBrightness(1F, glow);
		
		GlStateManager.popMatrix();
		
		GlStateManager.pushMatrix();
		GlStateManager.scaled(2, 2, 2);
		GlStateManager.translated(0.9, 0, 1);
		GlStateManager.rotated(24, -1, 0, 1);
		GlStateManager.translated(0.3, -0.25, 0.25);
		Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
	}
	
}