package net.tardis.mod.client.models.consoles;// Made with Blockbench
// Paste this code into your mod.
// Make sure to generate all required imports

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.controls.FastReturnControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.tileentities.ConsoleTile;

public class ModelConsoleCopper extends Model implements IConsoleModel {
	private final RendererModel rotor;
	private final RendererModel rotortop;
	private final RendererModel copper;
	private final RendererModel controls;
	private final RendererModel panelone;
	private final RendererModel greyshit;
	private final RendererModel blackbordnon;
	private final RendererModel blackboy2;
	private final RendererModel misc;
	private final RendererModel blackboy;
	private final RendererModel handbreak;
	private final RendererModel blackboard;
	private final RendererModel throttle;
	private final RendererModel levers;
	private final RendererModel white;
	private final RendererModel redswitches;
	private final RendererModel paneltwo;
	private final RendererModel whity;
	private final RendererModel slider;
	private final RendererModel rotator;
	private final RendererModel bone109;
	private final RendererModel bone113;
	private final RendererModel keyboard;
	private final RendererModel panelthree;
	private final RendererModel bluevalves;
	private final RendererModel bluevalves2;
	private final RendererModel plugandkey;
	private final RendererModel panelfour;
	private final RendererModel panelfour2;
	private final RendererModel panelfive;
	private final RendererModel panelfive2;
	private final RendererModel bone120;
	private final RendererModel panelfive3;
	private final RendererModel bone121;
	private final RendererModel panelsix;
	private final RendererModel wibbly;
	private final RendererModel pumps;
	private final RendererModel pump;
	private final RendererModel pump3;
	private final RendererModel redlever;
	private final RendererModel bigboy;
	private final RendererModel blackboy3;
	private final RendererModel pump4;
	private final RendererModel pump5;
	private final RendererModel redlever2;
	private final RendererModel bigreder;
	private final RendererModel smallblack;
	private final RendererModel falsepump;
	private final RendererModel bone112;
	private final RendererModel rotorframe;
	private final RendererModel bone110;
	private final RendererModel bone111;
	private final RendererModel unit;
	private final RendererModel console2;
	private final RendererModel model;
	private final RendererModel console;
	private final RendererModel frame;
	private final RendererModel upper;
	private final RendererModel bone4;
	private final RendererModel monster2;
	private final RendererModel bone5;
	private final RendererModel monster;
	private final RendererModel upper2;
	private final RendererModel bone6;
	private final RendererModel bone59;
	private final RendererModel yeeeet;
	private final RendererModel bone71;
	private final RendererModel bone57;
	private final RendererModel bone58;
	private final RendererModel bone7;
	private final RendererModel bone72;
	private final RendererModel upper3;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel bone70;
	private final RendererModel upp;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel bone13;
	private final RendererModel bone14;
	private final RendererModel bone15;
	private final RendererModel bone16;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel bone17;
	private final RendererModel frame2;
	private final RendererModel upper4;
	private final RendererModel bone10;
	private final RendererModel bone20;
	private final RendererModel upper5;
	private final RendererModel bone21;
	private final RendererModel bone22;
	private final RendererModel upper6;
	private final RendererModel bone23;
	private final RendererModel bone24;
	private final RendererModel upp2;
	private final RendererModel bone25;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel upp3;
	private final RendererModel bone39;
	private final RendererModel bone40;
	private final RendererModel bone41;
	private final RendererModel bone28;
	private final RendererModel bone29;
	private final RendererModel bone30;
	private final RendererModel bone31;
	private final RendererModel bone32;
	private final RendererModel bone33;
	private final RendererModel cubes3;
	private final RendererModel cubes2;
	private final RendererModel cubes;
	private final RendererModel outerframe;
	private final RendererModel bone;
	private final RendererModel bone56;
	private final RendererModel bone2;
	private final RendererModel bone54;
	private final RendererModel bone3;
	private final RendererModel bone55;
	private final RendererModel bone119;
	private final RendererModel hollowglow;
	private final RendererModel bone36;
	private final RendererModel bone35;
	private final RendererModel bone34;
	private final RendererModel ihaveexam;
	private final RendererModel bone42;
	private final RendererModel bone38;
	private final RendererModel rando;
	private final RendererModel yes;
	private final RendererModel bone60;
	private final RendererModel bone65;
	private final RendererModel bone66;
	private final RendererModel bone67;
	private final RendererModel bone68;
	private final RendererModel bone69;
	private final RendererModel ihaveexam2;
	private final RendererModel bone43;
	private final RendererModel bone44;
	private final RendererModel bone45;
	private final RendererModel ihaveexam3;
	private final RendererModel bone48;
	private final RendererModel bone47;
	private final RendererModel bone52;
	private final RendererModel bone53;
	private final RendererModel bone46;
	private final RendererModel bone49;
	private final RendererModel bone50;
	private final RendererModel bone62;
	private final RendererModel bone63;
	private final RendererModel bone51;
	private final RendererModel bone73;
	private final RendererModel bone37;
	private final RendererModel bone64;
	private final RendererModel underpillar3;
	private final RendererModel bone79;
	private final RendererModel bone80;
	private final RendererModel bone81;
	private final RendererModel underpillar2;
	private final RendererModel bone61;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel underpillar4;
	private final RendererModel bone76;
	private final RendererModel bone77;
	private final RendererModel bone78;
	private final RendererModel underpillar5;
	private final RendererModel bone82;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel underpillar6;
	private final RendererModel bone85;
	private final RendererModel bone86;
	private final RendererModel bone87;
	private final RendererModel underpillar7;
	private final RendererModel bone88;
	private final RendererModel bone89;
	private final RendererModel bone90;
	private final RendererModel greenfloor;
	private final RendererModel bone91;
	private final RendererModel bone92;
	private final RendererModel bone93;
	private final RendererModel bone94;
	private final RendererModel bone95;
	private final RendererModel bone96;
	private final RendererModel greenfloor2;
	private final RendererModel bone97;
	private final RendererModel bone98;
	private final RendererModel bone99;
	private final RendererModel bone100;
	private final RendererModel bone101;
	private final RendererModel bone102;
	private final RendererModel greenwall;
	private final RendererModel wall;
	private final RendererModel under6;
	private final RendererModel bone108;
	private final RendererModel under5;
	private final RendererModel bone107;
	private final RendererModel under4;
	private final RendererModel bone106;
	private final RendererModel under3;
	private final RendererModel bone105;
	private final RendererModel under2;
	private final RendererModel bone104;
	private final RendererModel under;
	private final RendererModel bone103;
	private final RendererModel pump2;

	public ModelConsoleCopper() {
		textureWidth = 512;
		textureHeight = 512;

		rotor = new RendererModel(this);
        rotor.setRotationPoint(0.0F, 24.0F, 0.0F);
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -7.0F, -90.0F, -7.0F, 14, 8, 14, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -10.0F, -82.0F, -10.0F, 20, 17, 20, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -6.0F, -98.0F, -6.0F, 12, 8, 12, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -4.0F, -106.0F, -4.0F, 8, 8, 8, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -2.0F, -109.0F, -2.0F, 4, 3, 4, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -2.0F, -116.0F, -2.0F, 4, 3, 4, 0.0F, false));
		rotor.cubeList.add(new ModelBox(rotor, 418, 61, -3.0F, -113.0F, -3.0F, 6, 4, 6, 0.0F, false));

		rotortop = new RendererModel(this);
		rotortop.setRotationPoint(0.0F, 24.0F, 0.0F);
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -7.0F, -192.0F, -7.0F, 14, 8, 14, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 416, 49, -10.0F, -184.0F, -10.0F, 20, 17, 20, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -6.0F, -205.0F, -6.0F, 12, 13, 12, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -4.0F, -216.0F, -4.0F, 8, 12, 8, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -2.0F, -221.0F, -2.0F, 4, 14, 4, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -4.0F, -232.0F, -4.0F, 8, 11, 8, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -7.0F, -243.0F, -7.0F, 14, 13, 14, 0.0F, false));
		rotortop.cubeList.add(new ModelBox(rotortop, 418, 61, -7.0F, -167.0F, -7.0F, 14, 2, 14, 0.0F, false));

		copper = new RendererModel(this);
		copper.setRotationPoint(0.0F, 24.0F, 0.0F);

		controls = new RendererModel(this);
		controls.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(controls);

		panelone = new RendererModel(this);
		panelone.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(panelone);

		greyshit = new RendererModel(this);
		greyshit.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(greyshit, 0.3491F, -0.5236F, 0.0F);
		panelone.addChild(greyshit);
		greyshit.cubeList.add(new ModelBox(greyshit, 399, 184, 4.5F, -70.7F, -28.0F, 3, 4, 5, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 399, 184, 4.5F, -69.7F, -21.0F, 3, 3, 3, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 399, 184, 4.5F, -69.7F, -33.0F, 3, 3, 3, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 399, 197, 5.5F, -69.7F, -30.0F, 1, 3, 3, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 399, 197, 5.5F, -69.7F, -24.0F, 1, 3, 3, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 339, 507, -8.5F, -69.7F, -38.0F, 5, 4, 8, 0.0F, false));
		greyshit.cubeList.add(new ModelBox(greyshit, 430, 206, -7.5F, -69.2F, -12.0F, 3, 4, 3, 0.0F, false));

		blackbordnon = new RendererModel(this);
		blackbordnon.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackbordnon, 0.0F, -1.0472F, 0.0F);
		panelone.addChild(blackbordnon);
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 380, 256, -5.5F, -76.7F, -24.0F, 3, 7, 4, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 389, 180, -11.5F, -63.7F, -31.0F, 3, 3, 2, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 397, 195, -10.5F, -64.7F, -29.0F, 3, 4, 2, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 411, 439, -5.5F, -80.7F, -28.0F, 3, 3, 5, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 411, 439, -5.5F, -77.7F, -27.0F, 3, 1, 5, 0.0F, false));
		blackbordnon.cubeList.add(new ModelBox(blackbordnon, 380, 256, -4.9F, -81.7F, -27.6F, 2, 1, 2, 0.0F, false));

		blackboy2 = new RendererModel(this);
		blackboy2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackboy2, 0.0F, -0.5236F, 0.0F);
		panelone.addChild(blackboy2);
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, 3.5F, -47.7F, -64.0F, 2, 3, 2, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, 6.5F, -45.7F, -64.0F, 2, 3, 2, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, 3.5F, -47.7F, -64.0F, 2, 3, 2, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, 4.5F, -42.7F, -64.0F, 1, 1, 2, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, -8.5F, -46.7F, -64.0F, 5, 5, 2, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, -7.5F, -45.7F, -65.0F, 3, 3, 2, 0.0F, false));
		blackboy2.cubeList.add(new ModelBox(blackboy2, 358, 237, -6.5F, -44.7F, -66.0F, 1, 1, 2, 0.0F, false));

		misc = new RendererModel(this);
		misc.setRotationPoint(0.0F, 0.0F, 0.0F);
		blackboy2.addChild(misc);

		blackboy = new RendererModel(this);
		blackboy.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackboy, 0.0F, -1.5708F, 0.0F);
		panelone.addChild(blackboy);
		blackboy.cubeList.add(new ModelBox(blackboy, 230, 320, 3.5F, -45.7F, -65.0F, 5, 4, 3, 0.0F, false));
		blackboy.cubeList.add(new ModelBox(blackboy, 230, 320, 5.5F, -47.7F, -65.0F, 3, 2, 3, 0.0F, false));
		blackboy.cubeList.add(new ModelBox(blackboy, 384, 229, -8.5F, -46.7F, -64.0F, 5, 5, 2, 0.0F, false));
		blackboy.cubeList.add(new ModelBox(blackboy, 384, 229, -7.5F, -45.7F, -65.0F, 3, 3, 2, 0.0F, false));

		handbreak = new RendererModel(this);
		handbreak.setRotationPoint(0.0F, -44.0F, -62.0F);
		setRotationAngle(handbreak, 0.6109F, 0.0F, 0.0F);
		blackboy.addChild(handbreak);
		handbreak.cubeList.add(new ModelBox(handbreak, 384, 229, -2.4F, -2.9676F, -1.2216F, 5, 5, 4, 0.0F, false));
		handbreak.cubeList.add(new ModelBox(handbreak, 384, 229, -0.8F, -16.9676F, -2.2216F, 2, 16, 2, 0.0F, false));

		blackboard = new RendererModel(this);
		blackboard.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(blackboard, -0.0873F, -1.0472F, 0.0F);
		panelone.addChild(blackboard);
		blackboard.cubeList.add(new ModelBox(blackboard, 498, 250, -1.5F, -71.7F, -30.0F, 9, 6, 1, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 380, 256, -7.5F, -75.7F, -30.0F, 2, 6, 2, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, -0.5F, -70.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 1.5F, -70.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 3.5F, -70.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 5.5F, -70.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, -0.5F, -68.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 1.5F, -68.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 3.5F, -68.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 5.5F, -68.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 4.5F, -67.7F, -30.1F, 1, 1, 0, 0.0F, false));
		blackboard.cubeList.add(new ModelBox(blackboard, 459, 359, 2.5F, -67.7F, -30.1F, 1, 1, 0, 0.0F, false));

		throttle = new RendererModel(this);
		throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(throttle, 0.0F, -1.5708F, 0.3491F);
		panelone.addChild(throttle);
		throttle.cubeList.add(new ModelBox(throttle, 267, 316, -8.0F, -71.0F, -24.0F, 4, 7, 10, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 267, 316, -8.0F, -73.0F, -22.0F, 4, 2, 6, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 22, 271, -9.0F, -71.5F, -20.0F, 1, 1, 2, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -8.0F, -73.0F, -18.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -8.0F, -73.0F, -21.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -8.0F, -71.0F, -16.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -8.0F, -71.0F, -23.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -5.0F, -71.0F, -23.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -5.0F, -73.0F, -21.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -5.0F, -73.0F, -18.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 331, 374, -5.0F, -71.0F, -16.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 370, 161, 4.0F, -69.0F, -32.0F, 4, 5, 9, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 416, 225, 6.5F, -70.0F, -31.0F, 1, 0, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 370, 161, 4.5F, -70.0F, -32.0F, 3, 1, 8, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 370, 161, 6.5F, -69.0F, -33.0F, 1, 1, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 370, 161, 4.5F, -69.0F, -33.0F, 1, 1, 1, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 370, 161, 6.5F, -69.0F, -24.0F, 1, 2, 2, 0.0F, false));
		throttle.cubeList.add(new ModelBox(throttle, 370, 161, 4.5F, -69.0F, -24.0F, 1, 2, 2, 0.0F, false));

		levers = new RendererModel(this);
		levers.setRotationPoint(-5.6429F, -69.4286F, -19.5714F);
		throttle.addChild(levers);
		levers.cubeList.add(new ModelBox(levers, 408, 220, 1.6429F, -1.0714F, -0.4286F, 1, 2, 4, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 391, 203, 1.6429F, -0.0714F, 3.5714F, 1, 1, 5, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 416, 225, 0.6429F, -0.0714F, 8.5714F, 2, 1, 1, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 416, 225, -3.3571F, -0.0714F, 8.5714F, 2, 1, 1, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 391, 203, -3.3571F, -0.0714F, 3.5714F, 1, 1, 5, 0.0F, false));
		levers.cubeList.add(new ModelBox(levers, 408, 220, -3.3571F, -1.0714F, -0.4286F, 1, 2, 4, 0.0F, false));

		white = new RendererModel(this);
		white.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(white, 0.0F, -1.5708F, 0.3491F);
		panelone.addChild(white);
		white.cubeList.add(new ModelBox(white, 416, 225, 4.5F, -70.0F, -31.0F, 1, 0, 1, 0.0F, false));

		redswitches = new RendererModel(this);
		redswitches.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(redswitches, 0.3491F, -1.0472F, 0.0F);
		panelone.addChild(redswitches);
		redswitches.cubeList.add(new ModelBox(redswitches, 389, 400, -1.0F, -66.0F, -28.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 389, 400, -4.0F, -66.0F, -18.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 389, 400, 2.0F, -66.0F, -18.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 389, 400, 4.0F, -66.0F, -23.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 389, 400, -6.0F, -66.0F, -23.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 388, 225, -1.0F, -66.0F, -23.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 129, 446, -9.0F, -66.0F, -31.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 448, 233, -14.0F, -66.0F, -31.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 448, 233, -13.0F, -66.0F, -27.0F, 2, 4, 2, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 407, 291, 5.0F, -66.0F, -32.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 478, 222, -12.5F, -66.4F, -26.5F, 1, 1, 1, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 478, 222, -13.5F, -66.4F, -30.5F, 1, 1, 1, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 407, 291, 1.0F, -65.5F, -19.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 407, 291, -5.0F, -65.5F, -19.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 407, 291, -7.0F, -65.5F, -24.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 407, 291, 3.0F, -65.5F, -24.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 407, 291, -2.0F, -65.5F, -29.0F, 4, 4, 4, 0.0F, false));
		redswitches.cubeList.add(new ModelBox(redswitches, 416, 239, -0.5F, -67.0F, -22.5F, 1, 1, 1, 0.0F, false));

		paneltwo = new RendererModel(this);
		paneltwo.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(paneltwo);

		whity = new RendererModel(this);
		whity.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(whity, 0.0F, -1.5708F, 0.3491F);
		paneltwo.addChild(whity);
		whity.cubeList.add(new ModelBox(whity, 370, 161, 4.0F, -69.0F, -32.0F, 4, 5, 9, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 416, 225, 4.5F, -70.0F, -31.0F, 1, 0, 1, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 416, 225, 6.5F, -70.0F, -31.0F, 1, 0, 1, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 370, 161, 4.5F, -70.0F, -32.0F, 3, 1, 8, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 370, 161, 6.5F, -69.0F, -33.0F, 1, 1, 1, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 370, 161, 4.5F, -69.0F, -33.0F, 1, 1, 1, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 370, 161, 6.5F, -69.0F, -24.0F, 1, 2, 2, 0.0F, false));
		whity.cubeList.add(new ModelBox(whity, 370, 161, 4.5F, -69.0F, -24.0F, 1, 2, 2, 0.0F, false));

		slider = new RendererModel(this);
		slider.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(slider, 0.3491F, -2.0944F, 0.0F);
		paneltwo.addChild(slider);
		slider.cubeList.add(new ModelBox(slider, 348, 165, -17.0F, -69.0F, -17.0F, 6, 4, 4, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 397, 260, -17.0F, -69.1F, -16.0F, 6, 0, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 374, 197, -3.0F, -65.4F, -23.0F, 5, 4, 5, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 381, 411, -2.0F, -65.4F, -29.0F, 3, 3, 3, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 346, 397, -1.5F, -65.4F, -28.5F, 2, 0, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 480, 420, -10.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, -10.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 480, 420, -7.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, -7.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, -7.0F, -64.4F, -48.0F, 6, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 292, 356, -4.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, -4.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 292, 356, -1.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, -1.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, -1.0F, -64.4F, -48.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 130, 438, 2.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, 2.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 369, 253, 2.0F, -64.4F, -48.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 169, 494, 5.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 356, 376, 5.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 356, 376, 5.0F, -64.4F, -48.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 106, 360, 8.0F, -64.4F, -42.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 356, 376, 8.0F, -64.4F, -45.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 356, 376, 8.0F, -64.4F, -48.0F, 2, 2, 2, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 416, 209, -9.0F, -66.0F, -28.0F, 4, 3, 4, 0.0F, false));
		slider.cubeList.add(new ModelBox(slider, 416, 209, -8.0F, -67.0F, -27.0F, 2, 1, 2, 0.0F, false));

		rotator = new RendererModel(this);
		rotator.setRotationPoint(39.0F, -62.0F, 21.0F);
		setRotationAngle(rotator, 0.3491F, -2.0944F, 0.0F);
		paneltwo.addChild(rotator);
		rotator.cubeList.add(new ModelBox(rotator, 334, 474, -1.1865F, 3.4039F, -2.2004F, 4, 3, 4, 0.0F, false));
		rotator.cubeList.add(new ModelBox(rotator, 334, 474, -0.1865F, 6.4039F, -1.2004F, 2, 6, 2, 0.0F, false));
		rotator.cubeList.add(new ModelBox(rotator, 334, 474, -0.1865F, 1.4039F, -1.2004F, 2, 3, 2, 0.0F, false));

		bone109 = new RendererModel(this);
		bone109.setRotationPoint(1.3135F, 73.4039F, 20.3996F);
		setRotationAngle(bone109, 0.0F, 0.0F, -0.3491F);
		rotator.addChild(bone109);
		bone109.cubeList.add(new ModelBox(bone109, 111, 409, 19.0F, -65.5F, -21.6F, 8, 2, 2, 0.0F, false));

		bone113 = new RendererModel(this);
		bone113.setRotationPoint(1.3135F, 49.4039F, 20.3996F);
		setRotationAngle(bone113, -0.3491F, -1.5708F, -0.3491F);
		rotator.addChild(bone113);
		bone113.cubeList.add(new ModelBox(bone113, 241, 425, -24.6681F, -35.5743F, -28.8171F, 8, 2, 2, 0.0F, false));

		keyboard = new RendererModel(this);
		keyboard.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(keyboard, -0.3491F, 1.0472F, 0.0F);
		paneltwo.addChild(keyboard);
		keyboard.cubeList.add(new ModelBox(keyboard, 464, 227, -13.0F, -64.0F, 38.5F, 26, 2, 11, 0.0F, false));
		keyboard.cubeList.add(new ModelBox(keyboard, 418, 206, 1.0F, -64.0F, 27.5F, 2, 3, 11, 0.0F, false));
		keyboard.cubeList.add(new ModelBox(keyboard, 418, 206, -3.0F, -64.0F, 27.5F, 2, 3, 11, 0.0F, false));
		keyboard.cubeList.add(new ModelBox(keyboard, 358, 188, -12.0F, -64.1F, 39.5F, 24, 0, 9, 0.0F, false));

		panelthree = new RendererModel(this);
		panelthree.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelthree, 0.0F, 3.1416F, 0.0F);
		controls.addChild(panelthree);

		bluevalves = new RendererModel(this);
		bluevalves.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bluevalves, 0.3491F, 0.0F, 0.0F);
		panelthree.addChild(bluevalves);
		bluevalves.cubeList.add(new ModelBox(bluevalves, 116, 423, -13.4F, -67.7F, -27.2F, 2, 0, 2, 0.0F, false));
		bluevalves.cubeList.add(new ModelBox(bluevalves, 116, 423, -9.6F, -67.7F, -28.0F, 2, 0, 2, 0.0F, false));
		bluevalves.cubeList.add(new ModelBox(bluevalves, 116, 423, -12.5F, -67.7F, -30.2F, 2, 0, 2, 0.0F, false));

		bluevalves2 = new RendererModel(this);
		bluevalves2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bluevalves2, 0.3491F, 0.0F, 0.0F);
		panelthree.addChild(bluevalves2);
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 374, 202, -12.9F, -67.7F, -26.5F, 0, 3, 0, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 374, 202, -8.9F, -67.7F, -27.5F, 0, 3, 0, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 374, 202, -11.9F, -67.7F, -29.5F, 0, 3, 0, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 392, 195, 2.1F, -65.7F, -24.5F, 6, 1, 5, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 392, 195, -4.7F, -65.7F, -25.5F, 6, 1, 5, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 246, 134, -1.9F, -66.7F, -23.5F, 2, 3, 2, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 220, 134, -0.9F, -70.7F, -22.6F, 0, 6, 0, 0.0F, false));
		bluevalves2.cubeList.add(new ModelBox(bluevalves2, 437, 209, 4.1F, -66.7F, -23.5F, 3, 3, 3, 0.0F, false));

		plugandkey = new RendererModel(this);
		plugandkey.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plugandkey, 0.3491F, -0.5236F, 0.0F);
		panelthree.addChild(plugandkey);
		plugandkey.cubeList.add(new ModelBox(plugandkey, 409, 220, -7.8F, -69.0F, -40.0F, 3, 1, 9, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 413, 197, -6.8F, -70.0F, -39.0F, 1, 1, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 411, 202, -6.8F, -70.0F, -37.0F, 1, 1, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 413, 197, -6.8F, -69.4F, -35.0F, 1, 1, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 395, 430, -6.8F, -71.4F, -35.0F, 1, 2, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 413, 197, -6.8F, -69.4F, -33.0F, 1, 1, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 355, 251, -6.8F, -70.4F, -33.0F, 1, 1, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 418, 211, -6.8F, -71.0F, -33.0F, 1, 0, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 399, 195, -8.7F, -69.0F, -16.0F, 5, 1, 4, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 197, 132, -7.7F, -70.0F, -15.0F, 3, 1, 2, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 320, 199, -7.7F, -73.0F, -15.0F, 3, 1, 2, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 227, 106, -7.7F, -71.0F, -15.0F, 3, 1, 2, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 197, 132, -7.7F, -72.0F, -15.0F, 3, 1, 2, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 399, 195, -8.7F, -69.0F, -28.0F, 5, 1, 9, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 403, 236, 4.2F, -69.0F, -39.0F, 3, 1, 3, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 403, 236, 4.2F, -69.0F, -33.0F, 3, 1, 3, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 435, 238, 5.2F, -70.0F, -38.0F, 1, 1, 1, 0.0F, false));
		plugandkey.cubeList.add(new ModelBox(plugandkey, 435, 238, 5.2F, -70.0F, -32.0F, 1, 1, 1, 0.0F, false));

		panelfour = new RendererModel(this);
		panelfour.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfour, 0.0F, -1.0472F, 0.0F);
		controls.addChild(panelfour);
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, -4.2F, -73.1F, 24.0F, 4, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, 5.0F, -71.1F, 23.3F, 2, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, 2.8F, -71.1F, 23.3F, 2, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, 2.8F, -68.8F, 23.3F, 2, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, 5.0F, -68.8F, 23.3F, 2, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 455, 224, 3.3F, -70.6F, 23.5F, 1, 0, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 455, 224, 5.5F, -70.6F, 23.5F, 1, 0, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 455, 224, 5.5F, -68.3F, 23.5F, 1, 0, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 455, 224, 3.3F, -68.3F, 23.5F, 1, 0, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, 0.0F, -73.1F, 24.0F, 2, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, 0.0F, -66.3F, 24.0F, 2, 1, 1, 0.0F, false));
		panelfour.cubeList.add(new ModelBox(panelfour, 460, 162, -4.2F, -66.3F, 24.0F, 4, 1, 1, 0.0F, false));

		panelfour2 = new RendererModel(this);
		panelfour2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfour2, 0.0F, -1.0472F, 0.0F);
		controls.addChild(panelfour2);
		panelfour2.cubeList.add(new ModelBox(panelfour2, 391, 236, 0.0F, -71.2F, 24.0F, 2, 4, 1, 0.0F, false));
		panelfour2.cubeList.add(new ModelBox(panelfour2, 391, 236, -4.2F, -71.2F, 24.0F, 4, 4, 1, 0.0F, false));

		panelfive = new RendererModel(this);
		panelfive.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfive, 0.0F, -0.5236F, 0.0F);
		controls.addChild(panelfive);
		panelfive.cubeList.add(new ModelBox(panelfive, 184, 435, -39.0F, -57.0F, -1.4F, 3, 4, 3, 0.0F, false));

		panelfive2 = new RendererModel(this);
		panelfive2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfive2, 0.0F, -0.5236F, 0.0F);
		controls.addChild(panelfive2);
		panelfive2.cubeList.add(new ModelBox(panelfive2, 393, 219, -39.0F, -61.0F, -1.4F, 3, 4, 3, 0.0F, false));

		bone120 = new RendererModel(this);
		bone120.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone120, -0.2618F, -1.5708F, 0.0F);
		panelfive2.addChild(bone120);
		bone120.cubeList.add(new ModelBox(bone120, 361, 216, -1.0F, -68.1F, 18.0F, 2, 2, 6, 0.0F, false));

		panelfive3 = new RendererModel(this);
		panelfive3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelfive3, 0.0F, -1.5708F, 0.0F);
		controls.addChild(panelfive3);
		panelfive3.cubeList.add(new ModelBox(panelfive3, 327, 386, -40.0F, -61.0F, -2.4F, 5, 4, 5, 0.0F, false));
		panelfive3.cubeList.add(new ModelBox(panelfive3, 327, 386, -39.0F, -62.0F, -1.4F, 3, 9, 3, 0.0F, false));

		bone121 = new RendererModel(this);
		bone121.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone121, -0.2618F, -1.5708F, 0.0F);
		panelfive3.addChild(bone121);

		panelsix = new RendererModel(this);
		panelsix.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(panelsix, -0.3491F, 3.1416F, 0.0F);
		controls.addChild(panelsix);
		panelsix.cubeList.add(new ModelBox(panelsix, 354, 192, -3.0F, -65.5F, 12.7F, 6, 6, 6, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 216, 224, 14.0F, -68.0F, 16.0F, 4, 4, 4, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 354, 192, -14.0F, -65.5F, 28.7F, 4, 6, 2, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 354, 192, -10.0F, -65.5F, 23.7F, 2, 6, 4, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 354, 192, -14.0F, -65.5F, 25.7F, 3, 6, 2, 0.0F, false));
		panelsix.cubeList.add(new ModelBox(panelsix, 354, 192, 7.0F, -65.5F, 26.7F, 4, 6, 4, 0.0F, false));

		wibbly = new RendererModel(this);
		wibbly.setRotationPoint(16.0F, -68.0F, 16.0F);
		panelsix.addChild(wibbly);
		wibbly.cubeList.add(new ModelBox(wibbly, 416, 187, -1.0F, -1.0F, 2.0F, 2, 1, 7, 0.0F, false));
		wibbly.cubeList.add(new ModelBox(wibbly, 388, 233, -1.0F, -1.0F, 0.0F, 2, 3, 2, 0.0F, false));
		wibbly.cubeList.add(new ModelBox(wibbly, 416, 248, -1.0F, -1.0F, 9.0F, 2, 1, 7, 0.0F, false));

		pumps = new RendererModel(this);
		pumps.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(pumps);

		pump = new RendererModel(this);
		pump.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump, 0.3491F, -0.5236F, 0.0F);
		pumps.addChild(pump);
		pump.cubeList.add(new ModelBox(pump, 306, 105, -3.0F, -71.0F, -30.0F, 6, 7, 16, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 460, 190, -1.5F, -69.7F, -33.0F, 3, 3, 5, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 395, 288, -2.5F, -70.7F, -35.0F, 5, 5, 2, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 478, 222, -1.5F, -69.7F, -36.0F, 3, 3, 1, 0.0F, false));
		pump.cubeList.add(new ModelBox(pump, 460, 190, -3.5F, -71.7F, -15.0F, 7, 6, 2, 0.0F, false));

		pump3 = new RendererModel(this);
		pump3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump3, 0.3491F, -2.618F, 0.0F);
		pumps.addChild(pump3);
		pump3.cubeList.add(new ModelBox(pump3, 306, 105, -3.0F, -71.0F, -30.0F, 6, 7, 16, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 460, 190, -1.5F, -69.7F, -33.0F, 3, 3, 5, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 395, 288, -2.5F, -70.7F, -35.0F, 5, 5, 2, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 478, 222, -1.5F, -69.7F, -36.0F, 3, 3, 1, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 460, 190, -3.5F, -71.7F, -15.0F, 7, 6, 2, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 392, 237, -8.3F, -69.0F, -27.0F, 5, 7, 10, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 347, 334, -7.7F, -69.8F, -21.6F, 4, 1, 4, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 347, 334, -7.7F, -69.8F, -26.4F, 4, 1, 4, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 485, 241, -7.5F, -68.8F, -34.4F, 3, 1, 3, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 246, 134, 3.7F, -69.0F, -12.0F, 5, 7, 5, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 339, 430, 4.7F, -71.0F, -11.0F, 3, 5, 3, 0.0F, false));
		pump3.cubeList.add(new ModelBox(pump3, 339, 430, 5.2F, -72.0F, -10.4F, 2, 1, 2, 0.0F, false));

		redlever = new RendererModel(this);
		redlever.setRotationPoint(0.0F, 0.0F, 0.0F);
		pump3.addChild(redlever);
		redlever.cubeList.add(new ModelBox(redlever, 418, 188, 3.7F, -71.0F, -27.0F, 5, 9, 10, 0.0F, false));
		redlever.cubeList.add(new ModelBox(redlever, 418, 188, 4.2F, -72.0F, -25.0F, 2, 1, 5, 0.0F, false));

		bigboy = new RendererModel(this);
		bigboy.setRotationPoint(6.0F, -70.0F, -22.0F);
		setRotationAngle(bigboy, -0.6981F, 0.0F, 0.0F);
		redlever.addChild(bigboy);
		bigboy.cubeList.add(new ModelBox(bigboy, 430, 514, -1.8F, -6.5182F, -1.1519F, 2, 1, 1, 0.0F, false));
		bigboy.cubeList.add(new ModelBox(bigboy, 418, 188, -1.6F, -4.7182F, -1.1519F, 0, 4, 1, 0.0F, false));
		bigboy.cubeList.add(new ModelBox(bigboy, 430, 514, -2.8F, -7.5182F, -1.1519F, 4, 1, 1, 0.0F, false));
		bigboy.cubeList.add(new ModelBox(bigboy, 418, 188, -0.5F, -4.7182F, -1.1519F, 0, 4, 1, 0.0F, false));

		blackboy3 = new RendererModel(this);
		blackboy3.setRotationPoint(7.0F, -67.0F, -21.0F);
		setRotationAngle(blackboy3, 0.6981F, 0.0F, 0.0F);
		redlever.addChild(blackboy3);
		blackboy3.cubeList.add(new ModelBox(blackboy3, 439, 295, -0.8F, -7.1765F, 0.0202F, 1, 1, 1, 0.0F, false));
		blackboy3.cubeList.add(new ModelBox(blackboy3, 418, 188, -0.6F, -5.3765F, 0.0202F, 0, 4, 1, 0.0F, false));
		blackboy3.cubeList.add(new ModelBox(blackboy3, 434, 306, -1.8F, -8.1765F, 0.0202F, 4, 1, 1, 0.0F, false));

		pump4 = new RendererModel(this);
		pump4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump4, 0.3491F, 0.5236F, 0.0F);
		pumps.addChild(pump4);
		pump4.cubeList.add(new ModelBox(pump4, 306, 105, -3.0F, -71.0F, -30.0F, 6, 7, 16, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 460, 190, -1.5F, -69.7F, -33.0F, 3, 3, 5, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 395, 288, -2.5F, -70.7F, -35.0F, 5, 5, 2, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 478, 222, -1.5F, -69.7F, -36.0F, 3, 3, 1, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 460, 190, -3.5F, -71.7F, -15.0F, 7, 6, 2, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 246, 125, -8.0F, -73.0F, -19.0F, 4, 3, 7, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 411, 211, -8.0F, -73.0F, -23.0F, 4, 3, 4, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 411, 211, -7.0F, -71.0F, -28.0F, 2, 2, 6, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 411, 211, -8.0F, -73.0F, -12.0F, 4, 3, 1, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 396, 256, -8.0F, -73.0F, -11.0F, 4, 3, 6, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 411, 211, -7.0F, -70.0F, -14.0F, 2, 3, 2, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 411, 211, -7.0F, -70.0F, -18.0F, 2, 3, 2, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 457, 228, -7.0F, -72.0F, -5.0F, 2, 1, 20, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 411, 211, -7.0F, -70.0F, -28.0F, 2, 3, 2, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 428, 241, -8.0F, -69.0F, -39.0F, 4, 3, 4, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 428, 241, 4.0F, -69.0F, -39.0F, 4, 3, 4, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 425, 216, 4.0F, -69.0F, -16.0F, 4, 3, 4, 0.0F, false));
		pump4.cubeList.add(new ModelBox(pump4, 448, 194, 4.0F, -70.0F, -10.0F, 4, 4, 4, 0.0F, false));

		pump5 = new RendererModel(this);
		pump5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump5, 0.3491F, 1.5708F, 0.0F);
		pumps.addChild(pump5);
		pump5.cubeList.add(new ModelBox(pump5, 306, 105, -3.0F, -71.0F, -30.0F, 6, 7, 16, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 460, 190, -1.5F, -69.7F, -33.0F, 3, 3, 5, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 395, 288, -2.5F, -70.7F, -35.0F, 5, 5, 2, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 478, 222, -1.5F, -69.7F, -36.0F, 3, 3, 1, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 460, 190, -3.5F, -71.7F, -15.0F, 7, 6, 2, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 199, 110, -5.1F, -68.9F, -19.1F, 1, 1, 5, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 221, 98, -6.4F, -68.9F, -23.1F, 0, 1, 13, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 199, 110, -8.1F, -68.9F, -19.1F, 1, 1, 5, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 199, 110, -6.8F, -69.3F, -32.0F, 2, 1, 4, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 199, 110, -6.3F, -69.3F, -35.6F, 1, 1, 8, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 199, 110, -7.8F, -69.3F, -28.0F, 1, 1, 1, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 199, 110, -4.8F, -69.3F, -28.0F, 1, 1, 1, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 393, 241, -6.3F, -69.3F, -35.6F, 1, 0, 1, 0.0F, false));
		pump5.cubeList.add(new ModelBox(pump5, 243, 123, -8.3F, -69.0F, -36.0F, 5, 7, 10, 0.0F, false));

		redlever2 = new RendererModel(this);
		redlever2.setRotationPoint(0.0F, 0.0F, 0.0F);
		pump5.addChild(redlever2);
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 3.7F, -71.0F, -27.0F, 5, 9, 10, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 4.2F, -72.0F, -25.0F, 2, 1, 5, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 3.7F, -69.0F, -13.0F, 5, 7, 5, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 4.7F, -70.0F, -12.0F, 3, 1, 3, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 438, 162, 4.7F, -70.0F, -12.0F, 3, 0, 3, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 430, 228, 3.7F, -69.0F, -37.0F, 5, 7, 5, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 4.7F, -72.0F, -35.0F, 3, 3, 1, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 5.7F, -72.0F, -34.0F, 1, 3, 1, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 418, 188, 5.7F, -72.0F, -36.0F, 1, 3, 1, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 430, 228, 4.7F, -72.0F, -36.0F, 3, 0, 3, 0.0F, false));
		redlever2.cubeList.add(new ModelBox(redlever2, 324, 512, 5.2F, -73.0F, -35.6F, 2, 1, 2, 0.0F, false));

		bigreder = new RendererModel(this);
		bigreder.setRotationPoint(5.0F, -68.0F, -22.0F);
		setRotationAngle(bigreder, -0.6981F, 0.0F, 0.0F);
		redlever2.addChild(bigreder);
		bigreder.cubeList.add(new ModelBox(bigreder, 430, 514, -0.8F, -8.0503F, -2.4375F, 2, 1, 1, 0.0F, false));
		bigreder.cubeList.add(new ModelBox(bigreder, 418, 188, -0.6F, -6.2503F, -2.4375F, 0, 4, 1, 0.0F, false));
		bigreder.cubeList.add(new ModelBox(bigreder, 430, 514, -1.8F, -9.0503F, -2.4375F, 4, 1, 1, 0.0F, false));
		bigreder.cubeList.add(new ModelBox(bigreder, 418, 188, 0.5F, -6.2503F, -2.4375F, 0, 4, 1, 0.0F, false));

		smallblack = new RendererModel(this);
		smallblack.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(smallblack, 0.6981F, 0.0F, 0.0F);
		redlever2.addChild(smallblack);
		smallblack.cubeList.add(new ModelBox(smallblack, 439, 295, 6.2F, -72.0F, 27.0F, 1, 1, 1, 0.0F, false));
		smallblack.cubeList.add(new ModelBox(smallblack, 418, 188, 6.4F, -70.2F, 27.0F, 0, 4, 1, 0.0F, false));
		smallblack.cubeList.add(new ModelBox(smallblack, 434, 306, 5.2F, -73.0F, 27.0F, 4, 1, 1, 0.0F, false));

		falsepump = new RendererModel(this);
		falsepump.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(falsepump, 0.3491F, 2.618F, 0.0F);
		pumps.addChild(falsepump);
		falsepump.cubeList.add(new ModelBox(falsepump, 411, 206, -2.3F, -67.7F, -18.0F, 3, 5, 3, 0.0F, false));
		falsepump.cubeList.add(new ModelBox(falsepump, 411, 206, 0.5F, -68.7F, -20.3F, 2, 5, 2, 0.0F, false));

		bone112 = new RendererModel(this);
		bone112.setRotationPoint(-2.0F, -66.0F, -28.0F);
		setRotationAngle(bone112, 0.0F, 0.3491F, 0.0F);
		falsepump.addChild(bone112);
		bone112.cubeList.add(new ModelBox(bone112, 411, 206, 1.1F, -1.7F, -4.0F, 3, 5, 5, 0.0F, false));

		rotorframe = new RendererModel(this);
		rotorframe.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(rotorframe);

		bone110 = new RendererModel(this);
		bone110.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone110, 0.0175F, 0.0F, 0.0F);
		rotorframe.addChild(bone110);
		bone110.cubeList.add(new ModelBox(bone110, 16, 102, -8.0F, -243.0F, -14.0F, 16, 172, 4, 0.0F, false));

		bone111 = new RendererModel(this);
		bone111.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone111, -0.0175F, 0.0F, 0.0F);
		rotorframe.addChild(bone111);
		bone111.cubeList.add(new ModelBox(bone111, 16, 102, -8.0F, -243.0F, 9.0F, 16, 172, 4, 0.0F, false));

		unit = new RendererModel(this);
		unit.setRotationPoint(0.0F, 0.0F, 0.0F);
		copper.addChild(unit);

		console2 = new RendererModel(this);
		console2.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(console2);

		model = new RendererModel(this);
		model.setRotationPoint(0.0F, 0.0F, 0.0F);
		console2.addChild(model);

		console = new RendererModel(this);
		console.setRotationPoint(0.0F, -44.0F, 0.0F);
		model.addChild(console);

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(frame);

		upper = new RendererModel(this);
		upper.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(upper);

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone4, 0.0F, 0.0F, -1.2217F);
		upper.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 202, 194, 21.1809F, 6.0261F, -9.0F, 6, 52, 6, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 202, 194, 21.1809F, 6.0261F, 3.0F, 6, 52, 6, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 256, 188, 18.1809F, 8.0261F, -6.0F, 6, 48, 10, 0.0F, false));

		monster2 = new RendererModel(this);
		monster2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(monster2);

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone5, 0.0F, 0.0F, 1.2217F);
		upper.addChild(bone5);
        bone5.cubeList.add(new ModelBox(bone5, 195, 188, -30.9397F, 4.658F, 3.0F, 6, 52, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 195, 188, -27.1809F, 6.0261F, -9.0F, 6, 52, 6, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 243, 224, -24.1809F, 8.0261F, -3.0F, 6, 48, 6, 0.0F, false));

		monster = new RendererModel(this);
		monster.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(monster, 0.0F, 3.1416F, 0.0F);
		bone5.addChild(monster);

		upper2 = new RendererModel(this);
		upper2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper2, 0.0F, -1.0472F, 0.0F);
		frame.addChild(upper2);

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone6, 0.0F, 0.0F, -1.2217F);
		upper2.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 195, 188, 21.1809F, 6.0261F, -9.0F, 6, 52, 6, 0.0F, false));

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone59, 0.0F, 0.0F, -1.2217F);
		upper2.addChild(bone59);
		bone59.cubeList.add(new ModelBox(bone59, 261, 188, 18.1809F, 8.0261F, -6.0F, 6, 48, 10, 0.0F, false));

		yeeeet = new RendererModel(this);
		yeeeet.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(yeeeet, 0.0F, 0.0F, -1.2217F);
		upper2.addChild(yeeeet);
		yeeeet.cubeList.add(new ModelBox(yeeeet, 195, 188, 21.1809F, 6.0261F, 3.0F, 6, 52, 6, 0.0F, false));
		yeeeet.cubeList.add(new ModelBox(yeeeet, 42, 163, 18.1809F, 8.0261F, -6.0F, 6, 48, 10, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		yeeeet.addChild(bone71);

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(0.0F, 0.0F, 0.0F);
		yeeeet.addChild(bone57);

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone58, 0.0F, 3.1416F, -1.2217F);
		upper2.addChild(bone58);

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone7, 0.0F, 0.0F, 1.2217F);
		upper2.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 195, 188, -27.1809F, 6.0261F, 3.0F, 6, 52, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 202, 204, -27.1809F, 6.0261F, -9.0F, 6, 52, 6, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 256, 188, -24.1809F, 8.0261F, -3.0F, 6, 48, 6, 0.0F, false));

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone72, 0.0F, 3.1416F, 0.0F);
		bone7.addChild(bone72);

		upper3 = new RendererModel(this);
		upper3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper3, 0.0F, -2.0944F, 0.0F);
		frame.addChild(upper3);

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone8, 0.0F, 0.0F, -1.2217F);
		upper3.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 195, 188, 21.1809F, 6.0261F, -9.0F, 6, 52, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 195, 188, 21.1809F, 6.0261F, 3.0F, 6, 52, 6, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 243, 224, 18.1809F, 12.0261F, -6.0F, 6, 44, 10, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone9, 0.0F, 0.0F, 1.2217F);
		upper3.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 190, 193, -27.1809F, 6.0261F, -9.0F, 6, 52, 6, 0.0F, false));
		bone9.cubeList.add(new ModelBox(bone9, 200, 190, -27.1809F, 6.0261F, 3.0F, 6, 52, 6, 0.0F, false));
		bone9.cubeList.add(new ModelBox(bone9, 256, 188, -24.1809F, 8.0261F, -3.0F, 6, 48, 6, 0.0F, false));

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone70, 0.0F, 3.1416F, 1.2217F);
		upper3.addChild(bone70);

		upp = new RendererModel(this);
		upp.setRotationPoint(0.0F, 1.0F, 0.0F);
		frame.addChild(upp);

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		upp.addChild(bone11);

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		upp.addChild(bone12);

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone13, 0.0F, -2.0944F, 0.0F);
		upp.addChild(bone13);

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone14, -0.3491F, 0.0F, 0.0F);
		frame.addChild(bone14);
		bone14.cubeList.add(new ModelBox(bone14, 437, 84, -23.0F, -24.0F, 43.5F, 46, 6, 6, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 16, 260, -5.0F, -24.0F, 7.5F, 10, 6, 10, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 452, 27, -16.0F, -24.0F, 31.5F, 32, 6, 6, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 408, 3, -11.0F, -24.0F, 21.5F, 22, 6, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 417, 16, -13.0F, -24.0F, 25.5F, 26, 6, 6, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 190, 193, -9.0F, -26.0603F, 17.842F, 18, 6, 4, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 441, 41, -20.0F, -24.0F, 37.5F, 40, 6, 6, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 190, 193, -5.0F, -26.0603F, 13.842F, 10, 6, 4, 0.0F, false));

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone15, -0.3491F, 3.1416F, 0.0F);
		frame.addChild(bone15);
		bone15.cubeList.add(new ModelBox(bone15, 441, 41, -19.0F, -24.0F, 37.5F, 38, 6, 6, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 8, 260, -5.0F, -24.0F, 7.5F, 10, 6, 10, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 452, 27, -16.0F, -24.0F, 31.5F, 32, 6, 6, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 408, 3, -11.0F, -24.0F, 21.5F, 22, 6, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 417, 16, -13.0F, -24.0F, 25.5F, 26, 6, 6, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 190, 193, -9.0F, -26.0603F, 17.842F, 18, 6, 4, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 437, 84, -23.0F, -24.0F, 43.5F, 46, 6, 5, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 190, 193, -5.0F, -26.0603F, 13.842F, 10, 6, 4, 0.0F, false));

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone16, -0.3491F, -1.0472F, 0.0F);
		frame.addChild(bone16);
		bone16.cubeList.add(new ModelBox(bone16, 437, 84, -23.0F, -24.0F, 43.5F, 46, 6, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 0, 260, -5.0F, -24.0F, 7.5F, 10, 6, 10, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 452, 27, -16.0F, -24.0F, 31.5F, 32, 6, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 408, 3, -11.0F, -24.0F, 21.5F, 22, 6, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 417, 16, -13.0F, -24.0F, 25.5F, 26, 6, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 190, 193, -9.0F, -26.0603F, 17.842F, 18, 6, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 441, 41, -19.0F, -24.0F, 37.5F, 38, 6, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 209, 254, -9.0F, -26.0603F, 14.842F, 18, 6, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 352, 167, -6.0F, -26.0F, 29.5F, 12, 6, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 196, 110, -7.0F, -26.0F, 29.5F, 1, 6, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 196, 110, -7.0F, -26.0F, 33.5F, 1, 6, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 196, 110, 6.0F, -26.0F, 33.5F, 1, 6, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 196, 110, 6.0F, -26.0F, 29.5F, 1, 6, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 125, 381, -9.0F, -25.0F, 41.5F, 12, 2, 11, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 125, 381, -9.0F, -28.0F, 41.5F, 12, 3, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 125, 381, -7.0F, -29.0F, 38.5F, 8, 3, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -6.6F, -29.1F, 39.2F, 7, 1, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -8.6F, -25.1F, 48.2F, 11, 1, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 440, 211, 1.0F, -28.1F, 40.2F, 1, 3, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 440, 211, -6.0F, -28.1F, 41.2F, 7, 3, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -5.6F, -28.1F, 43.2F, 5, 1, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -7.6F, -28.1F, 45.2F, 2, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -0.6F, -28.1F, 45.2F, 2, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 371, 231, 8.0F, -25.0F, 42.5F, 4, 3, 4, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 371, 231, 9.0F, -26.0F, 43.5F, 2, 1, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 226, 135, 9.5F, -26.2F, 44.0F, 1, 0, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 376, 209, 9.0F, -26.0F, 43.5F, 2, 0, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, 2.4F, -26.1F, 31.2F, 3, 1, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -5.6F, -26.1F, 31.2F, 3, 1, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 361, 246, -2.6F, -26.1F, 32.2F, 5, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 320, 189, 9.8F, -28.0F, 26.5F, 5, 5, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 267, 148, 10.0F, -28.0F, 32.5F, 5, 5, 2, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 366, 206, 13.0F, -28.2F, 27.5F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 12.0F, -28.2F, 27.5F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 366, 206, 11.0F, -28.2F, 27.5F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 10.0F, -28.2F, 27.5F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 14.0F, -28.2F, 28.1F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 366, 206, 13.0F, -28.2F, 29.0F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 12.0F, -28.2F, 29.0F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 366, 206, 11.0F, -28.2F, 29.0F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 10.0F, -28.2F, 29.0F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 14.0F, -28.2F, 29.6F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 366, 206, 13.0F, -28.2F, 30.4F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 12.0F, -28.2F, 30.4F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 366, 206, 11.0F, -28.2F, 30.4F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 10.0F, -28.2F, 30.4F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 393, 204, 14.0F, -28.2F, 31.0F, 1, 1, 1, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 334, 201, 8.8F, -27.0F, 27.5F, 1, 5, 6, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 320, 189, 14.8F, -28.0F, 29.5F, 1, 5, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 320, 189, 14.8F, -28.0F, 29.5F, 1, 5, 3, 0.0F, false));
		bone16.cubeList.add(new ModelBox(bone16, 267, 148, 10.0F, -27.0F, 34.5F, 5, 4, 3, 0.0F, false));

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone18, -0.3491F, 1.0472F, 0.0F);
		frame.addChild(bone18);
		bone18.cubeList.add(new ModelBox(bone18, 437, 84, -23.0F, -24.0F, 43.5F, 46, 6, 6, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 24, 260, -5.0F, -24.0F, 7.5F, 10, 6, 10, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 452, 27, -16.0F, -24.0F, 31.5F, 32, 6, 6, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 408, 3, -11.0F, -24.0F, 21.5F, 22, 6, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 417, 16, -13.0F, -24.0F, 25.5F, 26, 6, 6, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 190, 193, -9.0F, -26.0603F, 17.842F, 18, 6, 4, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 441, 41, -19.0F, -24.0F, 37.5F, 38, 6, 6, 0.0F, false));
		bone18.cubeList.add(new ModelBox(bone18, 190, 193, -5.0F, -26.0603F, 13.842F, 10, 6, 4, 0.0F, false));

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone19, -0.3491F, 2.0944F, 0.0F);
		frame.addChild(bone19);
		bone19.cubeList.add(new ModelBox(bone19, 86, 262, -5.0F, -24.0F, 7.5F, 10, 6, 6, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 452, 27, -16.0F, -24.0F, 31.5F, 32, 6, 6, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 40, 254, -7.0F, -24.0F, 13.5F, 14, 6, 8, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 417, 16, -13.0F, -24.0F, 25.5F, 26, 6, 6, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 408, 3, -11.0F, -24.0F, 21.5F, 22, 6, 4, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 190, 193, -9.0F, -26.0603F, 17.842F, 18, 6, 4, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 437, 84, -23.0F, -24.0F, 43.5F, 46, 6, 6, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 441, 41, -19.0F, -24.0F, 37.5F, 38, 6, 6, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 190, 193, -5.0F, -26.0603F, 13.842F, 10, 6, 4, 0.0F, false));

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone17, -0.3491F, -2.0944F, 0.0F);
		frame.addChild(bone17);
		bone17.cubeList.add(new ModelBox(bone17, 437, 84, -23.0F, -24.0F, 43.5F, 46, 6, 6, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 92, 260, -5.0F, -24.0F, 7.5F, 10, 6, 6, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 452, 27, -16.0F, -24.0F, 31.5F, 32, 6, 6, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 408, 3, -11.0F, -24.0F, 21.5F, 22, 6, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 417, 16, -13.0F, -24.0F, 25.5F, 26, 6, 6, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 32, 260, -7.0F, -24.0F, 13.5F, 14, 6, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 190, 193, -9.0F, -26.0603F, 17.842F, 18, 6, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 441, 41, -19.0F, -24.0F, 37.5F, 38, 6, 6, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 209, 254, -5.0F, -26.0603F, 13.842F, 10, 6, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, -2.5F, -25.0F, 39.5F, 5, 6, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, -1.1F, -25.0F, 43.5F, 2, 6, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, -3.5F, -25.0F, 42.5F, 2, 6, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, -3.5F, -25.0F, 38.5F, 2, 6, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, 1.5F, -25.0F, 42.5F, 2, 6, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, 1.5F, -25.0F, 38.5F, 2, 6, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, -1.1F, -25.0F, 37.5F, 2, 6, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 354, 192, -1.5F, -25.1F, 40.0F, 3, 6, 3, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 371, 219, 8.0F, -24.8F, 42.0F, 4, 6, 4, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 371, 219, 9.0F, -25.8F, 43.0F, 2, 1, 2, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 354, 192, -3.0F, -24.1F, 28.0F, 6, 6, 6, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 339, 393, -13.0F, -25.8F, 45.0F, 2, 1, 2, 0.0F, false));

		frame2 = new RendererModel(this);
		frame2.setRotationPoint(0.0F, -6.0F, 0.0F);
		setRotationAngle(frame2, 0.0F, 0.0F, 3.1416F);
		console.addChild(frame2);

		upper4 = new RendererModel(this);
		upper4.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame2.addChild(upper4);

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone10, 0.0F, 0.0F, -1.2217F);
		upper4.addChild(bone10);

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone20, 0.0F, 0.0F, 1.2217F);
		upper4.addChild(bone20);

		upper5 = new RendererModel(this);
		upper5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper5, 0.0F, -1.0472F, 0.0F);
		frame2.addChild(upper5);

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone21, 0.0F, 0.0F, -1.2217F);
		upper5.addChild(bone21);

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone22, 0.0F, 0.0F, 1.2217F);
		upper5.addChild(bone22);

		upper6 = new RendererModel(this);
		upper6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(upper6, 0.0F, -2.0944F, 0.0F);
		frame2.addChild(upper6);

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone23, 0.0F, 0.0F, -1.2217F);
		upper6.addChild(bone23);

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone24, 0.0F, 0.0F, 1.2217F);
		upper6.addChild(bone24);

		upp2 = new RendererModel(this);
		upp2.setRotationPoint(0.0F, 1.0F, 0.0F);
		frame2.addChild(upp2);

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		upp2.addChild(bone25);

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone26, 0.0F, -1.0472F, 0.0F);
		upp2.addChild(bone26);

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone27, 0.0F, -2.0944F, 0.0F);
		upp2.addChild(bone27);

		upp3 = new RendererModel(this);
		upp3.setRotationPoint(0.0F, -24.0F, 0.0F);
		frame2.addChild(upp3);

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, 0.0F, 0.0F);
		upp3.addChild(bone39);

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone40, 0.0F, -1.0472F, 0.0F);
		upp3.addChild(bone40);

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone41, 0.0F, -2.0944F, 0.0F);
		upp3.addChild(bone41);

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone28, -0.3491F, 0.0F, 0.0F);
		frame2.addChild(bone28);

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone29, -0.3491F, 3.1416F, 0.0F);
		frame2.addChild(bone29);

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone30, -0.3491F, -1.0472F, 0.0F);
		frame2.addChild(bone30);

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone31, -0.3491F, 1.0472F, 0.0F);
		frame2.addChild(bone31);

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone32, -0.3491F, 2.0944F, 0.0F);
		frame2.addChild(bone32);

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone33, -0.3491F, -2.0944F, 0.0F);
		frame2.addChild(bone33);

		cubes3 = new RendererModel(this);
		cubes3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(cubes3, 0.0F, -2.0944F, 0.0F);
		console.addChild(cubes3);

		cubes2 = new RendererModel(this);
		cubes2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(cubes2, 0.0F, -1.0472F, 0.0F);
		console.addChild(cubes2);

		cubes = new RendererModel(this);
		cubes.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(cubes);

		outerframe = new RendererModel(this);
		outerframe.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(outerframe);

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		outerframe.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 357, 254, -26.0F, -6.0F, 54.0F, 52, 11, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 357, 254, -26.0F, -6.0F, -55.0F, 52, 11, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 190, 193, -8.0F, -29.0F, -24.0F, 16, 11, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 202, 202, 4.0F, -32.0F, -24.0F, 4, 3, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 190, 193, -4.0F, -31.0F, -24.0F, 8, 2, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 202, 202, -8.0F, -32.0F, -24.0F, 4, 3, 6, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 384, 241, -7.1F, -28.0F, -25.0F, 3, 5, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 384, 241, -3.4F, -28.0F, -25.0F, 3, 5, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 384, 241, 0.4F, -28.0F, -25.0F, 3, 5, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 384, 241, 4.1F, -28.0F, -25.0F, 3, 5, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 428, 177, -7.1F, -27.0F, -25.1F, 3, 2, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 428, 177, -3.4F, -27.0F, -25.1F, 3, 2, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 428, 177, 0.4F, -27.0F, -25.1F, 3, 2, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 428, 177, 4.1F, -27.0F, -25.1F, 3, 2, 0, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(0.0F, -19.0F, 21.0F);
		setRotationAngle(bone56, 0.0873F, 0.0F, 0.0F);
		bone.addChild(bone56);
		bone56.cubeList.add(new ModelBox(bone56, 190, 193, -8.0F, -14.0F, -3.0F, 16, 18, 6, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 190, 193, -6.0F, -16.0F, -3.0F, 12, 2, 6, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 439, 213, 3.0F, -9.0F, 3.0F, 4, 4, 1, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 346, 167, -7.0F, -15.0F, 4.0F, 4, 12, 2, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 346, 167, 3.0F, -15.0F, 4.0F, 4, 12, 2, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 439, 213, -7.0F, -9.0F, 3.0F, 4, 4, 1, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 446, 197, -2.0F, -11.0F, 3.0F, 4, 6, 1, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 413, 246, 3.0F, -15.0F, 4.0F, 4, 0, 2, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 413, 246, -7.0F, -15.0F, 4.0F, 4, 0, 2, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 413, 246, 3.0F, -2.2F, 4.0F, 4, 0, 2, 0.0F, false));
		bone56.cubeList.add(new ModelBox(bone56, 413, 246, -7.0F, -2.2F, 4.0F, 4, 0, 2, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		outerframe.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 357, 254, -26.0F, -6.0F, 54.0F, 52, 11, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 357, 254, -25.0F, -6.0F, -56.0F, 50, 11, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 188, 232, -8.0F, -29.0F, 18.0F, 16, 12, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 208, 205, 4.0F, -32.0F, 18.0F, 4, 3, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 186, 208, -4.0F, -31.0F, 18.0F, 8, 2, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 208, 205, -8.0F, -32.0F, 18.0F, 4, 3, 6, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 451, 223, -7.0F, -3.0F, -56.1F, 13, 4, 0, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(0.0F, -18.0F, -20.0F);
		setRotationAngle(bone54, -0.0873F, 0.0F, 0.0F);
		bone2.addChild(bone54);
		bone54.cubeList.add(new ModelBox(bone54, 202, 202, -8.0F, -15.0F, -4.0F, 16, 18, 6, 0.0F, false));
		bone54.cubeList.add(new ModelBox(bone54, 209, 254, -6.0F, -17.0F, -4.0F, 12, 6, 6, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone3, 0.0F, -2.0944F, 0.0F);
		outerframe.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 357, 254, -26.0F, -6.0F, 54.0F, 52, 11, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 357, 254, -26.0F, -6.0F, -56.0F, 52, 11, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 446, 244, -19.0F, -3.0F, -57.0F, 4, 4, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 221, 206, -8.0F, -32.0F, -24.0F, 4, 3, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 190, 193, 4.0F, -32.0F, -24.0F, 4, 3, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 221, 206, -4.0F, -31.0F, -24.0F, 8, 2, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 190, 193, -8.0F, -29.0F, -24.0F, 16, 11, 6, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 446, 244, 15.0F, -3.0F, -57.0F, 4, 4, 2, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(0.0F, -19.0F, 21.0F);
		setRotationAngle(bone55, 0.0873F, 0.0F, 0.0F);
		bone3.addChild(bone55);
		bone55.cubeList.add(new ModelBox(bone55, 168, 216, -8.0F, -14.0F, -3.0F, 16, 18, 6, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 193, 209, -6.0F, -16.0F, -3.0F, 12, 2, 6, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 384, 224, -4.0F, -10.0F, 3.0F, 8, 5, 1, 0.0F, false));
		bone55.cubeList.add(new ModelBox(bone55, 384, 224, -1.0F, -10.0F, 4.0F, 2, 5, 1, 0.0F, false));

		bone119 = new RendererModel(this);
		bone119.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone119, -0.5236F, 0.0F, 0.0F);
		bone55.addChild(bone119);
		bone119.cubeList.add(new ModelBox(bone119, 391, 167, -5.0F, -11.0F, -1.0F, 10, 2, 7, 0.0F, false));
		bone119.cubeList.add(new ModelBox(bone119, 391, 167, -3.0F, -13.0F, 1.0F, 6, 2, 3, 0.0F, false));
		bone119.cubeList.add(new ModelBox(bone119, 384, 224, -2.0F, -13.3F, 1.6F, 4, 1, 2, 0.0F, false));

		hollowglow = new RendererModel(this);
		hollowglow.setRotationPoint(0.0F, 44.0F, 0.0F);
		console.addChild(hollowglow);

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone36, 0.0F, -2.0944F, 0.0F);
		hollowglow.addChild(bone36);

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, -60.0F, 0.0F);
		setRotationAngle(bone35, 0.0F, -1.0472F, 0.0F);
		hollowglow.addChild(bone35);

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		hollowglow.addChild(bone34);

		ihaveexam = new RendererModel(this);
		ihaveexam.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(ihaveexam);

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone42, 0.0F, 1.5708F, 0.0F);
		ihaveexam.addChild(bone42);
		bone42.cubeList.add(new ModelBox(bone42, 195, 188, 3.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 195, 188, -9.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 398, 238, 4.0F, -45.6F, -64.8F, 4, 4, 2, 0.0F, false));
		bone42.cubeList.add(new ModelBox(bone42, 349, 201, 5.0F, -44.6F, -66.8F, 2, 2, 2, 0.0F, false));

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone38, 0.0F, 0.5236F, 0.0F);
		ihaveexam.addChild(bone38);
		bone38.cubeList.add(new ModelBox(bone38, 213, 194, 3.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 213, 194, -9.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 264, 122, -9.0F, -48.6F, -67.8F, 6, 8, 4, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 420, 187, -9.0F, -47.2F, -72.8F, 6, 6, 5, 0.0F, false));
        bone38.cubeList.add(new ModelBox(bone38, 403, 238, -7.0F, -47.3F, -72.8F, 2, 0, 5, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 403, 238, -8.0F, -47.2F, -72.8F, 1, 0, 1, 0.0F, false));
        bone38.cubeList.add(new ModelBox(bone38, 403, 238, -5.0F, -47.3F, -70.8F, 1, 0, 1, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 403, 238, -5.0F, -47.2F, -68.8F, 1, 0, 1, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 403, 238, -8.0F, -47.2F, -68.8F, 1, 0, 1, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 403, 238, -8.0F, -47.2F, -70.8F, 1, 0, 1, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 514, 162, 7.0F, -47.6F, -64.8F, 1, 6, 2, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 514, 162, 4.0F, -47.6F, -64.8F, 1, 6, 2, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 514, 162, 6.0F, -44.6F, -64.8F, 1, 2, 2, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 514, 162, 5.0F, -46.6F, -64.8F, 1, 2, 2, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 514, 162, 4.0F, -41.6F, -64.8F, 4, 1, 2, 0.0F, false));
		bone38.cubeList.add(new ModelBox(bone38, 514, 162, 4.0F, -48.6F, -64.8F, 4, 1, 2, 0.0F, false));

		rando = new RendererModel(this);
		rando.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone38.addChild(rando);
		rando.cubeList.add(new ModelBox(rando, 406, 199, -5.0F, -48.2F, -72.8F, 1, 1, 1, 0.0F, false));
		rando.cubeList.add(new ModelBox(rando, 391, 256, -5.0F, -49.2F, -72.8F, 1, 1, 1, 0.0F, false));

		yes = new RendererModel(this);
		yes.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(yes, 0.0F, -0.5236F, 0.0F);
		ihaveexam.addChild(yes);
		yes.cubeList.add(new ModelBox(yes, 184, 233, 3.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		yes.cubeList.add(new ModelBox(yes, 184, 233, -9.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone60, -0.1745F, 0.0F, 0.0F);
		yes.addChild(bone60);

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone65, -0.1745F, 3.1416F, 0.0F);
		yes.addChild(bone65);

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone66, -0.1745F, -1.0472F, 0.0F);
		yes.addChild(bone66);

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone67, -0.1745F, -2.0944F, 0.0F);
		yes.addChild(bone67);

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone68, -0.1745F, 1.0472F, 0.0F);
		yes.addChild(bone68);

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone69, -0.1745F, 2.0944F, 0.0F);
		yes.addChild(bone69);

		ihaveexam2 = new RendererModel(this);
		ihaveexam2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(ihaveexam2, 0.0F, 3.1416F, 0.0F);
		unit.addChild(ihaveexam2);

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone43, 0.0F, 1.5708F, 0.0F);
		ihaveexam2.addChild(bone43);
		bone43.cubeList.add(new ModelBox(bone43, 202, 194, 3.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone43.cubeList.add(new ModelBox(bone43, 202, 194, -9.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone44, 0.0F, 0.5236F, 0.0F);
		ihaveexam2.addChild(bone44);
		bone44.cubeList.add(new ModelBox(bone44, 195, 188, 3.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 195, 188, -9.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 265, 110, -8.0F, -46.6F, -64.2F, 4, 4, 1, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 406, 216, 4.5F, -46.1F, -64.2F, 3, 3, 1, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 406, 216, 5.5F, -46.1F, -66.2F, 1, 3, 1, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 406, 216, 5.5F, -44.1F, -65.2F, 1, 1, 1, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 248, 132, -8.5F, -47.1F, -64.4F, 1, 1, 1, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 248, 132, -6.5F, -42.6F, -64.2F, 1, 1, 1, 0.0F, false));
		bone44.cubeList.add(new ModelBox(bone44, 248, 132, -4.6F, -47.1F, -64.7F, 1, 1, 1, 0.0F, false));

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone45, 0.0F, -0.5236F, 0.0F);
		ihaveexam2.addChild(bone45);
		bone45.cubeList.add(new ModelBox(bone45, 195, 188, 3.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 195, 188, -9.0F, -49.6F, -63.8F, 6, 10, 6, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 403, 253, -8.0F, -46.6F, -64.8F, 4, 4, 1, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 403, 253, -7.0F, -45.6F, -65.8F, 2, 2, 1, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 278, 132, 4.0F, -44.6F, -66.0F, 4, 2, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 418, 214, 4.0F, -47.6F, -64.0F, 4, 5, 1, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 408, 221, 5.0F, -46.6F, -65.0F, 1, 2, 1, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 514, 199, 4.0F, -45.6F, -65.0F, 1, 1, 1, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 514, 199, 7.0F, -45.6F, -65.0F, 1, 1, 1, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 408, 221, 4.0F, -47.6F, -66.0F, 1, 1, 2, 0.0F, false));
		bone45.cubeList.add(new ModelBox(bone45, 349, 265, 4.0F, -47.6F, -67.0F, 1, 1, 1, 0.0F, false));

		ihaveexam3 = new RendererModel(this);
		ihaveexam3.setRotationPoint(0.0F, -63.0F, 0.0F);
		unit.addChild(ihaveexam3);

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone48, 0.0F, -2.0944F, 0.0F);
		ihaveexam3.addChild(bone48);
		bone48.cubeList.add(new ModelBox(bone48, 105, 171, -8.0F, -12.0F, -18.0F, 16, 12, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 251, 150, -8.0F, -12.0F, 14.0F, 16, 12, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 457, 0, -16.0F, -3.0F, -14.0F, 33, 8, 28, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 379, 285, -8.0F, -27.0F, -14.0F, 16, 6, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 469, 99, -8.0F, -56.0F, -14.0F, 16, 29, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 437, 143, -8.0F, -21.0F, -14.0F, 16, 10, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 468, 102, -8.0F, -99.0F, -14.0F, 16, 37, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 454, 102, -8.0F, -136.0F, -14.0F, 16, 31, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 464, 106, -8.0F, -169.0F, -14.0F, 16, 27, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 379, 285, -8.0F, -27.0F, 10.0F, 16, 6, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 458, 104, -8.0F, -56.0F, 10.0F, 16, 29, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 458, 104, -8.0F, -21.0F, 10.0F, 16, 11, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 458, 104, -8.0F, -99.0F, 10.0F, 16, 37, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 458, 104, -8.0F, -169.0F, 10.0F, 16, 27, 4, 0.0F, false));
        bone48.cubeList.add(new ModelBox(bone48, 458, 104, -8.0F, -136.0F, 10.0F, 16, 31, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 379, 285, -8.0F, -62.0F, 10.0F, 16, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 379, 285, -8.0F, -62.0F, -14.0F, 16, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 379, 285, -8.0F, -105.0F, 10.0F, 16, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 386, 192, -8.0F, -105.0F, -14.0F, 16, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 386, 192, -8.0F, -142.0F, -14.0F, 16, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 379, 285, -8.0F, -142.0F, 10.0F, 16, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 386, 192, -8.0F, -175.0F, -14.0F, 18, 6, 4, 0.0F, false));
		bone48.cubeList.add(new ModelBox(bone48, 386, 192, -10.0F, -175.0F, 10.0F, 18, 6, 4, 0.0F, false));

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone47, 0.0F, -1.0472F, 0.0F);
		ihaveexam3.addChild(bone47);
		bone47.cubeList.add(new ModelBox(bone47, 90, 244, -8.0F, -12.0F, -18.0F, 16, 12, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 90, 244, -8.0F, -12.0F, 14.0F, 16, 12, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 379, 285, -8.0F, -27.0F, -14.0F, 16, 6, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 458, 96, -8.0F, -56.0F, -14.0F, 16, 29, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 458, 96, -8.0F, -99.0F, -14.0F, 16, 37, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 458, 96, -8.0F, -136.0F, -14.0F, 16, 31, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 458, 96, -8.0F, -169.0F, -14.0F, 16, 27, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 454, 112, -8.0F, -21.0F, -14.0F, 16, 15, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 379, 285, -8.0F, -27.0F, 10.0F, 16, 6, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 471, 114, -8.0F, -56.0F, 10.0F, 16, 29, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 471, 114, -8.0F, -21.0F, 10.0F, 16, 10, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 471, 114, -8.0F, -99.0F, 10.0F, 16, 37, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 471, 114, -8.0F, -136.0F, 10.0F, 16, 31, 4, 0.0F, false));
        bone47.cubeList.add(new ModelBox(bone47, 471, 114, -8.0F, -169.0F, 10.0F, 16, 27, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 379, 285, -8.0F, -62.0F, -14.0F, 16, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 379, 285, -8.0F, -62.0F, 10.0F, 16, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 386, 192, -8.0F, -105.0F, -14.0F, 16, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 379, 285, -8.0F, -105.0F, 10.0F, 16, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 386, 192, -8.0F, -142.0F, -14.0F, 16, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 379, 285, -8.0F, -142.0F, 10.0F, 16, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 386, 192, -10.0F, -175.0F, -14.0F, 18, 6, 4, 0.0F, false));
		bone47.cubeList.add(new ModelBox(bone47, 386, 192, -8.0F, -175.0F, 10.0F, 18, 6, 4, 0.0F, false));

		bone52 = new RendererModel(this);
		bone52.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone52, 0.0262F, 0.0F, 0.0F);
		bone47.addChild(bone52);

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone53, 0.0262F, 3.1416F, 0.0F);
		bone47.addChild(bone53);

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(0.0F, 0.0F, 0.0F);
		ihaveexam3.addChild(bone46);
		bone46.cubeList.add(new ModelBox(bone46, 105, 171, -8.0F, -12.0F, -18.0F, 16, 12, 4, 0.0F, false));
		bone46.cubeList.add(new ModelBox(bone46, 90, 244, -8.0F, -12.0F, 14.0F, 16, 12, 4, 0.0F, false));

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(0.0F, 64.0F, 0.0F);
		ihaveexam3.addChild(bone49);
		bone49.cubeList.add(new ModelBox(bone49, 90, 244, -20.0F, -76.0F, -2.0F, 4, 16, 4, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 105, 171, 16.0F, -76.0F, -2.0F, 4, 16, 4, 0.0F, false));
		bone49.cubeList.add(new ModelBox(bone49, 238, 18, -61.0F, -48.6F, -6.0F, 41, 8, 12, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(0.0F, 64.0F, 0.0F);
		setRotationAngle(bone50, 0.0F, -1.0472F, 0.0F);
		ihaveexam3.addChild(bone50);
		bone50.cubeList.add(new ModelBox(bone50, 105, 171, -20.0F, -76.0F, -2.0F, 4, 16, 4, 0.0F, false));
		bone50.cubeList.add(new ModelBox(bone50, 105, 171, 16.0F, -76.0F, -2.0F, 4, 16, 4, 0.0F, false));
		bone50.cubeList.add(new ModelBox(bone50, 238, 18, 24.0F, -48.6F, -6.0F, 37, 8, 12, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(0.0F, -1.0F, 0.0F);
		bone50.addChild(bone62);
		bone62.cubeList.add(new ModelBox(bone62, 238, 18, -61.0F, -47.6F, -6.0F, 36, 8, 12, 0.0F, false));
		bone62.cubeList.add(new ModelBox(bone62, 271, 214, -29.0F, -41.6F, -6.0F, 4, 41, 12, 0.0F, false));
		bone62.cubeList.add(new ModelBox(bone62, 271, 214, -53.0F, -2.0F, -4.0F, 28, 2, 8, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.0F, -1.0F, 0.0F);
		setRotationAngle(bone63, 0.0F, 3.1416F, 0.0F);
		bone50.addChild(bone63);
		bone63.cubeList.add(new ModelBox(bone63, 271, 214, -29.0F, -41.6F, -6.0F, 4, 41, 12, 0.0F, false));
		bone63.cubeList.add(new ModelBox(bone63, 271, 214, -53.4F, -2.0F, -4.0F, 29, 2, 8, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(0.0F, 64.0F, 0.0F);
		setRotationAngle(bone51, 0.0F, -2.0944F, 0.0F);
		ihaveexam3.addChild(bone51);
		bone51.cubeList.add(new ModelBox(bone51, 105, 171, -20.0F, -76.0F, -2.0F, 4, 16, 4, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 105, 171, 16.0F, -76.0F, -2.0F, 4, 16, 4, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 238, 18, -61.0F, -48.6F, -6.0F, 36, 8, 12, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 271, 214, -29.0F, -42.6F, -6.0F, 4, 41, 12, 0.0F, false));
		bone51.cubeList.add(new ModelBox(bone51, 271, 214, -53.3F, -2.6F, -4.0F, 28, 2, 8, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone73, 0.0F, 3.1416F, 0.0F);
		bone51.addChild(bone73);
		bone73.cubeList.add(new ModelBox(bone73, 271, 214, -53.4F, -3.0F, -4.0F, 30, 2, 8, 0.0F, false));
		bone73.cubeList.add(new ModelBox(bone73, 271, 214, -29.0F, -42.6F, -6.0F, 4, 41, 12, 0.0F, false));
		bone73.cubeList.add(new ModelBox(bone73, 238, 18, -61.0F, -48.6F, -6.0F, 36, 8, 12, 0.0F, false));

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.0F, 63.0F, 0.0F);
		ihaveexam3.addChild(bone37);
		bone37.cubeList.add(new ModelBox(bone37, 271, 214, -29.0F, -41.6F, -6.0F, 4, 41, 12, 0.0F, false));
		bone37.cubeList.add(new ModelBox(bone37, 271, 214, -53.0F, -1.6F, -4.0F, 28, 2, 8, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(0.0F, 63.0F, 0.0F);
		setRotationAngle(bone64, 0.0F, 3.1416F, 0.0F);
		ihaveexam3.addChild(bone64);
		bone64.cubeList.add(new ModelBox(bone64, 238, 18, -61.0F, -47.6F, -6.0F, 36, 8, 12, 0.0F, false));
		bone64.cubeList.add(new ModelBox(bone64, 271, 214, -29.0F, -41.6F, -6.0F, 4, 41, 12, 0.0F, false));
		bone64.cubeList.add(new ModelBox(bone64, 271, 214, -53.4F, -2.0F, -4.0F, 28, 2, 8, 0.0F, false));

		underpillar3 = new RendererModel(this);
		underpillar3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar3, 0.0F, -2.618F, 0.0F);
		unit.addChild(underpillar3);

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone79, -1.2217F, 0.0F, 0.0F);
		underpillar3.addChild(bone79);
		bone79.cubeList.add(new ModelBox(bone79, 180, 209, -9.0191F, -73.4739F, -21.4F, 6, 37, 6, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 256, 188, 2.9809F, -65.4739F, -29.4F, 6, 40, 8, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 251, 208, -9.0191F, -65.4739F, -29.4F, 6, 41, 8, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 209, 254, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));
		bone79.cubeList.add(new ModelBox(bone79, 204, 194, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone80, 0.1745F, 0.0F, 0.0F);
		underpillar3.addChild(bone80);
		bone80.cubeList.add(new ModelBox(bone80, 202, 194, 3.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 251, 208, -9.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 251, 208, 3.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone80.cubeList.add(new ModelBox(bone80, 202, 194, -9.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone81, -0.0873F, 0.0F, 0.0F);
		underpillar3.addChild(bone81);
		bone81.cubeList.add(new ModelBox(bone81, 165, 217, 3.0F, -8.6F, 35.2F, 6, 4, 18, 0.0F, false));
		bone81.cubeList.add(new ModelBox(bone81, 165, 217, -9.0F, -8.6F, 32.2F, 6, 4, 21, 0.0F, false));

		underpillar2 = new RendererModel(this);
		underpillar2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar2, 0.0F, -0.5236F, 0.0F);
		unit.addChild(underpillar2);

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone61, -1.2217F, 0.0F, 0.0F);
		underpillar2.addChild(bone61);
		bone61.cubeList.add(new ModelBox(bone61, 204, 204, -9.0191F, -73.4739F, -21.4F, 6, 37, 6, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 251, 208, 2.9809F, -65.4739F, -29.4F, 6, 40, 8, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 251, 208, -9.0191F, -65.4739F, -29.4F, 6, 41, 8, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 204, 204, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 209, 254, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone74, 0.1745F, 0.0F, 0.0F);
		underpillar2.addChild(bone74);
		bone74.cubeList.add(new ModelBox(bone74, 202, 194, 3.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 251, 208, -9.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 251, 208, 3.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone74.cubeList.add(new ModelBox(bone74, 202, 194, -9.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone75, -0.0873F, 0.0F, 0.0F);
		underpillar2.addChild(bone75);
		bone75.cubeList.add(new ModelBox(bone75, 165, 217, 3.0F, -8.6F, 35.2F, 6, 4, 18, 0.0F, false));
		bone75.cubeList.add(new ModelBox(bone75, 165, 217, -9.0F, -8.6F, 32.2F, 6, 4, 21, 0.0F, false));

		underpillar4 = new RendererModel(this);
		underpillar4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar4, 0.0F, -1.5708F, 0.0F);
		unit.addChild(underpillar4);

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone76, -1.2217F, 0.0F, 0.0F);
		underpillar4.addChild(bone76);
		bone76.cubeList.add(new ModelBox(bone76, 204, 204, -9.0191F, -73.4739F, -21.4F, 6, 37, 6, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 251, 208, 2.9809F, -65.4739F, -29.4F, 6, 40, 8, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 251, 208, -9.0191F, -65.4739F, -29.4F, 6, 41, 8, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 204, 204, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));
		bone76.cubeList.add(new ModelBox(bone76, 209, 254, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));

		bone77 = new RendererModel(this);
		bone77.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone77, 0.1745F, 0.0F, 0.0F);
		underpillar4.addChild(bone77);
		bone77.cubeList.add(new ModelBox(bone77, 202, 194, 3.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 251, 208, -9.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 251, 208, 3.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone77.cubeList.add(new ModelBox(bone77, 202, 194, -9.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone78, -0.0873F, 0.0F, 0.0F);
		underpillar4.addChild(bone78);
		bone78.cubeList.add(new ModelBox(bone78, 165, 217, 3.0F, -8.6F, 35.2F, 6, 4, 18, 0.0F, false));
		bone78.cubeList.add(new ModelBox(bone78, 165, 217, -9.0F, -8.6F, 32.2F, 6, 4, 21, 0.0F, false));

		underpillar5 = new RendererModel(this);
		underpillar5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar5, 0.0F, 2.618F, 0.0F);
		unit.addChild(underpillar5);

		bone82 = new RendererModel(this);
		bone82.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone82, -1.2217F, 0.0F, 0.0F);
		underpillar5.addChild(bone82);
		bone82.cubeList.add(new ModelBox(bone82, 185, 198, -9.0191F, -73.4739F, -21.4F, 6, 37, 6, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 251, 208, 2.9809F, -65.4739F, -29.4F, 6, 40, 8, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 251, 208, -9.0191F, -65.4739F, -29.4F, 6, 41, 8, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 209, 254, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));
		bone82.cubeList.add(new ModelBox(bone82, 204, 194, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone83, 0.1745F, 0.0F, 0.0F);
		underpillar5.addChild(bone83);
		bone83.cubeList.add(new ModelBox(bone83, 202, 194, 3.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 251, 208, -9.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 251, 208, 3.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone83.cubeList.add(new ModelBox(bone83, 202, 194, -9.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone84, -0.0873F, 0.0F, 0.0F);
		underpillar5.addChild(bone84);
		bone84.cubeList.add(new ModelBox(bone84, 165, 217, 3.0F, -8.6F, 35.2F, 6, 4, 18, 0.0F, false));
		bone84.cubeList.add(new ModelBox(bone84, 165, 217, -9.0F, -8.6F, 32.2F, 6, 4, 21, 0.0F, false));

		underpillar6 = new RendererModel(this);
		underpillar6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar6, 0.0F, 0.5236F, 0.0F);
		unit.addChild(underpillar6);

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone85, -1.2217F, 0.0F, 0.0F);
		underpillar6.addChild(bone85);
		bone85.cubeList.add(new ModelBox(bone85, 204, 204, -9.0191F, -73.4739F, -21.4F, 6, 37, 6, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 251, 208, 2.9809F, -65.4739F, -29.4F, 6, 40, 8, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 251, 208, -9.0191F, -65.4739F, -29.4F, 6, 41, 8, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 209, 254, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));
		bone85.cubeList.add(new ModelBox(bone85, 204, 194, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));

		bone86 = new RendererModel(this);
		bone86.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone86, 0.1745F, 0.0F, 0.0F);
		underpillar6.addChild(bone86);
		bone86.cubeList.add(new ModelBox(bone86, 202, 194, 3.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));
		bone86.cubeList.add(new ModelBox(bone86, 251, 208, -9.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone86.cubeList.add(new ModelBox(bone86, 251, 208, 3.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone86.cubeList.add(new ModelBox(bone86, 202, 194, -9.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));

		bone87 = new RendererModel(this);
		bone87.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone87, -0.0873F, 0.0F, 0.0F);
		underpillar6.addChild(bone87);
		bone87.cubeList.add(new ModelBox(bone87, 165, 217, 3.0F, -8.6F, 35.2F, 6, 4, 18, 0.0F, false));
		bone87.cubeList.add(new ModelBox(bone87, 165, 217, -9.0F, -8.6F, 32.2F, 6, 4, 21, 0.0F, false));

		underpillar7 = new RendererModel(this);
		underpillar7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(underpillar7, 0.0F, 1.5708F, 0.0F);
		unit.addChild(underpillar7);

		bone88 = new RendererModel(this);
		bone88.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone88, -1.2217F, 0.0F, 0.0F);
		underpillar7.addChild(bone88);
		bone88.cubeList.add(new ModelBox(bone88, 204, 204, -9.0191F, -73.4739F, -21.4F, 6, 37, 6, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 251, 208, 2.9809F, -65.4739F, -29.4F, 6, 40, 8, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 251, 208, -9.0191F, -65.4739F, -29.4F, 6, 41, 8, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 204, 204, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));
		bone88.cubeList.add(new ModelBox(bone88, 209, 254, 3.0809F, -73.4739F, -21.4F, 5, 37, 6, 0.0F, false));

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone89, 0.1745F, 0.0F, 0.0F);
		underpillar7.addChild(bone89);
		bone89.cubeList.add(new ModelBox(bone89, 202, 194, 3.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 251, 208, -9.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 251, 208, 3.0F, -27.6F, 10.2F, 6, 31, 22, 0.0F, false));
		bone89.cubeList.add(new ModelBox(bone89, 202, 194, -9.0F, -27.6F, 32.2F, 6, 32, 5, 0.0F, false));

		bone90 = new RendererModel(this);
		bone90.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone90, -0.0873F, 0.0F, 0.0F);
		underpillar7.addChild(bone90);
		bone90.cubeList.add(new ModelBox(bone90, 165, 217, 3.0F, -8.6F, 35.2F, 6, 4, 18, 0.0F, false));
		bone90.cubeList.add(new ModelBox(bone90, 165, 217, -9.0F, -8.6F, 32.2F, 6, 4, 21, 0.0F, false));

		greenfloor = new RendererModel(this);
		greenfloor.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(greenfloor, 0.0F, -0.5236F, 0.0F);
		unit.addChild(greenfloor);

		bone91 = new RendererModel(this);
		bone91.setRotationPoint(0.0F, 0.0F, 0.0F);
		greenfloor.addChild(bone91);
		bone91.cubeList.add(new ModelBox(bone91, 240, 10, 39.8F, -2.0F, -19.0F, 11, 2, 38, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone91.cubeList.add(new ModelBox(bone91, 271, 214, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone92 = new RendererModel(this);
		bone92.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone92, 0.0F, -1.0472F, 0.0F);
		greenfloor.addChild(bone92);
		bone92.cubeList.add(new ModelBox(bone92, 240, 8, 39.8F, -2.0F, -19.0F, 11, 2, 38, 0.0F, false));
		bone92.cubeList.add(new ModelBox(bone92, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone92.cubeList.add(new ModelBox(bone92, 271, 214, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone93 = new RendererModel(this);
		bone93.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone93, 0.0F, -2.0944F, 0.0F);
		greenfloor.addChild(bone93);
		bone93.cubeList.add(new ModelBox(bone93, 240, 10, 39.8F, -2.0F, -19.0F, 11, 2, 38, 0.0F, false));
		bone93.cubeList.add(new ModelBox(bone93, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone93.cubeList.add(new ModelBox(bone93, 271, 214, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone94 = new RendererModel(this);
		bone94.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone94, 0.0F, 3.1416F, 0.0F);
		greenfloor.addChild(bone94);
		bone94.cubeList.add(new ModelBox(bone94, 240, 10, 39.8F, -2.0F, -19.0F, 11, 2, 38, 0.0F, false));
		bone94.cubeList.add(new ModelBox(bone94, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone94.cubeList.add(new ModelBox(bone94, 271, 214, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone95 = new RendererModel(this);
		bone95.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone95, 0.0F, 1.0472F, 0.0F);
		greenfloor.addChild(bone95);
		bone95.cubeList.add(new ModelBox(bone95, 240, 10, 39.8F, -2.0F, -19.0F, 11, 2, 38, 0.0F, false));
		bone95.cubeList.add(new ModelBox(bone95, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone95.cubeList.add(new ModelBox(bone95, 271, 214, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone96 = new RendererModel(this);
		bone96.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone96, 0.0F, 2.0944F, 0.0F);
		greenfloor.addChild(bone96);
		bone96.cubeList.add(new ModelBox(bone96, 240, 10, 39.8F, -2.0F, -19.0F, 11, 2, 38, 0.0F, false));
		bone96.cubeList.add(new ModelBox(bone96, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone96.cubeList.add(new ModelBox(bone96, 271, 214, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		greenfloor2 = new RendererModel(this);
		greenfloor2.setRotationPoint(0.0F, -44.0F, 0.0F);
		setRotationAngle(greenfloor2, 0.0F, -0.5236F, 0.0F);
		unit.addChild(greenfloor2);

		bone97 = new RendererModel(this);
		bone97.setRotationPoint(0.0F, 0.0F, 0.0F);
		greenfloor2.addChild(bone97);
		bone97.cubeList.add(new ModelBox(bone97, 271, 214, 39.8F, -2.0F, -21.0F, 14, 2, 42, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone97.cubeList.add(new ModelBox(bone97, 96, 138, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone98 = new RendererModel(this);
		bone98.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone98, 0.0F, -1.0472F, 0.0F);
		greenfloor2.addChild(bone98);
		bone98.cubeList.add(new ModelBox(bone98, 271, 214, 39.8F, -2.0F, -21.0F, 14, 2, 42, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone98.cubeList.add(new ModelBox(bone98, 0, 144, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone99 = new RendererModel(this);
		bone99.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone99, 0.0F, -2.0944F, 0.0F);
		greenfloor2.addChild(bone99);
		bone99.cubeList.add(new ModelBox(bone99, 271, 214, 39.8F, -2.0F, -21.0F, 14, 2, 42, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone99.cubeList.add(new ModelBox(bone99, 96, 144, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone100 = new RendererModel(this);
		bone100.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone100, 0.0F, 3.1416F, 0.0F);
		greenfloor2.addChild(bone100);
		bone100.cubeList.add(new ModelBox(bone100, 271, 214, 39.8F, -2.0F, -21.0F, 14, 2, 42, 0.0F, false));
		bone100.cubeList.add(new ModelBox(bone100, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone100.cubeList.add(new ModelBox(bone100, 0, 156, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone101 = new RendererModel(this);
		bone101.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone101, 0.0F, 1.0472F, 0.0F);
		greenfloor2.addChild(bone101);
		bone101.cubeList.add(new ModelBox(bone101, 271, 214, 39.8F, -2.0F, -21.0F, 15, 2, 41, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone101.cubeList.add(new ModelBox(bone101, 112, 144, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		bone102 = new RendererModel(this);
		bone102.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone102, 0.0F, 2.0944F, 0.0F);
		greenfloor2.addChild(bone102);
		bone102.cubeList.add(new ModelBox(bone102, 271, 214, 39.8F, -2.0F, -21.0F, 14, 2, 42, 0.0F, false));
		bone102.cubeList.add(new ModelBox(bone102, 271, 214, 28.8F, -2.0F, -13.0F, 11, 2, 26, 0.0F, false));
		bone102.cubeList.add(new ModelBox(bone102, 64, 144, 17.8F, -2.0F, -12.0F, 11, 2, 24, 0.0F, false));

		greenwall = new RendererModel(this);
		greenwall.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(greenwall);

		wall = new RendererModel(this);
		wall.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(wall, 0.0F, -1.0472F, 0.0F);
		greenwall.addChild(wall);

		under6 = new RendererModel(this);
		under6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under6, 0.0F, 3.1416F, 0.0F);
		unit.addChild(under6);

		bone108 = new RendererModel(this);
		bone108.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone108, 0.2618F, 0.0F, 0.0F);
		under6.addChild(bone108);
		bone108.cubeList.add(new ModelBox(bone108, 271, 214, -11.0F, -38.0F, 18.0F, 23, 6, 25, 0.0F, false));

		under5 = new RendererModel(this);
		under5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under5, 0.0F, 2.0944F, 0.0F);
		unit.addChild(under5);

		bone107 = new RendererModel(this);
		bone107.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone107, 0.2618F, 0.0F, 0.0F);
		under5.addChild(bone107);
		bone107.cubeList.add(new ModelBox(bone107, 271, 214, -11.0F, -38.0F, 18.0F, 23, 6, 25, 0.0F, false));

		under4 = new RendererModel(this);
		under4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under4, 0.0F, 1.0472F, 0.0F);
		unit.addChild(under4);

		bone106 = new RendererModel(this);
		bone106.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone106, 0.2618F, 0.0F, 0.0F);
		under4.addChild(bone106);
		bone106.cubeList.add(new ModelBox(bone106, 271, 214, -11.0F, -38.0F, 18.0F, 23, 6, 25, 0.0F, false));

		under3 = new RendererModel(this);
		under3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under3, 0.0F, -2.0944F, 0.0F);
		unit.addChild(under3);

		bone105 = new RendererModel(this);
		bone105.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone105, 0.2618F, 0.0F, 0.0F);
		under3.addChild(bone105);
		bone105.cubeList.add(new ModelBox(bone105, 271, 214, -11.0F, -38.0F, 18.0F, 23, 6, 25, 0.0F, false));

		under2 = new RendererModel(this);
		under2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(under2, 0.0F, -1.0472F, 0.0F);
		unit.addChild(under2);

		bone104 = new RendererModel(this);
		bone104.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone104, 0.2618F, 0.0F, 0.0F);
		under2.addChild(bone104);
		bone104.cubeList.add(new ModelBox(bone104, 271, 214, -11.0F, -38.0F, 18.0F, 23, 6, 25, 0.0F, false));

		under = new RendererModel(this);
		under.setRotationPoint(0.0F, 0.0F, 0.0F);
		unit.addChild(under);

		bone103 = new RendererModel(this);
		bone103.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone103, 0.2618F, 0.0F, 0.0F);
		under.addChild(bone103);
		bone103.cubeList.add(new ModelBox(bone103, 271, 214, -11.0F, -38.0F, 18.0F, 23, 6, 25, 0.0F, false));

		pump2 = new RendererModel(this);
		pump2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(pump2, 0.3491F, -1.5708F, 0.0F);
		copper.addChild(pump2);
		pump2.cubeList.add(new ModelBox(pump2, 306, 105, -3.0F, -71.0F, -30.0F, 6, 7, 16, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 460, 190, -1.5F, -69.7F, -33.0F, 3, 3, 5, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 395, 288, -2.5F, -70.7F, -35.0F, 5, 5, 2, 0.0F, false));
		pump2.cubeList.add(new ModelBox(pump2, 478, 222, -1.5F, -69.7F, -36.0F, 3, 3, 1, 0.0F, false));
        pump2.cubeList.add(new ModelBox(pump2, 460, 190, -3.5F, -71.7F, -15.0F, 7, 6, 2, 0.0F, false));
    }


	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}

	@Override
	public void render(ConsoleTile console, float scale) {

		//Throttle
		ThrottleControl throttle = console.getControl(ThrottleControl.class);
        if (throttle != null) {
            this.levers.rotateAngleX = throttle.getAmount() * 3;
            this.levers.rotateAngleY = 0;
            this.levers.rotateAngleZ = 0;
        }

        //Randomizer control
	/*	RandomiserControl randomizer = console.getControl(RandomiserControl.class);
		if(randomizer != null) {
			this.rotator.rotateAngleY = (float) Math.toRadians((randomizer.getAnimationTicks() * 36) + Minecraft.getInstance().getRenderPartialTicks() % 360.0);
		}*/

        //HandBrake
        HandbrakeControl handbrakeControl = console.getControl(HandbrakeControl.class);
        bigboy.rotateAngleX = (float) Math.toRadians(handbrakeControl.isFree() ? -25 : 25);
        blackboy3.rotateAngleX = (float) Math.toRadians(handbrakeControl.isFree() ? 5 : -5);

        //Fast Return
        FastReturnControl fastReturnControl = console.getControl(FastReturnControl.class);
        handbreak.rotateAngleX = fastReturnControl.getAnimationTicks() > 0 ? 65 : -30;


		//General Rendering
		float offset = MathHelper.cos(console.flightTicks * 0.1F) * -0.70F;
		GlStateManager.pushMatrix();
		if(console.isInFlight()){
			GlStateManager.translated(0, offset, 0);
			GlStateManager.rotated(console.getWorld().getGameTime() / 2 * (throttle.getAmount() * 10), 0, 1, 0);
		}
		rotor.render(scale);
		GlStateManager.popMatrix();

		GlStateManager.pushMatrix();
		if(console.isInFlight()){
			GlStateManager.translated(0, -offset, 0);
			GlStateManager.rotated(console.getWorld().getGameTime() / 2 * (throttle.getAmount() * 10), 0, 1, 0);
		}
		rotortop.render(scale);
		GlStateManager.popMatrix();

		copper.render(scale);
	}
}