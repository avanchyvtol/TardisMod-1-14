package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.client.models.IExteriorModel;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.helper.RenderUtil;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class PoliceBoxExteriorModel extends ExteriorModel implements IExteriorModel{
	private final RendererModel boti;
	private final RendererModel glow;
	private final RendererModel Newbery;
	private final RendererModel Base;
	private final RendererModel CornerPosts;
	private final RendererModel UpperSignage;
	private final RendererModel bone7;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel Trim;
	private final RendererModel RoofStacks;
	private final RendererModel SideWall1;
	private final RendererModel bone;
	private final RendererModel SideWall2;
	private final RendererModel bone2;
	private final RendererModel SideWall3;
	private final RendererModel bone3;
	private final RendererModel LeftDoor;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel RightDoor;
	private final RendererModel bone6;

	public PoliceBoxExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 0, 297, -13.0F, -65.0F, -14.5F, 26, 61, 0, 0.0F, false));

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		glow.cubeList.add(new ModelBox(glow, 34, 36, -1.5F, -82.5F, -1.5F, 3, 4, 3, 0.0F, false));
		glow.cubeList.add(new ModelBox(glow, 22, 50, -2.0F, -83.0F, -2.0F, 4, 5, 4, 0.0F, false));

		Newbery = new RendererModel(this);
		Newbery.setRotationPoint(0.0F, 24.0F, 0.0F);

		Base = new RendererModel(this);
		Base.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(Base);
		Base.cubeList.add(new ModelBox(Base, 0, 0, -23.5F, -3.0F, -23.5F, 47, 3, 47, 0.0F, false));

		CornerPosts = new RendererModel(this);
		CornerPosts.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(CornerPosts);
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 82, 131, 15.5F, -73.0F, -20.5F, 5, 70, 5, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 22, 74, 16.0F, -74.0F, -20.0F, 4, 1, 4, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 22, 74, -20.0F, -74.0F, -20.0F, 4, 1, 4, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 22, 74, -20.0F, -74.0F, 16.0F, 4, 1, 4, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 22, 74, 16.0F, -74.0F, 16.0F, 4, 1, 4, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 82, 131, -20.5F, -73.0F, -20.5F, 5, 70, 5, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 82, 131, -20.5F, -73.0F, 15.5F, 5, 70, 5, 0.0F, false));
		CornerPosts.cubeList.add(new ModelBox(CornerPosts, 82, 131, 15.5F, -73.0F, 15.5F, 5, 70, 5, 0.0F, false));

		UpperSignage = new RendererModel(this);
		UpperSignage.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(UpperSignage);
		UpperSignage.cubeList.add(new ModelBox(UpperSignage, 160, 138, -18.5F, -70.0F, -22.5F, 37, 4, 4, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone7, 0.0F, -1.5708F, 0.0F);
		UpperSignage.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 160, 138, -18.5F, -70.0F, -22.5F, 37, 4, 4, 0.0F, false));

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone8, 0.0F, 3.1416F, 0.0F);
		UpperSignage.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 160, 138, -18.5F, -70.0F, -22.5F, 37, 4, 4, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone9, 0.0F, 1.5708F, 0.0F);
		UpperSignage.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 160, 138, -18.5F, -70.0F, -22.5F, 37, 4, 4, 0.0F, false));

		Trim = new RendererModel(this);
		Trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(Trim);
		Trim.cubeList.add(new ModelBox(Trim, 208, 210, 14.5F, -65.0F, -18.5F, 4, 62, 4, 0.0F, false));
		Trim.cubeList.add(new ModelBox(Trim, 208, 210, -18.5F, -65.0F, -18.5F, 4, 62, 4, 0.0F, false));
		Trim.cubeList.add(new ModelBox(Trim, 208, 210, -18.5F, -65.0F, 14.5F, 4, 62, 4, 0.0F, false));
		Trim.cubeList.add(new ModelBox(Trim, 208, 210, 14.5F, -65.0F, 14.5F, 4, 62, 4, 0.0F, false));
		Trim.cubeList.add(new ModelBox(Trim, 114, 92, -18.5F, -66.0F, -18.5F, 37, 1, 37, 0.0F, false));

		RoofStacks = new RendererModel(this);
		RoofStacks.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(RoofStacks);
		RoofStacks.cubeList.add(new ModelBox(RoofStacks, 0, 50, -19.5F, -73.0F, -19.5F, 39, 3, 39, 0.0F, false));
		RoofStacks.cubeList.add(new ModelBox(RoofStacks, 0, 92, -19.0F, -74.0F, -19.0F, 38, 1, 38, 0.0F, false));
		RoofStacks.cubeList.add(new ModelBox(RoofStacks, 117, 50, -17.5F, -75.5F, -17.5F, 35, 2, 35, 0.0F, false));
		RoofStacks.cubeList.add(new ModelBox(RoofStacks, 13, 36, -3.5F, -78.5F, -3.5F, 7, 3, 7, 0.0F, false));

		SideWall1 = new RendererModel(this);
		SideWall1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(SideWall1, 0.0F, 1.5708F, 0.0F);
		Newbery.addChild(SideWall1);
		SideWall1.cubeList.add(new ModelBox(SideWall1, 220, 210, -12.5F, -5.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 224, 214, -12.5F, -20.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 224, 214, -12.5F, -35.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 224, 214, -12.5F, -50.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 224, 214, -12.5F, -65.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 224, 224, 12.5F, -65.0F, -17.5F, 2, 62, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 212, 146, -2.5F, -65.0F, -17.5F, 5, 62, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 230, 146, -0.5F, -65.0F, -18.5F, 1, 62, 1, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 224, 224, -14.5F, -65.0F, -17.5F, 2, 62, 2, 0.0F, false));
		SideWall1.cubeList.add(new ModelBox(SideWall1, 141, 0, -12.5F, -48.0F, -16.5F, 25, 43, 0, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, -50.0F, -16.5F);
		SideWall1.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 0, 92, 2.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 92, -12.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));

		SideWall2 = new RendererModel(this);
		SideWall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(SideWall2, 0.0F, 3.1416F, 0.0F);
		Newbery.addChild(SideWall2);
		SideWall2.cubeList.add(new ModelBox(SideWall2, 220, 210, -12.5F, -5.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 224, 214, -12.5F, -20.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 224, 214, -12.5F, -35.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 224, 214, -12.5F, -50.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 224, 214, -12.5F, -65.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 224, 224, 12.5F, -65.0F, -17.5F, 2, 62, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 212, 146, -2.5F, -65.0F, -17.5F, 5, 62, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 230, 146, -0.5F, -65.0F, -18.5F, 1, 62, 1, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 224, 224, -14.5F, -65.0F, -17.5F, 2, 62, 2, 0.0F, false));
		SideWall2.cubeList.add(new ModelBox(SideWall2, 141, 0, -12.5F, -48.0F, -16.5F, 25, 43, 0, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, -50.0F, -16.5F);
		setRotationAngle(bone2, 0.0873F, 0.0F, 0.0F);
		SideWall2.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 0, 92, 2.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 0, 92, -12.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));

		SideWall3 = new RendererModel(this);
		SideWall3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(SideWall3, 0.0F, -1.5708F, 0.0F);
		Newbery.addChild(SideWall3);
		SideWall3.cubeList.add(new ModelBox(SideWall3, 220, 210, -12.5F, -5.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 224, 214, -12.5F, -20.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 224, 214, -12.5F, -35.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 224, 214, -12.5F, -50.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 224, 214, -12.5F, -65.0F, -17.5F, 25, 2, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 224, 224, 12.5F, -65.0F, -17.5F, 2, 62, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 212, 146, -2.5F, -65.0F, -17.5F, 5, 62, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 230, 146, -0.5F, -65.0F, -18.5F, 1, 62, 1, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 224, 224, -14.5F, -65.0F, -17.5F, 2, 62, 2, 0.0F, false));
		SideWall3.cubeList.add(new ModelBox(SideWall3, 141, 0, -12.5F, -48.0F, -16.5F, 25, 43, 0, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, -50.0F, -16.5F);
		SideWall3.addChild(bone3);
		bone3.cubeList.add(new ModelBox(bone3, 0, 92, 2.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 0, 92, -12.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));

		LeftDoor = new RendererModel(this);
		LeftDoor.setRotationPoint(-14.5F, -3.0F, -15.5F);
		Newbery.addChild(LeftDoor);
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 45, 131, 2.0F, -2.0F, -2.0F, 10, 2, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 124, 2.0F, -17.0F, -2.0F, 10, 2, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 124, 2.0F, -32.0F, -2.0F, 10, 2, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 124, 2.0F, -47.0F, -2.0F, 10, 2, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 124, 2.0F, -62.0F, -2.0F, 10, 2, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 0, 215, 12.0F, -62.0F, -2.0F, 3, 62, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 230, 146, 14.0F, -62.0F, -3.0F, 1, 62, 1, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 224, 224, 0.0F, -62.0F, -2.0F, 2, 62, 2, 0.0F, false));
		LeftDoor.cubeList.add(new ModelBox(LeftDoor, 20, 0, 2.0F, -30.0F, -1.0F, 10, 28, 0, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(14.5F, -47.0F, -1.0F);
		LeftDoor.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 0, 92, -12.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(2.0F, -32.0F, -2.0F);
		LeftDoor.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 0, 64, 0.0F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));

		RightDoor = new RendererModel(this);
		RightDoor.setRotationPoint(14.5F, -3.0F, -15.5F);
		Newbery.addChild(RightDoor);
		RightDoor.cubeList.add(new ModelBox(RightDoor, 45, 131, -12.0F, -2.0F, -2.0F, 10, 2, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 124, -12.0F, -17.0F, -2.0F, 10, 2, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 124, -12.0F, -32.0F, -2.0F, 10, 2, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 124, -12.0F, -47.0F, -2.0F, 10, 2, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 124, -12.0F, -62.0F, -2.0F, 10, 2, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 224, 224, -14.0F, -62.0F, -2.0F, 2, 62, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 224, 224, -2.0F, -62.0F, -2.0F, 2, 62, 2, 0.0F, false));
		RightDoor.cubeList.add(new ModelBox(RightDoor, 0, 0, -12.0F, -45.0F, -1.0F, 10, 43, 0, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(-14.5F, -47.0F, -1.0F);
		RightDoor.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 0, 92, 2.5F, -13.0F, 0.0F, 10, 13, 1, 0.0F, false));
	}

	@Override
	public void render(ExteriorTile tile) {
		EnumDoorState state = tile.getOpen();
		switch(state) {
		case ONE:
			this.RightDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default
			this.LeftDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED)); 
			break;
		case BOTH:
			this.RightDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE)); 
			this.LeftDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open
			break;
		case CLOSED://close both doors
			this.RightDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED)); 
			this.LeftDoor.rotateAngleY = (float) Math.toRadians(
					EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
			break;
		default:
			break;
		}
		boti.render(0.0625F);
		Newbery.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1F, this.glow);
	}
	
	public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
		RendererModel.rotateAngleX = x;
		RendererModel.rotateAngleY = y;
		RendererModel.rotateAngleZ = z;
	}

	@Override
	public void renderEntity(TardisEntity ent) {
		GlStateManager.pushMatrix();
		
		GlStateManager.translated(0, -0.5, 0);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.5, 0.5, 0.5);
		Minecraft.getInstance().getTextureManager().bindTexture(TrunkExteriorRenderer.TEXTURE);
		
		boti.render(0.0625F);
		glow.render(0.0625F);
		Newbery.render(0.0625F);
		
		GlStateManager.disableRescaleNormal();
		
		GlStateManager.popMatrix();
		
	}
}