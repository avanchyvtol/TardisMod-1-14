package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

//Made with Blockbench
//Paste this code into your mod.

public class InteriorRedModel extends Model implements IInteriorDoorRenderer{
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/interior/red.png");
	
	private final RendererModel Border;
	private final RendererModel Topbit;
	private final RendererModel NorthPanel;
	private final RendererModel GlassPanel;
	private final RendererModel Beams;
	private final RendererModel Vertical2;
	private final RendererModel Horizontal2;
	private final RendererModel Handle;

	public InteriorRedModel() {
		textureWidth = 128;
		textureHeight = 128;

		Border = new RendererModel(this);
		Border.setRotationPoint(0.0F, 24.0F, 0.0F);
		Border.cubeList.add(new ModelBox(Border, 0, 0, 6.5F, -36.0F, 5.5F, 1, 36, 2, 0.0F, false));
		Border.cubeList.add(new ModelBox(Border, 0, 0, -7.5F, -32.0F, 5.5F, 15, 1, 2, 0.0F, false));
		Border.cubeList.add(new ModelBox(Border, 0, 0, -7.5F, -36.0F, 5.5F, 1, 36, 2, 0.0F, false));
		Border.cubeList.add(new ModelBox(Border, 34, 0, -8.5F, -37.0F, 5.25F, 1, 37, 2, 0.0F, false));
		Border.cubeList.add(new ModelBox(Border, 8, 6, -7.5F, -37.0F, 5.25F, 15, 1, 2, 0.0F, false));
		Border.cubeList.add(new ModelBox(Border, 8, 6, -7.5F, -33.0F, 5.25F, 15, 1, 1, 0.0F, false));
		Border.cubeList.add(new ModelBox(Border, 34, 0, 7.5F, -37.0F, 5.25F, 1, 37, 2, 0.0F, false));

		Topbit = new RendererModel(this);
		Topbit.setRotationPoint(0.0F, 0.0F, 0.0F);
		Border.addChild(Topbit);
		Topbit.cubeList.add(new ModelBox(Topbit, 0, 0, -7.75F, -36.0F, 5.75F, 15, 4, 2, 0.0F, true));

		NorthPanel = new RendererModel(this);
        NorthPanel.setRotationPoint(6.5472F, 5.9444F, 6.8056F);
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -12.0472F, -12.4444F, -0.0556F, 11, 0, 1, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -13.0472F, -12.9444F, -0.5556F, 1, 31, 1, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -12.5472F, -12.6944F, -0.5556F, 11, 0, 1, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -1.0472F, -12.9444F, -0.5556F, 1, 31, 1, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -1.0472F, -12.4444F, 0.6944F, 1, 30, 0, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -13.0472F, -12.4444F, 0.6944F, 1, 30, 0, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -13.0472F, 12.0556F, -0.5556F, 13, 6, 1, 0.0F, false));
        NorthPanel.cubeList.add(new ModelBox(NorthPanel, 8, 6, -13.0472F, -12.9444F, 0.6944F, 13, 31, 0, 0.0F, false));

		GlassPanel = new RendererModel(this);
        GlassPanel.setRotationPoint(-6.7972F, 18.0556F, 7.6944F);
		NorthPanel.addChild(GlassPanel);
		GlassPanel.cubeList.add(new ModelBox(GlassPanel, 4, 42, -5.75F, -30.0F, -7.75F, 12, 24, 0, 0.0F, false));

		Beams = new RendererModel(this);
		Beams.setRotationPoint(-2.0F, -17.5F, -7.75F);
		GlassPanel.addChild(Beams);

		Vertical2 = new RendererModel(this);
		Vertical2.setRotationPoint(0.0F, 0.0F, 0.25F);
		setRotationAngle(Vertical2, 0.0F, -0.7854F, 0.0F);
		Beams.addChild(Vertical2);
		Vertical2.cubeList.add(new ModelBox(Vertical2, 0, 40, 4.9801F, -13.5F, -5.8817F, 1, 25, 1, 0.0F, false));
		Vertical2.cubeList.add(new ModelBox(Vertical2, 30, 42, -0.3232F, -13.5F, -0.6768F, 1, 25, 1, 0.0F, false));
		Vertical2.cubeList.add(new ModelBox(Vertical2, 30, 42, 2.5052F, -13.5F, -3.5052F, 1, 25, 1, 0.0F, false));
		Vertical2.cubeList.add(new ModelBox(Vertical2, 0, 40, -2.7981F, -13.5F, 1.8964F, 1, 25, 1, 0.0F, false));

		Horizontal2 = new RendererModel(this);
		Horizontal2.setRotationPoint(2.0F, 8.0F, 0.25F);
		setRotationAngle(Horizontal2, -0.7854F, 0.0F, 0.0F);
		Beams.addChild(Horizontal2);
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 4, 68, -5.75F, -0.6768F, -0.6768F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 4, 68, -5.75F, -3.5052F, -3.5052F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 4, 68, -5.75F, -6.5104F, -6.5104F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 4, 68, -5.75F, -9.5156F, -9.5156F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 4, 68, -5.75F, -12.344F, -12.344F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -5.75F, -15.3492F, -15.3492F, 12, 1, 1, 0.0F, false));
		Horizontal2.cubeList.add(new ModelBox(Horizontal2, 0, 40, -5.75F, 1.9749F, 1.9749F, 12, 1, 1, 0.0F, false));

		Handle = new RendererModel(this);
        Handle.setRotationPoint(-0.5472F, 1.5556F, -0.5556F);
		setRotationAngle(Handle, 0.0F, -1.5708F, 0.0F);
		NorthPanel.addChild(Handle);
        Handle.cubeList.add(new ModelBox(Handle, 31, 67, -0.4F, -3.5F, 11.55F, 1, 4, 1, 0.0F, false));
    }

    public void render() {
        Border.render(0.0625F);
        NorthPanel.render(0.0625F);
    }

    public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
        RendererModel.rotateAngleX = x;
        RendererModel.rotateAngleY = y;
        RendererModel.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.translated(0, 0, 0.1);
        NorthPanel.rotateAngleY = (float) Math.toRadians(EnumDoorType.RED.getRotationForState(door.getOpenState()));
		Border.render(0.0625F);
		NorthPanel.render(0.0625F);
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		return TEXTURE;
	}
}