package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class ConsoleGalvanicModel extends Model {
	private final RendererModel glow_shaft_1;
	private final RendererModel glow_shaft_2;
	private final RendererModel glow_shaft_3;
	private final RendererModel console;
	private final RendererModel bottom_plate;
	private final RendererModel fins;
	private final RendererModel fin_1;
	private final RendererModel fin_2;
	private final RendererModel fin_3;
	private final RendererModel chunks;
	private final RendererModel chunk_1;
	private final RendererModel chunk_2;
	private final RendererModel chunk_3;
	private final RendererModel stations;
	private final RendererModel station_1;
	private final RendererModel edge_1;
	private final RendererModel cooling_blades_1;
	private final RendererModel plasma_coil_1;
	private final RendererModel belly_1;
	private final RendererModel plane_1;
	private final RendererModel rib_1;
	private final RendererModel rib_tilt_1;
	private final RendererModel rib_deco_1;
	private final RendererModel base_fin_1;
	private final RendererModel clawfoot_1;
	private final RendererModel leg_1;
	private final RendererModel ball_1;
	private final RendererModel station_2;
	private final RendererModel edge_2;
	private final RendererModel cooling_blades_2;
	private final RendererModel plasma_coil_2;
	private final RendererModel belly_2;
	private final RendererModel plane_2;
	private final RendererModel rib_2;
	private final RendererModel rib_tilt_2;
	private final RendererModel rib_deco_2;
	private final RendererModel base_fin_2;
	private final RendererModel clawfoot_2;
	private final RendererModel leg_2;
	private final RendererModel ball_2;
	private final RendererModel station_3;
	private final RendererModel edge_3;
	private final RendererModel cooling_blades_3;
	private final RendererModel plasma_coil_3;
	private final RendererModel belly_3;
	private final RendererModel plane_3;
	private final RendererModel rib_3;
	private final RendererModel rib_tilt_3;
	private final RendererModel rib_deco_3;
	private final RendererModel base_fin_3;
	private final RendererModel clawfoot_3;
	private final RendererModel leg_3;
	private final RendererModel ball_3;
	private final RendererModel station_4;
	private final RendererModel edge_4;
	private final RendererModel cooling_blades_4;
	private final RendererModel plasma_coil_4;
	private final RendererModel belly_4;
	private final RendererModel plane_4;
	private final RendererModel rib_4;
	private final RendererModel rib_tilt_4;
	private final RendererModel rib_deco_4;
	private final RendererModel base_fin_4;
	private final RendererModel clawfoot_4;
	private final RendererModel leg_4;
	private final RendererModel ball_4;
	private final RendererModel station_5;
	private final RendererModel edge_5;
	private final RendererModel cooling_blades_5;
	private final RendererModel plasma_coil_5;
	private final RendererModel belly_5;
	private final RendererModel plane_5;
	private final RendererModel rib_5;
	private final RendererModel rib_tilt_5;
	private final RendererModel rib_deco_5;
	private final RendererModel base_fin_5;
	private final RendererModel clawfoot_5;
	private final RendererModel leg_5;
	private final RendererModel ball_5;
	private final RendererModel station_6;
	private final RendererModel edge_6;
	private final RendererModel cooling_blades_6;
	private final RendererModel plasma_coil_6;
	private final RendererModel belly_6;
	private final RendererModel plane_6;
	private final RendererModel rib_6;
	private final RendererModel rib_tilt_6;
	private final RendererModel rib_deco_6;
	private final RendererModel base_fin_6;
	private final RendererModel clawfoot_6;
	private final RendererModel leg_6;
	private final RendererModel ball_6;
	private final RendererModel rotor_rotate_y;
	private final RendererModel controls;
	private final RendererModel controls_1;
	private final RendererModel fast_return;
	private final RendererModel return_button;
	private final RendererModel waypoints;
	private final RendererModel way_dial_1;
	private final RendererModel way_dial_2;
	private final RendererModel way_dial_3;
	private final RendererModel way_dial_4;
	private final RendererModel way_dial_5;
	private final RendererModel way_dial_6;
	private final RendererModel way_dial_7;
	private final RendererModel way_dial_8;
	private final RendererModel way_dial_9;
	private final RendererModel dummy_aa_1;
	private final RendererModel dummy_aa_4;
	private final RendererModel dummy_aa_2;
	private final RendererModel dummy_aa_5;
	private final RendererModel dummy_aa_3;
	private final RendererModel dummy_aa_6;
	private final RendererModel dummy_aa_7;
	private final RendererModel toggle_aa_7;
	private final RendererModel dummy_aa_8;
	private final RendererModel toggle_aa_2;
	private final RendererModel dummy_aa_9;
	private final RendererModel toggle_aa_3;
	private final RendererModel dummy_aa_10;
	private final RendererModel toggle_aa_4;
	private final RendererModel dummy_aa_11;
	private final RendererModel toggle_aa_5;
	private final RendererModel decoration_1;
	private final RendererModel controls_2;
	private final RendererModel stablizers;
	private final RendererModel telpathic_circuits;
	private final RendererModel tp_screen;
	private final RendererModel tp_frame;
	private final RendererModel eye;
	private final RendererModel door;
	private final RendererModel keyhole_base;
	private final RendererModel key_rotate_y;
	private final RendererModel dummy_b_1;
	private final RendererModel toggle_b_1_rotate_x;
	private final RendererModel togglebottom_b1;
	private final RendererModel toggletop_b1;
	private final RendererModel dummy_b_2;
	private final RendererModel toggle_b2_rotate_x;
	private final RendererModel togglebottom_b2;
	private final RendererModel toggletop_b2;
	private final RendererModel dummy_b_3;
	private final RendererModel toggle_b3_rotate_x;
	private final RendererModel togglebottom_b3;
	private final RendererModel toggletop_b3;
	private final RendererModel dummy_b_4;
	private final RendererModel toggle_b4_rotate_x;
	private final RendererModel togglebottom_b4;
	private final RendererModel toggletop_b4;
	private final RendererModel dummy_b_5;
	private final RendererModel dummy_b_6;
	private final RendererModel dummy_b_7;
	private final RendererModel decoration_2;
	private final RendererModel controls_3;
	private final RendererModel refueler;
	private final RendererModel dial_base;
	private final RendererModel round;
	private final RendererModel curve;
	private final RendererModel pointer_rotate_y;
	private final RendererModel sonic_port;
	private final RendererModel sonic_base;
	private final RendererModel randomizer;
	private final RendererModel rando_plate;
	private final RendererModel diamond;
	private final RendererModel dummy_c_1;
	private final RendererModel toggle_c_1;
	private final RendererModel dummy_c_2;
	private final RendererModel toggle_c_2;
	private final RendererModel dummy_c_3;
	private final RendererModel toggle_c_3;
	private final RendererModel dummy_c_4;
	private final RendererModel toggle_c_4;
	private final RendererModel dummy_c_5;
	private final RendererModel toggle_c_5;
	private final RendererModel dummy_c_6;
	private final RendererModel toggle_c_6;
	private final RendererModel dummy_c_7;
	private final RendererModel dummy_c_9;
	private final RendererModel dummy_c_8;
	private final RendererModel dummy_c_10;
	private final RendererModel controls_4;
	private final RendererModel throttle;
	private final RendererModel throttle_plate;
	private final RendererModel throttle_rotate_x;
	private final RendererModel dimentional_con;
	private final RendererModel dim_screen;
	private final RendererModel handbreak;
	private final RendererModel handbreak_plate;
	private final RendererModel handbreak_rotate_y;
	private final RendererModel dummy_d_1;
	private final RendererModel dummy_d_2;
	private final RendererModel dummy_d_3;
	private final RendererModel dummy_d_4;
	private final RendererModel dummy_d_5;
	private final RendererModel dummy_d_6;
	private final RendererModel dummy_d_7;
	private final RendererModel dummy_d_8;
	private final RendererModel decoration_4;
	private final RendererModel controls_5;
	private final RendererModel landing_type;
	private final RendererModel landing_screen;
	private final RendererModel increments;
	private final RendererModel inc_slider;
	private final RendererModel position_move_z;
	private final RendererModel xyz;
	private final RendererModel dial_x;
	private final RendererModel dial_y;
	private final RendererModel dial_z;
	private final RendererModel direction;
	private final RendererModel direction_screen;
	private final RendererModel dummy_e_1;
	private final RendererModel dummy_e_2;
	private final RendererModel dummy_e_3;
	private final RendererModel decoration_5;
	private final RendererModel arrows;
	private final RendererModel arrows2;
	private final RendererModel controls_6;
	private final RendererModel monitor;
	private final RendererModel screen;
	private final RendererModel circles;
	private final RendererModel frame;
	private final RendererModel coms;
	private final RendererModel speaker;
	private final RendererModel dummy_ff_5;
	private final RendererModel toggle_ff_5;
	private final RendererModel dummy_ff_1;
	private final RendererModel dummy_ff_2;
	private final RendererModel dummy_ff_3;
	private final RendererModel toggle_ff_3;
	private final RendererModel dummy_ff_4;
	private final RendererModel toggle_ff_4;
	private final RendererModel dummy_ff_6;
	private final RendererModel dummy_ff_7;
	private final RendererModel dummy_ff_8;
	private final RendererModel decoration_6;
	private final RendererModel control_hitboxes;
	private final RendererModel hitbox_monitor;
	private final RendererModel hitbox_coms;
	private final RendererModel hitbox_fast_return;
	private final RendererModel hitbox_waypoint_1;
	private final RendererModel hitbox_waypoint_2;
	private final RendererModel hitbox_waypoint_3;
	private final RendererModel hitbox_waypoint_4;
	private final RendererModel hitbox_waypoint_5;
	private final RendererModel hitbox_waypoint_6;
	private final RendererModel hitbox_waypoint_7;
	private final RendererModel hitbox_waypoint_8;
	private final RendererModel hitbox_waypoint_9;
	private final RendererModel hitbox_door;
	private final RendererModel hitbox_stablizers;
	private final RendererModel hitbox_telepathic;
	private final RendererModel hitbox_refueler;
	private final RendererModel hitbox_randomizer;
	private final RendererModel hitbox_sonicport;
	private final RendererModel hitbox_dimentions;
	private final RendererModel hitbox_throttle;
	private final RendererModel hitbox_handbreak;
	private final RendererModel hitbox_landingtype;
	private final RendererModel hitbox_direction;
	private final RendererModel hitbox_increment;
	private final RendererModel hitbox_cords_x;
	private final RendererModel hitbox_cords_y;
	private final RendererModel hitbox_cords_z;

	public ConsoleGalvanicModel() {
		textureWidth = 1024;
		textureHeight = 1024;

		glow_shaft_1 = new RendererModel(this);
		glow_shaft_1.setRotationPoint(0.0F, 24.0F, 0.0F);
		glow_shaft_1.cubeList.add(new ModelBox(glow_shaft_1, 442, 23, -8.0F, -149.0F, -12.0F, 16, 148, 24, 0.0F, false));
		glow_shaft_1.cubeList.add(new ModelBox(glow_shaft_1, 422, 4, -8.0F, -221.0F, -22.0F, 16, 72, 44, 0.0F, false));

		glow_shaft_2 = new RendererModel(this);
		glow_shaft_2.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(glow_shaft_2, 0.0F, -1.0472F, 0.0F);
		glow_shaft_2.cubeList.add(new ModelBox(glow_shaft_2, 439, 23, -8.0F, -149.0F, -12.0F, 16, 148, 24, 0.0F, false));
		glow_shaft_2.cubeList.add(new ModelBox(glow_shaft_2, 422, 6, -8.0F, -221.0F, -22.0F, 16, 72, 44, 0.0F, false));

		glow_shaft_3 = new RendererModel(this);
		glow_shaft_3.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(glow_shaft_3, 0.0F, -2.0944F, 0.0F);
		glow_shaft_3.cubeList.add(new ModelBox(glow_shaft_3, 440, 22, -8.0F, -149.0F, -12.0F, 16, 148, 24, 0.0F, false));
		glow_shaft_3.cubeList.add(new ModelBox(glow_shaft_3, 422, 4, -8.0F, -221.0F, -22.0F, 16, 72, 44, 0.0F, false));

		console = new RendererModel(this);
		console.setRotationPoint(0.0F, 24.0F, 0.0F);

		bottom_plate = new RendererModel(this);
		bottom_plate.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(bottom_plate);
		bottom_plate.cubeList.add(new ModelBox(bottom_plate, 256, 98, -20.0F, -1.6F, -8.0F, 40, 2, 16, 0.0F, false));
		bottom_plate.cubeList.add(new ModelBox(bottom_plate, 256, 98, -16.0F, -1.6F, -16.0F, 32, 2, 8, 0.0F, false));
		bottom_plate.cubeList.add(new ModelBox(bottom_plate, 256, 98, -16.0F, -1.6F, 8.0F, 32, 2, 8, 0.0F, false));

		fins = new RendererModel(this);
		fins.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(fins);

		fin_1 = new RendererModel(this);
		fin_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(fin_1, 0.0F, -0.5236F, 0.0F);
		fins.addChild(fin_1);
		fin_1.cubeList.add(new ModelBox(fin_1, 61, 182, -2.4F, -150.0F, -16.0F, 5, 86, 32, 0.0F, false));
		fin_1.cubeList.add(new ModelBox(fin_1, 61, 182, -6.4F, -221.8F, -24.0F, 13, 82, 48, 0.0F, false));
		fin_1.cubeList.add(new ModelBox(fin_1, 61, 182, -2.4F, -218.0F, -28.0F, 5, 74, 56, 0.0F, false));

		fin_2 = new RendererModel(this);
		fin_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(fin_2, 0.0F, -1.5708F, 0.0F);
		fins.addChild(fin_2);
		fin_2.cubeList.add(new ModelBox(fin_2, 61, 182, -2.4F, -150.0F, -16.0F, 5, 86, 32, 0.0F, false));
		fin_2.cubeList.add(new ModelBox(fin_2, 61, 182, -6.4F, -222.0F, -24.0F, 13, 82, 48, 0.0F, false));
		fin_2.cubeList.add(new ModelBox(fin_2, 61, 182, -2.4F, -218.0F, -28.0F, 5, 74, 56, 0.0F, false));

		fin_3 = new RendererModel(this);
		fin_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(fin_3, 0.0F, -2.618F, 0.0F);
		fins.addChild(fin_3);
		fin_3.cubeList.add(new ModelBox(fin_3, 61, 182, -2.4F, -150.0F, -16.0F, 5, 86, 32, 0.0F, false));
		fin_3.cubeList.add(new ModelBox(fin_3, 61, 182, -6.4F, -222.1F, -24.0F, 13, 82, 48, 0.0F, false));
		fin_3.cubeList.add(new ModelBox(fin_3, 61, 182, -2.4F, -218.0F, -28.0F, 5, 74, 56, 0.0F, false));

		chunks = new RendererModel(this);
		chunks.setRotationPoint(0.0F, -1.0F, 0.0F);
		console.addChild(chunks);

		chunk_1 = new RendererModel(this);
		chunk_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		chunks.addChild(chunk_1);
		chunk_1.cubeList.add(new ModelBox(chunk_1, 118, 246, -12.0F, -64.0F, -24.0F, 24, 16, 48, 0.0F, false));
		chunk_1.cubeList.add(new ModelBox(chunk_1, 118, 246, -12.0F, -148.0F, -24.0F, 24, 4, 48, 0.0F, false));

		chunk_2 = new RendererModel(this);
		chunk_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(chunk_2, 0.0F, -1.0472F, 0.0F);
		chunks.addChild(chunk_2);
		chunk_2.cubeList.add(new ModelBox(chunk_2, 118, 246, -12.0F, -64.0F, -24.0F, 24, 16, 48, 0.0F, false));
		chunk_2.cubeList.add(new ModelBox(chunk_2, 118, 246, -12.0F, -148.0F, -24.0F, 24, 4, 48, 0.0F, false));

		chunk_3 = new RendererModel(this);
		chunk_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(chunk_3, 0.0F, -2.0944F, 0.0F);
		chunks.addChild(chunk_3);
		chunk_3.cubeList.add(new ModelBox(chunk_3, 118, 246, -12.0F, -64.0F, -24.0F, 24, 16, 48, 0.0F, false));
		chunk_3.cubeList.add(new ModelBox(chunk_3, 118, 246, -12.0F, -148.0F, -24.0F, 24, 4, 48, 0.0F, false));

		stations = new RendererModel(this);
		stations.setRotationPoint(0.0F, 0.0F, 0.0F);
		console.addChild(stations);

		station_1 = new RendererModel(this);
		station_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		stations.addChild(station_1);

		edge_1 = new RendererModel(this);
		edge_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_1.addChild(edge_1);
		edge_1.cubeList.add(new ModelBox(edge_1, 51, 12, -34.0F, -60.0F, -61.0F, 68, 4, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 177, 229, -10.0F, -79.0F, -21.0F, 20, 16, 10, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 157, 251, -10.0F, -85.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 144, 226, -10.0F, -144.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 160, 235, -10.0F, -89.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 166, 292, -10.0F, -136.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 270, 99, -10.0F, -87.0F, -15.0F, 20, 4, 4, 0.0F, false));
		edge_1.cubeList.add(new ModelBox(edge_1, 276, 94, -10.0F, -140.0F, -15.0F, 20, 4, 4, 0.0F, false));

		cooling_blades_1 = new RendererModel(this);
		cooling_blades_1.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_1.addChild(cooling_blades_1);
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -11.0F, -25.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -11.0F, -37.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -12.0F, -26.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -12.0F, -38.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -11.0F, -27.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -11.0F, -39.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -8.0F, -27.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -8.0F, -39.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -8.0F, -25.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_1.cubeList.add(new ModelBox(cooling_blades_1, 287, 100, -8.0F, -37.0F, -17.0F, 16, 1, 5, 0.0F, false));

		plasma_coil_1 = new RendererModel(this);
		plasma_coil_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_1.addChild(plasma_coil_1);
		plasma_coil_1.cubeList.add(new ModelBox(plasma_coil_1, 482, 123, -5.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_1.cubeList.add(new ModelBox(plasma_coil_1, 482, 123, -1.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_1.cubeList.add(new ModelBox(plasma_coil_1, 482, 123, 2.9F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_1.cubeList.add(new ModelBox(plasma_coil_1, 212, 310, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_1 = new RendererModel(this);
		belly_1.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_1.addChild(belly_1);
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -8.0F, -15.0F, -24.0F, 16, 4, 8, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -8.0F, -1.0F, -24.0F, 16, 3, 8, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 313, 55, -4.0F, -1.0F, -28.0F, 8, 3, 4, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -8.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, 5.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, 2.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -4.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -1.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -5.0F, -12.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -5.0F, -2.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -11.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, 8.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_1.cubeList.add(new ModelBox(belly_1, 270, 89, -8.0F, -17.0F, -19.0F, 16, 4, 4, 0.0F, false));

		plane_1 = new RendererModel(this);
		plane_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_1, 0.5236F, 0.0F, 0.0F);
		station_1.addChild(plane_1);
		plane_1.cubeList.add(new ModelBox(plane_1, 31, 132, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 37, 124, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 41, 108, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 52, 96, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 253, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 267, 84, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 67, 85, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 70, 76, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_1.cubeList.add(new ModelBox(plane_1, 67, 64, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_1 = new RendererModel(this);
		rib_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_1, 0.0F, -0.5236F, 0.0F);
		station_1.addChild(rib_1);

		rib_tilt_1 = new RendererModel(this);
		rib_tilt_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_1, 0.4363F, 0.0F, 0.0F);
		rib_1.addChild(rib_tilt_1);
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -2.0F, -84.0F, -40.0F, 4, 8, 52, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -2.0F, -76.0F, -16.0F, 4, 6, 29, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -76.0F, -20.0F, 2, 26, 4, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -76.0F, -12.0F, 2, 26, 10, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -53.0F, -16.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -59.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -65.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -71.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -2.0F, -70.0F, 0.0F, 4, 6, 14, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -23.8F, -45.0F, 2, 2, 7, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -25.8F, -49.0F, 2, 2, 11, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -25.8F, -34.0F, 2, 4, 4, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -25.8F, -26.3F, 2, 4, 4, 0.0F, false));
		rib_tilt_1.cubeList.add(new ModelBox(rib_tilt_1, 180, 260, -1.0F, -27.8F, -49.0F, 2, 2, 28, 0.0F, false));

		rib_deco_1 = new RendererModel(this);
		rib_deco_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_1.addChild(rib_deco_1);
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 180, 260, -4.0F, -85.0F, -41.0F, 8, 8, 8, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 180, 260, -3.0F, -79.0F, -42.0F, 6, 4, 4, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 180, 260, -4.0F, -85.0F, 7.0F, 8, 8, 8, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 180, 260, -3.0F, -84.0F, 8.0F, 6, 7, 8, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 306, 89, -4.0F, -85.3F, -9.0F, 8, 10, 2, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 317, 89, -4.0F, -84.9F, -13.0F, 8, 10, 2, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 310, 100, -4.0F, -84.2F, -17.0F, 8, 10, 2, 0.0F, false));
		rib_deco_1.cubeList.add(new ModelBox(rib_deco_1, 180, 260, -4.0F, -85.0F, 4.0F, 8, 10, 2, 0.0F, false));

		base_fin_1 = new RendererModel(this);
		base_fin_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_1, 0.0F, -0.5236F, 0.0F);
		station_1.addChild(base_fin_1);
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -1.4F, -64.0F, -31.0F, 3, 63, 23, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -0.4F, -40.0F, -39.0F, 1, 28, 2, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -0.4F, -41.0F, -33.0F, 1, 28, 2, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -0.4F, -16.0F, -37.0F, 1, 4, 6, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -0.4F, -24.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -0.4F, -32.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_1.cubeList.add(new ModelBox(base_fin_1, 180, 260, -0.4F, -40.0F, -37.0F, 1, 4, 6, 0.0F, false));

		clawfoot_1 = new RendererModel(this);
		clawfoot_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_1, 0.0F, -0.5236F, 0.0F);
		station_1.addChild(clawfoot_1);

		leg_1 = new RendererModel(this);
		leg_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_1.addChild(leg_1);
		leg_1.cubeList.add(new ModelBox(leg_1, 180, 260, -2.4F, -4.0F, -52.0F, 5, 4, 35, 0.0F, false));
		leg_1.cubeList.add(new ModelBox(leg_1, 180, 260, -2.2F, -3.9F, -55.0F, 4, 3, 3, 0.0F, false));

		ball_1 = new RendererModel(this);
		ball_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_1.addChild(ball_1);
		ball_1.cubeList.add(new ModelBox(ball_1, 267, 95, -1.4F, -8.0F, -63.0F, 3, 8, 8, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 267, 95, -2.4F, -7.5F, -62.5F, 5, 7, 7, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 267, 95, -3.4F, -7.0F, -62.1F, 7, 6, 6, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 267, 95, -4.4F, -6.5F, -61.7F, 9, 5, 5, 0.0F, false));
		ball_1.cubeList.add(new ModelBox(ball_1, 228, 408, -5.4F, -5.5F, -60.7F, 11, 3, 3, 0.0F, false));

		station_2 = new RendererModel(this);
		station_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_2, 0.0F, -1.0472F, 0.0F);
		stations.addChild(station_2);

		edge_2 = new RendererModel(this);
		edge_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_2.addChild(edge_2);
		edge_2.cubeList.add(new ModelBox(edge_2, 51, 12, -34.0F, -60.0F, -61.0F, 68, 4, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 177, 229, -10.0F, -79.0F, -21.0F, 20, 16, 10, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 157, 251, -10.0F, -85.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 144, 226, -10.0F, -144.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 160, 235, -10.0F, -89.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 166, 292, -10.0F, -136.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 270, 99, -10.0F, -87.0F, -15.0F, 20, 4, 4, 0.0F, false));
		edge_2.cubeList.add(new ModelBox(edge_2, 276, 94, -10.0F, -140.0F, -15.0F, 20, 4, 4, 0.0F, false));

		cooling_blades_2 = new RendererModel(this);
		cooling_blades_2.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_2.addChild(cooling_blades_2);
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -11.0F, -25.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -11.0F, -37.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -12.0F, -26.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -12.0F, -38.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -11.0F, -27.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -11.0F, -39.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -8.0F, -27.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -8.0F, -39.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -8.0F, -25.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_2.cubeList.add(new ModelBox(cooling_blades_2, 287, 100, -8.0F, -37.0F, -17.0F, 16, 1, 5, 0.0F, false));

		plasma_coil_2 = new RendererModel(this);
		plasma_coil_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_2.addChild(plasma_coil_2);
		plasma_coil_2.cubeList.add(new ModelBox(plasma_coil_2, 482, 123, -5.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_2.cubeList.add(new ModelBox(plasma_coil_2, 482, 123, -1.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_2.cubeList.add(new ModelBox(plasma_coil_2, 482, 123, 2.9F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_2.cubeList.add(new ModelBox(plasma_coil_2, 212, 310, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_2 = new RendererModel(this);
		belly_2.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_2.addChild(belly_2);
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -8.0F, -15.0F, -24.0F, 16, 4, 8, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -8.0F, -1.0F, -24.0F, 16, 3, 8, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 313, 55, -4.0F, -1.0F, -28.0F, 8, 3, 4, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -8.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, 5.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, 2.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -4.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -1.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -5.0F, -12.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -5.0F, -2.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -11.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, 8.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_2.cubeList.add(new ModelBox(belly_2, 270, 89, -8.0F, -17.0F, -19.0F, 16, 4, 4, 0.0F, false));

		plane_2 = new RendererModel(this);
		plane_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_2, 0.5236F, 0.0F, 0.0F);
		station_2.addChild(plane_2);
		plane_2.cubeList.add(new ModelBox(plane_2, 31, 132, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 37, 124, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 41, 108, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 52, 96, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 253, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 267, 84, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 67, 85, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 70, 76, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_2.cubeList.add(new ModelBox(plane_2, 67, 64, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_2 = new RendererModel(this);
		rib_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_2, 0.0F, -0.5236F, 0.0F);
		station_2.addChild(rib_2);

		rib_tilt_2 = new RendererModel(this);
		rib_tilt_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_2, 0.4363F, 0.0F, 0.0F);
		rib_2.addChild(rib_tilt_2);
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -2.0F, -84.0F, -40.0F, 4, 8, 52, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -2.0F, -76.0F, -16.0F, 4, 6, 29, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -76.0F, -20.0F, 2, 26, 4, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -76.0F, -12.0F, 2, 26, 10, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -53.0F, -16.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -59.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -65.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -71.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -2.0F, -70.0F, 0.0F, 4, 6, 14, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -23.8F, -45.0F, 2, 2, 7, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -25.8F, -49.0F, 2, 2, 11, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -25.8F, -34.0F, 2, 4, 4, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -25.8F, -26.3F, 2, 4, 4, 0.0F, false));
		rib_tilt_2.cubeList.add(new ModelBox(rib_tilt_2, 180, 260, -1.0F, -27.8F, -49.0F, 2, 2, 28, 0.0F, false));

		rib_deco_2 = new RendererModel(this);
		rib_deco_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_2.addChild(rib_deco_2);
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 180, 260, -4.0F, -85.0F, -41.0F, 8, 8, 8, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 180, 260, -3.0F, -79.0F, -42.0F, 6, 4, 4, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 180, 260, -4.0F, -85.0F, 7.0F, 8, 8, 8, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 180, 260, -3.0F, -84.0F, 8.0F, 6, 7, 8, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 306, 89, -4.0F, -85.3F, 1.0F, 8, 10, 2, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 317, 89, -4.0F, -84.9F, -13.0F, 8, 10, 2, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 310, 100, -4.0F, -84.2F, -32.0F, 8, 10, 2, 0.0F, false));
		rib_deco_2.cubeList.add(new ModelBox(rib_deco_2, 180, 260, -4.0F, -85.0F, 4.0F, 8, 10, 2, 0.0F, false));

		base_fin_2 = new RendererModel(this);
		base_fin_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_2, 0.0F, -0.5236F, 0.0F);
		station_2.addChild(base_fin_2);
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -1.4F, -64.0F, -31.0F, 3, 63, 23, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -0.4F, -40.0F, -39.0F, 1, 28, 2, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -0.4F, -41.0F, -33.0F, 1, 28, 2, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -0.4F, -16.0F, -37.0F, 1, 4, 6, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -0.4F, -24.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -0.4F, -32.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_2.cubeList.add(new ModelBox(base_fin_2, 180, 260, -0.4F, -40.0F, -37.0F, 1, 4, 6, 0.0F, false));

		clawfoot_2 = new RendererModel(this);
		clawfoot_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_2, 0.0F, -0.5236F, 0.0F);
		station_2.addChild(clawfoot_2);

		leg_2 = new RendererModel(this);
		leg_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_2.addChild(leg_2);
		leg_2.cubeList.add(new ModelBox(leg_2, 180, 260, -2.4F, -4.0F, -52.0F, 5, 4, 35, 0.0F, false));
		leg_2.cubeList.add(new ModelBox(leg_2, 180, 260, -2.2F, -3.9F, -55.0F, 4, 3, 3, 0.0F, false));

		ball_2 = new RendererModel(this);
		ball_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_2.addChild(ball_2);
		ball_2.cubeList.add(new ModelBox(ball_2, 267, 95, -1.4F, -8.0F, -63.0F, 3, 8, 8, 0.0F, false));
		ball_2.cubeList.add(new ModelBox(ball_2, 267, 95, -2.4F, -7.5F, -62.5F, 5, 7, 7, 0.0F, false));
		ball_2.cubeList.add(new ModelBox(ball_2, 267, 95, -3.4F, -7.0F, -62.1F, 7, 6, 6, 0.0F, false));
		ball_2.cubeList.add(new ModelBox(ball_2, 267, 95, -4.4F, -6.5F, -61.7F, 9, 5, 5, 0.0F, false));
		ball_2.cubeList.add(new ModelBox(ball_2, 228, 408, -5.4F, -5.5F, -60.7F, 11, 3, 3, 0.0F, false));

		station_3 = new RendererModel(this);
		station_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_3, 0.0F, -2.0944F, 0.0F);
		stations.addChild(station_3);

		edge_3 = new RendererModel(this);
		edge_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_3.addChild(edge_3);
		edge_3.cubeList.add(new ModelBox(edge_3, 51, 12, -34.0F, -60.0F, -61.0F, 68, 4, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 177, 229, -10.0F, -79.0F, -21.0F, 20, 16, 10, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 157, 251, -10.0F, -85.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 144, 226, -10.0F, -144.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 160, 235, -10.0F, -89.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 166, 292, -10.0F, -136.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 270, 99, -10.0F, -87.0F, -15.0F, 20, 4, 4, 0.0F, false));
		edge_3.cubeList.add(new ModelBox(edge_3, 276, 94, -10.0F, -140.0F, -15.0F, 20, 4, 4, 0.0F, false));

		cooling_blades_3 = new RendererModel(this);
		cooling_blades_3.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_3.addChild(cooling_blades_3);
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -11.0F, -25.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -11.0F, -37.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -12.0F, -26.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -12.0F, -38.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -11.0F, -27.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -11.0F, -39.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -8.0F, -27.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -8.0F, -39.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -8.0F, -25.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_3.cubeList.add(new ModelBox(cooling_blades_3, 287, 100, -8.0F, -37.0F, -17.0F, 16, 1, 5, 0.0F, false));

		plasma_coil_3 = new RendererModel(this);
		plasma_coil_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_3.addChild(plasma_coil_3);
		plasma_coil_3.cubeList.add(new ModelBox(plasma_coil_3, 482, 123, -5.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_3.cubeList.add(new ModelBox(plasma_coil_3, 482, 123, -1.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_3.cubeList.add(new ModelBox(plasma_coil_3, 482, 123, 2.9F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_3.cubeList.add(new ModelBox(plasma_coil_3, 212, 310, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_3 = new RendererModel(this);
		belly_3.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_3.addChild(belly_3);
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -8.0F, -15.0F, -24.0F, 16, 4, 8, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -8.0F, -1.0F, -24.0F, 16, 3, 8, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 313, 55, -4.0F, -1.0F, -28.0F, 8, 3, 4, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -8.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, 5.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, 2.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -4.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -1.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -5.0F, -12.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -5.0F, -2.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -11.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, 8.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_3.cubeList.add(new ModelBox(belly_3, 270, 89, -8.0F, -17.0F, -19.0F, 16, 4, 4, 0.0F, false));

		plane_3 = new RendererModel(this);
		plane_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_3, 0.5236F, 0.0F, 0.0F);
		station_3.addChild(plane_3);
		plane_3.cubeList.add(new ModelBox(plane_3, 31, 132, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 37, 124, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 41, 108, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 52, 96, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 253, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 267, 84, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 67, 85, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 70, 76, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_3.cubeList.add(new ModelBox(plane_3, 67, 64, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_3 = new RendererModel(this);
		rib_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_3, 0.0F, -0.5236F, 0.0F);
		station_3.addChild(rib_3);

		rib_tilt_3 = new RendererModel(this);
		rib_tilt_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_3, 0.4363F, 0.0F, 0.0F);
		rib_3.addChild(rib_tilt_3);
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -2.0F, -84.0F, -40.0F, 4, 8, 52, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -2.0F, -76.0F, -16.0F, 4, 6, 29, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -76.0F, -20.0F, 2, 26, 4, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -76.0F, -12.0F, 2, 26, 10, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -53.0F, -16.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -59.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -65.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -71.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -2.0F, -70.0F, 0.0F, 4, 6, 14, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -23.8F, -45.0F, 2, 2, 7, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -25.8F, -49.0F, 2, 2, 11, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -25.8F, -34.0F, 2, 4, 4, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -25.8F, -26.3F, 2, 4, 4, 0.0F, false));
		rib_tilt_3.cubeList.add(new ModelBox(rib_tilt_3, 180, 260, -1.0F, -27.8F, -49.0F, 2, 2, 28, 0.0F, false));

		rib_deco_3 = new RendererModel(this);
		rib_deco_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_3.addChild(rib_deco_3);
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 180, 260, -4.0F, -85.0F, -41.0F, 8, 8, 8, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 180, 260, -3.0F, -79.0F, -42.0F, 6, 4, 4, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 180, 260, -4.0F, -85.0F, 7.0F, 8, 8, 8, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 180, 260, -3.0F, -84.0F, 8.0F, 6, 7, 8, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 306, 89, -4.0F, -85.3F, -9.0F, 8, 10, 2, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 317, 89, -4.0F, -84.9F, -13.0F, 8, 10, 2, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 310, 100, -4.0F, -84.2F, -17.0F, 8, 10, 2, 0.0F, false));
		rib_deco_3.cubeList.add(new ModelBox(rib_deco_3, 180, 260, -4.0F, -85.0F, 4.0F, 8, 10, 2, 0.0F, false));

		base_fin_3 = new RendererModel(this);
		base_fin_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_3, 0.0F, -0.5236F, 0.0F);
		station_3.addChild(base_fin_3);
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -1.4F, -64.0F, -31.0F, 3, 63, 23, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -0.4F, -40.0F, -39.0F, 1, 28, 2, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -0.4F, -41.0F, -33.0F, 1, 28, 2, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -0.4F, -16.0F, -37.0F, 1, 4, 6, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -0.4F, -24.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -0.4F, -32.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_3.cubeList.add(new ModelBox(base_fin_3, 180, 260, -0.4F, -40.0F, -37.0F, 1, 4, 6, 0.0F, false));

		clawfoot_3 = new RendererModel(this);
		clawfoot_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_3, 0.0F, -0.5236F, 0.0F);
		station_3.addChild(clawfoot_3);

		leg_3 = new RendererModel(this);
		leg_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_3.addChild(leg_3);
		leg_3.cubeList.add(new ModelBox(leg_3, 180, 260, -2.4F, -4.0F, -52.0F, 5, 4, 35, 0.0F, false));
		leg_3.cubeList.add(new ModelBox(leg_3, 180, 260, -2.2F, -3.9F, -55.0F, 4, 3, 3, 0.0F, false));

		ball_3 = new RendererModel(this);
		ball_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_3.addChild(ball_3);
		ball_3.cubeList.add(new ModelBox(ball_3, 267, 95, -1.4F, -8.0F, -63.0F, 3, 8, 8, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 267, 95, -2.4F, -7.5F, -62.5F, 5, 7, 7, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 267, 95, -3.4F, -7.0F, -62.1F, 7, 6, 6, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 267, 95, -4.4F, -6.5F, -61.7F, 9, 5, 5, 0.0F, false));
		ball_3.cubeList.add(new ModelBox(ball_3, 228, 408, -5.4F, -5.5F, -60.7F, 11, 3, 3, 0.0F, false));

		station_4 = new RendererModel(this);
		station_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_4, 0.0F, 3.1416F, 0.0F);
		stations.addChild(station_4);

		edge_4 = new RendererModel(this);
		edge_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_4.addChild(edge_4);
		edge_4.cubeList.add(new ModelBox(edge_4, 51, 12, -34.0F, -60.0F, -61.0F, 68, 4, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 177, 229, -10.0F, -79.0F, -21.0F, 20, 16, 10, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 157, 251, -10.0F, -85.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 144, 226, -10.0F, -144.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 160, 235, -10.0F, -89.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 166, 292, -10.0F, -136.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 270, 99, -10.0F, -87.0F, -15.0F, 20, 4, 4, 0.0F, false));
		edge_4.cubeList.add(new ModelBox(edge_4, 276, 94, -10.0F, -140.0F, -15.0F, 20, 4, 4, 0.0F, false));

		cooling_blades_4 = new RendererModel(this);
		cooling_blades_4.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_4.addChild(cooling_blades_4);
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -11.0F, -25.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -11.0F, -37.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -12.0F, -26.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -12.0F, -38.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -11.0F, -27.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -11.0F, -39.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -8.0F, -27.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -8.0F, -39.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -8.0F, -25.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_4.cubeList.add(new ModelBox(cooling_blades_4, 287, 100, -8.0F, -37.0F, -17.0F, 16, 1, 5, 0.0F, false));

		plasma_coil_4 = new RendererModel(this);
		plasma_coil_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_4.addChild(plasma_coil_4);
		plasma_coil_4.cubeList.add(new ModelBox(plasma_coil_4, 482, 123, -5.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_4.cubeList.add(new ModelBox(plasma_coil_4, 482, 123, -1.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_4.cubeList.add(new ModelBox(plasma_coil_4, 482, 123, 2.9F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_4.cubeList.add(new ModelBox(plasma_coil_4, 212, 310, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_4 = new RendererModel(this);
		belly_4.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_4.addChild(belly_4);
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -8.0F, -15.0F, -24.0F, 16, 4, 8, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -8.0F, -1.0F, -24.0F, 16, 3, 8, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 313, 55, -4.0F, -1.0F, -28.0F, 8, 3, 4, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -8.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, 5.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, 2.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -4.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -1.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -5.0F, -12.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -5.0F, -2.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -11.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, 8.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_4.cubeList.add(new ModelBox(belly_4, 270, 89, -8.0F, -17.0F, -19.0F, 16, 4, 4, 0.0F, false));

		plane_4 = new RendererModel(this);
		plane_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_4, 0.5236F, 0.0F, 0.0F);
		station_4.addChild(plane_4);
		plane_4.cubeList.add(new ModelBox(plane_4, 31, 132, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 37, 124, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 41, 108, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 52, 96, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 253, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 267, 84, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 67, 85, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 70, 76, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_4.cubeList.add(new ModelBox(plane_4, 67, 64, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_4 = new RendererModel(this);
		rib_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_4, 0.0F, -0.5236F, 0.0F);
		station_4.addChild(rib_4);

		rib_tilt_4 = new RendererModel(this);
		rib_tilt_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_4, 0.4363F, 0.0F, 0.0F);
		rib_4.addChild(rib_tilt_4);
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -2.0F, -84.0F, -40.0F, 4, 8, 52, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -2.0F, -76.0F, -16.0F, 4, 6, 29, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -76.0F, -20.0F, 2, 26, 4, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -76.0F, -12.0F, 2, 26, 10, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -53.0F, -16.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -59.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -65.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -71.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -2.0F, -70.0F, 0.0F, 4, 6, 14, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -23.8F, -45.0F, 2, 2, 7, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -25.8F, -49.0F, 2, 2, 11, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -25.8F, -34.0F, 2, 4, 4, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -25.8F, -26.3F, 2, 4, 4, 0.0F, false));
		rib_tilt_4.cubeList.add(new ModelBox(rib_tilt_4, 180, 260, -1.0F, -27.8F, -49.0F, 2, 2, 28, 0.0F, false));

		rib_deco_4 = new RendererModel(this);
		rib_deco_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_4.addChild(rib_deco_4);
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 180, 260, -4.0F, -85.0F, -41.0F, 8, 8, 8, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 180, 260, -3.0F, -79.0F, -42.0F, 6, 4, 4, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 180, 260, -4.0F, -85.0F, 7.0F, 8, 8, 8, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 180, 260, -3.0F, -84.0F, 8.0F, 6, 7, 8, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 306, 89, -4.0F, -85.3F, 1.0F, 8, 10, 2, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 317, 89, -4.0F, -84.9F, -13.0F, 8, 10, 2, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 310, 100, -4.0F, -84.2F, -32.0F, 8, 10, 2, 0.0F, false));
		rib_deco_4.cubeList.add(new ModelBox(rib_deco_4, 180, 260, -4.0F, -85.0F, 4.0F, 8, 10, 2, 0.0F, false));

		base_fin_4 = new RendererModel(this);
		base_fin_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_4, 0.0F, -0.5236F, 0.0F);
		station_4.addChild(base_fin_4);
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -1.4F, -64.0F, -31.0F, 3, 63, 23, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -0.4F, -40.0F, -39.0F, 1, 28, 2, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -0.4F, -41.0F, -33.0F, 1, 28, 2, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -0.4F, -16.0F, -37.0F, 1, 4, 6, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -0.4F, -24.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -0.4F, -32.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_4.cubeList.add(new ModelBox(base_fin_4, 180, 260, -0.4F, -40.0F, -37.0F, 1, 4, 6, 0.0F, false));

		clawfoot_4 = new RendererModel(this);
		clawfoot_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_4, 0.0F, -0.5236F, 0.0F);
		station_4.addChild(clawfoot_4);

		leg_4 = new RendererModel(this);
		leg_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_4.addChild(leg_4);
		leg_4.cubeList.add(new ModelBox(leg_4, 180, 260, -2.4F, -4.0F, -52.0F, 5, 4, 35, 0.0F, false));
		leg_4.cubeList.add(new ModelBox(leg_4, 180, 260, -2.2F, -3.9F, -55.0F, 4, 3, 3, 0.0F, false));

		ball_4 = new RendererModel(this);
		ball_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_4.addChild(ball_4);
		ball_4.cubeList.add(new ModelBox(ball_4, 267, 95, -1.4F, -8.0F, -63.0F, 3, 8, 8, 0.0F, false));
		ball_4.cubeList.add(new ModelBox(ball_4, 267, 95, -2.4F, -7.5F, -62.5F, 5, 7, 7, 0.0F, false));
		ball_4.cubeList.add(new ModelBox(ball_4, 267, 95, -3.4F, -7.0F, -62.1F, 7, 6, 6, 0.0F, false));
		ball_4.cubeList.add(new ModelBox(ball_4, 267, 95, -4.4F, -6.5F, -61.7F, 9, 5, 5, 0.0F, false));
		ball_4.cubeList.add(new ModelBox(ball_4, 228, 408, -5.4F, -5.5F, -60.7F, 11, 3, 3, 0.0F, false));

		station_5 = new RendererModel(this);
		station_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_5, 0.0F, 2.0944F, 0.0F);
		stations.addChild(station_5);

		edge_5 = new RendererModel(this);
		edge_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_5.addChild(edge_5);
		edge_5.cubeList.add(new ModelBox(edge_5, 51, 12, -34.0F, -60.0F, -61.0F, 68, 4, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 177, 229, -10.0F, -79.0F, -21.0F, 20, 16, 10, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 157, 251, -10.0F, -85.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 144, 226, -10.0F, -144.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 160, 235, -10.0F, -89.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 166, 292, -10.0F, -136.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 270, 99, -10.0F, -87.0F, -15.0F, 20, 4, 4, 0.0F, false));
		edge_5.cubeList.add(new ModelBox(edge_5, 276, 94, -10.0F, -140.0F, -15.0F, 20, 4, 4, 0.0F, false));

		cooling_blades_5 = new RendererModel(this);
		cooling_blades_5.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_5.addChild(cooling_blades_5);
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -11.0F, -25.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -11.0F, -37.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -12.0F, -26.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -12.0F, -38.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -11.0F, -27.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -11.0F, -39.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -8.0F, -27.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -8.0F, -39.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -8.0F, -25.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_5.cubeList.add(new ModelBox(cooling_blades_5, 287, 100, -8.0F, -37.0F, -17.0F, 16, 1, 5, 0.0F, false));

		plasma_coil_5 = new RendererModel(this);
		plasma_coil_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_5.addChild(plasma_coil_5);
		plasma_coil_5.cubeList.add(new ModelBox(plasma_coil_5, 482, 123, -5.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_5.cubeList.add(new ModelBox(plasma_coil_5, 482, 123, -1.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_5.cubeList.add(new ModelBox(plasma_coil_5, 482, 123, 2.9F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_5.cubeList.add(new ModelBox(plasma_coil_5, 212, 310, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_5 = new RendererModel(this);
		belly_5.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_5.addChild(belly_5);
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -8.0F, -15.0F, -24.0F, 16, 4, 8, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -8.0F, -1.0F, -24.0F, 16, 3, 8, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 313, 55, -4.0F, -1.0F, -28.0F, 8, 3, 4, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -8.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, 5.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, 2.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -4.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -1.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -5.0F, -12.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -5.0F, -2.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -11.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, 8.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_5.cubeList.add(new ModelBox(belly_5, 270, 89, -8.0F, -17.0F, -19.0F, 16, 4, 4, 0.0F, false));

		plane_5 = new RendererModel(this);
		plane_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_5, 0.5236F, 0.0F, 0.0F);
		station_5.addChild(plane_5);
		plane_5.cubeList.add(new ModelBox(plane_5, 31, 132, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 37, 124, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 41, 108, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 52, 96, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 253, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 267, 84, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 67, 85, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 70, 76, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_5.cubeList.add(new ModelBox(plane_5, 67, 64, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_5 = new RendererModel(this);
		rib_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_5, 0.0F, -0.5236F, 0.0F);
		station_5.addChild(rib_5);

		rib_tilt_5 = new RendererModel(this);
		rib_tilt_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_5, 0.4363F, 0.0F, 0.0F);
		rib_5.addChild(rib_tilt_5);
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -2.0F, -84.0F, -40.0F, 4, 8, 52, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -2.0F, -76.0F, -16.0F, 4, 6, 29, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -76.0F, -20.0F, 2, 26, 4, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -76.0F, -12.0F, 2, 26, 10, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -53.0F, -16.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -59.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -65.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -71.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -2.0F, -70.0F, 0.0F, 4, 6, 14, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -23.8F, -45.0F, 2, 2, 7, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -25.8F, -49.0F, 2, 2, 11, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -25.8F, -34.0F, 2, 4, 4, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -25.8F, -26.3F, 2, 4, 4, 0.0F, false));
		rib_tilt_5.cubeList.add(new ModelBox(rib_tilt_5, 180, 260, -1.0F, -27.8F, -49.0F, 2, 2, 28, 0.0F, false));

		rib_deco_5 = new RendererModel(this);
		rib_deco_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_5.addChild(rib_deco_5);
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 180, 260, -4.0F, -85.0F, -41.0F, 8, 8, 8, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 180, 260, -3.0F, -79.0F, -42.0F, 6, 4, 4, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 180, 260, -4.0F, -85.0F, 7.0F, 8, 8, 8, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 180, 260, -3.0F, -84.0F, 8.0F, 6, 7, 8, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 306, 89, -4.0F, -85.3F, -9.0F, 8, 10, 2, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 317, 89, -4.0F, -84.9F, -13.0F, 8, 10, 2, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 310, 100, -4.0F, -84.2F, -17.0F, 8, 10, 2, 0.0F, false));
		rib_deco_5.cubeList.add(new ModelBox(rib_deco_5, 180, 260, -4.0F, -85.0F, 4.0F, 8, 10, 2, 0.0F, false));

		base_fin_5 = new RendererModel(this);
		base_fin_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_5, 0.0F, -0.5236F, 0.0F);
		station_5.addChild(base_fin_5);
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -1.4F, -64.0F, -31.0F, 3, 63, 23, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -0.4F, -40.0F, -39.0F, 1, 28, 2, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -0.4F, -41.0F, -33.0F, 1, 28, 2, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -0.4F, -16.0F, -37.0F, 1, 4, 6, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -0.4F, -24.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -0.4F, -32.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_5.cubeList.add(new ModelBox(base_fin_5, 180, 260, -0.4F, -40.0F, -37.0F, 1, 4, 6, 0.0F, false));

		clawfoot_5 = new RendererModel(this);
		clawfoot_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_5, 0.0F, -0.5236F, 0.0F);
		station_5.addChild(clawfoot_5);

		leg_5 = new RendererModel(this);
		leg_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_5.addChild(leg_5);
		leg_5.cubeList.add(new ModelBox(leg_5, 180, 260, -2.4F, -4.0F, -52.0F, 5, 4, 35, 0.0F, false));
		leg_5.cubeList.add(new ModelBox(leg_5, 180, 260, -2.2F, -3.9F, -55.0F, 4, 3, 3, 0.0F, false));

		ball_5 = new RendererModel(this);
		ball_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_5.addChild(ball_5);
		ball_5.cubeList.add(new ModelBox(ball_5, 267, 95, -1.4F, -8.0F, -63.0F, 3, 8, 8, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 267, 95, -2.4F, -7.5F, -62.5F, 5, 7, 7, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 267, 95, -3.4F, -7.0F, -62.1F, 7, 6, 6, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 267, 95, -4.4F, -6.5F, -61.7F, 9, 5, 5, 0.0F, false));
		ball_5.cubeList.add(new ModelBox(ball_5, 228, 408, -5.4F, -5.5F, -60.7F, 11, 3, 3, 0.0F, false));

		station_6 = new RendererModel(this);
		station_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(station_6, 0.0F, 1.0472F, 0.0F);
		stations.addChild(station_6);

		edge_6 = new RendererModel(this);
		edge_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		station_6.addChild(edge_6);
		edge_6.cubeList.add(new ModelBox(edge_6, 51, 12, -34.0F, -60.0F, -61.0F, 68, 4, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 177, 229, -10.0F, -79.0F, -21.0F, 20, 16, 10, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 157, 251, -10.0F, -85.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 144, 226, -10.0F, -144.0F, -17.0F, 20, 6, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 160, 235, -10.0F, -89.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 166, 292, -10.0F, -136.0F, -17.0F, 20, 2, 6, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 270, 99, -10.0F, -87.0F, -15.0F, 20, 4, 4, 0.0F, false));
		edge_6.cubeList.add(new ModelBox(edge_6, 276, 94, -10.0F, -140.0F, -15.0F, 20, 4, 4, 0.0F, false));

		cooling_blades_6 = new RendererModel(this);
		cooling_blades_6.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_6.addChild(cooling_blades_6);
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -11.0F, -25.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -11.0F, -37.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -12.0F, -26.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -12.0F, -38.0F, -24.0F, 24, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -11.0F, -27.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -11.0F, -39.0F, -22.0F, 22, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -8.0F, -27.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -8.0F, -39.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -8.0F, -25.0F, -17.0F, 16, 1, 5, 0.0F, false));
		cooling_blades_6.cubeList.add(new ModelBox(cooling_blades_6, 287, 100, -8.0F, -37.0F, -17.0F, 16, 1, 5, 0.0F, false));

		plasma_coil_6 = new RendererModel(this);
		plasma_coil_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		edge_6.addChild(plasma_coil_6);
		plasma_coil_6.cubeList.add(new ModelBox(plasma_coil_6, 482, 123, -5.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_6.cubeList.add(new ModelBox(plasma_coil_6, 482, 123, -1.1F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_6.cubeList.add(new ModelBox(plasma_coil_6, 482, 123, 2.9F, -83.0F, -21.0F, 2, 4, 4, 0.0F, false));
		plasma_coil_6.cubeList.add(new ModelBox(plasma_coil_6, 212, 310, -10.0F, -81.0F, -19.0F, 20, 2, 2, 0.0F, false));

		belly_6 = new RendererModel(this);
		belly_6.setRotationPoint(0.0F, -1.0F, 0.0F);
		edge_6.addChild(belly_6);
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -8.0F, -15.0F, -24.0F, 16, 4, 8, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -8.0F, -1.0F, -24.0F, 16, 3, 8, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 313, 55, -4.0F, -1.0F, -28.0F, 8, 3, 4, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -8.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, 5.0F, -11.0F, -21.0F, 3, 10, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, 2.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -4.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -1.0F, -10.0F, -21.0F, 2, 8, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -5.0F, -12.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -5.0F, -2.0F, -21.0F, 10, 2, 5, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -11.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, 8.0F, -15.0F, -24.0F, 3, 17, 8, 0.0F, false));
		belly_6.cubeList.add(new ModelBox(belly_6, 270, 89, -8.0F, -17.0F, -19.0F, 16, 4, 4, 0.0F, false));

		plane_6 = new RendererModel(this);
		plane_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(plane_6, 0.5236F, 0.0F, 0.0F);
		station_6.addChild(plane_6);
		plane_6.cubeList.add(new ModelBox(plane_6, 31, 132, -31.2F, -80.0F, -20.0F, 62, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 37, 124, -28.2F, -80.0F, -14.0F, 56, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 41, 108, -26.0F, -80.0F, -8.0F, 51, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 52, 96, -22.0F, -80.0F, -2.0F, 44, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 253, 82, -17.0F, -77.0F, -17.0F, 34, 8, 30, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 267, 84, -11.0F, -69.0F, -11.0F, 22, 4, 24, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 67, 85, -19.0F, -80.0F, 4.0F, 38, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 70, 76, -16.0F, -80.0F, 10.0F, 32, 4, 6, 0.0F, false));
		plane_6.cubeList.add(new ModelBox(plane_6, 67, 64, -14.0F, -80.0F, 16.0F, 27, 4, 6, 0.0F, false));

		rib_6 = new RendererModel(this);
		rib_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_6, 0.0F, -0.5236F, 0.0F);
		station_6.addChild(rib_6);

		rib_tilt_6 = new RendererModel(this);
		rib_tilt_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(rib_tilt_6, 0.4363F, 0.0F, 0.0F);
		rib_6.addChild(rib_tilt_6);
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -2.0F, -84.0F, -40.0F, 4, 8, 52, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -2.0F, -76.0F, -16.0F, 4, 6, 29, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -76.0F, -20.0F, 2, 26, 4, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -76.0F, -12.0F, 2, 26, 10, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -53.0F, -16.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -59.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -65.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -71.0F, -16.0F, 2, 2, 4, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -2.0F, -70.0F, 0.0F, 4, 6, 14, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -23.8F, -45.0F, 2, 2, 7, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -21.8F, -41.0F, 2, 2, 17, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -25.8F, -49.0F, 2, 2, 11, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -25.8F, -34.0F, 2, 4, 4, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -19.8F, -34.0F, 2, 3, 9, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -25.8F, -26.3F, 2, 4, 4, 0.0F, false));
		rib_tilt_6.cubeList.add(new ModelBox(rib_tilt_6, 180, 260, -1.0F, -27.8F, -49.0F, 2, 2, 28, 0.0F, false));

		rib_deco_6 = new RendererModel(this);
		rib_deco_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		rib_tilt_6.addChild(rib_deco_6);
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 180, 260, -4.0F, -85.0F, -41.0F, 8, 8, 8, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 180, 260, -3.0F, -79.0F, -42.0F, 6, 4, 4, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 180, 260, -4.0F, -85.0F, 7.0F, 8, 8, 8, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 180, 260, -3.0F, -84.0F, 8.0F, 6, 7, 8, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 306, 89, -4.0F, -85.3F, 1.0F, 8, 10, 2, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 317, 89, -4.0F, -84.9F, -13.0F, 8, 10, 2, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 310, 100, -4.0F, -84.2F, -32.0F, 8, 10, 2, 0.0F, false));
		rib_deco_6.cubeList.add(new ModelBox(rib_deco_6, 180, 260, -4.0F, -85.0F, 4.0F, 8, 10, 2, 0.0F, false));

		base_fin_6 = new RendererModel(this);
		base_fin_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(base_fin_6, 0.0F, -0.5236F, 0.0F);
		station_6.addChild(base_fin_6);
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -1.4F, -64.0F, -31.0F, 3, 63, 23, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -0.4F, -40.0F, -39.0F, 1, 28, 2, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -0.4F, -41.0F, -33.0F, 1, 28, 2, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -0.4F, -16.0F, -37.0F, 1, 4, 6, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -0.4F, -24.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -0.4F, -32.0F, -37.0F, 1, 4, 4, 0.0F, false));
		base_fin_6.cubeList.add(new ModelBox(base_fin_6, 180, 260, -0.4F, -40.0F, -37.0F, 1, 4, 6, 0.0F, false));

		clawfoot_6 = new RendererModel(this);
		clawfoot_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(clawfoot_6, 0.0F, -0.5236F, 0.0F);
		station_6.addChild(clawfoot_6);

		leg_6 = new RendererModel(this);
		leg_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_6.addChild(leg_6);
		leg_6.cubeList.add(new ModelBox(leg_6, 180, 260, -2.4F, -4.0F, -52.0F, 5, 4, 35, 0.0F, false));
		leg_6.cubeList.add(new ModelBox(leg_6, 180, 260, -2.2F, -3.9F, -55.0F, 4, 3, 3, 0.0F, false));

		ball_6 = new RendererModel(this);
		ball_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		clawfoot_6.addChild(ball_6);
		ball_6.cubeList.add(new ModelBox(ball_6, 267, 95, -1.4F, -8.0F, -63.0F, 3, 8, 8, 0.0F, false));
		ball_6.cubeList.add(new ModelBox(ball_6, 267, 95, -2.4F, -7.5F, -62.5F, 5, 7, 7, 0.0F, false));
		ball_6.cubeList.add(new ModelBox(ball_6, 267, 95, -3.4F, -7.0F, -62.1F, 7, 6, 6, 0.0F, false));
		ball_6.cubeList.add(new ModelBox(ball_6, 267, 95, -4.4F, -6.5F, -61.7F, 9, 5, 5, 0.0F, false));
		ball_6.cubeList.add(new ModelBox(ball_6, 228, 408, -5.4F, -5.5F, -60.7F, 11, 3, 3, 0.0F, false));

		rotor_rotate_y = new RendererModel(this);
		rotor_rotate_y.setRotationPoint(-0.0167F, -87.1667F, 0.0F);
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 257, 99, -27.9833F, 23.9667F, -2.0F, 56, 2, 4, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 249, 99, -27.9833F, -26.8333F, -2.0F, 56, 2, 4, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 268, 73, 25.6167F, -25.0333F, -1.5F, 2, 49, 3, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 273, 67, -27.6833F, -25.0333F, -1.5F, 2, 49, 3, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 212, 400, -28.9833F, -1.0333F, -4.0F, 4, 4, 8, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 214, 413, 24.6167F, -1.0333F, -4.0F, 4, 4, 8, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 293, 93, -30.9833F, -3.0333F, -2.4F, 8, 8, 5, 0.0F, false));
		rotor_rotate_y.cubeList.add(new ModelBox(rotor_rotate_y, 274, 84, 22.6167F, -3.0333F, -2.4F, 8, 8, 5, 0.0F, false));

		controls = new RendererModel(this);
		controls.setRotationPoint(0.0F, 24.0F, 0.0F);

		controls_1 = new RendererModel(this);
		controls_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls.addChild(controls_1);

		fast_return = new RendererModel(this);
		fast_return.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_1.addChild(fast_return);

		return_button = new RendererModel(this);
		return_button.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(return_button, 0.5236F, 0.0F, 0.0F);
		fast_return.addChild(return_button);
		return_button.cubeList.add(new ModelBox(return_button, 68, 397, 32.0F, 0.0F, -1.0F, 6, 4, 8, 0.0F, false));
		return_button.cubeList.add(new ModelBox(return_button, 102, 14, 33.0F, -1.0F, 1.0F, 4, 4, 4, 0.0F, false));

		waypoints = new RendererModel(this);
		waypoints.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_1.addChild(waypoints);

		way_dial_1 = new RendererModel(this);
		way_dial_1.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_1, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_1);
		way_dial_1.cubeList.add(new ModelBox(way_dial_1, 68, 397, 6.2F, -0.6F, 19.3F, 6, 2, 2, 0.0F, false));
		way_dial_1.cubeList.add(new ModelBox(way_dial_1, 93, 282, 5.2F, -1.1F, 18.8F, 1, 3, 3, 0.0F, false));
		way_dial_1.cubeList.add(new ModelBox(way_dial_1, 116, 279, 12.2F, -1.1F, 18.8F, 1, 3, 3, 0.0F, false));

		way_dial_2 = new RendererModel(this);
		way_dial_2.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_2, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_2);
		way_dial_2.cubeList.add(new ModelBox(way_dial_2, 68, 397, 14.7F, -0.6F, 19.3F, 6, 2, 2, 0.0F, false));
		way_dial_2.cubeList.add(new ModelBox(way_dial_2, 104, 292, 13.7F, -1.1F, 18.8F, 1, 3, 3, 0.0F, false));
		way_dial_2.cubeList.add(new ModelBox(way_dial_2, 108, 289, 20.7F, -1.1F, 18.8F, 1, 3, 3, 0.0F, false));

		way_dial_3 = new RendererModel(this);
		way_dial_3.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_3, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_3);
		way_dial_3.cubeList.add(new ModelBox(way_dial_3, 68, 397, 23.2F, -0.6F, 19.3F, 6, 2, 2, 0.0F, false));
		way_dial_3.cubeList.add(new ModelBox(way_dial_3, 72, 305, 22.2F, -1.1F, 18.8F, 1, 3, 3, 0.0F, false));
		way_dial_3.cubeList.add(new ModelBox(way_dial_3, 72, 305, 29.2F, -1.1F, 18.8F, 1, 3, 3, 0.0F, false));

		way_dial_4 = new RendererModel(this);
		way_dial_4.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_4, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_4);
		way_dial_4.cubeList.add(new ModelBox(way_dial_4, 68, 397, 6.2F, -0.6F, 15.6F, 6, 2, 2, 0.0F, false));
		way_dial_4.cubeList.add(new ModelBox(way_dial_4, 92, 294, 5.2F, -1.1F, 15.1F, 1, 3, 3, 0.0F, false));
		way_dial_4.cubeList.add(new ModelBox(way_dial_4, 59, 300, 12.2F, -1.1F, 15.1F, 1, 3, 3, 0.0F, false));

		way_dial_5 = new RendererModel(this);
		way_dial_5.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_5, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_5);
		way_dial_5.cubeList.add(new ModelBox(way_dial_5, 68, 397, 14.8F, -0.6F, 15.6F, 6, 2, 2, 0.0F, false));
		way_dial_5.cubeList.add(new ModelBox(way_dial_5, 74, 326, 13.8F, -1.1F, 15.1F, 1, 3, 3, 0.0F, false));
		way_dial_5.cubeList.add(new ModelBox(way_dial_5, 84, 319, 20.8F, -1.1F, 15.1F, 1, 3, 3, 0.0F, false));

		way_dial_6 = new RendererModel(this);
		way_dial_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_6, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_6);
		way_dial_6.cubeList.add(new ModelBox(way_dial_6, 68, 397, 23.2F, -0.6F, 15.6F, 6, 2, 2, 0.0F, false));
		way_dial_6.cubeList.add(new ModelBox(way_dial_6, 83, 304, 22.2F, -1.1F, 15.1F, 1, 3, 3, 0.0F, false));
		way_dial_6.cubeList.add(new ModelBox(way_dial_6, 144, 280, 29.2F, -1.1F, 15.1F, 1, 3, 3, 0.0F, false));

		way_dial_7 = new RendererModel(this);
		way_dial_7.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_7, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_7);
		way_dial_7.cubeList.add(new ModelBox(way_dial_7, 68, 397, 6.2F, -0.6F, 12.0F, 6, 2, 2, 0.0F, false));
		way_dial_7.cubeList.add(new ModelBox(way_dial_7, 100, 297, 5.2F, -1.1F, 11.5F, 1, 3, 3, 0.0F, false));
		way_dial_7.cubeList.add(new ModelBox(way_dial_7, 80, 283, 12.2F, -1.1F, 11.5F, 1, 3, 3, 0.0F, false));

		way_dial_8 = new RendererModel(this);
		way_dial_8.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_8, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_8);
		way_dial_8.cubeList.add(new ModelBox(way_dial_8, 68, 397, 14.8F, -0.6F, 12.0F, 6, 2, 2, 0.0F, false));
		way_dial_8.cubeList.add(new ModelBox(way_dial_8, 73, 316, 13.8F, -1.1F, 11.5F, 1, 3, 3, 0.0F, false));
		way_dial_8.cubeList.add(new ModelBox(way_dial_8, 78, 298, 20.8F, -1.1F, 11.5F, 1, 3, 3, 0.0F, false));

		way_dial_9 = new RendererModel(this);
		way_dial_9.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(way_dial_9, 0.5236F, 0.0F, 0.0F);
		waypoints.addChild(way_dial_9);
		way_dial_9.cubeList.add(new ModelBox(way_dial_9, 68, 397, 23.2F, -0.6F, 12.0F, 6, 2, 2, 0.0F, false));
		way_dial_9.cubeList.add(new ModelBox(way_dial_9, 78, 329, 22.2F, -1.1F, 11.5F, 1, 3, 3, 0.0F, false));
		way_dial_9.cubeList.add(new ModelBox(way_dial_9, 74, 332, 29.2F, -1.1F, 11.5F, 1, 3, 3, 0.0F, false));

		dummy_aa_1 = new RendererModel(this);
		dummy_aa_1.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_aa_1, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_1);
		dummy_aa_1.cubeList.add(new ModelBox(dummy_aa_1, 484, 10, -4.3F, 0.0F, 29.4F, 3, 4, 3, 0.0F, false));

		dummy_aa_4 = new RendererModel(this);
		dummy_aa_4.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_aa_4, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_4);
		dummy_aa_4.cubeList.add(new ModelBox(dummy_aa_4, 477, 25, -4.3F, 0.0F, 25.4F, 3, 4, 3, 0.0F, false));

		dummy_aa_2 = new RendererModel(this);
		dummy_aa_2.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_aa_2, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_2);
		dummy_aa_2.cubeList.add(new ModelBox(dummy_aa_2, 475, 29, 0.3F, 0.0F, 29.4F, 3, 4, 3, 0.0F, false));

		dummy_aa_5 = new RendererModel(this);
		dummy_aa_5.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_aa_5, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_5);
		dummy_aa_5.cubeList.add(new ModelBox(dummy_aa_5, 473, 25, 0.3F, 0.0F, 25.4F, 3, 4, 3, 0.0F, false));

		dummy_aa_3 = new RendererModel(this);
		dummy_aa_3.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_aa_3, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_3);
		dummy_aa_3.cubeList.add(new ModelBox(dummy_aa_3, 475, 29, 5.0F, 0.0F, 29.4F, 3, 4, 3, 0.0F, false));

		dummy_aa_6 = new RendererModel(this);
		dummy_aa_6.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_aa_6, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_6);
		dummy_aa_6.cubeList.add(new ModelBox(dummy_aa_6, 473, 27, 5.1F, 0.0F, 25.4F, 3, 4, 3, 0.0F, false));

		dummy_aa_7 = new RendererModel(this);
		dummy_aa_7.setRotationPoint(-12.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_aa_7, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_7);
		dummy_aa_7.cubeList.add(new ModelBox(dummy_aa_7, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_aa_7 = new RendererModel(this);
		toggle_aa_7.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_aa_7, -0.5236F, 0.0F, 0.0F);
		dummy_aa_7.addChild(toggle_aa_7);
		toggle_aa_7.cubeList.add(new ModelBox(toggle_aa_7, 102, 232, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_aa_7.cubeList.add(new ModelBox(toggle_aa_7, 102, 232, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_aa_8 = new RendererModel(this);
		dummy_aa_8.setRotationPoint(-7.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_aa_8, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_8);
		dummy_aa_8.cubeList.add(new ModelBox(dummy_aa_8, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_aa_2 = new RendererModel(this);
		toggle_aa_2.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_aa_2, -0.5236F, 0.0F, 0.0F);
		dummy_aa_8.addChild(toggle_aa_2);
		toggle_aa_2.cubeList.add(new ModelBox(toggle_aa_2, 111, 300, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_aa_2.cubeList.add(new ModelBox(toggle_aa_2, 111, 300, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_aa_9 = new RendererModel(this);
		dummy_aa_9.setRotationPoint(-2.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_aa_9, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_9);
		dummy_aa_9.cubeList.add(new ModelBox(dummy_aa_9, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_aa_3 = new RendererModel(this);
		toggle_aa_3.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_aa_3, -0.5236F, 0.0F, 0.0F);
		dummy_aa_9.addChild(toggle_aa_3);
		toggle_aa_3.cubeList.add(new ModelBox(toggle_aa_3, 71, 265, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_aa_3.cubeList.add(new ModelBox(toggle_aa_3, 71, 265, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_aa_10 = new RendererModel(this);
		dummy_aa_10.setRotationPoint(3.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_aa_10, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_10);
		dummy_aa_10.cubeList.add(new ModelBox(dummy_aa_10, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_aa_4 = new RendererModel(this);
		toggle_aa_4.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_aa_4, -0.5236F, 0.0F, 0.0F);
		dummy_aa_10.addChild(toggle_aa_4);
		toggle_aa_4.cubeList.add(new ModelBox(toggle_aa_4, 70, 291, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_aa_4.cubeList.add(new ModelBox(toggle_aa_4, 70, 291, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_aa_11 = new RendererModel(this);
		dummy_aa_11.setRotationPoint(8.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_aa_11, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(dummy_aa_11);
		dummy_aa_11.cubeList.add(new ModelBox(dummy_aa_11, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_aa_5 = new RendererModel(this);
		toggle_aa_5.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_aa_5, -0.5236F, 0.0F, 0.0F);
		dummy_aa_11.addChild(toggle_aa_5);
		toggle_aa_5.cubeList.add(new ModelBox(toggle_aa_5, 103, 276, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_aa_5.cubeList.add(new ModelBox(toggle_aa_5, 103, 276, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		decoration_1 = new RendererModel(this);
		decoration_1.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(decoration_1, 0.5236F, 0.0F, 0.0F);
		controls_1.addChild(decoration_1);
		decoration_1.cubeList.add(new ModelBox(decoration_1, 105, 291, 10.0F, 0.4F, 34.7F, 16, 4, 1, 0.0F, false));
		decoration_1.cubeList.add(new ModelBox(decoration_1, 105, 291, 10.0F, 0.4F, 24.7F, 16, 4, 1, 0.0F, false));
		decoration_1.cubeList.add(new ModelBox(decoration_1, 105, 291, 10.0F, 0.4F, 25.7F, 1, 4, 9, 0.0F, false));
		decoration_1.cubeList.add(new ModelBox(decoration_1, 105, 291, 25.0F, 0.4F, 25.7F, 1, 4, 9, 0.0F, false));

		controls_2 = new RendererModel(this);
		controls_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controls_2, 0.0F, 1.0472F, 0.0F);
		controls.addChild(controls_2);

		stablizers = new RendererModel(this);
		stablizers.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(stablizers, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(stablizers);
		stablizers.cubeList.add(new ModelBox(stablizers, 224, 413, 30.0F, -2.0F, 4.0F, 6, 5, 2, 0.0F, false));
		stablizers.cubeList.add(new ModelBox(stablizers, 68, 397, 29.0F, 0.0F, 3.0F, 8, 4, 4, 0.0F, false));
		stablizers.cubeList.add(new ModelBox(stablizers, 168, 251, 28.5F, 0.3F, 2.5F, 9, 4, 5, 0.0F, false));

		telpathic_circuits = new RendererModel(this);
		telpathic_circuits.setRotationPoint(-1.0F, 0.0F, 0.0F);
		controls_2.addChild(telpathic_circuits);

		tp_screen = new RendererModel(this);
		tp_screen.setRotationPoint(-0.4F, -80.8F, -41.2F);
		setRotationAngle(tp_screen, -1.0472F, 0.0F, 0.0F);
		telpathic_circuits.addChild(tp_screen);
		tp_screen.cubeList.add(new ModelBox(tp_screen, 203, 263, -10.2F, -4.0F, 9.2F, 1, 4, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 193, 397, -9.2F, -6.0F, 9.2F, 20, 8, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 470, 24, -9.7F, -3.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 472, 28, -6.7F, -6.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 473, 25, -6.7F, -0.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 474, 21, -3.7F, -3.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 475, 19, -0.7F, -6.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 473, 14, -0.7F, -0.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 479, 22, 2.3F, -3.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 472, 27, 5.3F, -6.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 473, 30, 5.3F, -0.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 471, 20, 8.3F, -3.6F, 9.0F, 3, 3, 4, 0.0F, false));
		tp_screen.cubeList.add(new ModelBox(tp_screen, 203, 263, 10.8F, -4.0F, 9.2F, 1, 4, 4, 0.0F, false));

		tp_frame = new RendererModel(this);
		tp_frame.setRotationPoint(0.4F, 80.8F, 41.2F);
		tp_screen.addChild(tp_frame);
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -11.6F, -85.2F, -33.0F, 1, 5, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -10.6F, -80.8F, -33.0F, 1, 3, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, 11.4F, -85.2F, -33.0F, 1, 5, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -9.6F, -87.8F, -33.0F, 20, 1, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -4.3F, -93.8F, -30.8F, 1, 5, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, 3.7F, -93.8F, -30.8F, 1, 5, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -5.3F, -93.4F, -30.8F, 1, 4, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, 4.7F, -93.4F, -30.8F, 1, 4, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -7.3F, -92.4F, -30.8F, 2, 2, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, 5.7F, -92.4F, -30.8F, 2, 2, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -3.3F, -94.6F, -30.8F, 7, 7, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -9.6F, -78.8F, -33.0F, 20, 1, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, -10.6F, -87.8F, -33.0F, 1, 3, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, 10.4F, -87.8F, -33.0F, 1, 3, 4, 0.0F, false));
		tp_frame.cubeList.add(new ModelBox(tp_frame, 203, 263, 10.4F, -80.8F, -33.0F, 1, 3, 4, 0.0F, false));

		eye = new RendererModel(this);
		eye.setRotationPoint(0.0F, 0.0F, 0.0F);
		tp_frame.addChild(eye);
		eye.cubeList.add(new ModelBox(eye, 203, 263, -1.3F, -92.8F, -32.0F, 3, 3, 4, 0.0F, false));
		eye.cubeList.add(new ModelBox(eye, 206, 405, -2.3F, -93.8F, -31.8F, 5, 5, 4, 0.0F, false));
		eye.cubeList.add(new ModelBox(eye, 72, 407, -3.3F, -93.3F, -31.4F, 7, 4, 4, 0.0F, false));
		eye.cubeList.add(new ModelBox(eye, 78, 416, -4.3F, -92.9F, -31.1F, 9, 3, 4, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_2.addChild(door);

		keyhole_base = new RendererModel(this);
		keyhole_base.setRotationPoint(-17.0F, -62.0F, -50.0F);
		setRotationAngle(keyhole_base, 0.5236F, 0.0F, 0.0F);
		door.addChild(keyhole_base);
		keyhole_base.cubeList.add(new ModelBox(keyhole_base, 68, 397, -2.0F, -2.0F, -1.0F, 1, 4, 6, 0.0F, false));
		keyhole_base.cubeList.add(new ModelBox(keyhole_base, 68, 397, 1.0F, -2.0F, -1.0F, 1, 4, 6, 0.0F, false));
		keyhole_base.cubeList.add(new ModelBox(keyhole_base, 68, 397, -1.0F, -2.0F, -2.0F, 2, 4, 2, 0.0F, false));
		keyhole_base.cubeList.add(new ModelBox(keyhole_base, 68, 397, -1.0F, -2.0F, 4.0F, 2, 4, 2, 0.0F, false));
		keyhole_base.cubeList.add(new ModelBox(keyhole_base, 68, 397, -1.0F, -2.0F, 0.0F, 1, 4, 2, 0.0F, false));

		key_rotate_y = new RendererModel(this);
		key_rotate_y.setRotationPoint(0.0F, -3.2641F, 3.1464F);
		setRotationAngle(key_rotate_y, 0.0F, 0.7854F, 0.0F);
		keyhole_base.addChild(key_rotate_y);
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, -0.5F, -1.0F, -0.5F, 1, 3, 1, 0.0F, false));
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, -1.0F, -1.0F, -1.0F, 2, 1, 2, 0.0F, false));
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, -1.5F, -2.0F, -0.5F, 3, 1, 1, 0.0F, false));
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, -1.5F, -4.0F, -0.5F, 3, 1, 1, 0.0F, false));
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, -0.5F, -5.0F, -0.5F, 1, 1, 1, 0.0F, false));
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, -1.5F, -3.0F, -0.5F, 1, 1, 1, 0.0F, false));
		key_rotate_y.cubeList.add(new ModelBox(key_rotate_y, 263, 124, 0.5F, -3.0F, -0.5F, 1, 1, 1, 0.0F, false));

		dummy_b_1 = new RendererModel(this);
		dummy_b_1.setRotationPoint(-6.4F, -61.2F, -55.6F);
		setRotationAngle(dummy_b_1, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_1);

		toggle_b_1_rotate_x = new RendererModel(this);
		toggle_b_1_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
		setRotationAngle(toggle_b_1_rotate_x, -0.2618F, 0.0F, 0.0F);
		dummy_b_1.addChild(toggle_b_1_rotate_x);

		togglebottom_b1 = new RendererModel(this);
		togglebottom_b1.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(togglebottom_b1, -0.3491F, 0.0F, 0.0F);
		toggle_b_1_rotate_x.addChild(togglebottom_b1);
		togglebottom_b1.cubeList.add(new ModelBox(togglebottom_b1, 68, 397, -1.0F, -1.6F, -2.0F, 2, 4, 4, 0.0F, false));

		toggletop_b1 = new RendererModel(this);
		toggletop_b1.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(toggletop_b1, 0.2618F, 0.0F, 0.0F);
		toggle_b_1_rotate_x.addChild(toggletop_b1);
		toggletop_b1.cubeList.add(new ModelBox(toggletop_b1, 68, 397, -1.0F, -1.6F, -1.0F, 2, 4, 4, 0.0F, false));

		dummy_b_2 = new RendererModel(this);
		dummy_b_2.setRotationPoint(-2.4F, -61.2F, -55.6F);
		setRotationAngle(dummy_b_2, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_2);

		toggle_b2_rotate_x = new RendererModel(this);
		toggle_b2_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
		setRotationAngle(toggle_b2_rotate_x, -0.2618F, 0.0F, 0.0F);
		dummy_b_2.addChild(toggle_b2_rotate_x);

		togglebottom_b2 = new RendererModel(this);
		togglebottom_b2.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(togglebottom_b2, -0.3491F, 0.0F, 0.0F);
		toggle_b2_rotate_x.addChild(togglebottom_b2);
		togglebottom_b2.cubeList.add(new ModelBox(togglebottom_b2, 68, 397, -1.0F, -1.6F, -2.0F, 2, 4, 4, 0.0F, false));

		toggletop_b2 = new RendererModel(this);
		toggletop_b2.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(toggletop_b2, 0.2618F, 0.0F, 0.0F);
		toggle_b2_rotate_x.addChild(toggletop_b2);
		toggletop_b2.cubeList.add(new ModelBox(toggletop_b2, 68, 397, -1.0F, -1.6F, -1.0F, 2, 4, 4, 0.0F, false));

		dummy_b_3 = new RendererModel(this);
		dummy_b_3.setRotationPoint(1.6F, -61.2F, -55.6F);
		setRotationAngle(dummy_b_3, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_3);

		toggle_b3_rotate_x = new RendererModel(this);
		toggle_b3_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
		setRotationAngle(toggle_b3_rotate_x, 0.2618F, 0.0F, 0.0F);
		dummy_b_3.addChild(toggle_b3_rotate_x);

		togglebottom_b3 = new RendererModel(this);
		togglebottom_b3.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(togglebottom_b3, -0.3491F, 0.0F, 0.0F);
		toggle_b3_rotate_x.addChild(togglebottom_b3);
		togglebottom_b3.cubeList.add(new ModelBox(togglebottom_b3, 68, 397, -1.0F, -1.6F, -2.0F, 2, 4, 4, 0.0F, false));

		toggletop_b3 = new RendererModel(this);
		toggletop_b3.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(toggletop_b3, 0.2618F, 0.0F, 0.0F);
		toggle_b3_rotate_x.addChild(toggletop_b3);
		toggletop_b3.cubeList.add(new ModelBox(toggletop_b3, 68, 397, -1.0F, -1.6F, -1.0F, 2, 4, 4, 0.0F, false));

		dummy_b_4 = new RendererModel(this);
		dummy_b_4.setRotationPoint(5.6F, -61.2F, -55.6F);
		setRotationAngle(dummy_b_4, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_4);

		toggle_b4_rotate_x = new RendererModel(this);
		toggle_b4_rotate_x.setRotationPoint(0.0F, 1.0F, 6.6F);
		setRotationAngle(toggle_b4_rotate_x, -0.2618F, 0.0F, 0.0F);
		dummy_b_4.addChild(toggle_b4_rotate_x);

		togglebottom_b4 = new RendererModel(this);
		togglebottom_b4.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(togglebottom_b4, -0.3491F, 0.0F, 0.0F);
		toggle_b4_rotate_x.addChild(togglebottom_b4);
		togglebottom_b4.cubeList.add(new ModelBox(togglebottom_b4, 68, 397, -1.0F, -1.6F, -2.0F, 2, 4, 4, 0.0F, false));

		toggletop_b4 = new RendererModel(this);
		toggletop_b4.setRotationPoint(0.0F, 1.0F, -0.6F);
		setRotationAngle(toggletop_b4, 0.2618F, 0.0F, 0.0F);
		toggle_b4_rotate_x.addChild(toggletop_b4);
		toggletop_b4.cubeList.add(new ModelBox(toggletop_b4, 68, 397, -1.0F, -1.6F, -1.0F, 2, 4, 4, 0.0F, false));

		dummy_b_5 = new RendererModel(this);
		dummy_b_5.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_b_5, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_5);
		dummy_b_5.cubeList.add(new ModelBox(dummy_b_5, 107, 284, 9.0F, 0.0F, 11.0F, 4, 4, 4, 0.0F, false));

		dummy_b_6 = new RendererModel(this);
		dummy_b_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_b_6, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_6);
		dummy_b_6.cubeList.add(new ModelBox(dummy_b_6, 122, 279, 16.0F, 0.0F, 11.0F, 4, 4, 4, 0.0F, false));

		dummy_b_7 = new RendererModel(this);
		dummy_b_7.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_b_7, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(dummy_b_7);
		dummy_b_7.cubeList.add(new ModelBox(dummy_b_7, 134, 266, 23.0F, 0.0F, 11.0F, 4, 4, 4, 0.0F, false));

		decoration_2 = new RendererModel(this);
		decoration_2.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(decoration_2, 0.5236F, 0.0F, 0.0F);
		controls_2.addChild(decoration_2);
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, 9.6F, 0.4F, 8.0F, 16, 4, 1, 0.0F, false));
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, 9.6F, 0.4F, 0.0F, 16, 4, 1, 0.0F, false));
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, 8.6F, 0.4F, 0.0F, 1, 4, 9, 0.0F, false));
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, -0.5F, 0.4F, 1.0F, 3, 4, 9, 0.0F, false));
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, 2.5F, 0.4F, 2.0F, 1, 4, 7, 0.0F, false));
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, -1.5F, 0.4F, 2.0F, 1, 4, 7, 0.0F, false));
		decoration_2.cubeList.add(new ModelBox(decoration_2, 227, 274, 25.6F, 0.4F, 0.0F, 1, 4, 9, 0.0F, false));

		controls_3 = new RendererModel(this);
		controls_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controls_3, 0.0F, 2.0944F, 0.0F);
		controls.addChild(controls_3);

		refueler = new RendererModel(this);
		refueler.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_3.addChild(refueler);

		dial_base = new RendererModel(this);
		dial_base.setRotationPoint(15.0F, -63.0F, -49.0F);
		setRotationAngle(dial_base, 0.6109F, 0.0F, 0.0F);
		refueler.addChild(dial_base);
		dial_base.cubeList.add(new ModelBox(dial_base, 128, 277, -4.0F, -2.0F, -4.0F, 8, 4, 8, 0.0F, false));

		round = new RendererModel(this);
		round.setRotationPoint(-15.0F, 63.0F, 49.0F);
		dial_base.addChild(round);
		round.cubeList.add(new ModelBox(round, 49, 424, 12.5F, -65.4F, -52.6F, 5, 4, 7, 0.0F, false));
		round.cubeList.add(new ModelBox(round, 57, 437, 11.5F, -65.4F, -51.6F, 1, 4, 5, 0.0F, false));
		round.cubeList.add(new ModelBox(round, 87, 416, 17.5F, -65.4F, -51.6F, 1, 4, 5, 0.0F, false));

		curve = new RendererModel(this);
		curve.setRotationPoint(-15.0F, 63.0F, 49.0F);
		dial_base.addChild(curve);
		curve.cubeList.add(new ModelBox(curve, 128, 277, 17.0F, -66.0F, -52.9F, 1, 4, 1, 0.0F, false));
		curve.cubeList.add(new ModelBox(curve, 128, 277, 13.0F, -66.0F, -52.9F, 4, 4, 2, 0.0F, false));
		curve.cubeList.add(new ModelBox(curve, 128, 277, 12.0F, -66.0F, -52.9F, 1, 4, 1, 0.0F, false));

		pointer_rotate_y = new RendererModel(this);
		pointer_rotate_y.setRotationPoint(0.1F, -0.8F, -2.7F);
		setRotationAngle(pointer_rotate_y, 0.0F, -0.7854F, 0.0F);
		dial_base.addChild(pointer_rotate_y);
		pointer_rotate_y.cubeList.add(new ModelBox(pointer_rotate_y, 85, 102, -0.5F, -2.0F, -0.9F, 1, 4, 5, 0.0F, false));

		sonic_port = new RendererModel(this);
		sonic_port.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_3.addChild(sonic_port);

		sonic_base = new RendererModel(this);
		sonic_base.setRotationPoint(-2.0F, -70.0F, -40.0F);
		setRotationAngle(sonic_base, 0.5236F, 0.0F, 0.0F);
		sonic_port.addChild(sonic_base);
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, -2.0F, -0.4F, 4.0F, 2, 4, 8, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, -4.0F, -0.4F, 5.0F, 2, 4, 6, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, 6.0F, -0.4F, 5.0F, 2, 4, 6, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, -7.0F, -0.4F, 7.0F, 3, 4, 2, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, 8.0F, -0.4F, 7.0F, 3, 4, 2, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, 4.0F, -0.4F, 4.0F, 2, 4, 8, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, 0.0F, -0.4F, 10.0F, 4, 4, 2, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 68, 397, 0.0F, -0.4F, 4.0F, 4, 4, 2, 0.0F, false));
		sonic_base.cubeList.add(new ModelBox(sonic_base, 110, 264, 0.0F, 0.0F, 6.0F, 4, 4, 4, 0.0F, false));

		randomizer = new RendererModel(this);
		randomizer.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_3.addChild(randomizer);

		rando_plate = new RendererModel(this);
		rando_plate.setRotationPoint(-7.0F, -32.0F, -25.0F);
		setRotationAngle(rando_plate, 0.5236F, 0.0F, 0.0F);
		randomizer.addChild(rando_plate);
		rando_plate.cubeList.add(new ModelBox(rando_plate, 68, 397, -12.0F, -41.0F, -9.4F, 8, 4, 8, 0.0F, false));
		rando_plate.cubeList.add(new ModelBox(rando_plate, 149, 10, -10.0F, -42.0F, -7.4F, 4, 4, 4, 0.0F, false));

		diamond = new RendererModel(this);
		diamond.setRotationPoint(-8.0F, -39.0F, -5.4F);
		setRotationAngle(diamond, 0.0F, -0.7854F, 0.0F);
		rando_plate.addChild(diamond);
		diamond.cubeList.add(new ModelBox(diamond, 74, 294, -4.0F, -1.6F, -4.0F, 8, 4, 8, 0.0F, false));

		dummy_c_1 = new RendererModel(this);
		dummy_c_1.setRotationPoint(-5.0F, -77.0F, -29.0F);
		setRotationAngle(dummy_c_1, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_1);
		dummy_c_1.cubeList.add(new ModelBox(dummy_c_1, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_c_1 = new RendererModel(this);
		toggle_c_1.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_c_1, -0.5236F, 0.0F, 0.0F);
		dummy_c_1.addChild(toggle_c_1);
		toggle_c_1.cubeList.add(new ModelBox(toggle_c_1, 117, 241, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_c_1.cubeList.add(new ModelBox(toggle_c_1, 117, 241, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_c_2 = new RendererModel(this);
		dummy_c_2.setRotationPoint(0.0F, -77.0F, -29.0F);
		setRotationAngle(dummy_c_2, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_2);
		dummy_c_2.cubeList.add(new ModelBox(dummy_c_2, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_c_2 = new RendererModel(this);
		toggle_c_2.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_c_2, -0.5236F, 0.0F, 0.0F);
		dummy_c_2.addChild(toggle_c_2);
		toggle_c_2.cubeList.add(new ModelBox(toggle_c_2, 189, 253, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_c_2.cubeList.add(new ModelBox(toggle_c_2, 189, 253, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_c_3 = new RendererModel(this);
		dummy_c_3.setRotationPoint(5.0F, -77.0F, -29.0F);
		setRotationAngle(dummy_c_3, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_3);
		dummy_c_3.cubeList.add(new ModelBox(dummy_c_3, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_c_3 = new RendererModel(this);
		toggle_c_3.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_c_3, -0.5236F, 0.0F, 0.0F);
		dummy_c_3.addChild(toggle_c_3);
		toggle_c_3.cubeList.add(new ModelBox(toggle_c_3, 157, 245, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_c_3.cubeList.add(new ModelBox(toggle_c_3, 157, 245, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_c_4 = new RendererModel(this);
		dummy_c_4.setRotationPoint(-7.0F, -67.0F, -46.0F);
		setRotationAngle(dummy_c_4, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_4);
		dummy_c_4.cubeList.add(new ModelBox(dummy_c_4, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_c_4 = new RendererModel(this);
		toggle_c_4.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_c_4, -0.5236F, 0.0F, 0.0F);
		dummy_c_4.addChild(toggle_c_4);
		toggle_c_4.cubeList.add(new ModelBox(toggle_c_4, 123, 281, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_c_4.cubeList.add(new ModelBox(toggle_c_4, 123, 281, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_c_5 = new RendererModel(this);
		dummy_c_5.setRotationPoint(0.0F, -67.0F, -46.0F);
		setRotationAngle(dummy_c_5, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_5);
		dummy_c_5.cubeList.add(new ModelBox(dummy_c_5, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_c_5 = new RendererModel(this);
		toggle_c_5.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_c_5, -0.5236F, 0.0F, 0.0F);
		dummy_c_5.addChild(toggle_c_5);
		toggle_c_5.cubeList.add(new ModelBox(toggle_c_5, 100, 253, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_c_5.cubeList.add(new ModelBox(toggle_c_5, 100, 253, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_c_6 = new RendererModel(this);
		dummy_c_6.setRotationPoint(7.0F, -67.0F, -46.0F);
		setRotationAngle(dummy_c_6, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_6);
		dummy_c_6.cubeList.add(new ModelBox(dummy_c_6, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_c_6 = new RendererModel(this);
		toggle_c_6.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_c_6, -0.5236F, 0.0F, 0.0F);
		dummy_c_6.addChild(toggle_c_6);
		toggle_c_6.cubeList.add(new ModelBox(toggle_c_6, 121, 258, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_c_6.cubeList.add(new ModelBox(toggle_c_6, 121, 258, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_c_7 = new RendererModel(this);
		dummy_c_7.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_c_7, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_7);
		dummy_c_7.cubeList.add(new ModelBox(dummy_c_7, 68, 397, -2.5F, 0.0F, 3.0F, 4, 4, 4, 0.0F, false));

		dummy_c_9 = new RendererModel(this);
		dummy_c_9.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_c_9, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_9);
		dummy_c_9.cubeList.add(new ModelBox(dummy_c_9, 253, 405, -0.5F, 0.0F, 0.0F, 2, 4, 2, 0.0F, false));
		dummy_c_9.cubeList.add(new ModelBox(dummy_c_9, 78, 398, -2.5F, 0.0F, 0.0F, 2, 4, 2, 0.0F, false));
		dummy_c_9.cubeList.add(new ModelBox(dummy_c_9, 241, 413, -2.5F, 0.0F, -2.0F, 2, 4, 2, 0.0F, false));
		dummy_c_9.cubeList.add(new ModelBox(dummy_c_9, 64, 416, -0.5F, 0.0F, -2.0F, 2, 4, 2, 0.0F, false));

		dummy_c_8 = new RendererModel(this);
		dummy_c_8.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_c_8, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_8);
		dummy_c_8.cubeList.add(new ModelBox(dummy_c_8, 113, 416, 4.4F, 0.0F, 5.0F, 2, 4, 2, 0.0F, false));
		dummy_c_8.cubeList.add(new ModelBox(dummy_c_8, 74, 409, 2.4F, 0.0F, 3.0F, 2, 4, 2, 0.0F, false));
		dummy_c_8.cubeList.add(new ModelBox(dummy_c_8, 215, 388, 2.4F, 0.0F, 5.0F, 2, 4, 2, 0.0F, false));
		dummy_c_8.cubeList.add(new ModelBox(dummy_c_8, 228, 413, 4.4F, 0.0F, 3.0F, 2, 4, 2, 0.0F, false));

		dummy_c_10 = new RendererModel(this);
		dummy_c_10.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_c_10, 0.5236F, 0.0F, 0.0F);
		controls_3.addChild(dummy_c_10);
		dummy_c_10.cubeList.add(new ModelBox(dummy_c_10, 68, 397, 2.4F, 0.0F, -2.0F, 4, 4, 4, 0.0F, false));

		controls_4 = new RendererModel(this);
		controls_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controls_4, 0.0F, 3.1416F, 0.0F);
		controls.addChild(controls_4);

		throttle = new RendererModel(this);
		throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_4.addChild(throttle);

		throttle_plate = new RendererModel(this);
		throttle_plate.setRotationPoint(-17.0F, -62.0F, -50.0F);
		setRotationAngle(throttle_plate, 0.5236F, 0.0F, 0.0F);
		throttle.addChild(throttle_plate);
		throttle_plate.cubeList.add(new ModelBox(throttle_plate, 298, 87, 29.0F, -2.0F, -3.0F, 10, 4, 5, 0.0F, false));
		throttle_plate.cubeList.add(new ModelBox(throttle_plate, 68, 397, 29.4F, -2.4F, -2.4F, 9, 4, 4, 0.0F, false));
		throttle_plate.cubeList.add(new ModelBox(throttle_plate, 81, 283, 30.5F, -3.7641F, -2.0536F, 1, 3, 3, 0.0F, false));
		throttle_plate.cubeList.add(new ModelBox(throttle_plate, 117, 268, 36.5F, -3.7641F, -2.0536F, 1, 3, 3, 0.0F, false));
		throttle_plate.cubeList.add(new ModelBox(throttle_plate, 202, 279, 32.5F, -3.7641F, -2.0536F, 3, 3, 3, 0.0F, false));
		throttle_plate.cubeList.add(new ModelBox(throttle_plate, 87, 247, 31.5F, -3.3641F, -1.5536F, 5, 3, 2, 0.0F, false));

		throttle_rotate_x = new RendererModel(this);
		throttle_rotate_x.setRotationPoint(34.0F, -2.25F, 0.3F);
		throttle_plate.addChild(throttle_rotate_x);
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 307, 96, 1.5F, -8.0141F, -1.4536F, 1, 9, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 311, 96, -2.5F, -8.0141F, -1.4536F, 1, 9, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, 2.8F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, 1.6F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, 0.4F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, -0.8F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, -2.0F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, -3.2F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 115, 251, -4.4F, -10.6141F, -2.5536F, 1, 3, 3, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 59, 428, -4.8F, -10.0141F, -1.9536F, 9, 2, 2, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 136, 19, -5.2F, -9.7141F, -1.5536F, 1, 1, 1, 0.0F, false));

		dimentional_con = new RendererModel(this);
		dimentional_con.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_4.addChild(dimentional_con);

		dim_screen = new RendererModel(this);
		dim_screen.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(dim_screen, 0.9599F, 0.0F, 0.0F);
		dimentional_con.addChild(dim_screen);
		dim_screen.cubeList.add(new ModelBox(dim_screen, 224, 401, -3.8F, 5.6F, 7.0F, 8, 4, 9, 0.0F, false));
		dim_screen.cubeList.add(new ModelBox(dim_screen, 117, 264, -4.8F, 5.2F, 6.0F, 1, 9, 10, 0.0F, false));
		dim_screen.cubeList.add(new ModelBox(dim_screen, 117, 264, 4.2F, 5.2F, 6.0F, 1, 9, 10, 0.0F, false));
		dim_screen.cubeList.add(new ModelBox(dim_screen, 117, 264, -4.8F, 5.2F, 16.0F, 10, 9, 1, 0.0F, false));

		handbreak = new RendererModel(this);
		handbreak.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_4.addChild(handbreak);

		handbreak_plate = new RendererModel(this);
		handbreak_plate.setRotationPoint(-17.0F, -62.0F, -50.0F);
		setRotationAngle(handbreak_plate, 0.5236F, 0.0F, 0.0F);
		handbreak.addChild(handbreak_plate);
		handbreak_plate.cubeList.add(new ModelBox(handbreak_plate, 68, 397, 0.0F, -2.0F, -2.0F, 4, 4, 6, 0.0F, false));
		handbreak_plate.cubeList.add(new ModelBox(handbreak_plate, 68, 397, 0.5F, -5.2641F, -0.3536F, 3, 4, 3, 0.0F, false));

		handbreak_rotate_y = new RendererModel(this);
		handbreak_rotate_y.setRotationPoint(2.0F, -6.7641F, 1.5631F);
		setRotationAngle(handbreak_rotate_y, 0.0F, 0.8727F, 0.0F);
		handbreak_plate.addChild(handbreak_rotate_y);
		handbreak_rotate_y.cubeList.add(new ModelBox(handbreak_rotate_y, 258, 100, -2.0F, -1.5F, -2.5167F, 4, 3, 4, 0.0F, false));
		handbreak_rotate_y.cubeList.add(new ModelBox(handbreak_rotate_y, 68, 397, -1.0F, -1.0F, -13.5167F, 2, 2, 11, 0.0F, false));
		handbreak_rotate_y.cubeList.add(new ModelBox(handbreak_rotate_y, 151, 19, -1.5F, -1.5F, -5.9167F, 3, 3, 2, 0.0F, false));
		handbreak_rotate_y.cubeList.add(new ModelBox(handbreak_rotate_y, 119, 14, -1.5F, -1.5F, -8.2167F, 3, 3, 2, 0.0F, false));
		handbreak_rotate_y.cubeList.add(new ModelBox(handbreak_rotate_y, 157, 25, -1.5F, -1.5F, -10.5167F, 3, 3, 2, 0.0F, false));
		handbreak_rotate_y.cubeList.add(new ModelBox(handbreak_rotate_y, 149, 27, -1.5F, -1.5F, -12.8167F, 3, 3, 2, 0.0F, false));

		dummy_d_1 = new RendererModel(this);
		dummy_d_1.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_1, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_1);
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 1.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -2.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -5.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 4.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 7.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 10.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 10.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 7.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 4.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 1.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -2.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -5.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -8.0F, 0.0F, 6.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -8.0F, 0.0F, 12.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -6.4F, 0.0F, 9.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -6.4F, 0.0F, 3.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -3.4F, 0.0F, 9.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, -0.4F, 0.0F, 9.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 2.6F, 0.0F, 9.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 5.6F, 0.0F, 9.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 292, 108, -3.4F, 0.0F, 3.0F, 11, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 8.6F, 0.0F, 9.0F, 2, 4, 2, 0.0F, false));
		dummy_d_1.cubeList.add(new ModelBox(dummy_d_1, 68, 397, 8.6F, 0.0F, 3.0F, 2, 4, 2, 0.0F, false));

		dummy_d_2 = new RendererModel(this);
		dummy_d_2.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_2, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_2);
		dummy_d_2.cubeList.add(new ModelBox(dummy_d_2, 68, 397, -8.8F, 0.0F, 21.0F, 3, 4, 3, 0.0F, false));

		dummy_d_3 = new RendererModel(this);
		dummy_d_3.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_3, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_3);
		dummy_d_3.cubeList.add(new ModelBox(dummy_d_3, 68, 397, -8.8F, 0.0F, 17.0F, 3, 4, 3, 0.0F, false));

		dummy_d_4 = new RendererModel(this);
		dummy_d_4.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_4, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_4);
		dummy_d_4.cubeList.add(new ModelBox(dummy_d_4, 68, 397, 10.0F, 0.0F, 21.0F, 3, 4, 3, 0.0F, false));

		dummy_d_5 = new RendererModel(this);
		dummy_d_5.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_5, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_5);
		dummy_d_5.cubeList.add(new ModelBox(dummy_d_5, 68, 397, 10.0F, 0.0F, 17.0F, 3, 4, 3, 0.0F, false));

		dummy_d_6 = new RendererModel(this);
		dummy_d_6.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_6, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_6);
		dummy_d_6.cubeList.add(new ModelBox(dummy_d_6, 302, 104, -3.4F, 0.0F, 17.0F, 3, 4, 3, 0.0F, false));

		dummy_d_7 = new RendererModel(this);
		dummy_d_7.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_7, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_7);
		dummy_d_7.cubeList.add(new ModelBox(dummy_d_7, 300, 91, 0.6F, 0.0F, 17.0F, 3, 4, 3, 0.0F, false));

		dummy_d_8 = new RendererModel(this);
		dummy_d_8.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_d_8, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(dummy_d_8);
		dummy_d_8.cubeList.add(new ModelBox(dummy_d_8, 309, 89, 4.6F, 0.0F, 17.0F, 3, 4, 3, 0.0F, false));

		decoration_4 = new RendererModel(this);
		decoration_4.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(decoration_4, 0.5236F, 0.0F, 0.0F);
		controls_4.addChild(decoration_4);
		decoration_4.cubeList.add(new ModelBox(decoration_4, 138, 241, 12.3F, 0.4F, 22.6F, 12, 4, 12, 0.0F, false));

		controls_5 = new RendererModel(this);
		controls_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controls_5, 0.0F, -2.0944F, 0.0F);
		controls.addChild(controls_5);

		landing_type = new RendererModel(this);
		landing_type.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_5.addChild(landing_type);

		landing_screen = new RendererModel(this);
		landing_screen.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(landing_screen, 0.9599F, 0.0F, 0.0F);
		landing_type.addChild(landing_screen);
		landing_screen.cubeList.add(new ModelBox(landing_screen, 217, 398, 9.2F, -3.3F, -11.0F, 8, 4, 9, 0.0F, false));
		landing_screen.cubeList.add(new ModelBox(landing_screen, 149, 279, 8.2F, -3.7F, -11.0F, 1, 9, 9, 0.0F, false));
		landing_screen.cubeList.add(new ModelBox(landing_screen, 149, 279, 17.2F, -3.7F, -11.0F, 1, 5, 9, 0.0F, false));
		landing_screen.cubeList.add(new ModelBox(landing_screen, 149, 279, 8.2F, -3.7F, -2.0F, 10, 9, 1, 0.0F, false));

		increments = new RendererModel(this);
		increments.setRotationPoint(0.0F, 0.0F, 0.6F);
		controls_5.addChild(increments);

		inc_slider = new RendererModel(this);
		inc_slider.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(inc_slider, 0.6109F, 0.0F, 0.0F);
		increments.addChild(inc_slider);
		inc_slider.cubeList.add(new ModelBox(inc_slider, 68, 397, -1.4F, -0.3F, -11.0F, 1, 4, 9, 0.0F, false));
		inc_slider.cubeList.add(new ModelBox(inc_slider, 68, 397, 0.6F, -0.3F, -11.0F, 1, 4, 9, 0.0F, false));
		inc_slider.cubeList.add(new ModelBox(inc_slider, 68, 397, -1.4F, -0.3F, -2.0F, 3, 4, 1, 0.0F, false));
		inc_slider.cubeList.add(new ModelBox(inc_slider, 68, 397, -1.4F, -0.3F, -12.0F, 3, 4, 1, 0.0F, false));
		inc_slider.cubeList.add(new ModelBox(inc_slider, 83, 260, -0.4F, 0.1F, -11.0F, 1, 4, 9, 0.0F, false));

		position_move_z = new RendererModel(this);
		position_move_z.setRotationPoint(0.0F, 70.0F, 42.0F);
		inc_slider.addChild(position_move_z);
		position_move_z.cubeList.add(new ModelBox(position_move_z, 108, 21, -1.4F, -71.3F, -53.0F, 3, 1, 1, 0.0F, false));

		xyz = new RendererModel(this);
		xyz.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_5.addChild(xyz);

		dial_x = new RendererModel(this);
		dial_x.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(dial_x, 0.6109F, 0.0F, 0.0F);
		xyz.addChild(dial_x);
		dial_x.cubeList.add(new ModelBox(dial_x, 72, 411, -8.6F, 1.7F, 7.0F, 5, 4, 5, 0.0F, false));
		dial_x.cubeList.add(new ModelBox(dial_x, 87, 258, -7.6F, 0.7F, 8.0F, 3, 4, 2, 0.0F, false));
		dial_x.cubeList.add(new ModelBox(dial_x, 87, 258, -7.1F, 0.7F, 10.0F, 2, 4, 1, 0.0F, false));

		dial_y = new RendererModel(this);
		dial_y.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(dial_y, 0.6109F, 0.0F, 0.0F);
		xyz.addChild(dial_y);
		dial_y.cubeList.add(new ModelBox(dial_y, 85, 422, -2.6F, 1.7F, 7.0F, 5, 4, 5, 0.0F, false));
		dial_y.cubeList.add(new ModelBox(dial_y, 68, 236, -1.6F, 0.7F, 8.0F, 3, 4, 2, 0.0F, false));
		dial_y.cubeList.add(new ModelBox(dial_y, 68, 236, -1.1F, 0.7F, 10.0F, 2, 4, 1, 0.0F, false));

		dial_z = new RendererModel(this);
		dial_z.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(dial_z, 0.6109F, 0.0F, 0.0F);
		xyz.addChild(dial_z);
		dial_z.cubeList.add(new ModelBox(dial_z, 49, 422, 3.4F, 1.7F, 7.0F, 5, 4, 5, 0.0F, false));
		dial_z.cubeList.add(new ModelBox(dial_z, 85, 256, 4.4F, 0.7F, 8.0F, 3, 4, 2, 0.0F, false));
		dial_z.cubeList.add(new ModelBox(dial_z, 85, 256, 4.9F, 0.7F, 10.0F, 2, 4, 1, 0.0F, false));

		direction = new RendererModel(this);
		direction.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_5.addChild(direction);

		direction_screen = new RendererModel(this);
		direction_screen.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(direction_screen, 0.9599F, 0.0F, 0.0F);
		direction.addChild(direction_screen);
		direction_screen.cubeList.add(new ModelBox(direction_screen, 209, 375, -16.8F, -3.3F, -11.0F, 8, 4, 9, 0.0F, false));
		direction_screen.cubeList.add(new ModelBox(direction_screen, 89, 245, -17.8F, -3.7F, -11.0F, 1, 5, 9, 0.0F, false));
		direction_screen.cubeList.add(new ModelBox(direction_screen, 89, 245, -8.8F, -3.7F, -11.0F, 1, 8, 9, 0.0F, false));
		direction_screen.cubeList.add(new ModelBox(direction_screen, 89, 245, -17.8F, -3.7F, -2.0F, 10, 8, 1, 0.0F, false));

		dummy_e_1 = new RendererModel(this);
		dummy_e_1.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_e_1, 0.5236F, 0.0F, 0.0F);
		controls_5.addChild(dummy_e_1);
		dummy_e_1.cubeList.add(new ModelBox(dummy_e_1, 68, 397, -3.4F, 0.0F, 29.0F, 3, 4, 3, 0.0F, false));

		dummy_e_2 = new RendererModel(this);
		dummy_e_2.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_e_2, 0.5236F, 0.0F, 0.0F);
		controls_5.addChild(dummy_e_2);
		dummy_e_2.cubeList.add(new ModelBox(dummy_e_2, 68, 397, 0.6F, 0.0F, 29.0F, 3, 4, 3, 0.0F, false));

		dummy_e_3 = new RendererModel(this);
		dummy_e_3.setRotationPoint(-2.0F, -63.0F, -53.0F);
		setRotationAngle(dummy_e_3, 0.5236F, 0.0F, 0.0F);
		controls_5.addChild(dummy_e_3);
		dummy_e_3.cubeList.add(new ModelBox(dummy_e_3, 68, 397, 4.6F, 0.0F, 29.0F, 3, 4, 3, 0.0F, false));

		decoration_5 = new RendererModel(this);
		decoration_5.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(decoration_5, 0.5236F, 0.0F, 0.0F);
		controls_5.addChild(decoration_5);
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, -0.8F, 0.4F, 2.4F, 12, 4, 12, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 7.8F, 0.4F, 20.0F, 20, 4, 8, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 27.8F, 0.4F, 22.0F, 2, 4, 4, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 5.8F, 0.4F, 22.0F, 2, 4, 4, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 6.8F, 0.4F, 21.0F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 6.8F, 0.4F, 26.0F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 27.8F, 0.4F, 26.0F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 27.8F, 0.4F, 21.0F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 25.2F, 0.4F, 2.4F, 12, 4, 12, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 15.2F, 0.4F, 2.4F, 6, 4, 12, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 17.2F, 0.4F, 0.4F, 2, 4, 2, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 17.2F, 0.4F, 14.4F, 2, 4, 2, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 19.2F, 0.4F, 1.4F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 19.2F, 0.4F, 14.4F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 16.2F, 0.4F, 1.4F, 1, 4, 1, 0.0F, false));
		decoration_5.cubeList.add(new ModelBox(decoration_5, 136, 264, 16.2F, 0.4F, 14.4F, 1, 4, 1, 0.0F, false));

		arrows = new RendererModel(this);
		arrows.setRotationPoint(5.2F, 2.4F, 8.4F);
		setRotationAngle(arrows, 0.0F, -0.7854F, 0.0F);
		decoration_5.addChild(arrows);
		arrows.cubeList.add(new ModelBox(arrows, 57, 416, -5.0F, -1.8F, -5.0F, 10, 4, 10, 0.0F, false));

		arrows2 = new RendererModel(this);
		arrows2.setRotationPoint(5.2F, 2.4F, 8.4F);
		setRotationAngle(arrows2, 0.0F, -0.7854F, 0.0F);
		decoration_5.addChild(arrows2);
		arrows2.cubeList.add(new ModelBox(arrows2, 136, 264, 15.6F, -1.8F, -20.5F, 8, 4, 7, 0.0F, false));
		arrows2.cubeList.add(new ModelBox(arrows2, 76, 407, 13.6F, -1.8F, -23.5F, 8, 4, 7, 0.0F, false));

		controls_6 = new RendererModel(this);
		controls_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(controls_6, 0.0F, -1.0472F, 0.0F);
		controls.addChild(controls_6);

		monitor = new RendererModel(this);
		monitor.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_6.addChild(monitor);

		screen = new RendererModel(this);
		screen.setRotationPoint(0.0F, -70.0F, -42.0F);
		setRotationAngle(screen, 0.9599F, 0.0F, 0.0F);
		monitor.addChild(screen);
		screen.cubeList.add(new ModelBox(screen, 208, 412, -10.0F, -1.0F, -5.0F, 20, 4, 15, 0.0F, false));

		circles = new RendererModel(this);
		circles.setRotationPoint(0.0F, 70.0F, 42.0F);
		screen.addChild(circles);
		circles.cubeList.add(new ModelBox(circles, 473, 17, -10.0F, -71.1F, -43.0F, 4, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -6.0F, -71.1F, -42.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -4.0F, -71.1F, -41.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -2.0F, -71.1F, -40.0F, 1, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -1.0F, -71.1F, -39.0F, 1, 0, 2, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 0.0F, -71.1F, -37.0F, 1, 0, 2, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 1.0F, -71.1F, -35.0F, 1, 0, 4, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -10.0F, -71.1F, -35.0F, 3, 0, 3, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 5.0F, -71.1F, -46.0F, 1, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 7.0F, -71.1F, -44.0F, 1, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 8.0F, -71.1F, -41.0F, 1, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 8.0F, -71.1F, -37.0F, 1, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 7.0F, -71.1F, -34.0F, 1, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 3.0F, -71.1F, -38.0F, 4, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 6.0F, -71.1F, -39.0F, 4, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -3.0F, -71.1F, -38.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -5.0F, -71.1F, -37.0F, 3, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -7.0F, -71.1F, -36.0F, 3, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 3.0F, -71.1F, -36.0F, 1, 0, 4, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -10.0F, -71.1F, -45.0F, 5, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -5.0F, -71.1F, -44.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -3.0F, -71.1F, -43.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, -1.0F, -71.1F, -44.0F, 1, 0, 4, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 2.0F, -71.1F, -44.0F, 1, 0, 4, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 0.0F, -71.1F, -41.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 0.0F, -71.1F, -44.0F, 2, 0, 1, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 2.0F, -71.1F, -38.0F, 1, 0, 2, 0.0F, false));
		circles.cubeList.add(new ModelBox(circles, 473, 17, 1.0F, -71.1F, -40.0F, 1, 0, 2, 0.0F, false));

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 70.0F, 42.0F);
		screen.addChild(frame);
		frame.cubeList.add(new ModelBox(frame, 161, 256, -11.0F, -72.0F, -48.0F, 1, 9, 16, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, 10.0F, -72.0F, -48.0F, 1, 9, 16, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, -11.0F, -72.0F, -32.0F, 22, 9, 1, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, -1.1F, -72.0F, -31.0F, 2, 11, 2, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, -2.1F, -72.0F, -31.0F, 1, 11, 1, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, 0.9F, -72.0F, -31.0F, 1, 11, 1, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, -0.5F, -72.4F, -30.5F, 1, 1, 1, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 161, 256, -11.0F, -72.0F, -48.0F, 22, 5, 1, 0.0F, false));

		coms = new RendererModel(this);
		coms.setRotationPoint(0.0F, 0.0F, 0.0F);
		controls_6.addChild(coms);

		speaker = new RendererModel(this);
		speaker.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(speaker, 0.5236F, 0.0F, 0.0F);
		coms.addChild(speaker);
		speaker.cubeList.add(new ModelBox(speaker, 262, 113, -5.0F, 0.0F, -1.0F, 8, 4, 8, 0.0F, false));
		speaker.cubeList.add(new ModelBox(speaker, 87, 323, -4.0F, -0.6F, 3.2F, 6, 4, 1, 0.0F, false));
		speaker.cubeList.add(new ModelBox(speaker, 84, 290, -3.6F, -0.6F, 1.6F, 5, 4, 1, 0.0F, false));
		speaker.cubeList.add(new ModelBox(speaker, 68, 397, -3.2F, -0.6F, -0.4F, 1, 4, 1, 0.0F, false));
		speaker.cubeList.add(new ModelBox(speaker, 68, 397, 0.2F, -0.6F, -0.4F, 1, 4, 1, 0.0F, false));
		speaker.cubeList.add(new ModelBox(speaker, 69, 313, -3.6F, -0.6F, 4.8F, 5, 4, 1, 0.0F, false));

		dummy_ff_5 = new RendererModel(this);
		dummy_ff_5.setRotationPoint(21.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_5, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_5);
		dummy_ff_5.cubeList.add(new ModelBox(dummy_ff_5, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_ff_5 = new RendererModel(this);
		toggle_ff_5.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_ff_5, -0.5236F, 0.0F, 0.0F);
		dummy_ff_5.addChild(toggle_ff_5);
		toggle_ff_5.cubeList.add(new ModelBox(toggle_ff_5, 93, 247, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_ff_5.cubeList.add(new ModelBox(toggle_ff_5, 93, 247, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_ff_1 = new RendererModel(this);
		dummy_ff_1.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_1, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_1);
		dummy_ff_1.cubeList.add(new ModelBox(dummy_ff_1, 68, 397, 10.0F, 0.0F, 0.0F, 4, 4, 4, 0.0F, false));

		dummy_ff_2 = new RendererModel(this);
		dummy_ff_2.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_2, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_2);
		dummy_ff_2.cubeList.add(new ModelBox(dummy_ff_2, 68, 397, 22.4F, 0.0F, 0.0F, 4, 4, 4, 0.0F, false));

		dummy_ff_3 = new RendererModel(this);
		dummy_ff_3.setRotationPoint(16.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_3, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_3);
		dummy_ff_3.cubeList.add(new ModelBox(dummy_ff_3, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_ff_3 = new RendererModel(this);
		toggle_ff_3.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_ff_3, -0.5236F, 0.0F, 0.0F);
		dummy_ff_3.addChild(toggle_ff_3);
		toggle_ff_3.cubeList.add(new ModelBox(toggle_ff_3, 72, 268, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_ff_3.cubeList.add(new ModelBox(toggle_ff_3, 72, 268, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_ff_4 = new RendererModel(this);
		dummy_ff_4.setRotationPoint(16.0F, -65.0F, -49.0F);
		setRotationAngle(dummy_ff_4, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_4);
		dummy_ff_4.cubeList.add(new ModelBox(dummy_ff_4, 68, 397, -2.0F, 0.0F, 2.0F, 4, 4, 4, 0.0F, false));

		toggle_ff_4 = new RendererModel(this);
		toggle_ff_4.setRotationPoint(0.0F, 1.0F, 4.0F);
		setRotationAngle(toggle_ff_4, -0.5236F, 0.0F, 0.0F);
		dummy_ff_4.addChild(toggle_ff_4);
		toggle_ff_4.cubeList.add(new ModelBox(toggle_ff_4, 78, 238, -1.0F, -1.7F, -1.4F, 2, 4, 2, 0.0F, false));
		toggle_ff_4.cubeList.add(new ModelBox(toggle_ff_4, 78, 238, -1.0F, -3.7F, -0.9F, 2, 2, 1, 0.0F, false));

		dummy_ff_6 = new RendererModel(this);
		dummy_ff_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_6, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_6);
		dummy_ff_6.cubeList.add(new ModelBox(dummy_ff_6, 68, 397, 11.0F, 0.0F, 30.0F, 4, 4, 4, 0.0F, false));

		dummy_ff_7 = new RendererModel(this);
		dummy_ff_7.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_7, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_7);
		dummy_ff_7.cubeList.add(new ModelBox(dummy_ff_7, 68, 397, 16.0F, 0.0F, 32.0F, 4, 4, 4, 0.0F, false));

		dummy_ff_8 = new RendererModel(this);
		dummy_ff_8.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(dummy_ff_8, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(dummy_ff_8);
		dummy_ff_8.cubeList.add(new ModelBox(dummy_ff_8, 68, 397, 21.0F, 0.0F, 30.0F, 4, 4, 4, 0.0F, false));

		decoration_6 = new RendererModel(this);
		decoration_6.setRotationPoint(-18.0F, -62.0F, -54.0F);
		setRotationAngle(decoration_6, 0.5236F, 0.0F, 0.0F);
		controls_6.addChild(decoration_6);
		decoration_6.cubeList.add(new ModelBox(decoration_6, 87, 272, 5.8F, 0.4F, 7.0F, 24, 4, 21, 0.0F, false));
		decoration_6.cubeList.add(new ModelBox(decoration_6, 94, 288, 16.4F, 0.4F, 28.0F, 3, 4, 2, 0.0F, false));
		decoration_6.cubeList.add(new ModelBox(decoration_6, 95, 285, 19.4F, 0.4F, 28.0F, 1, 4, 1, 0.0F, false));
		decoration_6.cubeList.add(new ModelBox(decoration_6, 78, 268, 15.4F, 0.4F, 28.0F, 1, 4, 1, 0.0F, false));

		control_hitboxes = new RendererModel(this);
		control_hitboxes.setRotationPoint(0.0F, 24.0F, 0.0F);

		hitbox_monitor = new RendererModel(this);
		hitbox_monitor.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_monitor);
		hitbox_monitor.cubeList.add(new ModelBox(hitbox_monitor, 567, 435, 24.0F, -80.0F, -28.0F, 12, 16, 12, 0.0F, false));
		hitbox_monitor.cubeList.add(new ModelBox(hitbox_monitor, 567, 435, 28.0F, -80.0F, -32.0F, 8, 16, 4, 0.0F, false));
		hitbox_monitor.cubeList.add(new ModelBox(hitbox_monitor, 567, 435, 28.0F, -80.0F, -16.0F, 8, 16, 8, 0.0F, false));
		hitbox_monitor.cubeList.add(new ModelBox(hitbox_monitor, 567, 435, 36.0F, -80.0F, -28.0F, 4, 16, 20, 0.0F, false));
		hitbox_monitor.cubeList.add(new ModelBox(hitbox_monitor, 567, 435, 40.0F, -80.0F, -24.0F, 4, 16, 12, 0.0F, false));

		hitbox_coms = new RendererModel(this);
		hitbox_coms.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_coms);
		hitbox_coms.cubeList.add(new ModelBox(hitbox_coms, 567, 435, 28.0F, -68.0F, -48.0F, 12, 12, 12, 0.0F, false));

		hitbox_fast_return = new RendererModel(this);
		hitbox_fast_return.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_fast_return);
		hitbox_fast_return.cubeList.add(new ModelBox(hitbox_fast_return, 567, 435, 14.0F, -68.0F, -55.0F, 6, 12, 7, 0.0F, false));

		hitbox_waypoint_1 = new RendererModel(this);
		hitbox_waypoint_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_1);
		hitbox_waypoint_1.cubeList.add(new ModelBox(hitbox_waypoint_1, 567, 435, -13.0F, -75.0F, -38.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_2 = new RendererModel(this);
		hitbox_waypoint_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_2);
		hitbox_waypoint_2.cubeList.add(new ModelBox(hitbox_waypoint_2, 567, 435, -4.2F, -75.0F, -38.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_3 = new RendererModel(this);
		hitbox_waypoint_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_3);
		hitbox_waypoint_3.cubeList.add(new ModelBox(hitbox_waypoint_3, 567, 435, 3.8F, -75.0F, -38.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_4 = new RendererModel(this);
		hitbox_waypoint_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_4);
		hitbox_waypoint_4.cubeList.add(new ModelBox(hitbox_waypoint_4, 567, 435, -12.9F, -73.0F, -42.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_5 = new RendererModel(this);
		hitbox_waypoint_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_5);
		hitbox_waypoint_5.cubeList.add(new ModelBox(hitbox_waypoint_5, 567, 435, -3.9F, -73.0F, -42.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_6 = new RendererModel(this);
		hitbox_waypoint_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_6);
		hitbox_waypoint_6.cubeList.add(new ModelBox(hitbox_waypoint_6, 567, 435, 4.1F, -73.0F, -42.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_7 = new RendererModel(this);
		hitbox_waypoint_7.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_7);
		hitbox_waypoint_7.cubeList.add(new ModelBox(hitbox_waypoint_7, 567, 435, -12.9F, -71.0F, -45.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_8 = new RendererModel(this);
		hitbox_waypoint_8.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_8);
		hitbox_waypoint_8.cubeList.add(new ModelBox(hitbox_waypoint_8, 567, 435, -3.9F, -71.0F, -45.0F, 8, 7, 3, 0.0F, false));

		hitbox_waypoint_9 = new RendererModel(this);
		hitbox_waypoint_9.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_waypoint_9);
		hitbox_waypoint_9.cubeList.add(new ModelBox(hitbox_waypoint_9, 567, 435, 4.1F, -71.0F, -45.0F, 8, 7, 3, 0.0F, false));

		hitbox_door = new RendererModel(this);
		hitbox_door.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_door);
		hitbox_door.cubeList.add(new ModelBox(hitbox_door, 567, 435, -56.9F, -69.0F, -15.0F, 7, 9, 8, 0.0F, false));

		hitbox_stablizers = new RendererModel(this);
		hitbox_stablizers.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_stablizers);
		hitbox_stablizers.cubeList.add(new ModelBox(hitbox_stablizers, 567, 435, -39.3F, -69.0F, -42.0F, 7, 9, 8, 0.0F, false));

		hitbox_telepathic = new RendererModel(this);
		hitbox_telepathic.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_telepathic);
		hitbox_telepathic.cubeList.add(new ModelBox(hitbox_telepathic, 567, 435, -37.9F, -77.0F, -15.0F, 13, 9, 12, 0.0F, false));
		hitbox_telepathic.cubeList.add(new ModelBox(hitbox_telepathic, 567, 435, -30.9F, -77.0F, -27.0F, 13, 9, 12, 0.0F, false));
		hitbox_telepathic.cubeList.add(new ModelBox(hitbox_telepathic, 567, 435, -33.9F, -77.0F, -23.0F, 3, 9, 8, 0.0F, false));
		hitbox_telepathic.cubeList.add(new ModelBox(hitbox_telepathic, 567, 435, -36.9F, -77.0F, -18.0F, 3, 9, 3, 0.0F, false));

		hitbox_refueler = new RendererModel(this);
		hitbox_refueler.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_refueler);
		hitbox_refueler.cubeList.add(new ModelBox(hitbox_refueler, 567, 435, -53.9F, -69.0F, 8.0F, 7, 9, 8, 0.0F, false));

		hitbox_randomizer = new RendererModel(this);
		hitbox_randomizer.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_randomizer);
		hitbox_randomizer.cubeList.add(new ModelBox(hitbox_randomizer, 567, 435, -39.9F, -69.0F, 34.0F, 7, 9, 8, 0.0F, false));

		hitbox_sonicport = new RendererModel(this);
		hitbox_sonicport.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_sonicport);
		hitbox_sonicport.cubeList.add(new ModelBox(hitbox_sonicport, 567, 435, -31.9F, -77.0F, 13.0F, 7, 9, 8, 0.0F, false));

		hitbox_dimentions = new RendererModel(this);
		hitbox_dimentions.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_dimentions);
		hitbox_dimentions.cubeList.add(new ModelBox(hitbox_dimentions, 567, 435, -4.9F, -81.0F, 25.0F, 10, 9, 8, 0.0F, false));

		hitbox_throttle = new RendererModel(this);
		hitbox_throttle.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_throttle);
		hitbox_throttle.cubeList.add(new ModelBox(hitbox_throttle, 567, 435, -22.9F, -72.0F, 47.0F, 12, 13, 10, 0.0F, false));

		hitbox_handbreak = new RendererModel(this);
		hitbox_handbreak.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_handbreak);
		hitbox_handbreak.cubeList.add(new ModelBox(hitbox_handbreak, 567, 435, 13.1F, -72.0F, 47.0F, 14, 13, 12, 0.0F, false));

		hitbox_landingtype = new RendererModel(this);
		hitbox_landingtype.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_landingtype);
		hitbox_landingtype.cubeList.add(new ModelBox(hitbox_landingtype, 567, 435, 30.1F, -72.0F, 31.0F, 9, 13, 9, 0.0F, false));

		hitbox_direction = new RendererModel(this);
		hitbox_direction.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_direction);
		hitbox_direction.cubeList.add(new ModelBox(hitbox_direction, 567, 435, 43.1F, -72.0F, 8.0F, 9, 13, 9, 0.0F, false));

		hitbox_increment = new RendererModel(this);
		hitbox_increment.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_increment);
		hitbox_increment.cubeList.add(new ModelBox(hitbox_increment, 567, 435, 37.1F, -72.0F, 20.6F, 5, 13, 5, 0.0F, false));
		hitbox_increment.cubeList.add(new ModelBox(hitbox_increment, 567, 435, 42.1F, -72.0F, 22.6F, 5, 13, 5, 0.0F, false));

		hitbox_cords_x = new RendererModel(this);
		hitbox_cords_x.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_cords_x);
		hitbox_cords_x.cubeList.add(new ModelBox(hitbox_cords_x, 567, 435, 29.1F, -76.0F, 8.6F, 5, 13, 5, 0.0F, false));

		hitbox_cords_y = new RendererModel(this);
		hitbox_cords_y.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_cords_y);
		hitbox_cords_y.cubeList.add(new ModelBox(hitbox_cords_y, 567, 435, 26.1F, -76.0F, 13.6F, 5, 13, 5, 0.0F, false));

		hitbox_cords_z = new RendererModel(this);
		hitbox_cords_z.setRotationPoint(0.0F, 0.0F, 0.0F);
		control_hitboxes.addChild(hitbox_cords_z);
		hitbox_cords_z.cubeList.add(new ModelBox(hitbox_cords_z, 567, 435, 23.1F, -76.0F, 19.2F, 5, 13, 5, 0.0F, false));
	}

	public void render(ConsoleTile tile) {
		
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		float rot = (tile.flightTicks * 2) % 360.0F;
		this.rotor_rotate_y.rotateAngleY = (float)Math.toRadians(rot);
		
		//Control animations
		HandbrakeControl handbrake = tile.getControl(HandbrakeControl.class);
		if(handbreak != null) {
			this.handbreak_rotate_y.rotateAngleY = (float)Math.toRadians(handbrake.isFree() ? 55 : 0);
		}
		
		ThrottleControl throttle = tile.getControl(ThrottleControl.class);
		if(throttle != null) {
			this.throttle_rotate_x.rotateAngleX = (float) Math.toRadians(45 + (-120 * throttle.getAmount()));
		}
		
		RefuelerControl refuel = tile.getControl(RefuelerControl.class);
		if(refuel != null) {
			this.pointer_rotate_y.rotateAngleY = (float) Math.toRadians(refuel.isRefueling() ? 45 : -45);
		}
		
		IncModControl inc = tile.getControl(IncModControl.class);
		this.position_move_z.offsetZ = (inc.index / (float)IncModControl.COORD_MODS.length) * 0.6F;
		
		console.render(0.0625F);
		rotor_rotate_y.render(0.0625F);
		controls.render(0.0625F);
		ModelHelper.renderPartBrightness(1F, glow_shaft_1, glow_shaft_2, glow_shaft_3);
		
		//Sonic
		GlStateManager.pushMatrix();
		GlStateManager.scaled(1.5, 1.5, 1.5);
		GlStateManager.translated(-0.6, -1.5, 1.4);
		GlStateManager.rotated(-22, 0, 0, 1);
		GlStateManager.rotated(-10, 1, 0, 0);
		Minecraft.getInstance().getItemRenderer().renderItem(tile.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
		
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}