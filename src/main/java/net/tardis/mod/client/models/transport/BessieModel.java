package net.tardis.mod.client.models.transport;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.misc.EntityGlowRenderer;
import net.tardis.mod.client.renderers.RenderText;
import net.tardis.mod.entity.BessieEntity;

import java.util.ArrayList;
import java.util.List;

public class BessieModel extends EntityModel<BessieEntity> implements RenderText.IRenderText {

    public static ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/entity/transport/bessie.png");

    private final RendererModel bb_main;
    private final RendererModel Floor;
    private final RendererModel bone13;
    private final RendererModel bone;
    private final RendererModel bone2;
    private final RendererModel FrontLicensePlate;
    private final RendererModel RightWall;
    private final RendererModel UnderSeatToCarrage2;
    private final RendererModel right_walls;
    private final RendererModel back_right;
    private final RendererModel front_right;
    private final RendererModel Engine2;
    private final RendererModel bone10;
    private final RendererModel bone21;
    private final RendererModel Fenders2;
    private final RendererModel Front2;
    private final RendererModel bone22;
    private final RendererModel bone23;
    private final RendererModel bone24;
    private final RendererModel bone25;
    private final RendererModel bone26;
    private final RendererModel Back2;
    private final RendererModel bone62;
    private final RendererModel bone63;
    private final RendererModel bone64;
    private final RendererModel bone65;
    private final RendererModel Edging2;
    private final RendererModel bone66;
    private final RendererModel bone67;
    private final RendererModel bone68;
    private final RendererModel bone69;
    private final RendererModel bone70;
    private final RendererModel bone71;
    private final RendererModel bone72;
    private final RendererModel LeftWall;
    private final RendererModel UnderSeatToCarrage;
    private final RendererModel front_left2;
    private final RendererModel front_left;
    private final RendererModel Engine;
    private final RendererModel bone3;
    private final RendererModel bone4;
    private final RendererModel Fenders;
    private final RendererModel Front;
    private final RendererModel bone5;
    private final RendererModel bone8;
    private final RendererModel bone9;
    private final RendererModel bone6;
    private final RendererModel bone7;
    private final RendererModel Back;
    private final RendererModel bone11;
    private final RendererModel bone12;
    private final RendererModel bone14;
    private final RendererModel bone15;
    private final RendererModel Edging;
    private final RendererModel bone16;
    private final RendererModel bone17;
    private final RendererModel bone19;
    private final RendererModel bone18;
    private final RendererModel bone20;
    private final RendererModel bone60;
    private final RendererModel bone61;
    private final RendererModel CrossBeams;
    private final RendererModel WindowFrame;
    private final RendererModel Supports;
    private final RendererModel Axles;
    private final RendererModel front;
    private final RendererModel Wheel_FrontRight;
    private final RendererModel south2;
    private final RendererModel east2;
    private final RendererModel north_east2;
    private final RendererModel south_east2;
    private final RendererModel south_west2;
    private final RendererModel west2;
    private final RendererModel north_west2;
    private final RendererModel north2;
    private final RendererModel Wheel_FrontLeft;
    private final RendererModel south4;
    private final RendererModel west4;
    private final RendererModel north_west4;
    private final RendererModel south_west4;
    private final RendererModel south_east4;
    private final RendererModel east4;
    private final RendererModel north_east4;
    private final RendererModel north4;
    private final RendererModel back;
    private final RendererModel Wheel_BackRight;
    private final RendererModel south;
    private final RendererModel east;
    private final RendererModel north_east;
    private final RendererModel south_east;
    private final RendererModel south_west;
    private final RendererModel west;
    private final RendererModel north_west;
    private final RendererModel north;
    private final RendererModel Wheel_BackLeft;
    private final RendererModel south3;
    private final RendererModel west3;
    private final RendererModel north_west3;
    private final RendererModel south_west3;
    private final RendererModel south_east3;
    private final RendererModel east3;
    private final RendererModel north_east3;
    private final RendererModel north3;
    private final RendererModel Spare_Wheel;
    private final RendererModel south5;
    private final RendererModel west5;
    private final RendererModel north_west5;
    private final RendererModel south_west5;
    private final RendererModel south_east5;
    private final RendererModel east5;
    private final RendererModel north_east5;
    private final RendererModel north5;
    private final RendererModel Headlights;
    private final RendererModel Right;
    private final RendererModel Left;
    private final RendererModel light_fright;
    private final RendererModel light_fleft;
    private final RendererModel light_frightlantern;
    private final RendererModel light_fleftlantern;
    private final RendererModel light_rleft;
    private final RendererModel light_rright;
    private final RendererModel light_bumperRearLeft;
    private final RendererModel light_bumperRearRight;
    private final RendererModel Steering;
    private final RendererModel steering_assembly_whole;
    private final RendererModel steering_stick;
    private final RendererModel steering_wheel;
    private final RendererModel RearLicensPlate;
    private final RendererModel LIGHTME;

    private final List<RenderText> textToRender = new ArrayList<>();


    public BessieModel() {
        textureWidth = 512;
        textureHeight = 512;

        bb_main = new RendererModel(this);
        bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
        bb_main.cubeList.add(new ModelBox(bb_main, 0, 178, -24.0F, -57.828F, -141.4F, 48, 4, 108, 0.0F, false));
        bb_main.cubeList.add(new ModelBox(bb_main, 41, 363, 43.0F, -11.0F, -148.0F, 8, 8, 2, 0.0F, false));
        bb_main.cubeList.add(new ModelBox(bb_main, 41, 363, -51.0F, -11.0F, -148.0F, 8, 8, 2, 0.0F, false));

        Floor = new RendererModel(this);
        Floor.setRotationPoint(0.0F, 24.0F, 0.0F);
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -68.0F, -3.0F, -36.0F, 136, 4, 88, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -52.0F, -19.0F, -32.0F, 104, 4, 48, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -52.0F, -35.0F, 52.0F, 104, 4, 40, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -52.0F, -47.0F, 16.0F, 104, 4, 32, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 70, 345, -52.0F, -48.0F, 17.0F, 104, 1, 30, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 68, 347, -52.0F, -66.0F, 93.0F, 104, 1, 30, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -52.0F, -65.0F, 92.0F, 104, 4, 32, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 116, -56.0F, -3.0F, 52.0F, 112, 4, 60, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 170, -56.0F, -21.7846F, 116.5359F, 112, 4, 12, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -58.0F, -23.0F, 128.0F, 116, 8, 4, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -56.0F, -3.0F, -148.0F, 112, 4, 4, 0.0F, false));
        Floor.cubeList.add(new ModelBox(Floor, 0, 0, -36.0F, -3.0F, -144.0F, 72, 4, 108, 0.0F, false));

        bone13 = new RendererModel(this);
        bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone13, 1.0472F, 0.0F, 0.0F);
        Floor.addChild(bone13);
        bone13.cubeList.add(new ModelBox(bone13, 155, 415, -56.0F, 89.4948F, 55.134F, 112, 8, 24, 0.0F, false));

        bone = new RendererModel(this);
        bone.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone, 0.0F, -0.0873F, 0.0F);
        Floor.addChild(bone);
        bone.cubeList.add(new ModelBox(bone, 0, 0, -48.4134F, -3.0F, -140.3144F, 12, 4, 112, 0.0F, false));

        bone2 = new RendererModel(this);
        bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone2, 0.0F, 0.0873F, 0.0F);
        Floor.addChild(bone2);
        bone2.cubeList.add(new ModelBox(bone2, 0, 0, 36.4134F, -3.0F, -140.3144F, 12, 4, 112, 0.0F, false));

        FrontLicensePlate = new RendererModel(this);
        FrontLicensePlate.setRotationPoint(0.0F, 24.0F, 0.0F);
        FrontLicensePlate.cubeList.add(new ModelBox(FrontLicensePlate, 0, 0, -17.0F, -3.0F, -150.0F, 1, 10, 2, 0.0F, false));
        FrontLicensePlate.cubeList.add(new ModelBox(FrontLicensePlate, 0, 0, -16.0F, -3.0F, -150.0F, 32, 1, 2, 0.0F, false));
        FrontLicensePlate.cubeList.add(new ModelBox(FrontLicensePlate, 0, 0, -16.0F, 6.0F, -150.0F, 32, 1, 2, 0.0F, false));
        FrontLicensePlate.cubeList.add(new ModelBox(FrontLicensePlate, 0, 0, 16.0F, -3.0F, -150.0F, 1, 10, 2, 0.0F, false));
        FrontLicensePlate.cubeList.add(new ModelBox(FrontLicensePlate, 0, 0, -16.0F, -2.0F, -149.0F, 32, 8, 1, 0.0F, false));

        RightWall = new RendererModel(this);
        RightWall.setRotationPoint(0.0F, 24.0F, 0.0F);

        UnderSeatToCarrage2 = new RendererModel(this);
        UnderSeatToCarrage2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(UnderSeatToCarrage2);
        UnderSeatToCarrage2.cubeList.add(new ModelBox(UnderSeatToCarrage2, 0, 0, -56.0F, -114.0F, 111.0F, 4, 4, 15, 0.0F, false));
        UnderSeatToCarrage2.cubeList.add(new ModelBox(UnderSeatToCarrage2, 0, 180, -56.0F, -23.0F, -32.0F, 4, 4, 16, 0.0F, false));
        UnderSeatToCarrage2.cubeList.add(new ModelBox(UnderSeatToCarrage2, 0, 180, -56.0F, -31.0F, -32.0F, 4, 8, 5, 0.0F, false));
        UnderSeatToCarrage2.cubeList.add(new ModelBox(UnderSeatToCarrage2, 0, 182, -56.0F, -26.0F, -27.0F, 4, 3, 5, 0.0F, false));
        UnderSeatToCarrage2.cubeList.add(new ModelBox(UnderSeatToCarrage2, 0, 180, -56.0F, -35.0F, -32.0F, 4, 4, 2, 0.0F, false));

        right_walls = new RendererModel(this);
        right_walls.setRotationPoint(0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage2.addChild(right_walls);
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, 16.5914F, -32.1826F, 4, 16, 140, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, 16.5914F, 107.8174F, 4, 12, 4, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, 16.5914F, 111.8174F, 4, 4, 4, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, 0.5914F, 15.8174F, 4, 16, 112, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, -39.4086F, 15.8174F, 4, 40, 36, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, -4.4086F, 51.8174F, 4, 5, 18, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 180, -56.2679F, -8.4086F, 51.8174F, 4, 4, 9, 0.0F, false));
        right_walls.cubeList.add(new ModelBox(right_walls, 0, 171, -56.2679F, -14.4086F, 51.8174F, 4, 6, 4, 0.0F, false));

        back_right = new RendererModel(this);
        back_right.setRotationPoint(0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage2.addChild(back_right);
        back_right.cubeList.add(new ModelBox(back_right, 0, 182, -56.2679F, -58.4086F, 89.8174F, 4, 12, 36, 0.0F, false));
        back_right.cubeList.add(new ModelBox(back_right, 0, 182, -56.2679F, -46.4086F, 87.8174F, 4, 47, 40, 0.0F, false));
        back_right.cubeList.add(new ModelBox(back_right, 0, 182, -56.2679F, -66.4086F, 94.8174F, 4, 4, 31, 0.0F, false));
        back_right.cubeList.add(new ModelBox(back_right, 0, 182, -56.2679F, -62.4086F, 91.8174F, 4, 4, 34, 0.0F, false));
        back_right.cubeList.add(new ModelBox(back_right, 0, 182, -56.2679F, -74.4086F, 106.8174F, 4, 4, 19, 0.0F, false));
        back_right.cubeList.add(new ModelBox(back_right, 0, 182, -56.2679F, -70.4086F, 97.8174F, 4, 4, 28, 0.0F, false));

        front_right = new RendererModel(this);
        front_right.setRotationPoint(0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage2.addChild(front_right);
        front_right.cubeList.add(new ModelBox(front_right, 0, 180, -56.2679F, -51.4086F, 18.8174F, 4, 12, 33, 0.0F, false));
        front_right.cubeList.add(new ModelBox(front_right, 0, 180, -56.2679F, -55.4086F, 20.8174F, 4, 4, 31, 0.0F, false));
        front_right.cubeList.add(new ModelBox(front_right, 0, 180, -56.2679F, -63.4086F, 26.8174F, 4, 4, 25, 0.0F, false));
        front_right.cubeList.add(new ModelBox(front_right, 0, 180, -56.2679F, -59.4086F, 23.8174F, 4, 4, 28, 0.0F, false));
        front_right.cubeList.add(new ModelBox(front_right, 0, 180, -56.2679F, -67.4086F, 35.8174F, 4, 4, 16, 0.0F, false));
        front_right.cubeList.add(new ModelBox(front_right, 0, 180, -56.2679F, -71.4086F, 39.8174F, 4, 4, 12, 0.0F, false));

        Engine2 = new RendererModel(this);
        Engine2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(Engine2);

        bone10 = new RendererModel(this);
        bone10.setRotationPoint(-8.0F, 0.0F, 0.0F);
        setRotationAngle(bone10, 0.0F, -0.0873F, 0.0F);
        Engine2.addChild(bone10);
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -39.0F, -60.3486F, 4, 20, 28, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -39.0F, -76.3486F, 4, 20, 8, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -39.0F, -140.3486F, 4, 20, 24, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -39.0F, -92.3486F, 4, 20, 8, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -39.0F, -108.3486F, 4, 20, 8, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -19.0F, -140.3486F, 4, 16, 108, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -32.0152F, -55.0F, -140.3486F, 4, 16, 108, 0.0F, false));
        bone10.cubeList.add(new ModelBox(bone10, 0, 178, -29.1868F, -57.8284F, -140.3486F, 12, 4, 108, 0.0F, false));

        bone21 = new RendererModel(this);
        bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone21, 0.0F, 0.0F, 0.7854F);
        bone10.addChild(bone21);
        bone21.cubeList.add(new ModelBox(bone21, 0, 178, -61.5291F, -20.2527F, -140.3486F, 4, 4, 108, 0.0F, false));

        Fenders2 = new RendererModel(this);
        Fenders2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(Fenders2);

        Front2 = new RendererModel(this);
        Front2.setRotationPoint(0.0F, 0.0F, 0.0F);
        Fenders2.addChild(Front2);

        bone22 = new RendererModel(this);
        bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone22, 0.3491F, 0.0F, 0.0F);
        Front2.addChild(bone22);
        bone22.cubeList.add(new ModelBox(bone22, 404, 299, -68.0F, -43.373F, -34.171F, 20, 32, 4, 0.0F, false));

        bone23 = new RendererModel(this);
        bone23.setRotationPoint(0.0F, -12.0F, -72.0F);
        Front2.addChild(bone23);
        bone23.cubeList.add(new ModelBox(bone23, 392, 418, -68.0F, -30.5666F, -26.7205F, 20, 4, 40, 0.0F, false));

        bone24 = new RendererModel(this);
        bone24.setRotationPoint(0.0F, -12.0F, -72.0F);
        setRotationAngle(bone24, 0.2618F, 0.0F, 0.0F);
        Front2.addChild(bone24);
        bone24.cubeList.add(new ModelBox(bone24, 406, 464, -68.0F, -36.4408F, -37.8988F, 20, 4, 20, 0.0F, false));

        bone25 = new RendererModel(this);
        bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone25, 0.6981F, 0.0F, 0.0F);
        Front2.addChild(bone25);
        bone25.cubeList.add(new ModelBox(bone25, 404, 354, -68.0F, -59.0764F, -17.517F, 20, 8, 4, 0.0F, false));

        bone26 = new RendererModel(this);
        bone26.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone26, 1.0472F, 0.0F, 0.0F);
        Front2.addChild(bone26);
        bone26.cubeList.add(new ModelBox(bone26, 409, 381, -68.0F, -72.1367F, 3.5035F, 20, 12, 4, 0.0F, false));

        Back2 = new RendererModel(this);
        Back2.setRotationPoint(0.0F, 0.0F, 36.0F);
        Fenders2.addChild(Back2);

        bone62 = new RendererModel(this);
        bone62.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone62, -0.5236F, 0.0F, 0.0F);
        Back2.addChild(bone62);
        bone62.cubeList.add(new ModelBox(bone62, 410, 250, -68.0F, -39.134F, 10.3564F, 20, 32, 4, 0.0F, false));

        bone63 = new RendererModel(this);
        bone63.setRotationPoint(0.0F, -12.0F, 72.0F);
        Back2.addChild(bone63);
        bone63.cubeList.add(new ModelBox(bone63, 389, 160, -68.0F, -29.7241F, -28.492F, 17, 4, 40, 0.0F, false));

        bone64 = new RendererModel(this);
        bone64.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone64, -0.6981F, 0.0F, 0.0F);
        Back2.addChild(bone64);
        bone64.cubeList.add(new ModelBox(bone64, 410, 230, -68.0F, -48.3378F, 3.4035F, 20, 8, 4, 0.0F, false));

        bone65 = new RendererModel(this);
        bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone65, -0.9599F, 0.0F, 0.0F);
        Back2.addChild(bone65);
        bone65.cubeList.add(new ModelBox(bone65, 415, 210, -68.0F, -59.5716F, -9.2232F, 17, 12, 4, 0.0F, false));

        Edging2 = new RendererModel(this);
        Edging2.setRotationPoint(0.0F, 0.0F, 0.0F);
        RightWall.addChild(Edging2);
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -87.6413F, -33.6873F, 6, 54, 4, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -110.0F, 48.9492F, 6, 58, 4, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -117.1224F, 125.8983F, 6, 99, 4, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -82.0F, 85.9492F, 6, 41, 4, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -75.0F, 15.0F, 6, 54, 4, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -101.2628F, 27.2465F, 6, 4, 6, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -108.2628F, 98.1957F, 6, 4, 6, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -110.4811F, 39.9492F, 6, 4, 13, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -117.4811F, 110.8983F, 6, 4, 15, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -21.0F, -12.0F, 6, 4, 31, 0.0F, false));
        Edging2.cubeList.add(new ModelBox(Edging2, 0, 0, -57.0F, -39.4811F, 70.6365F, 6, 4, 18, 0.0F, false));

        bone66 = new RendererModel(this);
        bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone66, -0.1745F, 0.0F, 0.0F);
        Edging2.addChild(bone66);
        bone66.cubeList.add(new ModelBox(bone66, 0, 471, -57.0F, -119.4601F, -48.3943F, 6, 39, 4, 0.0F, false));

        bone67 = new RendererModel(this);
        bone67.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone67, -0.1745F, 0.0F, 0.0F);
        Edging2.addChild(bone67);
        bone67.cubeList.add(new ModelBox(bone67, 0, 0, -57.0F, -91.4653F, 1.7485F, 6, 15, 4, 0.0F, false));
        bone67.cubeList.add(new ModelBox(bone67, 0, 0, -57.0F, -110.6792F, 70.4043F, 6, 15, 4, 0.0F, false));

        bone68 = new RendererModel(this);
        bone68.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone68, -0.6981F, 0.0F, 0.0F);
        Edging2.addChild(bone68);
        bone68.cubeList.add(new ModelBox(bone68, 0, 0, -57.0F, -95.0855F, -44.2184F, 6, 15, 4, 0.0F, false));
        bone68.cubeList.add(new ModelBox(bone68, 0, 0, -57.0F, -146.0531F, 5.6323F, 6, 15, 4, 0.0F, false));

        bone69 = new RendererModel(this);
        bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone69, -0.1745F, 0.0F, 0.0F);
        bone68.addChild(bone69);
        bone69.cubeList.add(new ModelBox(bone69, 0, 0, -57.0F, -95.5648F, -56.6528F, 6, 7, 4, 0.0F, false));
        bone69.cubeList.add(new ModelBox(bone69, 0, 0, -57.0F, -154.4145F, -16.4099F, 6, 7, 4, 0.0F, false));

        bone70 = new RendererModel(this);
        bone70.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone70, 0.1745F, 0.0F, 0.0F);
        bone68.addChild(bone70);
        bone70.cubeList.add(new ModelBox(bone70, 0, 0, -57.0F, -115.2188F, -20.6435F, 6, 7, 4, 0.0F, false));
        bone70.cubeList.add(new ModelBox(bone70, 0, 0, -57.0F, -156.7556F, 37.3002F, 6, 7, 4, 0.0F, false));

        bone71 = new RendererModel(this);
        bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone71, -0.4363F, 0.0F, 0.0F);
        Edging2.addChild(bone71);
        bone71.cubeList.add(new ModelBox(bone71, 0, 0, -57.0F, -14.3358F, -32.0602F, 6, 4, 14, 0.0F, false));
        bone71.cubeList.add(new ModelBox(bone71, 0, 0, -57.0F, -66.009F, 35.0235F, 6, 4, 14, 0.0F, false));

        bone72 = new RendererModel(this);
        bone72.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone72, -0.8727F, 0.0F, 0.0F);
        Edging2.addChild(bone72);
        bone72.cubeList.add(new ModelBox(bone72, 0, 0, -57.0F, 0.1818F, -47.4245F, 6, 4, 14, 0.0F, false));
        bone72.cubeList.add(new ModelBox(bone72, 6, 0, -57.0F, -75.0008F, -8.4641F, 6, 4, 14, 0.0F, false));

        LeftWall = new RendererModel(this);
        LeftWall.setRotationPoint(0.0F, 24.0F, 0.0F);

        UnderSeatToCarrage = new RendererModel(this);
        UnderSeatToCarrage.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(UnderSeatToCarrage);
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 180, 52.0F, -94.0F, 90.0F, 4, 12, 36, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 180, 52.0F, -98.0F, 92.0F, 4, 4, 34, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 180, 52.0F, -102.0F, 95.0F, 4, 4, 31, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 180, 52.0F, -106.0F, 98.0F, 4, 4, 28, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 180, 52.0F, -110.0F, 107.0F, 4, 4, 19, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 180, 52.0F, -114.0F, 111.0F, 4, 4, 15, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 178, 52.0F, -23.0F, -32.0F, 4, 4, 16, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 178, 52.0F, -31.0F, -32.0F, 4, 8, 5, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 178, 52.0F, -26.0F, -27.0F, 4, 3, 5, 0.0F, false));
        UnderSeatToCarrage.cubeList.add(new ModelBox(UnderSeatToCarrage, 0, 182, 52.0F, -35.0F, -32.0F, 4, 4, 2, 0.0F, false));

        front_left2 = new RendererModel(this);
        front_left2.setRotationPoint(-0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage.addChild(front_left2);
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, 16.5914F, -32.1826F, 4, 16, 140, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, 16.5914F, 107.8174F, 4, 12, 4, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, 16.5914F, 111.8174F, 4, 4, 4, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, 0.5914F, 15.8174F, 4, 16, 112, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, -39.4086F, 15.8174F, 4, 40, 36, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, -4.4086F, 51.8174F, 4, 5, 18, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, -8.4086F, 51.8174F, 4, 4, 9, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 171, 52.2679F, -14.4086F, 51.8174F, 4, 6, 4, 0.0F, false));
        front_left2.cubeList.add(new ModelBox(front_left2, 0, 180, 52.2679F, -46.4086F, 87.8174F, 4, 47, 40, 0.0F, false));

        front_left = new RendererModel(this);
        front_left.setRotationPoint(-0.2679F, -35.5914F, 0.1826F);
        UnderSeatToCarrage.addChild(front_left);
        front_left.cubeList.add(new ModelBox(front_left, 0, 180, 52.2679F, -51.4086F, 18.8174F, 4, 12, 33, 0.0F, false));
        front_left.cubeList.add(new ModelBox(front_left, 0, 180, 52.2679F, -55.4086F, 20.8174F, 4, 4, 31, 0.0F, false));
        front_left.cubeList.add(new ModelBox(front_left, 0, 180, 52.2679F, -59.4086F, 23.8174F, 4, 4, 28, 0.0F, false));
        front_left.cubeList.add(new ModelBox(front_left, 0, 180, 52.2679F, -63.4086F, 26.8174F, 4, 4, 25, 0.0F, false));
        front_left.cubeList.add(new ModelBox(front_left, 0, 180, 52.2679F, -67.4086F, 35.8174F, 4, 4, 16, 0.0F, false));
        front_left.cubeList.add(new ModelBox(front_left, 0, 180, 52.2679F, -71.4086F, 39.8174F, 4, 4, 12, 0.0F, false));

        Engine = new RendererModel(this);
        Engine.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(Engine);

        bone3 = new RendererModel(this);
        bone3.setRotationPoint(8.0F, 0.0F, 0.0F);
        setRotationAngle(bone3, 0.0F, 0.0873F, 0.0F);
        Engine.addChild(bone3);
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -39.0F, -60.3486F, 4, 20, 28, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -39.0F, -76.3486F, 4, 20, 8, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -39.0F, -140.3486F, 4, 20, 24, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -39.0F, -92.3486F, 4, 20, 8, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -39.0F, -108.3486F, 4, 20, 8, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -19.0F, -140.3486F, 4, 16, 108, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 28.0152F, -55.0F, -140.3486F, 4, 16, 108, 0.0F, false));
        bone3.cubeList.add(new ModelBox(bone3, 0, 178, 17.1868F, -57.8284F, -140.3486F, 12, 4, 108, 0.0F, false));

        bone4 = new RendererModel(this);
        bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone4, 0.0F, 0.0F, -0.7854F);
        bone3.addChild(bone4);
        bone4.cubeList.add(new ModelBox(bone4, 0, 178, 57.5291F, -20.2527F, -140.3486F, 4, 4, 108, 0.0F, false));

        Fenders = new RendererModel(this);
        Fenders.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(Fenders);

        Front = new RendererModel(this);
        Front.setRotationPoint(0.0F, 0.0F, 0.0F);
        Fenders.addChild(Front);

        bone5 = new RendererModel(this);
        bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone5, 0.3491F, 0.0F, 0.0F);
        Front.addChild(bone5);
        bone5.cubeList.add(new ModelBox(bone5, 404, 299, 48.0F, -43.373F, -34.171F, 20, 32, 4, 0.0F, false));

        bone8 = new RendererModel(this);
        bone8.setRotationPoint(0.0F, -12.0F, -72.0F);
        Front.addChild(bone8);
        bone8.cubeList.add(new ModelBox(bone8, 392, 418, 48.0F, -30.5666F, -26.7205F, 20, 4, 40, 0.0F, false));

        bone9 = new RendererModel(this);
        bone9.setRotationPoint(0.0F, -12.0F, -72.0F);
        setRotationAngle(bone9, 0.2618F, 0.0F, 0.0F);
        Front.addChild(bone9);
        bone9.cubeList.add(new ModelBox(bone9, 406, 464, 48.0F, -36.4408F, -37.8988F, 20, 4, 20, 0.0F, false));

        bone6 = new RendererModel(this);
        bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone6, 0.6981F, 0.0F, 0.0F);
        Front.addChild(bone6);
        bone6.cubeList.add(new ModelBox(bone6, 404, 354, 48.0F, -59.0764F, -17.517F, 20, 8, 4, 0.0F, false));

        bone7 = new RendererModel(this);
        bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone7, 1.0472F, 0.0F, 0.0F);
        Front.addChild(bone7);
        bone7.cubeList.add(new ModelBox(bone7, 409, 381, 48.0F, -72.1367F, 3.5035F, 20, 12, 4, 0.0F, false));

        Back = new RendererModel(this);
        Back.setRotationPoint(0.0F, 0.0F, 36.0F);
        Fenders.addChild(Back);

        bone11 = new RendererModel(this);
        bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone11, -0.5236F, 0.0F, 0.0F);
        Back.addChild(bone11);
        bone11.cubeList.add(new ModelBox(bone11, 410, 250, 48.0F, -39.134F, 10.3564F, 20, 32, 4, 0.0F, false));

        bone12 = new RendererModel(this);
        bone12.setRotationPoint(0.0F, -12.0F, 72.0F);
        Back.addChild(bone12);
        bone12.cubeList.add(new ModelBox(bone12, 389, 160, 51.0F, -29.7241F, -28.492F, 17, 4, 40, 0.0F, false));

        bone14 = new RendererModel(this);
        bone14.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone14, -0.6981F, 0.0F, 0.0F);
        Back.addChild(bone14);
        bone14.cubeList.add(new ModelBox(bone14, 410, 230, 48.0F, -48.3378F, 3.4035F, 20, 8, 4, 0.0F, false));

        bone15 = new RendererModel(this);
        bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone15, -0.9599F, 0.0F, 0.0F);
        Back.addChild(bone15);
        bone15.cubeList.add(new ModelBox(bone15, 415, 210, 51.0F, -59.5716F, -9.2232F, 17, 12, 4, 0.0F, false));

        Edging = new RendererModel(this);
        Edging.setRotationPoint(0.0F, 0.0F, 0.0F);
        LeftWall.addChild(Edging);
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -87.6413F, -33.6873F, 6, 54, 4, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -110.0F, 48.9492F, 6, 58, 4, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -117.1224F, 125.8983F, 6, 99, 4, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -82.0F, 85.9492F, 6, 41, 4, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -75.0F, 15.0F, 6, 54, 4, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -101.2628F, 27.2465F, 6, 4, 6, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -108.2628F, 98.1957F, 6, 4, 6, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -110.4811F, 39.9492F, 6, 4, 13, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -117.4811F, 110.8983F, 6, 4, 15, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -21.0F, -12.0F, 6, 4, 31, 0.0F, false));
        Edging.cubeList.add(new ModelBox(Edging, 0, 0, 51.0F, -39.4811F, 70.6365F, 6, 4, 18, 0.0F, false));

        bone16 = new RendererModel(this);
        bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone16, -0.1745F, 0.0F, 0.0F);
        Edging.addChild(bone16);
        bone16.cubeList.add(new ModelBox(bone16, 0, 471, 51.0F, -119.4601F, -48.3943F, 6, 39, 4, 0.0F, false));

        bone17 = new RendererModel(this);
        bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone17, -0.1745F, 0.0F, 0.0F);
        Edging.addChild(bone17);
        bone17.cubeList.add(new ModelBox(bone17, 0, 0, 51.0F, -91.4653F, 1.7485F, 6, 15, 4, 0.0F, false));
        bone17.cubeList.add(new ModelBox(bone17, 0, 0, 51.0F, -110.6792F, 70.4043F, 6, 15, 4, 0.0F, false));

        bone19 = new RendererModel(this);
        bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone19, -0.6981F, 0.0F, 0.0F);
        Edging.addChild(bone19);
        bone19.cubeList.add(new ModelBox(bone19, 0, 0, 51.0F, -95.0855F, -44.2184F, 6, 15, 4, 0.0F, false));
        bone19.cubeList.add(new ModelBox(bone19, 0, 0, 51.0F, -146.0531F, 5.6323F, 6, 15, 4, 0.0F, false));

        bone18 = new RendererModel(this);
        bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone18, -0.1745F, 0.0F, 0.0F);
        bone19.addChild(bone18);
        bone18.cubeList.add(new ModelBox(bone18, 0, 0, 51.0F, -95.5648F, -56.6528F, 6, 7, 4, 0.0F, false));
        bone18.cubeList.add(new ModelBox(bone18, 0, 0, 51.0F, -154.4145F, -16.4099F, 6, 7, 4, 0.0F, false));

        bone20 = new RendererModel(this);
        bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone20, 0.1745F, 0.0F, 0.0F);
        bone19.addChild(bone20);
        bone20.cubeList.add(new ModelBox(bone20, 0, 0, 51.0F, -115.2188F, -20.6435F, 6, 7, 4, 0.0F, false));
        bone20.cubeList.add(new ModelBox(bone20, 0, 0, 51.0F, -156.7556F, 37.3002F, 6, 7, 4, 0.0F, false));

        bone60 = new RendererModel(this);
        bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone60, -0.4363F, 0.0F, 0.0F);
        Edging.addChild(bone60);
        bone60.cubeList.add(new ModelBox(bone60, 0, 0, 51.0F, -14.3358F, -32.0602F, 6, 4, 14, 0.0F, false));
        bone60.cubeList.add(new ModelBox(bone60, 0, 0, 51.0F, -66.009F, 35.0235F, 6, 4, 14, 0.0F, false));

        bone61 = new RendererModel(this);
        bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(bone61, -0.8727F, 0.0F, 0.0F);
        Edging.addChild(bone61);
        bone61.cubeList.add(new ModelBox(bone61, 0, 0, 51.0F, 0.1818F, -47.4245F, 6, 4, 14, 0.0F, false));
        bone61.cubeList.add(new ModelBox(bone61, 0, 0, 51.0F, -75.0008F, -8.4641F, 6, 4, 14, 0.0F, false));

        CrossBeams = new RendererModel(this);
        CrossBeams.setRotationPoint(0.0F, 24.0F, 0.0F);
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 0, -28.0F, -55.0F, -136.0F, 56, 52, 88, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 180, -56.0F, -75.0F, -36.0F, 112, 72, 4, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 0, -24.0F, -55.0F, -141.2F, 48, 52, 4, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 180, -52.0F, -43.0F, 16.0F, 104, 40, 4, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 180, -52.0F, -61.0F, 92.0F, 104, 54, 4, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 180, -52.0F, -109.0F, 48.0F, 104, 105, 4, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 0, -52.0F, -110.5F, 48.0F, 104, 2, 5, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 0, -52.0F, -109.0F, 47.0F, 104, 106, 1, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 0, -52.0F, -115.0F, 123.0F, 104, 94, 1, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 196, -52.0F, -115.0F, 124.0F, 104, 92, 4, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 0, 0, -52.0F, -117.5F, 124.0F, 104, 3, 5, 0.0F, false));
        CrossBeams.cubeList.add(new ModelBox(CrossBeams, 82, 361, 40.0F, -8.0F, 108.0F, 7, 7, 21, 0.0F, false));

        WindowFrame = new RendererModel(this);
        WindowFrame.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(WindowFrame, -0.1745F, 0.0F, 0.0F);
        CrossBeams.addChild(WindowFrame);
        WindowFrame.cubeList.add(new ModelBox(WindowFrame, 0, 458, -56.0F, -119.6092F, -48.4767F, 8, 52, 4, 0.0F, false));
        WindowFrame.cubeList.add(new ModelBox(WindowFrame, 0, 458, 48.0F, -119.6092F, -48.4767F, 8, 52, 4, 0.0F, false));
        WindowFrame.cubeList.add(new ModelBox(WindowFrame, 0, 502, -48.0F, -119.6092F, -48.4767F, 96, 8, 4, 0.0F, false));

        Supports = new RendererModel(this);
        Supports.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Supports, -0.3491F, 0.0F, 0.0F);
        WindowFrame.addChild(Supports);
        Supports.cubeList.add(new ModelBox(Supports, 0, 410, 48.0F, -95.8159F, -86.462F, 8, 100, 4, 0.0F, false));
        Supports.cubeList.add(new ModelBox(Supports, 0, 410, -56.0F, -95.8159F, -86.462F, 8, 100, 4, 0.0F, false));

        Axles = new RendererModel(this);
        Axles.setRotationPoint(0.0F, 24.0F, 0.0F);

        front = new RendererModel(this);
        front.setRotationPoint(0.0F, -9.0F, -90.0F);
        Axles.addChild(front);
        front.cubeList.add(new ModelBox(front, 0, 0, -64.0F, -2.0F, -2.0F, 128, 4, 4, 0.0F, false));

        Wheel_FrontRight = new RendererModel(this);
        Wheel_FrontRight.setRotationPoint(63.0F, 0.0F, 0.0F);
        front.addChild(Wheel_FrontRight);

        south2 = new RendererModel(this);
        south2.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontRight.addChild(south2);
        south2.cubeList.add(new ModelBox(south2, 0, 180, -4.0F, 1.0F, -4.0F, 4, 20, 8, 0.0F, false));
        south2.cubeList.add(new ModelBox(south2, 0, 187, -4.0F, -4.0F, -4.0F, 4, 9, 8, 0.0F, false));
        south2.cubeList.add(new ModelBox(south2, 0, 0, -4.0F, 21.0F, -12.0F, 6, 4, 24, 0.0F, false));

        east2 = new RendererModel(this);
        east2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(east2, 1.5708F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(east2);
        east2.cubeList.add(new ModelBox(east2, 0, 180, -4.0F, 4.9706F, -0.0294F, 4, 20, 8, 0.0F, false));
        east2.cubeList.add(new ModelBox(east2, 0, 185, -4.0F, -0.0294F, -0.0294F, 4, 9, 8, 0.0F, false));
        east2.cubeList.add(new ModelBox(east2, 0, 0, -4.0F, 24.9706F, -8.0294F, 6, 4, 24, 0.0F, false));

        north_east2 = new RendererModel(this);
        north_east2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_east2, 2.3562F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(north_east2);
        north_east2.cubeList.add(new ModelBox(north_east2, 0, 180, -4.0F, 7.7782F, -1.1924F, 4, 20, 8, 0.0F, false));
        north_east2.cubeList.add(new ModelBox(north_east2, 0, 187, -4.0F, 3.7782F, -1.1924F, 4, 8, 8, 0.0F, false));
        north_east2.cubeList.add(new ModelBox(north_east2, 0, 0, -4.0F, 27.7782F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_east2 = new RendererModel(this);
        south_east2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_east2, 0.7854F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(south_east2);
        south_east2.cubeList.add(new ModelBox(south_east2, 0, 180, -4.0F, 2.163F, -1.1924F, 4, 20, 8, 0.0F, false));
        south_east2.cubeList.add(new ModelBox(south_east2, 0, 187, -4.0F, 0.163F, -1.1924F, 4, 6, 8, 0.0F, false));
        south_east2.cubeList.add(new ModelBox(south_east2, 0, 0, -4.0F, 22.163F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_west2 = new RendererModel(this);
        south_west2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_west2, -0.7854F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(south_west2);
        south_west2.cubeList.add(new ModelBox(south_west2, 0, 180, -4.0F, 2.163F, -6.8076F, 4, 20, 8, 0.0F, false));
        south_west2.cubeList.add(new ModelBox(south_west2, 572, 1725, -4.0F, 0.163F, -6.8076F, 4, 6, 8, 0.0F, false));
        south_west2.cubeList.add(new ModelBox(south_west2, 0, 0, -4.0F, 22.163F, -14.8076F, 6, 4, 24, 0.0F, false));

        west2 = new RendererModel(this);
        west2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(west2, -1.5708F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(west2);
        west2.cubeList.add(new ModelBox(west2, 0, 180, -4.0F, 4.9706F, -7.9706F, 4, 20, 8, 0.0F, false));
        west2.cubeList.add(new ModelBox(west2, 0, 187, -4.0F, -0.0294F, -7.9706F, 4, 9, 8, 0.0F, false));
        west2.cubeList.add(new ModelBox(west2, 0, 0, -4.0F, 24.9706F, -15.9706F, 6, 4, 24, 0.0F, false));

        north_west2 = new RendererModel(this);
        north_west2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_west2, -2.3562F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(north_west2);
        north_west2.cubeList.add(new ModelBox(north_west2, 0, 180, -4.0F, 7.7782F, -6.8076F, 4, 20, 8, 0.0F, false));
        north_west2.cubeList.add(new ModelBox(north_west2, 0, 187, -4.0F, 3.7782F, -6.8076F, 4, 8, 8, 0.0F, false));
        north_west2.cubeList.add(new ModelBox(north_west2, 0, 0, -4.0F, 27.7782F, -14.8076F, 6, 4, 24, 0.0F, false));

        north2 = new RendererModel(this);
        north2.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north2, 3.1416F, 0.0F, 0.0F);
        Wheel_FrontRight.addChild(north2);
        north2.cubeList.add(new ModelBox(north2, 0, 180, -4.0F, 8.9411F, -4.0F, 4, 20, 8, 0.0F, false));
        north2.cubeList.add(new ModelBox(north2, 0, 178, -4.0F, 3.9411F, -4.0F, 4, 9, 8, 0.0F, false));
        north2.cubeList.add(new ModelBox(north2, 0, 0, -4.0F, 28.9411F, -12.0F, 6, 4, 24, 0.0F, false));

        Wheel_FrontLeft = new RendererModel(this);
        Wheel_FrontLeft.setRotationPoint(-63.0F, 0.0F, 0.0F);
        front.addChild(Wheel_FrontLeft);

        south4 = new RendererModel(this);
        south4.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_FrontLeft.addChild(south4);
        south4.cubeList.add(new ModelBox(south4, 0, 182, 0.0F, 1.0F, -4.0F, 4, 20, 8, 0.0F, false));
        south4.cubeList.add(new ModelBox(south4, 0, 178, 0.0F, -4.0F, -4.0F, 4, 9, 8, 0.0F, false));
        south4.cubeList.add(new ModelBox(south4, 0, 0, -2.0F, 21.0F, -12.0F, 6, 4, 24, 0.0F, false));

        west4 = new RendererModel(this);
        west4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(west4, 1.5708F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(west4);
        west4.cubeList.add(new ModelBox(west4, 0, 182, 0.0F, 4.9706F, -0.0294F, 4, 20, 8, 0.0F, false));
        west4.cubeList.add(new ModelBox(west4, 0, 187, 0.0F, -0.0294F, -0.0294F, 4, 9, 8, 0.0F, false));
        west4.cubeList.add(new ModelBox(west4, 0, 0, -2.0F, 24.9706F, -8.0294F, 6, 4, 24, 0.0F, false));

        north_west4 = new RendererModel(this);
        north_west4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_west4, 2.3562F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(north_west4);
        north_west4.cubeList.add(new ModelBox(north_west4, 0, 182, 0.0F, 7.7782F, -1.1924F, 4, 20, 8, 0.0F, false));
        north_west4.cubeList.add(new ModelBox(north_west4, 0, 182, 0.0F, 3.7782F, -1.1924F, 4, 8, 8, 0.0F, false));
        north_west4.cubeList.add(new ModelBox(north_west4, 0, 0, -2.0F, 27.7782F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_west4 = new RendererModel(this);
        south_west4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_west4, 0.7854F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(south_west4);
        south_west4.cubeList.add(new ModelBox(south_west4, 0, 182, 0.0F, 2.163F, -1.1924F, 4, 20, 8, 0.0F, false));
        south_west4.cubeList.add(new ModelBox(south_west4, 0, 182, 0.0F, 0.163F, -1.1924F, 4, 6, 8, 0.0F, false));
        south_west4.cubeList.add(new ModelBox(south_west4, 0, 0, -2.0F, 22.163F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_east4 = new RendererModel(this);
        south_east4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_east4, -0.7854F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(south_east4);
        south_east4.cubeList.add(new ModelBox(south_east4, 0, 182, 0.0F, 2.163F, -6.8076F, 4, 20, 8, 0.0F, false));
        south_east4.cubeList.add(new ModelBox(south_east4, 0, 182, 0.0F, 0.163F, -6.8076F, 4, 6, 8, 0.0F, false));
        south_east4.cubeList.add(new ModelBox(south_east4, 0, 0, -2.0F, 22.163F, -14.8076F, 6, 4, 24, 0.0F, false));

        east4 = new RendererModel(this);
        east4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(east4, -1.5708F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(east4);
        east4.cubeList.add(new ModelBox(east4, 0, 182, 0.0F, 4.9706F, -7.9706F, 4, 20, 8, 0.0F, false));
        east4.cubeList.add(new ModelBox(east4, 0, 182, 0.0F, -0.0294F, -7.9706F, 4, 9, 8, 0.0F, false));
        east4.cubeList.add(new ModelBox(east4, 0, 0, -2.0F, 24.9706F, -15.9706F, 6, 4, 24, 0.0F, false));

        north_east4 = new RendererModel(this);
        north_east4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_east4, -2.3562F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(north_east4);
        north_east4.cubeList.add(new ModelBox(north_east4, 0, 182, 0.0F, 7.7782F, -6.8076F, 4, 20, 8, 0.0F, false));
        north_east4.cubeList.add(new ModelBox(north_east4, 0, 182, 0.0F, 3.7782F, -6.8076F, 4, 8, 8, 0.0F, false));
        north_east4.cubeList.add(new ModelBox(north_east4, 0, 0, -2.0F, 27.7782F, -14.8076F, 6, 4, 24, 0.0F, false));

        north4 = new RendererModel(this);
        north4.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north4, 3.1416F, 0.0F, 0.0F);
        Wheel_FrontLeft.addChild(north4);
        north4.cubeList.add(new ModelBox(north4, 0, 182, 0.0F, 8.9411F, -4.0F, 4, 20, 8, 0.0F, false));
        north4.cubeList.add(new ModelBox(north4, 0, 182, 0.0F, 3.9411F, -4.0F, 4, 9, 8, 0.0F, false));
        north4.cubeList.add(new ModelBox(north4, 0, 0, -2.0F, 28.9411F, -12.0F, 6, 4, 24, 0.0F, false));

        back = new RendererModel(this);
        back.setRotationPoint(0.0F, -9.0F, 94.0F);
        Axles.addChild(back);
        back.cubeList.add(new ModelBox(back, 0, 0, -64.0F, -2.0F, -2.0F, 128, 4, 4, 0.0F, false));

        Wheel_BackRight = new RendererModel(this);
        Wheel_BackRight.setRotationPoint(63.0F, 0.0F, 0.0F);
        back.addChild(Wheel_BackRight);

        south = new RendererModel(this);
        south.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackRight.addChild(south);
        south.cubeList.add(new ModelBox(south, 0, 180, -4.0F, 1.0F, -4.0F, 4, 20, 8, 0.0F, false));
        south.cubeList.add(new ModelBox(south, 0, 180, -4.0F, -4.0F, -4.0F, 4, 9, 8, 0.0F, false));
        south.cubeList.add(new ModelBox(south, 0, 0, -4.0F, 21.0F, -12.0F, 6, 4, 24, 0.0F, false));

        east = new RendererModel(this);
        east.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(east, 1.5708F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(east);
        east.cubeList.add(new ModelBox(east, 0, 180, -4.0F, 4.9706F, -0.0294F, 4, 20, 8, 0.0F, false));
        east.cubeList.add(new ModelBox(east, 0, 180, -4.0F, -0.0294F, -0.0294F, 4, 9, 8, 0.0F, false));
        east.cubeList.add(new ModelBox(east, 0, 0, -4.0F, 24.9706F, -8.0294F, 6, 4, 24, 0.0F, false));

        north_east = new RendererModel(this);
        north_east.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_east, 2.3562F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(north_east);
        north_east.cubeList.add(new ModelBox(north_east, 0, 180, -4.0F, 7.7782F, -1.1924F, 4, 20, 8, 0.0F, false));
        north_east.cubeList.add(new ModelBox(north_east, 0, 180, -4.0F, 3.7782F, -1.1924F, 4, 8, 8, 0.0F, false));
        north_east.cubeList.add(new ModelBox(north_east, 0, 0, -4.0F, 27.7782F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_east = new RendererModel(this);
        south_east.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_east, 0.7854F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(south_east);
        south_east.cubeList.add(new ModelBox(south_east, 0, 180, -4.0F, 2.163F, -1.1924F, 4, 20, 8, 0.0F, false));
        south_east.cubeList.add(new ModelBox(south_east, 596, 1725, -4.0F, 0.163F, -1.1924F, 4, 6, 8, 0.0F, false));
        south_east.cubeList.add(new ModelBox(south_east, 0, 0, -4.0F, 22.163F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_west = new RendererModel(this);
        south_west.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_west, -0.7854F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(south_west);
        south_west.cubeList.add(new ModelBox(south_west, 0, 180, -4.0F, 2.163F, -6.8076F, 4, 20, 8, 0.0F, false));
        south_west.cubeList.add(new ModelBox(south_west, 0, 180, -4.0F, 0.163F, -6.8076F, 4, 6, 8, 0.0F, false));
        south_west.cubeList.add(new ModelBox(south_west, 0, 0, -4.0F, 22.163F, -14.8076F, 6, 4, 24, 0.0F, false));

        west = new RendererModel(this);
        west.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(west, -1.5708F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(west);
        west.cubeList.add(new ModelBox(west, 0, 180, -4.0F, 4.9706F, -7.9706F, 4, 20, 8, 0.0F, false));
        west.cubeList.add(new ModelBox(west, 0, 194, -4.0F, -0.0294F, -7.9706F, 4, 9, 8, 0.0F, false));
        west.cubeList.add(new ModelBox(west, 0, 0, -4.0F, 24.9706F, -15.9706F, 6, 4, 24, 0.0F, false));

        north_west = new RendererModel(this);
        north_west.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_west, -2.3562F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(north_west);
        north_west.cubeList.add(new ModelBox(north_west, 0, 180, -4.0F, 7.7782F, -6.8076F, 4, 20, 8, 0.0F, false));
        north_west.cubeList.add(new ModelBox(north_west, 0, 180, -4.0F, 3.7782F, -6.8076F, 4, 8, 8, 0.0F, false));
        north_west.cubeList.add(new ModelBox(north_west, 0, 0, -4.0F, 27.7782F, -14.8076F, 6, 4, 24, 0.0F, false));

        north = new RendererModel(this);
        north.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north, 3.1416F, 0.0F, 0.0F);
        Wheel_BackRight.addChild(north);
        north.cubeList.add(new ModelBox(north, 0, 180, -4.0F, 8.9411F, -4.0F, 4, 20, 8, 0.0F, false));
        north.cubeList.add(new ModelBox(north, 0, 182, -4.0F, 3.9411F, -4.0F, 4, 9, 8, 0.0F, false));
        north.cubeList.add(new ModelBox(north, 0, 0, -4.0F, 28.9411F, -12.0F, 6, 4, 24, 0.0F, false));

        Wheel_BackLeft = new RendererModel(this);
        Wheel_BackLeft.setRotationPoint(-63.0F, 0.0F, 0.0F);
        back.addChild(Wheel_BackLeft);

        south3 = new RendererModel(this);
        south3.setRotationPoint(0.0F, 4.0F, 0.0F);
        Wheel_BackLeft.addChild(south3);
        south3.cubeList.add(new ModelBox(south3, 0, 180, 0.0F, 1.0F, -4.0F, 4, 20, 8, 0.0F, false));
        south3.cubeList.add(new ModelBox(south3, 0, 180, 0.0F, -4.0F, -4.0F, 4, 9, 8, 0.0F, false));
        south3.cubeList.add(new ModelBox(south3, 0, 0, -2.0F, 21.0F, -12.0F, 6, 4, 24, 0.0F, false));

        west3 = new RendererModel(this);
        west3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(west3, 1.5708F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(west3);
        west3.cubeList.add(new ModelBox(west3, 0, 180, 0.0F, 4.9706F, -0.0294F, 4, 20, 8, 0.0F, false));
        west3.cubeList.add(new ModelBox(west3, 0, 180, 0.0F, -0.0294F, -0.0294F, 4, 9, 8, 0.0F, false));
        west3.cubeList.add(new ModelBox(west3, 0, 0, -2.0F, 24.9706F, -8.0294F, 6, 4, 24, 0.0F, false));

        north_west3 = new RendererModel(this);
        north_west3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_west3, 2.3562F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(north_west3);
        north_west3.cubeList.add(new ModelBox(north_west3, 0, 180, 0.0F, 7.7782F, -1.1924F, 4, 20, 8, 0.0F, false));
        north_west3.cubeList.add(new ModelBox(north_west3, 0, 180, 0.0F, 3.7782F, -1.1924F, 4, 8, 8, 0.0F, false));
        north_west3.cubeList.add(new ModelBox(north_west3, 0, 0, -2.0F, 27.7782F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_west3 = new RendererModel(this);
        south_west3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_west3, 0.7854F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(south_west3);
        south_west3.cubeList.add(new ModelBox(south_west3, 0, 180, 0.0F, 2.163F, -1.1924F, 4, 20, 8, 0.0F, false));
        south_west3.cubeList.add(new ModelBox(south_west3, 500, 1725, 0.0F, 0.163F, -1.1924F, 4, 6, 8, 0.0F, false));
        south_west3.cubeList.add(new ModelBox(south_west3, 0, 0, -2.0F, 22.163F, -9.1924F, 6, 4, 24, 0.0F, false));

        south_east3 = new RendererModel(this);
        south_east3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_east3, -0.7854F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(south_east3);
        south_east3.cubeList.add(new ModelBox(south_east3, 0, 180, 0.0F, 2.163F, -6.8076F, 4, 20, 8, 0.0F, false));
        south_east3.cubeList.add(new ModelBox(south_east3, 0, 180, 0.0F, 0.163F, -6.8076F, 4, 6, 8, 0.0F, false));
        south_east3.cubeList.add(new ModelBox(south_east3, 0, 0, -2.0F, 22.163F, -14.8076F, 6, 4, 24, 0.0F, false));

        east3 = new RendererModel(this);
        east3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(east3, -1.5708F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(east3);
        east3.cubeList.add(new ModelBox(east3, 0, 180, 0.0F, 4.9706F, -7.9706F, 4, 20, 8, 0.0F, false));
        east3.cubeList.add(new ModelBox(east3, 0, 180, 0.0F, -0.0294F, -7.9706F, 4, 9, 8, 0.0F, false));
        east3.cubeList.add(new ModelBox(east3, 0, 0, -2.0F, 24.9706F, -15.9706F, 6, 4, 24, 0.0F, false));

        north_east3 = new RendererModel(this);
        north_east3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_east3, -2.3562F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(north_east3);
        north_east3.cubeList.add(new ModelBox(north_east3, 0, 180, 0.0F, 7.7782F, -6.8076F, 4, 20, 8, 0.0F, false));
        north_east3.cubeList.add(new ModelBox(north_east3, 0, 185, 0.0F, 3.7782F, -6.8076F, 4, 8, 8, 0.0F, false));
        north_east3.cubeList.add(new ModelBox(north_east3, 0, 0, -2.0F, 27.7782F, -14.8076F, 6, 4, 24, 0.0F, false));

        north3 = new RendererModel(this);
        north3.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north3, 3.1416F, 0.0F, 0.0F);
        Wheel_BackLeft.addChild(north3);
        north3.cubeList.add(new ModelBox(north3, 0, 180, 0.0F, 8.9411F, -4.0F, 4, 20, 8, 0.0F, false));
        north3.cubeList.add(new ModelBox(north3, 2, 176, 0.0F, 3.9411F, -4.0F, 4, 9, 8, 0.0F, false));
        north3.cubeList.add(new ModelBox(north3, 0, 0, -2.0F, 28.9411F, -12.0F, 6, 4, 24, 0.0F, false));

        Spare_Wheel = new RendererModel(this);
        Spare_Wheel.setRotationPoint(-3.0F, -46.0F, 132.0F);
        setRotationAngle(Spare_Wheel, 0.0F, 1.5708F, 0.0F);

        south5 = new RendererModel(this);
        south5.setRotationPoint(0.0F, 4.0F, 0.0F);
        Spare_Wheel.addChild(south5);
        south5.cubeList.add(new ModelBox(south5, 434, 114, 0.0F, 1.0F, -1.0F, 4, 20, 8, 0.0F, false));
        south5.cubeList.add(new ModelBox(south5, 434, 114, 0.0F, 1.0F, -1.0F, 4, 20, 8, 0.0F, false));
        south5.cubeList.add(new ModelBox(south5, 0, 180, 0.0F, -4.0F, -1.0F, 4, 9, 8, 0.0F, true));
        south5.cubeList.add(new ModelBox(south5, 0, 180, 0.0F, -4.0F, -1.0F, 4, 9, 8, 0.0F, true));
        south5.cubeList.add(new ModelBox(south5, 0, 0, -2.0F, 21.0F, -9.0F, 6, 4, 24, 0.0F, false));
        south5.cubeList.add(new ModelBox(south5, 0, 0, -2.0F, 21.0F, -9.0F, 6, 4, 24, 0.0F, false));

        west5 = new RendererModel(this);
        west5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(west5, 1.5708F, 0.0F, 0.0F);
        Spare_Wheel.addChild(west5);
        west5.cubeList.add(new ModelBox(west5, 390, 160, 0.0F, 7.9706F, -0.0294F, 4, 20, 8, 0.0F, false));
        west5.cubeList.add(new ModelBox(west5, 390, 160, 0.0F, 7.9706F, -0.0294F, 4, 20, 8, 0.0F, false));
        west5.cubeList.add(new ModelBox(west5, 0, 180, 0.0F, 2.9706F, -0.0294F, 4, 9, 8, 0.0F, true));
        west5.cubeList.add(new ModelBox(west5, 0, 180, 0.0F, 2.9706F, -0.0294F, 4, 9, 8, 0.0F, true));
        west5.cubeList.add(new ModelBox(west5, 0, 0, -2.0F, 27.9706F, -8.0294F, 6, 4, 24, 0.0F, false));
        west5.cubeList.add(new ModelBox(west5, 0, 0, -2.0F, 27.9706F, -8.0294F, 6, 4, 24, 0.0F, false));

        north_west5 = new RendererModel(this);
        north_west5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_west5, 2.3562F, 0.0F, 0.0F);
        Spare_Wheel.addChild(north_west5);
        north_west5.cubeList.add(new ModelBox(north_west5, 477, 114, 0.0F, 9.8995F, -3.3137F, 4, 20, 8, 0.0F, false));
        north_west5.cubeList.add(new ModelBox(north_west5, 477, 114, 0.0F, 9.8995F, -3.3137F, 4, 20, 8, 0.0F, false));
        north_west5.cubeList.add(new ModelBox(north_west5, 0, 180, 0.0F, 5.8995F, -3.3137F, 4, 8, 8, 0.0F, true));
        north_west5.cubeList.add(new ModelBox(north_west5, 0, 180, 0.0F, 5.8995F, -3.3137F, 4, 8, 8, 0.0F, true));
        north_west5.cubeList.add(new ModelBox(north_west5, 0, 0, -2.0F, 29.8995F, -11.3137F, 6, 4, 24, 0.0F, false));
        north_west5.cubeList.add(new ModelBox(north_west5, 0, 0, -2.0F, 29.8995F, -11.3137F, 6, 4, 24, 0.0F, false));

        south_west5 = new RendererModel(this);
        south_west5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_west5, 0.7854F, 0.0F, 0.0F);
        Spare_Wheel.addChild(south_west5);
        south_west5.cubeList.add(new ModelBox(south_west5, 395, 115, 0.0F, 4.2843F, 0.9289F, 4, 20, 8, 0.0F, false));
        south_west5.cubeList.add(new ModelBox(south_west5, 395, 115, 0.0F, 4.2843F, 0.9289F, 4, 20, 8, 0.0F, false));
        south_west5.cubeList.add(new ModelBox(south_west5, 452, 1725, 0.0F, 2.2843F, 0.9289F, 4, 6, 8, 0.0F, false));
        south_west5.cubeList.add(new ModelBox(south_west5, 452, 1725, 0.0F, 2.2843F, 0.9289F, 4, 6, 8, 0.0F, false));
        south_west5.cubeList.add(new ModelBox(south_west5, 0, 0, -2.0F, 24.2843F, -7.0711F, 6, 4, 24, 0.0F, false));
        south_west5.cubeList.add(new ModelBox(south_west5, 0, 0, -2.0F, 24.2843F, -7.0711F, 6, 4, 24, 0.0F, false));

        south_east5 = new RendererModel(this);
        south_east5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(south_east5, -0.7854F, 0.0F, 0.0F);
        Spare_Wheel.addChild(south_east5);
        south_east5.cubeList.add(new ModelBox(south_east5, 477, 114, 0.0F, 0.0416F, -4.6863F, 4, 20, 8, 0.0F, false));
        south_east5.cubeList.add(new ModelBox(south_east5, 477, 114, 0.0F, 0.0416F, -4.6863F, 4, 20, 8, 0.0F, false));
        south_east5.cubeList.add(new ModelBox(south_east5, 0, 180, 0.0F, -1.9584F, -4.6863F, 4, 6, 8, 0.0F, true));
        south_east5.cubeList.add(new ModelBox(south_east5, 0, 180, 0.0F, -1.9584F, -4.6863F, 4, 6, 8, 0.0F, true));
        south_east5.cubeList.add(new ModelBox(south_east5, 0, 0, -2.0F, 20.0416F, -12.6863F, 6, 4, 24, 0.0F, false));
        south_east5.cubeList.add(new ModelBox(south_east5, 0, 0, -2.0F, 20.0416F, -12.6863F, 6, 4, 24, 0.0F, false));

        east5 = new RendererModel(this);
        east5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(east5, -1.5708F, 0.0F, 0.0F);
        Spare_Wheel.addChild(east5);
        east5.cubeList.add(new ModelBox(east5, 390, 160, 0.0F, 1.9706F, -7.9706F, 4, 20, 8, 0.0F, false));
        east5.cubeList.add(new ModelBox(east5, 390, 160, 0.0F, 1.9706F, -7.9706F, 4, 20, 8, 0.0F, false));
        east5.cubeList.add(new ModelBox(east5, 0, 178, 0.0F, -3.0294F, -7.9706F, 4, 9, 8, 0.0F, false));
        east5.cubeList.add(new ModelBox(east5, 0, 178, 0.0F, -3.0294F, -7.9706F, 4, 9, 8, 0.0F, false));
        east5.cubeList.add(new ModelBox(east5, 0, 0, -2.0F, 21.9706F, -15.9706F, 6, 4, 24, 0.0F, false));
        east5.cubeList.add(new ModelBox(east5, 0, 0, -2.0F, 21.9706F, -15.9706F, 6, 4, 24, 0.0F, false));

        north_east5 = new RendererModel(this);
        north_east5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north_east5, -2.3562F, 0.0F, 0.0F);
        Spare_Wheel.addChild(north_east5);
        north_east5.cubeList.add(new ModelBox(north_east5, 395, 115, 0.0F, 5.6569F, -8.9289F, 4, 20, 8, 0.0F, false));
        north_east5.cubeList.add(new ModelBox(north_east5, 395, 115, 0.0F, 5.6569F, -8.9289F, 4, 20, 8, 0.0F, false));
        north_east5.cubeList.add(new ModelBox(north_east5, 0, 180, 0.0F, 1.6569F, -8.9289F, 4, 8, 8, 0.0F, true));
        north_east5.cubeList.add(new ModelBox(north_east5, 0, 180, 0.0F, 1.6569F, -8.9289F, 4, 8, 8, 0.0F, true));
        north_east5.cubeList.add(new ModelBox(north_east5, 0, 0, -2.0F, 25.6569F, -16.9289F, 6, 4, 24, 0.0F, false));
        north_east5.cubeList.add(new ModelBox(north_east5, 0, 0, -2.0F, 25.6569F, -16.9289F, 6, 4, 24, 0.0F, false));

        north5 = new RendererModel(this);
        north5.setRotationPoint(0.0F, 4.0F, 0.0F);
        setRotationAngle(north5, 3.1416F, 0.0F, 0.0F);
        Spare_Wheel.addChild(north5);
        north5.cubeList.add(new ModelBox(north5, 434, 114, 0.0F, 8.9411F, -7.0F, 4, 20, 8, 0.0F, false));
        north5.cubeList.add(new ModelBox(north5, 434, 114, 0.0F, 8.9411F, -7.0F, 4, 20, 8, 0.0F, false));
        north5.cubeList.add(new ModelBox(north5, 0, 180, 0.0F, 3.9411F, -7.0F, 4, 9, 8, 0.0F, false));
        north5.cubeList.add(new ModelBox(north5, 0, 180, 0.0F, 3.9411F, -7.0F, 4, 9, 8, 0.0F, false));
        north5.cubeList.add(new ModelBox(north5, 0, 0, -2.0F, 28.9411F, -15.0F, 6, 4, 24, 0.0F, false));
        north5.cubeList.add(new ModelBox(north5, 0, 0, -2.0F, 28.9411F, -15.0F, 6, 4, 24, 0.0F, false));

        Headlights = new RendererModel(this);
        Headlights.setRotationPoint(0.0F, 24.0F, 0.0F);

        Right = new RendererModel(this);
        Right.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Right, 0.0F, 0.0F, -0.0873F);
        Headlights.addChild(Right);
        Right.cubeList.add(new ModelBox(Right, 0, 0, -33.0F, -38.0F, -144.0F, 2, 35, 2, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -37.0F, -48.0F, -143.0F, 10, 10, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -37.0F, -48.0F, -144.0F, 1, 10, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -36.0F, -39.0F, -144.0F, 8, 1, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -36.0F, -48.0F, -144.0F, 8, 1, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -28.0F, -48.0F, -144.0F, 1, 10, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -37.8097F, -73.1663F, -37.0F, 10, 10, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -37.8097F, -73.1663F, -38.0F, 1, 10, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -37.3097F, -64.6663F, -38.5F, 9, 2, 2, -0.5F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -36.8097F, -73.1663F, -38.0F, 8, 1, 1, 0.0F, false));
        Right.cubeList.add(new ModelBox(Right, 0, 352, -28.8097F, -73.1663F, -38.0F, 1, 10, 1, 0.0F, false));

        Left = new RendererModel(this);
        Left.setRotationPoint(0.0F, 0.0F, 0.0F);
        setRotationAngle(Left, 0.0F, 0.0F, 0.0873F);
        Headlights.addChild(Left);
        Left.cubeList.add(new ModelBox(Left, 0, 0, 31.0F, -38.0F, -144.0F, 2, 35, 2, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 36.0F, -48.0F, -144.0F, 1, 10, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 28.0F, -39.0F, -144.0F, 8, 1, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 28.0F, -48.0F, -144.0F, 8, 1, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 27.0F, -48.0F, -144.0F, 1, 10, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 27.0F, -48.0F, -143.0F, 10, 10, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 24.8211F, -72.9049F, -37.0F, 10, 10, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 33.8211F, -72.9049F, -38.0F, 1, 10, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 25.3211F, -64.4049F, -38.5F, 9, 2, 2, -0.5F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 25.8211F, -72.9049F, -38.0F, 8, 1, 1, 0.0F, false));
        Left.cubeList.add(new ModelBox(Left, 0, 352, 24.8211F, -72.9049F, -38.0F, 1, 10, 1, 0.0F, false));

        light_fright = new EntityGlowRenderer(this);
        light_fright.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(light_fright, 0.0F, 0.0F, -0.0873F);
        light_fright.cubeList.add(new ModelBox(light_fright, 0, 350, -34.0F, -45.0F, -145.0F, 4, 4, 1, 0.0F, false));
        light_fright.cubeList.add(new ModelBox(light_fright, 0, 350, -35.0F, -46.0F, -144.0F, 6, 6, 1, 0.0F, false));

        light_fleft = new EntityGlowRenderer(this);
        light_fleft.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(light_fleft, 0.0F, 0.0F, 0.0873F);
        light_fleft.cubeList.add(new ModelBox(light_fleft, 0, 350, 30.0F, -45.0F, -145.0F, 4, 4, 1, 0.0F, false));
        light_fleft.cubeList.add(new ModelBox(light_fleft, 0, 350, 29.0F, -46.0F, -144.0F, 6, 6, 1, 0.0F, false));

        light_frightlantern = new EntityGlowRenderer(this);
        light_frightlantern.setRotationPoint(-3.0F, -1.0F, 106.0F);
        setRotationAngle(light_frightlantern, 0.0F, 0.0F, -0.0873F);
        light_frightlantern.cubeList.add(new ModelBox(light_frightlantern, 0, 350, -34.0F, -45.0F, -145.0F, 4, 4, 1, 0.0F, false));
        light_frightlantern.cubeList.add(new ModelBox(light_frightlantern, 0, 350, -35.0F, -46.0F, -144.0F, 6, 6, 1, 0.0F, false));

        light_fleftlantern = new EntityGlowRenderer(this);
        light_fleftlantern.setRotationPoint(0.0F, -1.0F, 106.0F);
        setRotationAngle(light_fleftlantern, 0.0F, 0.0F, 0.0873F);
        light_fleftlantern.cubeList.add(new ModelBox(light_fleftlantern, 0, 350, 30.0F, -45.0F, -145.0F, 4, 4, 1, 0.0F, false));
        light_fleftlantern.cubeList.add(new ModelBox(light_fleftlantern, 0, 350, 29.0F, -46.0F, -144.0F, 6, 6, 1, 0.0F, false));

        light_rleft = new EntityGlowRenderer(this);
        light_rleft.setRotationPoint(0.0F, -1.0F, 106.0F);
        setRotationAngle(light_rleft, 0.0F, 0.0F, 0.0873F);
        light_rleft.cubeList.add(new ModelBox(light_rleft, 6, 386, 39.9207F, -12.9134F, 23.0F, 1, 10, 1, 0.0F, false));
        light_rleft.cubeList.add(new ModelBox(light_rleft, 6, 386, 40.9207F, -12.9134F, 23.0F, 8, 1, 1, 0.0F, false));
        light_rleft.cubeList.add(new ModelBox(light_rleft, 6, 386, 48.9207F, -12.9134F, 23.0F, 1, 10, 1, 0.0F, false));
        light_rleft.cubeList.add(new ModelBox(light_rleft, 0, 386, 39.9207F, -12.9134F, 22.0F, 10, 10, 1, 0.0F, false));
        light_rleft.cubeList.add(new ModelBox(light_rleft, 6, 386, 40.9207F, -3.9134F, 23.0F, 8, 1, 1, 0.0F, false));
        light_rleft.cubeList.add(new ModelBox(light_rleft, 11, 390, 43.0996F, -10.0085F, 24.0F, 4, 4, 1, 0.0F, false));
        light_rleft.cubeList.add(new ModelBox(light_rleft, 11, 390, 42.0996F, -11.0085F, 23.0F, 6, 6, 1, 0.0F, false));

        light_rright = new EntityGlowRenderer(this);
        light_rright.setRotationPoint(-10.0F, 35.0F, -14.0F);
        setRotationAngle(light_rright, 0.0F, 0.0F, -0.0873F);
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 393, -34.0F, -45.0F, 144.0F, 4, 4, 1, 0.0F, false));
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 393, -35.0F, -46.0F, 143.0F, 6, 6, 1, 0.0F, false));
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 393, -36.9207F, -47.9134F, 142.0F, 10, 10, 1, 0.0F, false));
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 384, -27.9207F, -47.9134F, 143.0F, 1, 10, 1, 0.0F, false));
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 384, -35.9207F, -47.9134F, 143.0F, 8, 1, 1, 0.0F, false));
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 384, -36.9207F, -47.9134F, 143.0F, 1, 10, 1, 0.0F, false));
        light_rright.cubeList.add(new ModelBox(light_rright, 0, 384, -36.4207F, -39.4134F, 142.5F, 9, 2, 2, -0.5F, false));

        light_bumperRearLeft = new EntityGlowRenderer(this);
        light_bumperRearLeft.setRotationPoint(-10.0F, 35.0F, -14.0F);
        light_bumperRearLeft.cubeList.add(new ModelBox(light_bumperRearLeft, 4, 384, 49.0F, -33.0F, 146.0F, 8, 5, 2, 0.0F, false));
        light_bumperRearLeft.cubeList.add(new ModelBox(light_bumperRearLeft, 38, 384, 57.0F, -33.0F, 146.0F, 5, 5, 2, 0.0F, false));

        light_bumperRearRight = new EntityGlowRenderer(this);
        light_bumperRearRight.setRotationPoint(-10.0F, 35.0F, -14.0F);
        light_bumperRearRight.cubeList.add(new ModelBox(light_bumperRearRight, 4, 384, -37.0F, -33.0F, 146.0F, 8, 5, 2, 0.0F, false));
        light_bumperRearRight.cubeList.add(new ModelBox(light_bumperRearRight, 38, 384, -42.0F, -33.0F, 146.0F, 5, 5, 2, 0.0F, false));

        Steering = new RendererModel(this);
        Steering.setRotationPoint(0.0F, 24.0F, 0.0F);
        setRotationAngle(Steering, -0.0873F, 0.0F, 0.0F);

        steering_assembly_whole = new RendererModel(this);
        steering_assembly_whole.setRotationPoint(-30.0F, -65.4412F, -21.0F);
        setRotationAngle(steering_assembly_whole, 0.0F, -3.0543F, 0.0F);
        Steering.addChild(steering_assembly_whole);

        steering_stick = new RendererModel(this);
        steering_stick.setRotationPoint(0.0F, 51.4412F, 0.0F);
        steering_assembly_whole.addChild(steering_stick);
        steering_stick.cubeList.add(new ModelBox(steering_stick, 0, 0, -2.0F, -55.0F, -2.0F, 4, 57, 4, 0.0F, false));

        steering_wheel = new RendererModel(this);
        steering_wheel.setRotationPoint(0.0F, -0.5588F, 0.0F);
        setRotationAngle(steering_wheel, 0.0F, 0.7854F, 0.0F);
        steering_assembly_whole.addChild(steering_wheel);
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -2.0F, -3.0F, 2.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -11.0F, -3.0F, 2.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -12.0F, -3.0F, -4.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, 8.0F, -3.0F, -4.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -11.0F, -3.0F, -10.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, 7.0F, -3.0F, -10.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, 7.0F, -3.0F, 2.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -2.0F, -3.0F, -10.0F, 4, 4, 8, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -10.0F, -3.0F, 7.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -4.0F, -3.0F, 8.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -4.0F, -3.0F, -12.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, 2.0F, -3.0F, 7.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, 2.0F, -3.0F, -11.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -10.0F, -3.0F, -11.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, -10.0F, -3.0F, -2.0F, 8, 4, 4, 0.0F, false));
        steering_wheel.cubeList.add(new ModelBox(steering_wheel, 0, 0, 2.0F, -3.0F, -2.0F, 8, 4, 4, 0.0F, false));

        RearLicensPlate = new RendererModel(this);
        RearLicensPlate.setRotationPoint(0.0F, 8.0F, -16.0F);
        RearLicensPlate.cubeList.add(new ModelBox(RearLicensPlate, 0, 0, 15.0F, -3.0F, 148.0F, 1, 20, 2, 0.0F, false));
        RearLicensPlate.cubeList.add(new ModelBox(RearLicensPlate, 0, 0, -16.0F, -3.0F, 148.0F, 1, 20, 2, 0.0F, false));
        RearLicensPlate.cubeList.add(new ModelBox(RearLicensPlate, 0, 0, -15.0F, 16.0F, 148.0F, 30, 1, 2, 0.0F, false));
        RearLicensPlate.cubeList.add(new ModelBox(RearLicensPlate, 0, 0, -15.0F, -3.0F, 148.0F, 30, 1, 2, 0.0F, false));
        RearLicensPlate.cubeList.add(new ModelBox(RearLicensPlate, 0, 0, -15.0F, -2.0F, 148.0F, 30, 18, 1, 0.0F, false));

        LIGHTME = new EntityGlowRenderer(this);
        LIGHTME.setRotationPoint(0.0F, 24.0F, 0.0F);
        LIGHTME.cubeList.add(new ModelBox(LIGHTME, 38, 386, 44.0F, -9.75F, -149.0F, 6, 6, 2, 0.0F, false));
        LIGHTME.cubeList.add(new ModelBox(LIGHTME, 38, 386, -50.0F, -9.75F, -149.0F, 6, 6, 2, 0.0F, false));
        addText();
    }

    @Override
    public void render(BessieEntity entity, float f, float f1, float f2, float f3, float f4, float f5) {

        setRotationAngles(entity, f, f1, f2, f3, f4, f5);
        GlStateManager.pushMatrix();
        GlStateManager.translated(0, 1, 0);
        GlStateManager.scalef(0.2f, 0.2f, 0.2f);
        bb_main.render(f5);
        Floor.render(f5);
        FrontLicensePlate.render(f5);
        RightWall.render(f5);
        LeftWall.render(f5);
        CrossBeams.render(f5);
        Axles.render(f5);
        Spare_Wheel.render(f5);
        Headlights.render(f5);
        light_fright.render(f5);
        light_fleft.render(f5);
        light_frightlantern.render(f5);
        light_fleftlantern.render(f5);
        light_rleft.render(f5);
        light_rright.render(f5);
        light_bumperRearLeft.render(f5);
        light_bumperRearRight.render(f5);
        Steering.render(f5);
        RearLicensPlate.render(f5);
        LIGHTME.render(f5);
        GlStateManager.popMatrix();

        //Back Plate
        GlStateManager.pushMatrix();
        RearLicensPlate.postRender(f5);
        GlStateManager.rotated(-90, 0, 1, 0);
        GlStateManager.translated(1.5, -0.5, -1);
        // renderAllText();
        GlStateManager.popMatrix();

        //Front Plate
        GlStateManager.pushMatrix();
        FrontLicensePlate.postRender(f5);
        GlStateManager.rotated(90, 0, 1, 0);
        GlStateManager.translated(-1.5, 0.5, 1);
        //renderAllText();
        GlStateManager.popMatrix();
    }

    public void setRotationAngle(RendererModel RendererModel, float x, float y, float z) {
        RendererModel.rotateAngleX = x;
        RendererModel.rotateAngleY = y;
        RendererModel.rotateAngleZ = z;
    }


    @Override
    public void setRotationAngles(BessieEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {


        float motion = MathHelper.sqrt(Entity.horizontalMag(entityIn.getMotion()));
        boolean isMoving = motion >= 0.01D;
        double xMotion = entityIn.getMotion().x;

        //Wheel Moving Rotation
        if (isMoving && entityIn.getHealth() > 5) {
            Wheel_FrontLeft.rotateAngleX = (float) (xMotion < 0.0001 ? Math.toRadians(entityIn.ticksExisted * 100 * motion) : -Math.toRadians(entityIn.ticksExisted * 100 * motion));
            Wheel_FrontRight.rotateAngleX = (float) (xMotion < 0.0001 ? Math.toRadians(entityIn.ticksExisted * 100 * motion) : -Math.toRadians(entityIn.ticksExisted * 100 * motion));
            back.rotateAngleX = (float) (xMotion < 0.0001 ? Math.toRadians(entityIn.ticksExisted * 100 * motion) : -Math.toRadians(entityIn.ticksExisted * 100 * motion));
        } else {
            Wheel_FrontLeft.rotateAngleX = 0;
            Wheel_FrontRight.rotateAngleX = 0;
            back.rotateAngleX = 0;
        }

        // Turn Steering wheel and wheel direction
        if (entityIn.isBeingRidden() && isMoving) {
            float wheelRot = MathHelper.clamp(entityIn.getRotationYawHead(), -22, 22);
            Wheel_FrontLeft.rotateAngleY = (float) Math.toRadians(wheelRot);
            Wheel_FrontRight.rotateAngleY = (float) Math.toRadians(wheelRot);
            steering_wheel.rotateAngleY = (float) Math.toRadians(entityIn.getRotationYawHead());
        } else {
            steering_wheel.rotateAngleY = (float) Math.toRadians(0);
            Wheel_FrontLeft.rotateAngleY = (float) Math.toRadians(0);
            Wheel_FrontRight.rotateAngleY = (float) Math.toRadians(0);
        }

        //Damaged Bessie rotations
        if (entityIn.getHealth() <= 5) {
            Wheel_FrontLeft.rotateAngleZ = (float) Math.toRadians(45);
            Wheel_FrontRight.rotateAngleZ = (float) Math.toRadians(-45);
            Wheel_BackLeft.rotateAngleZ = (float) Math.toRadians(45);
            Wheel_BackRight.rotateAngleZ = (float) Math.toRadians(-45);
        } else {
            Wheel_FrontLeft.rotateAngleZ = (float) Math.toRadians(0);
            Wheel_FrontRight.rotateAngleZ = (float) Math.toRadians(0);
            Wheel_BackLeft.rotateAngleZ = (float) Math.toRadians(0);
            Wheel_BackRight.rotateAngleZ = (float) Math.toRadians(0);
        }
    }

    @Override
    public void addText() {
        this.textToRender.add(new RenderText("Who-3").setFontColor(TextFormatting.WHITE).setPosition(0, 0).centerText().setSmall(false));
    }

    @Override
    public void renderAllText() {
        FontRenderer fr = (Minecraft.getInstance()).fontRenderer;
        float fontZOffset = -0.975F;
        GlStateManager.pushMatrix();
        GlStateManager.translated(0.0F, -0.86F, 0.0F);
        if (fr != null)
            GlStateManager.pushMatrix();
        GlStateManager.translated(0.0F, 0.0F, fontZOffset);
        GlStateManager.rotated(90, 0, 1, 0);
        GlStateManager.translated(0, 0, -0.2);
        for (RenderText textPiece : this.textToRender) {
            //textPiece.setSmall(false);
            new RenderText("Who-3").setFontColor(TextFormatting.WHITE).setPosition(0, 0).centerText().setSmall(false).renderText();
        }
        GlStateManager.popMatrix();
        GlStateManager.popMatrix();
    }
}