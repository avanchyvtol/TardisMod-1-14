package net.tardis.mod.client.models.misc;

import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;

/**
 * Created by Swirtzly
 * on 08/04/2020 @ 23:51
 */
public class EntityGlowRenderer extends RendererModel {
    public EntityGlowRenderer(Model model, String boxNameIn) {
        super(model, boxNameIn);
    }

    public EntityGlowRenderer(Model model) {
        super(model);
    }

    public EntityGlowRenderer(Model model, int texOffX, int texOffY) {
        super(model, texOffX, texOffY);
    }


    @Override
    public void render(float scale) {
        GlStateManager.enableBlend();
        GlStateManager.disableAlphaTest();
        GlStateManager.blendFunc(GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ONE);
        GlStateManager.disableLighting();
        //GlStateManager.depthMask(!entityIn.isInvisible());
        GLX.glMultiTexCoord2f(GLX.GL_TEXTURE1, 61680.0F, 0.0F);
        GlStateManager.enableLighting();
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        GameRenderer gamerenderer = Minecraft.getInstance().gameRenderer;
        gamerenderer.setupFogColor(true);
        super.render(scale);
        gamerenderer.setupFogColor(false);
        //this.func_215334_a(entityIn);
        // GlStateManager.depthMask(true);
        GlStateManager.disableBlend();
        GlStateManager.enableAlphaTest();
    }
}
