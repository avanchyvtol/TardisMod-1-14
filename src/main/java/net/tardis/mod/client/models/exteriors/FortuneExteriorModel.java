package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class FortuneExteriorModel extends Model {
	private final RendererModel glow_crystal;
	private final RendererModel glow_crystal1;
	private final RendererModel glow_crystal2;
	private final RendererModel glow_crystal3;
	private final RendererModel glow_crystal4;
	private final RendererModel glow_topper;
	private final RendererModel door;
	private final RendererModel latch;
	private final RendererModel boti;
	private final RendererModel box;
	private final RendererModel base;
	private final RendererModel detailing;
	private final RendererModel detailing2;
	private final RendererModel front;
	private final RendererModel front_panels;
	private final RendererModel front_brasstrim_tilt;
	private final RendererModel front_brasstrim;
	private final RendererModel lid;
	private final RendererModel posts;
	private final RendererModel nw_post;
	private final RendererModel sw_post;
	private final RendererModel ne_post;
	private final RendererModel se_post;
	private final RendererModel curtains;
	private final RendererModel curtains_left;
	private final RendererModel curtains_right;
	private final RendererModel curtains_back;
	private final RendererModel dias;
	private final RendererModel coinreturn;
	private final RendererModel button;

	public FortuneExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		glow_crystal = new RendererModel(this);
		glow_crystal.setRotationPoint(0.0F, -76.0F, -8.0F);
		setRotationAngle(glow_crystal, -0.6109F, -0.6109F, 0.4363F);

		glow_crystal1 = new RendererModel(this);
		glow_crystal1.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow_crystal.addChild(glow_crystal1);
		glow_crystal1.cubeList.add(new ModelBox(glow_crystal1, 200, 21, -8.0F, -8.0F, -8.0F, 16, 16, 16, 0.0F, false));

		glow_crystal2 = new RendererModel(this);
		glow_crystal2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(glow_crystal2, -0.7854F, -0.7854F, 0.0F);
		glow_crystal.addChild(glow_crystal2);
		glow_crystal2.cubeList.add(new ModelBox(glow_crystal2, 200, 21, -8.0F, -8.0F, -8.0F, 16, 16, 16, 0.0F, false));

		glow_crystal3 = new RendererModel(this);
		glow_crystal3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(glow_crystal3, -0.7854F, 0.0F, -0.7854F);
		glow_crystal.addChild(glow_crystal3);
		glow_crystal3.cubeList.add(new ModelBox(glow_crystal3, 218, 68, -4.0F, -4.0F, -4.0F, 8, 8, 8, 0.0F, false));

		glow_crystal4 = new RendererModel(this);
		glow_crystal4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(glow_crystal4, 0.1745F, 0.5236F, -0.1745F);
		glow_crystal.addChild(glow_crystal4);
		glow_crystal4.cubeList.add(new ModelBox(glow_crystal4, 218, 68, -4.0F, -4.0F, -4.0F, 8, 8, 8, 0.0F, false));

		glow_topper = new RendererModel(this);
		glow_topper.setRotationPoint(0.0F, 24.0F, 0.0F);
		glow_topper.cubeList.add(new ModelBox(glow_topper, 161, 54, -22.0F, -164.0F, -22.0F, 44, 4, 44, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(-24.0F, 24.0F, 28.0F);
		door.cubeList.add(new ModelBox(door, 253, 106, 40.0F, -154.0F, 0.0F, 8, 148, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 179, 110, 2.0F, -151.6F, 2.0F, 44, 144, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 256, 108, 0.0F, -154.0F, 0.0F, 8, 148, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 224, 241, 8.0F, -18.0F, 0.0F, 32, 12, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 292, 125, 8.0F, -154.0F, 0.0F, 32, 12, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 139, 105, 8.0F, -112.4F, 0.0F, 32, 4, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 139, 105, 8.0F, -54.0F, 0.0F, 32, 4, 4, 0.0F, false));

		latch = new RendererModel(this);
		latch.setRotationPoint(0.0F, 0.0F, 0.0F);
		door.addChild(latch);
		latch.cubeList.add(new ModelBox(latch, 166, 8, 42.0F, -88.0F, -2.0F, 4, 8, 8, 0.0F, false));
		latch.cubeList.add(new ModelBox(latch, 110, 16, 42.0F, -78.0F, 1.0F, 4, 4, 4, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 0, 96, -24.0F, -156.0F, 25.0F, 48, 152, 4, 0.0F, false));

		box = new RendererModel(this);
		box.setRotationPoint(0.0F, 24.0F, 0.0F);

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(base);
		base.cubeList.add(new ModelBox(base, 126, 180, -36.0F, -4.0F, -36.0F, 72, 4, 72, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 132, 182, -34.0F, -6.0F, -34.0F, 68, 4, 68, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 155, 191, -30.0F, -12.0F, -30.0F, 60, 8, 56, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 122, 111, -30.0F, -76.0F, -30.0F, 60, 4, 56, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 290, 52, -18.0F, -82.0F, -22.0F, 36, 8, 36, 0.0F, false));
		base.cubeList.add(new ModelBox(base, 129, 135, -28.0F, -72.0F, -28.0F, 56, 60, 56, 0.0F, false));

		detailing = new RendererModel(this);
		detailing.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(detailing);
		detailing.cubeList.add(new ModelBox(detailing, 108, 30, 25.0F, -66.0F, -20.0F, 4, 2, 40, 0.0F, false));
		detailing.cubeList.add(new ModelBox(detailing, 105, 25, 25.0F, -22.0F, -20.0F, 4, 2, 40, 0.0F, false));
		detailing.cubeList.add(new ModelBox(detailing, 150, 30, 25.0F, -64.0F, -20.0F, 4, 42, 2, 0.0F, false));
		detailing.cubeList.add(new ModelBox(detailing, 130, 25, 25.0F, -64.0F, 18.0F, 4, 42, 2, 0.0F, false));
		detailing.cubeList.add(new ModelBox(detailing, 198, 168, 25.0F, -62.0F, -16.0F, 4, 36, 32, 0.0F, false));
		detailing.cubeList.add(new ModelBox(detailing, 207, 189, 26.0F, -54.0F, -8.0F, 4, 20, 16, 0.0F, false));

		detailing2 = new RendererModel(this);
		detailing2.setRotationPoint(0.0F, 0.0F, 0.0F);
		base.addChild(detailing2);
		detailing2.cubeList.add(new ModelBox(detailing2, 108, 19, -29.0F, -66.0F, -20.0F, 4, 2, 40, 0.0F, false));
		detailing2.cubeList.add(new ModelBox(detailing2, 108, 38, -29.0F, -22.0F, -20.0F, 4, 2, 40, 0.0F, false));
		detailing2.cubeList.add(new ModelBox(detailing2, 158, 25, -29.0F, -64.0F, -20.0F, 4, 42, 2, 0.0F, false));
		detailing2.cubeList.add(new ModelBox(detailing2, 139, 19, -29.0F, -64.0F, 18.0F, 4, 42, 2, 0.0F, false));
		detailing2.cubeList.add(new ModelBox(detailing2, 205, 168, -29.0F, -62.0F, -16.0F, 4, 36, 32, 0.0F, false));
		detailing2.cubeList.add(new ModelBox(detailing2, 208, 190, -30.0F, -54.0F, -8.0F, 4, 20, 16, 0.0F, false));

		front = new RendererModel(this);
		front.setRotationPoint(-0.5F, -142.0F, -29.5F);
		box.addChild(front);

		front_panels = new RendererModel(this);
		front_panels.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(front_panels, 0.0F, 0.0F, 0.7854F);
		front.addChild(front_panels);
		front_panels.cubeList.add(new ModelBox(front_panels, 306, 151, -2.5F, -21.0F, 0.5F, 28, 17, 1, 0.0F, false));
		front_panels.cubeList.add(new ModelBox(front_panels, 264, 148, -2.5F, -4.0F, 0.5F, 8, 8, 1, 0.0F, false));
		front_panels.cubeList.add(new ModelBox(front_panels, 268, 141, -7.5F, 25.0F, 0.5F, 9, 9, 1, 0.0F, false));
		front_panels.cubeList.add(new ModelBox(front_panels, 268, 141, 60.5F, 25.0F, 0.5F, 4, 9, 1, 0.0F, false));
		front_panels.cubeList.add(new ModelBox(front_panels, 294, 148, 25.5F, -9.0F, 0.5F, 9, 9, 1, 0.0F, false));
		front_panels.cubeList.add(new ModelBox(front_panels, 294, 148, 25.5F, 60.0F, 0.5F, 9, 3, 1, 0.0F, false));
		front_panels.cubeList.add(new ModelBox(front_panels, 300, 161, -19.5F, -4.0F, 0.5F, 17, 29, 1, 0.0F, false));

		front_brasstrim_tilt = new RendererModel(this);
		front_brasstrim_tilt.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(front_brasstrim_tilt, 0.0F, 0.0F, 0.7854F);
		front.addChild(front_brasstrim_tilt);
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 6.5F, -4.0F, 0.0F, 18, 1, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, -2.5F, 4.0F, 0.0F, 8, 1, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, -2.5F, 24.0F, 0.0F, 5, 1, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 24.5F, -4.0F, 0.0F, 1, 5, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 5.5F, -4.0F, 0.0F, 1, 9, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 1.5F, 25.0F, 0.0F, 1, 11, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 59.75F, 25.0F, 0.0F, 1, 11, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, -2.5F, 5.0F, 0.0F, 1, 19, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 25.5F, 0.0F, 0.0F, 10, 1, 2, 0.0F, false));
		front_brasstrim_tilt.cubeList.add(new ModelBox(front_brasstrim_tilt, 130, 61, 25.5F, 59.0F, 0.0F, 10, 1, 2, 0.0F, false));

		front_brasstrim = new RendererModel(this);
		front_brasstrim.setRotationPoint(0.0F, 0.0F, 0.0F);
		front.addChild(front_brasstrim);
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 130, 61, -18.5F, 65.0F, 0.1F, 38, 1, 2, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 111, 69, -23.5F, 70.0F, 1.1F, 24, 2, 1, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 111, 69, -23.0F, 128.0F, 1.1F, 23, 2, 1, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 108, 13, -23.0F, 72.0F, 1.1F, 2, 56, 1, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 108, 13, 22.0F, 72.0F, 1.1F, 2, 56, 1, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 111, 69, 0.5F, 70.0F, 1.1F, 24, 2, 1, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 111, 69, 0.0F, 128.0F, 1.1F, 24, 2, 1, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 141, 27, -23.0F, 25.0F, 0.1F, 1, 37, 2, 0.0F, false));
		front_brasstrim.cubeList.add(new ModelBox(front_brasstrim, 141, 27, 23.25F, 24.75F, 0.1F, 1, 37, 2, 0.0F, false));

		lid = new RendererModel(this);
		lid.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(lid);
		lid.cubeList.add(new ModelBox(lid, 131, 155, -36.0F, -160.0F, -36.0F, 72, 4, 72, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 144, 168, -34.0F, -158.0F, -34.0F, 68, 4, 68, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 300, 155, 26.0F, -154.0F, -26.0F, 4, 8, 52, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 288, 158, -30.0F, -154.0F, -26.0F, 4, 8, 52, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 330, 136, -25.0F, -154.0F, -30.0F, 52, 8, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 21.0F, -164.0F, -25.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -26.0F, -166.0F, -26.0F, 52, 2, 52, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -25.0F, -164.0F, -25.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -25.0F, -164.0F, 21.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 21.0F, -164.0F, 21.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 5.0F, -164.0F, -25.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 13.0F, -164.0F, -25.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -9.0F, -164.0F, -25.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -17.0F, -164.0F, -25.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 21.0F, -164.0F, -17.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 21.0F, -164.0F, -9.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 21.0F, -164.0F, 12.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 21.0F, -164.0F, 3.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 13.0F, -164.0F, 21.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, 5.0F, -164.0F, 21.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -17.0F, -164.0F, 21.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -9.0F, -164.0F, 21.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -25.0F, -164.0F, 13.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -25.0F, -164.0F, 5.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -25.0F, -164.0F, -17.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -25.0F, -164.0F, -9.0F, 4, 4, 4, 0.0F, false));
		lid.cubeList.add(new ModelBox(lid, 122, 111, -26.0F, -154.0F, 26.0F, 52, 8, 4, 0.0F, false));

		posts = new RendererModel(this);
		posts.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(posts);

		nw_post = new RendererModel(this);
		nw_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(nw_post);
		nw_post.cubeList.add(new ModelBox(nw_post, 178, 236, 24.0F, -16.0F, -32.0F, 8, 12, 8, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 115, 118, 24.0F, -141.0F, -32.0F, 8, 120, 8, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 299, 118, 24.0F, -156.0F, -32.0F, 8, 12, 8, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 168, 231, 25.0F, -24.0F, -31.0F, 6, 8, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 115, 25.0F, -148.0F, -31.0F, 6, 8, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 135, 25.0F, -123.4F, -32.4F, 6, 100, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 135, 26.6F, -123.4F, -30.8F, 6, 100, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 115, 25.0F, -139.4F, -32.4F, 6, 14, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 115, 26.6F, -139.4F, -30.8F, 6, 14, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 115, 23.6F, -139.4F, -30.8F, 6, 14, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 131, 23.6F, -123.4F, -30.8F, 6, 100, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 135, 25.0F, -123.4F, -29.8F, 6, 100, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 160, 115, 25.0F, -139.4F, -29.8F, 6, 14, 6, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 190, 241, 23.0F, -8.0F, -33.0F, 10, 4, 10, 0.0F, false));
		nw_post.cubeList.add(new ModelBox(nw_post, 297, 118, 23.0F, -156.0F, -33.0F, 10, 4, 10, 0.0F, false));

		sw_post = new RendererModel(this);
		sw_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(sw_post);
		sw_post.cubeList.add(new ModelBox(sw_post, 169, 237, 24.0F, -16.0F, 24.0F, 8, 12, 8, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 119, 114, 24.0F, -141.0F, 24.0F, 8, 120, 8, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 290, 114, 24.0F, -156.0F, 24.0F, 8, 12, 8, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 268, 232, 23.0F, -24.0F, 25.0F, 8, 8, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 115, 23.0F, -148.0F, 25.0F, 8, 8, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 135, 25.0F, -123.4F, 23.6F, 6, 100, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 135, 26.6F, -123.4F, 25.2F, 6, 100, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 115, 25.0F, -139.4F, 23.6F, 6, 14, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 115, 26.6F, -139.4F, 25.2F, 6, 14, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 135, 25.0F, -123.4F, 26.2F, 6, 100, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 115, 25.0F, -139.4F, 26.2F, 6, 14, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 161, 240, 23.0F, -8.0F, 23.0F, 10, 4, 10, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 371, 114, 23.0F, -156.0F, 23.0F, 10, 4, 10, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 115, 23.6F, -139.4F, 25.2F, 6, 14, 6, 0.0F, false));
		sw_post.cubeList.add(new ModelBox(sw_post, 160, 115, 23.6F, -123.4F, 25.2F, 6, 100, 6, 0.0F, false));

		ne_post = new RendererModel(this);
		ne_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(ne_post);
		ne_post.cubeList.add(new ModelBox(ne_post, 176, 232, -32.0F, -16.0F, -32.0F, 8, 12, 8, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 112, 109, -32.0F, -141.0F, -32.0F, 8, 120, 8, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 328, 155, -32.0F, -156.0F, -32.0F, 8, 12, 8, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 157, 233, -31.0F, -24.0F, -31.0F, 6, 8, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -31.0F, -148.0F, -31.0F, 6, 8, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 134, -31.0F, -123.4F, -32.4F, 6, 100, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -29.4F, -123.4F, -30.8F, 6, 100, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -31.0F, -139.4F, -32.4F, 6, 14, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -29.4F, -139.4F, -30.8F, 6, 14, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -32.4F, -139.4F, -30.8F, 6, 14, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 135, -32.4F, -123.4F, -30.8F, 6, 100, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -31.0F, -123.4F, -29.8F, 6, 100, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 160, 115, -31.0F, -139.4F, -29.8F, 6, 14, 6, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 194, 238, -33.0F, -8.0F, -33.0F, 10, 4, 10, 0.0F, false));
		ne_post.cubeList.add(new ModelBox(ne_post, 332, 121, -33.0F, -156.0F, -33.0F, 10, 4, 10, 0.0F, false));

		se_post = new RendererModel(this);
		se_post.setRotationPoint(0.0F, 0.0F, 0.0F);
		posts.addChild(se_post);
		se_post.cubeList.add(new ModelBox(se_post, 175, 235, -32.0F, -16.0F, 24.0F, 8, 12, 8, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 111, 114, -32.0F, -141.0F, 24.0F, 8, 120, 8, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 332, 118, -32.0F, -156.0F, 24.0F, 8, 12, 8, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 164, 231, -31.0F, -24.0F, 25.0F, 8, 8, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -31.0F, -148.0F, 25.0F, 8, 8, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -31.0F, -123.4F, 23.6F, 6, 100, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -31.0F, -139.4F, 23.6F, 6, 14, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -32.4F, -139.4F, 25.2F, 6, 14, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 200, 135, -32.4F, -123.4F, 25.2F, 6, 100, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 135, -31.0F, -123.4F, 26.2F, 6, 100, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -31.0F, -139.4F, 26.2F, 6, 14, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 169, 238, -33.0F, -8.0F, 23.0F, 10, 4, 10, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 320, 114, -33.0F, -156.0F, 23.0F, 10, 4, 10, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -29.4F, -123.4F, 25.2F, 6, 100, 6, 0.0F, false));
		se_post.cubeList.add(new ModelBox(se_post, 160, 115, -29.4F, -139.4F, 25.2F, 6, 14, 6, 0.0F, false));

		curtains = new RendererModel(this);
		curtains.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(curtains);

		curtains_left = new RendererModel(this);
		curtains_left.setRotationPoint(0.0F, 0.0F, 0.0F);
		curtains.addChild(curtains_left);
		curtains_left.cubeList.add(new ModelBox(curtains_left, 3, 2, 25.0F, -156.0F, -24.0F, 4, 80, 12, 0.0F, false));
		curtains_left.cubeList.add(new ModelBox(curtains_left, 33, 2, 25.0F, -156.0F, -6.0F, 4, 80, 12, 0.0F, false));
		curtains_left.cubeList.add(new ModelBox(curtains_left, 62, 2, 25.0F, -156.0F, 12.0F, 4, 80, 12, 0.0F, false));
		curtains_left.cubeList.add(new ModelBox(curtains_left, 50, 2, 23.0F, -156.0F, 1.0F, 4, 80, 12, 0.0F, false));
		curtains_left.cubeList.add(new ModelBox(curtains_left, 52, 2, 23.0F, -156.0F, -17.0F, 4, 80, 12, 0.0F, false));

		curtains_right = new RendererModel(this);
		curtains_right.setRotationPoint(0.0F, 0.0F, 0.0F);
		curtains.addChild(curtains_right);
		curtains_right.cubeList.add(new ModelBox(curtains_right, 61, 2, -29.0F, -156.0F, -24.0F, 4, 80, 12, 0.0F, false));
		curtains_right.cubeList.add(new ModelBox(curtains_right, 2, 2, -29.0F, -156.0F, 12.0F, 4, 80, 12, 0.0F, false));
		curtains_right.cubeList.add(new ModelBox(curtains_right, 43, 2, -29.0F, -156.0F, -6.0F, 4, 80, 12, 0.0F, false));
		curtains_right.cubeList.add(new ModelBox(curtains_right, 30, 2, -25.0F, -156.0F, 3.0F, 4, 80, 12, 0.0F, false));
		curtains_right.cubeList.add(new ModelBox(curtains_right, 32, 2, -25.0F, -156.0F, -15.0F, 4, 80, 12, 0.0F, false));

		curtains_back = new RendererModel(this);
		curtains_back.setRotationPoint(0.0F, 0.0F, 0.0F);
		curtains.addChild(curtains_back);
		curtains_back.cubeList.add(new ModelBox(curtains_back, 33, 10, 13.0F, -156.0F, 21.0F, 12, 80, 4, 0.0F, false));
		curtains_back.cubeList.add(new ModelBox(curtains_back, 33, 10, -24.0F, -156.0F, 21.0F, 12, 80, 4, 0.0F, false));
		curtains_back.cubeList.add(new ModelBox(curtains_back, 33, 10, -6.0F, -156.0F, 21.0F, 12, 80, 4, 0.0F, false));
		curtains_back.cubeList.add(new ModelBox(curtains_back, 33, 10, 3.0F, -156.0F, 17.0F, 12, 80, 4, 0.0F, false));
		curtains_back.cubeList.add(new ModelBox(curtains_back, 33, 10, -14.0F, -156.0F, 17.0F, 12, 80, 4, 0.0F, false));

		dias = new RendererModel(this);
		dias.setRotationPoint(0.0F, 0.0F, 0.0F);
		box.addChild(dias);
		dias.cubeList.add(new ModelBox(dias, 1, 36, -12.0F, -84.0F, -18.0F, 24, 4, 24, 0.0F, false));
		dias.cubeList.add(new ModelBox(dias, 119, 61, -8.0F, -86.0F, -14.0F, 16, 4, 16, 0.0F, false));
		dias.cubeList.add(new ModelBox(dias, 171, 54, -5.0F, -92.0F, -10.0F, 2, 8, 2, 0.0F, false));
		dias.cubeList.add(new ModelBox(dias, 148, 59, 3.0F, -92.0F, -10.0F, 2, 8, 2, 0.0F, false));
		dias.cubeList.add(new ModelBox(dias, 158, 44, -5.0F, -92.0F, -3.0F, 2, 8, 2, 0.0F, false));
		dias.cubeList.add(new ModelBox(dias, 130, 44, 3.0F, -92.0F, -3.0F, 2, 8, 2, 0.0F, false));

		coinreturn = new RendererModel(this);
		coinreturn.setRotationPoint(16.0F, -48.0F, -27.0F);
		setRotationAngle(coinreturn, -0.0873F, 0.0F, 0.0F);
		box.addChild(coinreturn);
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 44, -1.0F, -8.0F, -3.0F, 2, 2, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 48, 1.0F, -8.0F, -3.0F, 3, 10, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 44, -4.0F, -8.0F, -3.0F, 3, 10, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 44, -1.0F, -2.0F, -3.0F, 2, 4, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 67, -4.0F, 4.0F, -3.0F, 8, 4, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 44, 2.0F, 2.0F, -3.0F, 2, 2, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 141, 44, -4.0F, 2.0F, -3.0F, 2, 2, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 342, 69, -2.6F, 5.4F, -4.0F, 1, 1, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 379, 109, -2.0F, -6.0F, -2.0F, 4, 4, 4, 0.0F, false));
		coinreturn.cubeList.add(new ModelBox(coinreturn, 388, 107, -2.0F, 1.0F, -2.0F, 4, 4, 4, 0.0F, false));

		button = new RendererModel(this);
		button.setRotationPoint(-12.0F, -59.0F, -27.0F);
		setRotationAngle(button, 0.0F, 0.0F, 0.7854F);
		box.addChild(button);
		button.cubeList.add(new ModelBox(button, 52, 41, -2.0F, -2.0F, -3.0F, 4, 4, 4, 0.0F, false));
		button.cubeList.add(new ModelBox(button, 128, 40, -3.0F, -3.0F, -2.0F, 6, 6, 4, 0.0F, false));
	}

	public void render(ExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.25, 0.25, 0.25);
		GlStateManager.rotated(180, 0, 1, 0);
		
		this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.FORTUNE.getRotationForState(tile.getOpen()));

		door.render(0.0625F);
		boti.render(0.0625F);
		box.render(0.0625F);
		
		ModelHelper.renderPartBrightness(1F, glow_crystal, glow_topper);
		
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}
	
	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}