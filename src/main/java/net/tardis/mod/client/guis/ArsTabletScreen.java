package net.tardis.mod.client.guis;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.misc.GuiContext;

public class ArsTabletScreen extends Screen {

	public ArsTabletScreen() {
		super(new StringTextComponent(""));
	}
	
	public ArsTabletScreen(GuiContext cont) {
		this();
	}

}
