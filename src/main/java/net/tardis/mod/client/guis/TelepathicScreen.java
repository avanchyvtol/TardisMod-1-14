package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.TelepathicMessage;

import java.util.ArrayList;
import java.util.List;

public class TelepathicScreen extends Screen{

	public static final TranslationTextComponent TITLE = new TranslationTextComponent("gui.tardis.telepathic");
	private List<Biome> biomeNames = new ArrayList<>();
    private int page = 0;
    private TextFieldWidget textFieldWidget = null;
	
	public TelepathicScreen() {
		super(TITLE);
	}

	@Override
    public void init() {
        super.init();
        this.textFieldWidget = new TextFieldWidget(this.font, this.width / 2 - 100, 50, 200, 20, this.textFieldWidget, I18n.format("selectWorld.search"));
        this.children.add(this.textFieldWidget);
        this.biomeNames.clear();
        this.biomeNames.addAll(ForgeRegistries.BIOMES.getValues());


        for (Widget b : this.buttons)
            b.active = false;

        this.buttons.clear();

        createFilteredList("", true);
        textFieldWidget.setEnabled(true);
    }

    @Override
    public void tick() {
        super.tick();
        textFieldWidget.tick();
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        for (Widget w : this.buttons) {
            w.renderButton(mouseX, mouseY, partialTicks);
        }
        textFieldWidget.render(mouseX, mouseY, partialTicks);
        
        if (this.textFieldWidget.isFocused()) {
			this.textFieldWidget.setSuggestion("");
		}
		else if (this.textFieldWidget.getText().isEmpty()){
			this.textFieldWidget.setSuggestion("Search...");
		}
    }


    public void createFilteredList(String searchTerm, boolean addBackButtons) {

        this.biomeNames.clear();
        this.biomeNames.addAll(ForgeRegistries.BIOMES.getValues());
        buttons.clear();

        int index = 0;
        int start = page * 10;
        int end = start + 10;

        if (!searchTerm.isEmpty()) {
            biomeNames.removeIf(biome -> !biome.getDisplayName().getUnformattedComponentText().contains(searchTerm));
        }

        if (!biomeNames.isEmpty()) {
            for (Biome b : this.biomeNames.subList(start, biomeNames.size() < end ? biomeNames.size() : end)) {
                this.addButton(new TextButton(width / 2 - 50, height / 2 + 50 - (index * font.FONT_HEIGHT + 2), b.getDisplayName().getFormattedText(), but -> {
                    Network.sendToServer(new TelepathicMessage(b.getRegistryName()));
                    Minecraft.getInstance().displayGuiScreen(null);
                }));
                ++index;
            }
        }

        if (addBackButtons) {
            this.addButton(new Button(width / 2 + 50, height / 2 + 75, 20, 20, ">", but -> mod(1)));
            this.addButton(new Button(width / 2 - 80, height / 2 + 75, 20, 20, "<", but -> mod(-1)));
        }

    }

	public void mod(int m) {
        int pages = this.biomeNames.size() / 10;
        if (page + m >= 0 && page + m < pages)
            page += m;
        init();
	}

    @Override
    public boolean keyPressed(int p_keyPressed_1_, int p_keyPressed_2_, int p_keyPressed_3_) {
        if (textFieldWidget.isFocused()) {
            createFilteredList(textFieldWidget.getText(), true);
        }
        return super.keyPressed(p_keyPressed_1_, p_keyPressed_2_, p_keyPressed_3_);
    }

    @Override
    public boolean mouseClicked(double p_mouseClicked_1_, double p_mouseClicked_3_, int p_mouseClicked_5_) {
        textFieldWidget.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
        return super.mouseClicked(p_mouseClicked_1_, p_mouseClicked_3_, p_mouseClicked_5_);
    }

    @Override
    public boolean charTyped(char p_charTyped_1_, int p_charTyped_2_) {
        return this.textFieldWidget.charTyped(p_charTyped_1_, p_charTyped_2_);
    }
}
