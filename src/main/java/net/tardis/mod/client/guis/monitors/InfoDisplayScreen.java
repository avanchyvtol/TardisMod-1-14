package net.tardis.mod.client.guis.monitors;

import java.text.DecimalFormat;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Created by 50ap5ud5
 * on 3 May 2020 @ 10:45:48 pm
 */
public class InfoDisplayScreen extends MonitorScreen{
	
	public static DecimalFormat format = new DecimalFormat("###");
	
	public InfoDisplayScreen(IMonitorGui gui, String name) {
		super(gui, name);
	}
	
	@Override
	protected void init() {
		super.init();
	}
	
	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		super.render(mouseX, mouseY, partialTicks);
		TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile && te !=null) {
			ConsoleTile console = (ConsoleTile)te;
			this.drawCenteredString(this.font,new TranslationTextComponent("gui.tardis.info").getFormattedText(),this.parent.getMinX() + 100, this.parent.getMaxY() + 10, 0xFFFFFF);
			this.drawString(this.font,"Location: " + Helper.formatBlockPos(console.getLocation()),this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 4 - 3), 0xFFFFFF);
			this.drawString(this.font,"Dimension: " + Helper.formatDimName(console.getDimension()),this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 5 - 3), 0xFFFFFF);
			this.drawString(this.font,"Target: " + Helper.formatBlockPos(console.getDestination()),this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 6 - 3), 0xFFFFFF);
			this.drawString(this.font,"Target Dim: " + Helper.formatDimName(console.getDestinationDimension()),this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 7 - 3), 0xFFFFFF);
			this.drawString(this.font,"Artron Units: " + format.format(console.getArtron()) + "AU",this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 8 - 3), 0xFFFFFF);
			this.drawString(this.font,"Journey: " + format.format(console.getPercentageJourney() * 100.0F) + "%",this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 9 - 3), 0xFFFFFF);
		}
	}
	
	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
}
