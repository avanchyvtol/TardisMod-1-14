package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.item.ItemStack;

public class ItemButton extends Button{

	private ItemStack stack;
	
	public ItemButton(int x, int y, ItemStack stack, IPressable press) {
		super(x, y, 16, 16, "", press);
		this.stack = stack;
	}

	@Override
	public void renderButton(int x, int y, float p_renderButton_3_) {
		if(this.active && this.visible) {
			GlStateManager.pushMatrix();
			RenderHelper.enableGUIStandardItemLighting();
			Minecraft.getInstance().getItemRenderer().renderItemIntoGUI(stack, this.x, this.y);
			GlStateManager.popMatrix();
		}
	}
	
	public ItemStack getItemStack() {
		return this.stack;
	}

}
