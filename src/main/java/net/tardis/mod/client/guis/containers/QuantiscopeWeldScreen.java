package net.tardis.mod.client.guis.containers;

import net.minecraft.block.Blocks;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TabWidget;
import net.tardis.mod.containers.QuantiscopeWeldContainer;
import net.tardis.mod.items.TItems;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.QuantiscopeTabMessage;
import net.tardis.mod.tileentities.QuantiscopeTile;
import net.tardis.mod.tileentities.QuantiscopeTile.EnumMode;

public class QuantiscopeWeldScreen extends ContainerScreen<QuantiscopeWeldContainer>{

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/quantiscope/weld.png");
	private QuantiscopeTile tile;
	
	public QuantiscopeWeldScreen(QuantiscopeWeldContainer cont, PlayerInventory inv, ITextComponent titleIn) {
		super(cont, inv, titleIn);
		this.tile = cont.getQuantiscope();
	}

	@Override
	protected void init() {
		super.init();
		
		this.addButton(new TabWidget(guiLeft - 69, guiTop - 20, new ItemStack(TItems.SONIC), but -> {
			Network.sendToServer(new QuantiscopeTabMessage(this.getContainer().getQuantiscope().getPos(), EnumMode.SONIC));
		}));
		this.addButton(new TabWidget(guiLeft - 69, guiTop + 10, new ItemStack(Blocks.CRAFTING_TABLE), but -> {})).isSelected = true;
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
		this.renderBackground();
		this.minecraft.getTextureManager().bindTexture(TEXTURE);
		blit(width / 2 - 253 / 2, height / 2 - 212 / 2, 0, 0, 253, 212);

		blit(width / 2 + 3, height / 2 - 55, 216, 222, (int)(25 * tile.getProgress()), 17);
	}

}
