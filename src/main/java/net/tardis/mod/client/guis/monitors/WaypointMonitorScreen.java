package net.tardis.mod.client.guis.monitors;

import net.minecraft.client.Minecraft;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WaypointDeleteMessage;
import net.tardis.mod.network.packets.WaypointLoadMessage;
import net.tardis.mod.tileentities.ConsoleTile;

import java.util.ArrayList;
import java.util.List;

public class WaypointMonitorScreen extends MonitorScreen {

	private List<SpaceTimeCoord> list = new ArrayList<SpaceTimeCoord>();
	private SpaceTimeCoord coord;
	private int index = 0;
	
	public WaypointMonitorScreen(IMonitorGui mon, String menu) {
		super(mon, menu);
	}
	
	@Override
	protected void init() {
		super.init();
		
		TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
		if(te instanceof ConsoleTile)
			this.list.addAll(((ConsoleTile)te).getWaypoints());
		
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY(), MonitorScreen.backTranslation, press -> change(-1)));
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY(), MonitorScreen.selectTranslation, press -> {
			Network.sendToServer(new WaypointLoadMessage(index));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY(), MonitorScreen.nextTranslation, press -> change(1)));
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY() - 17, new StringTextComponent("> " + new TranslationTextComponent("gui.tardis.waypoint.new").getFormattedText()), pres -> {
			Minecraft.getInstance().displayGuiScreen(new WaypointNewMonitorScreen(this.parent));
		}));
		
		this.addButton(this.addButton(this.parent.getMinX(), this.parent.getMinY() + 5, new StringTextComponent("> " + new TranslationTextComponent("gui.tardis.waypoint.delete").getFormattedText()), pres -> {
			Network.sendToServer(new WaypointDeleteMessage(index));
			Minecraft.getInstance().displayGuiScreen(null);
		}));
		
		change(0);
	}
	
	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		super.render(mouseX, mouseY, p_render_3_);
		this.drawCenteredString(this.font, "Waypoints", this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY(), 0xFFFFFF);
		if(coord != null) {
			this.minecraft.fontRenderer.drawString(coord.getName().isEmpty() ? "Waypoint Name: NONE" : "Waypoint Name: " + coord.getName(),
					this.parent.getMinX(), this.parent.getMaxY() + 23, 0xFFFFFF);
			
			this.minecraft.fontRenderer.drawString("Location: " + Helper.formatBlockPos(coord.getPos()),
					this.parent.getMinX(), this.parent.getMaxY() + (this.font.FONT_HEIGHT * 4 - 3), 0xFFFFFF);
			
			this.minecraft.fontRenderer.drawString("Dimension: " + Helper.formatDimName(DimensionType.byName(coord.getDimType())),
					this.parent.getMinX(), this.parent.getMaxY() + (this.font.FONT_HEIGHT * 5 - 3), 0xFFFFFF);
		}
		else this.minecraft.fontRenderer.drawString("Waypoint Name: EMPTY", this.parent.getMinX(), this.parent.getMaxY() + 23, 0xFFFFFF);

        this.font.drawString("Waypoint Index: " + index + "/" + 15, this.parent.getMinX(), this.parent.getMaxY() + this.font.FONT_HEIGHT * 6 - 3, 0xFFFFFF);
	}
	
	public void change(int i) {
		if(index + i >= this.list.size())
			index = 0;
		else if(index + i < 0)
			index = this.list.size() - 1;
		else index += i;
		
		this.coord = this.list.get(index);
		
	}


}
