package net.tardis.mod.client.renderers.entity.transport;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.ModelRegistry;
import net.tardis.mod.entity.TardisEntity;

public class TardisEntityRenderer extends EntityRenderer<TardisEntity>{
	
	public TardisEntityRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public void doRender(TardisEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x, y, z);
		GlStateManager.rotated(180, 0, 0, 1);
		float prev = entity.ticksExisted;
		float cur = entity.ticksExisted + 1;
		GlStateManager.rotated(prev + (cur - prev) * partialTicks, 0, 1, 0);
		GlStateManager.translated(0, Math.sin(entity.ticksExisted * 0.05) * 0.1, 0);
        ModelRegistry.getExteriorModel(entity.getExterior()).renderEntity(entity);
		GlStateManager.popMatrix();
	}

	@Override
	protected ResourceLocation getEntityTexture(TardisEntity entity) {
		return null;
	}

}
