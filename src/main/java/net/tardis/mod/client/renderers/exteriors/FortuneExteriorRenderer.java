package net.tardis.mod.client.renderers.exteriors;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.FortuneExteriorModel;
import net.tardis.mod.tileentities.exteriors.FortuneExteriorTile;

public class FortuneExteriorRenderer extends ExteriorRenderer<FortuneExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/fortune.png");
	public static final FortuneExteriorModel MODEL = new FortuneExteriorModel();
	
	@Override
	public void renderExterior(FortuneExteriorTile tile) {
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
	}

}
