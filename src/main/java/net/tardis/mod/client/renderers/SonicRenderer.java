package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.Vector3f;
import net.minecraft.client.renderer.model.*;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.client.renderer.tileentity.ItemStackTileEntityRenderer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.items.sonicparts.SonicBasePart.SonicComponentTypes;
import net.tardis.mod.sonic.ISonicPart;
import net.tardis.mod.sonic.capability.SonicCapability;

import javax.annotation.Nullable;
import javax.vecmath.Matrix4f;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Random;

/**
 * Created by Swirtzly
 * on 21/03/2020 @ 20:27
 */
/*@Mod.EventBusSubscriber(value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)*/
public class SonicRenderer extends ItemStackTileEntityRenderer implements IBakedModel {

    private ItemStack sonicEmmiter = new ItemStack(TItems.SONIC_EMITTER);
    private ItemStack sonicActivator = new ItemStack(TItems.SONIC_ACTIVATOR);
    private ItemStack sonicHandle = new ItemStack(TItems.SONIC_HANDLE);
    private ItemStack sonicEnd = new ItemStack(TItems.SONIC_END);

    @Override
    public void renderByItem(ItemStack itemStackIn) {

        ClientPlayerEntity player = Minecraft.getInstance().player;

        GlStateManager.pushMatrix();
        if (player.isHandActive() && player.getHeldItem(player.getActiveHand()) == itemStackIn) {
            GlStateManager.rotated(-60, 1D,0D,0D);
            GlStateManager.translated(0, 0.5, 0);
        }
        SonicItem.readCapability(itemStackIn);
        
       SonicCapability.getForStack(itemStackIn).ifPresent((data) -> {
            SonicBasePart.setType(sonicEmmiter, data.getSonicPart(ISonicPart.SonicPart.EMITTER));
            SonicBasePart.setType(sonicActivator, data.getSonicPart(ISonicPart.SonicPart.ACTIVATOR));
            SonicBasePart.setType(sonicHandle, data.getSonicPart(ISonicPart.SonicPart.HANDLE));
            SonicBasePart.setType(sonicEnd, data.getSonicPart(ISonicPart.SonicPart.END));
        });
       if (!SonicCapability.getForStack(itemStackIn).isPresent()) { //We need this check because otherwise the sonic 
    	   SonicBasePart.setType(sonicEmmiter, SonicBasePart.SonicComponentTypes.MK_2); //We set the type to mk 2, because we want to match the default sonic type, which is 1. The mk 2's enum ordinal is 1, so it's correct
           SonicBasePart.setType(sonicActivator, SonicBasePart.SonicComponentTypes.MK_2);
           SonicBasePart.setType(sonicHandle, SonicBasePart.SonicComponentTypes.MK_2);
           SonicBasePart.setType(sonicEnd, SonicBasePart.SonicComponentTypes.MK_2);	
       }
       renderSonic();
        GlStateManager.popMatrix();
    }
    
    private void renderSonic() {
    	GlStateManager.pushMatrix();
		Minecraft.getInstance().getItemRenderer().renderItem(sonicEmmiter, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND);
		Minecraft.getInstance().getItemRenderer().renderItem(sonicActivator, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND);
		Minecraft.getInstance().getItemRenderer().renderItem(sonicHandle, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND);
		Minecraft.getInstance().getItemRenderer().renderItem(sonicEnd, ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND);
		GlStateManager.popMatrix();
    }

   /* @SubscribeEvent
    public static void onDoTheThing(ModelBakeEvent event) {
        event.getModelRegistry().put(new ModelResourceLocation(TItems.SONIC.getRegistryName(), "inventory"), new SonicRenderer());
    }
*/
    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand) {
        return null;
    }

    @Override
    public TextureAtlasSprite getParticleTexture() {
        return null;
    }

    @Override
    public ItemCameraTransforms getItemCameraTransforms() {
        return getCameraTrans();
    }

    @Override
    public ItemOverrideList getOverrides() {
        return ItemOverrideList.EMPTY;
    }

    @Override
    public boolean isAmbientOcclusion() {
        return true;
    }

    @Override
    public boolean isGui3d() {
        return false;
    }

    @Override
    public boolean isBuiltInRenderer() {
        return true;
    }
    
    
//    @Override
//	public Pair<? extends IBakedModel, Matrix4f> handlePerspective(TransformType cameraTransformType) {
//		return IBakedModel.super.handlePerspective(cameraTransformType);
//	}

	public ItemCameraTransforms getCameraTrans() {
        ItemTransformVec3f thirdperson_leftIn = getTransform(ItemCameraTransforms.TransformType.THIRD_PERSON_LEFT_HAND);
        ItemTransformVec3f thirdperson_rightIn = getTransform(ItemCameraTransforms.TransformType.THIRD_PERSON_RIGHT_HAND);
        ItemTransformVec3f firstperson_leftIn = getTransform(ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND);
        ItemTransformVec3f firstperson_rightIn = getTransform(ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND);
        ItemTransformVec3f headIn = getTransform(ItemCameraTransforms.TransformType.HEAD);
        ItemTransformVec3f guiIn = getTransform(ItemCameraTransforms.TransformType.GUI);
        ItemTransformVec3f groundIn = getTransform(ItemCameraTransforms.TransformType.GROUND);
        ItemTransformVec3f fixedIn = getTransform(ItemCameraTransforms.TransformType.FIXED);
        return new ItemCameraTransforms(thirdperson_leftIn, thirdperson_rightIn, firstperson_leftIn, firstperson_rightIn, headIn, guiIn, groundIn, fixedIn);
    }

    public ItemTransformVec3f getTransform(ItemCameraTransforms.TransformType type) {
        switch (type) {
            case HEAD:
                return new ItemTransformVec3f(new Vector3f(0, 0, 0), new Vector3f(0, 0, 0), new Vector3f(0, 0, 0));
            case GUI:
                return new ItemTransformVec3f(new Vector3f(0, 0, 45), new Vector3f(1, 1, 0), new Vector3f(1, 1, 1));
            case NONE:
            case FIXED:
            case GROUND:
            case FIRST_PERSON_LEFT_HAND:
            case THIRD_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                return new ItemTransformVec3f(new Vector3f(0, 0, 0), new Vector3f(0, 0, 0), new Vector3f(0, 0, 0));
            default:
                break;
        }
        return null;
    }
}
