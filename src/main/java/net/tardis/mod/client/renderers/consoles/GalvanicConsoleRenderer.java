package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ConsoleGalvanicModel;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;

public class GalvanicConsoleRenderer extends TileEntityRenderer<GalvanicConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic.png");
	public static final ConsoleGalvanicModel MODEL = new ConsoleGalvanicModel();
	
	
	@Override
	public void render(GalvanicConsoleTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.35, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}

}
