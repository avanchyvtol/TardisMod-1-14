package net.tardis.mod.client.renderers.exteriors;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.client.models.exteriors.ClockExteriorModel;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ClockExteriorRenderer extends TileEntityRenderer<ExteriorTile> {

	public static ClockExteriorModel model = new ClockExteriorModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/clock.png");
	
	public boolean hasWritten = false;
	
	BotiManager boti = new BotiManager();
	
	@Override
	public void render(ExteriorTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();

		GlStateManager.translated(x + 0.5, y - 0.625, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		//model.render(console);
		
		Minecraft.getInstance().getFramebuffer().unbindFramebuffer();
		
		boti.setupFramebuffer();
		
		PlayerEntity player = Minecraft.getInstance().player;
				
		GlStateManager.pushMatrix();
		GlStateManager.scaled(4, 4, 4);
		
		GlStateManager.pushMatrix();
		//boti.renderWorld(n);
		GlStateManager.popMatrix();
		
		boti.endFBO();
		Minecraft.getInstance().getFramebuffer().bindFramebuffer(true);
		
		int width = 1, height = 2;
		
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.translated(-0.425, -2, 0);
		boti.fbo.bindFramebufferTexture();
		BufferBuilder buffer = Tessellator.getInstance().getBuffer();
		buffer.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		buffer.pos(0, 0, 0).tex(0, 0).endVertex();
		buffer.pos(width, 0, 0).tex(1, 0).endVertex();
		buffer.pos(width, height, 0).tex(1, 1).endVertex();
		buffer.pos(0, height, 0).tex(0, 1).endVertex();
		Tessellator.getInstance().draw();
		GlStateManager.popMatrix();
		
		GlStateManager.popMatrix();
		
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
