package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.ModelCoralCopper;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;

public class CoralConsoleRenderer extends TileEntityRenderer<CoralConsoleTile> {

    public static ModelCoralCopper model = new ModelCoralCopper();
    public static ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/coral.png");

    @Override
    public void render(CoralConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
        GlStateManager.pushMatrix();
        GlStateManager.translated(x + 0.5, y + 0.9, z + 0.5);
        GlStateManager.rotated(180, 0, 0, 1);
        this.bindTexture(TEXTURE);
      //  GlStateManager.rotated(60, 0, 1, 0);
        double scale = 0.65;
        GlStateManager.enableRescaleNormal();
        GlStateManager.scaled(scale, scale, scale);
        model.render(console, ModelHelper.RENDER_SCALE);
        GlStateManager.disableRescaleNormal();
        GlStateManager.popMatrix();
    }

    @Override
    protected void bindTexture(ResourceLocation location) {
        Minecraft.getInstance().getTextureManager().bindTexture(location);
    }

}
