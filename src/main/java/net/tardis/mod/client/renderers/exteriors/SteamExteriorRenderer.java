package net.tardis.mod.client.renderers.exteriors;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.tileentities.exteriors.SteampunkExteriorTile;

public class SteamExteriorRenderer extends ExteriorRenderer<SteampunkExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/steampunk.png");
	private SteamExteriorModel model = new SteamExteriorModel();
	
	BotiManager mana = new BotiManager();

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

	@Override
	public void renderExterior(SteampunkExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 0.15625, 0);
		GlStateManager.scalef(0.25f, 0.25f, 0.25f);
		this.bindTexture(TEXTURE);
		this.model.render(tile);
		
		if(tile.getBotiWorld() == null) {
			tile.updateBoti();
			GlStateManager.popMatrix();
			return;
		}
		
		Minecraft.getInstance().getFramebuffer().unbindFramebuffer();
		
		GlStateManager.pushMatrix();
		Minecraft.getInstance().worldRenderer.renderSky(0);
		Minecraft.getInstance().gameRenderer.disableLightmap();
		GlStateManager.color3f(1, 1, 1);
		PlayerEntity player = Minecraft.getInstance().player;
		mana.setupFramebuffer();
		GlStateManager.loadIdentity();
		this.bindTexture(TEXTURE);
		mana.renderWorld(tile.getBotiWorld());
		mana.endFBO();
		Minecraft.getInstance().gameRenderer.enableLightmap();
		GlStateManager.popMatrix();
		
		Minecraft.getInstance().getFramebuffer().bindFramebuffer(true);
		
		mana.fbo.bindFramebufferTexture();
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
		double x = -1.5, y = -8, z = -2.9, width = 3, height = 9;
		double maxU = 1, maxV = 1, minV = 0.33;
		bb.pos(x, y, z).tex(0, minV).endVertex();
		bb.pos(x, y + height, z).tex(0, maxV).endVertex();
		bb.pos(x + width, y + height, z).tex(maxU, maxV).endVertex();
		bb.pos(x + width, y, z).tex(maxU, minV).endVertex();
		Tessellator.getInstance().draw();
		GlStateManager.popMatrix();
	}

}
