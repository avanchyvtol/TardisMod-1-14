package net.tardis.mod.client.renderers.boti;

import java.util.Map.Entry;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.boti.BlockStore;
import net.tardis.mod.boti.BotiWorld;
import net.tardis.mod.boti.WorldShell;

@OnlyIn(Dist.CLIENT)
public class BotiManager {
	
	private static VertexFormat format = DefaultVertexFormats.BLOCK;
	private VertexBuffer botiVbo;
	public Framebuffer fbo;
	
	public void setupVBO() {
		if(this.botiVbo == null)
			botiVbo = new VertexBuffer(format);
		botiVbo.bindBuffer();
	}
	
	public void drawData() {
		GlStateManager.vertexPointer(3, GL11.GL_FLOAT, format.getSize(), 0);
		GlStateManager.colorPointer(4, GL11.GL_UNSIGNED_BYTE, format.getSize(), format.getColorOffset());
		GlStateManager.texCoordPointer(2, GL11.GL_FLOAT, format.getSize(), format.getUvOffsetById(0));
		
		GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		GlStateManager.enableClientState(GL11.GL_COLOR_ARRAY);
		this.botiVbo.drawArrays(GL11.GL_QUADS);
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.disableClientState(GL11.GL_COLOR_ARRAY);
	}
	
	public void writeBufferData(BufferBuilder data) {
		data.finishDrawing();
		data.reset();
		this.botiVbo.bufferData(data.getByteBuffer());
	}
	
	public void endVBO() {
		VertexBuffer.unbindBuffer();
	}
	
	//Render the other world here
	@SuppressWarnings("deprecation")
	public void renderWorld(WorldShell shell) {
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
	
		RenderHelper.disableStandardItemLighting();
		Minecraft.getInstance().textureManager.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
		if(this.botiVbo == null || shell.needsUpdate()) {
			this.botiVbo = null;
			this.setupVBO();
			
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			bb.begin(GL11.GL_QUADS, format);
			for(Entry<BlockPos, BlockStore> entry : shell.getMap().entrySet()) {
				Minecraft.getInstance().getBlockRendererDispatcher()
				.renderBlock(entry.getValue().getState(), entry.getKey(), shell, bb, new Random());
				
				Minecraft.getInstance().getBlockRendererDispatcher()
				.renderFluid(entry.getKey(), shell, bb, entry.getValue().getState().getFluidState());
			}
			this.writeBufferData(bb);
			shell.setNeedsUpdate(false);
		}
		else {
			this.setupVBO();
			this.drawData();
			this.endVBO();
		}
		
		//TEs
		for(Entry<BlockPos, TileEntity> entry : shell.getTiles().entrySet()) {
			TileEntityRendererDispatcher.instance.render(entry.getValue(), entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ(), 0, 0, false);
		}
		
		//Minecraft.getInstance().gameRenderer.enableLightmap();
	}
	
	public void setupFramebuffer() {
		if(fbo == null) {
			fbo = new Framebuffer(Minecraft.getInstance().mainWindow.getFramebufferWidth(), Minecraft.getInstance().mainWindow.getFramebufferHeight(), true, false);
		}
		
		fbo.framebufferClear(false);
		fbo.bindFramebuffer(true);
		
		fbo.checkFramebufferComplete();
	}
	
	public void endFBO() {
		fbo.unbindFramebuffer();
	}
	
	public void drawTex() {
		fbo.bindFramebufferTexture();
	}

}
