package net.tardis.mod.client.renderers.layers;


import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;


public class VortexMRenderLayer extends LayerRenderer<AbstractClientPlayerEntity,PlayerModel<AbstractClientPlayerEntity>>{

	public VortexMRenderLayer(
			IEntityRenderer<AbstractClientPlayerEntity, PlayerModel<AbstractClientPlayerEntity>> entityRendererIn) {
		super(entityRendererIn);
		// TODO Auto-generated constructor stub
	}

	@OnlyIn(Dist.CLIENT)
	public static boolean hasSmallArms(AbstractClientPlayerEntity player) {
		return player.getSkinType().equals("slim");
	}

	@Override
	public boolean shouldCombineTextures() {
		return false;
	}

	@Override
	public void render(AbstractClientPlayerEntity entityIn, float limbSwing, float limbSwingAmount, float partialTicks,float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		
	}

}
