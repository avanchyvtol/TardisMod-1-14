package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.HartnelConsoleModel;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;

public class HartnelConsoleRenderer extends TileEntityRenderer<HartnelConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/hartnel.png");
	public static final HartnelConsoleModel MODEL = new HartnelConsoleModel();	
	
	
	@Override
	public void render(HartnelConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.9, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(console);
		GlStateManager.popMatrix();
	}

}
