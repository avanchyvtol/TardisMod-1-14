package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ShipComputerTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class AntennaSubsystem extends Subsystem{

	public AntennaSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

	@Override
	public boolean stopsFlight() {
		return false;
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {
		this.damage(null, 1);
		if(this.console != null && this.canBeUsed()) {
			ExteriorTile ext = this.console.getExterior().getExterior(console);
			if(ext != null && ext.getWorld() != null) {
				ChunkPos startCP = ext.getWorld().getChunk(ext.getPos()).getPos();
				int chunkRad = 3;
				for(int x = -chunkRad; x < chunkRad; ++x) {
					for(int z = -chunkRad; z < chunkRad; ++z) {
						Chunk c = ext.getWorld().getChunk(startCP.x + x, startCP.z + z);
						for(TileEntity te : c.getTileEntityMap().values()) {
							if(te instanceof ShipComputerTile) {
								ShipComputerTile comp = (ShipComputerTile)te;
								if(comp.getSchematic() != null) {
									console.getDistressSignals().add(te.getPos());
									console.updateClient();
									this.damage(null, 2);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void onFlightSecond() {}

	@Override
	public void explode(boolean softCrash) {}

}
