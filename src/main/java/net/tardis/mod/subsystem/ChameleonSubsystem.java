package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;

public class ChameleonSubsystem extends Subsystem{


	public ChameleonSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}
	
	@Override
	public boolean stopsFlight() {
		return false;
	}

    @Override
    public void onTakeoff() {}

    @Override
    public void onLand() {
        this.damage(null, 1);
        if (console.hasWorld() && console.getWorld().rand.nextDouble() < 0.05)
            this.damage(null, (int) (console.getWorld().rand.nextDouble() * 50));
    }

    @Override
    public void onFlightSecond() {}

}
