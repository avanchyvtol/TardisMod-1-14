package net.tardis.mod.helper;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.advancements.Advancement;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.state.IProperty;
import net.minecraft.state.IStateHolder;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraftforge.common.util.Constants;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.tileentities.ConsoleTile;

public class Helper {

    private static Random rand = new Random();

	public static String formatBlockPos(BlockPos pos) {
		return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
	}

	public static String formatDimName(DimensionType dim) {
		if(dim == null)
			return "UNKNOWN";
		return DimensionType.getKey(dim).getPath().trim().replace("	", "").replace("_", " ");
	}
	
	public static float getAngleFromFacing(Direction dir) {
		if(dir == Direction.NORTH)
			return 0F;
		else if(dir == Direction.EAST)
			return 90;
		else if(dir == Direction.SOUTH)
			return 180;
		return 270F;
	}
	
	/**
	 * Used to determine if an object is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */

	public static boolean canTravelToDimension(DimensionType dimension) {
		ResourceLocation key = DimensionType.getKey(dimension);
		if(key == null)
			return false;
		List<? extends String> blacklist = TConfig.CONFIG.blacklistedDims.get();
		for(String s : blacklist) {
			if(key.toString().contentEquals(s))
				return false;
		}
        return dimension.getModType() != TDimensions.TARDIS &&
        		dimension.getModType() != TDimensions.VORTEX &&
        		dimension != DimensionType.THE_END;
	}
	
	/**
	 * Used to determine if the VM is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will require a config option to blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */
	public static boolean canVMTravelToDimension(DimensionType dimension) {
		if (TConfig.CONFIG.toggleVMWhitelistDims.get()) { //If using whitelist
			return TConfig.CONFIG.whitelistedVMDims.get().stream().anyMatch(dimName ->
				dimension.getRegistryName().toString().contentEquals(dimName));
		}
		else if (!TConfig.CONFIG.toggleVMWhitelistDims.get()){ //If using blacklist
			return TConfig.CONFIG.blacklistedVMDims.get().stream().anyMatch(dimName ->
				!dimension.getRegistryName().toString().contains(dimName));
		}
		return dimension.getModType() != TDimensions.TARDIS;
	}
	
	/**
	 * A more performance efficient version of the above to check if an object can be used in the current dimension
	 * @implNote Used for items/blocks that can only be used in the Tardis dimension
	 * @param dimension
	 * @return
	 */
	public static boolean isDimensionBlocked(DimensionType dimension) {
		return dimension.getModType() == TDimensions.TARDIS;
	}

	public static boolean isInBounds(int testX, int testY, int x, int y, int u, int v) {
		return (testX > x &&
				testX < u &&
				testY > y &&
				testY < v);
	}

	public static ResourceLocation getKeyFromDimType(DimensionType dimension) {
		ResourceLocation rl = DimensionType.getKey(dimension);
		return rl != null ? rl : new ResourceLocation("overworld");
	}

	public static boolean isOwnerOn(World world, DimensionType type) {
		if(!world.isRemote) {
			return world.getServer().getPlayerList().getPlayerByUUID(getPlayerFromTARDIS(type)) != null;
		}
		return false;
	}
	
	public static UUID getPlayerFromTARDIS(DimensionType type) {
		ResourceLocation loc = DimensionType.getKey(type);
		return UUID.fromString(loc.getPath());
	}
	
	public static CompoundNBT serializeBlockState(BlockState state) {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("block_id", state.getBlock().getRegistryName().toString());
		ListNBT nbt = new ListNBT();
		for(IProperty<?> prop : state.getProperties()) {
			CompoundNBT val = new CompoundNBT();
			val.putString("name", prop.getName());
			val.putString("value", IStateHolder.writePropertyValueToString(prop, state.get(prop)));
			nbt.add(val);
		}
		tag.put("prop", nbt);
		return tag;
	}
	
	public static BlockState deserializeBlockState(CompoundNBT tag) {
		BlockState state = Registry.BLOCK.getOrDefault(new ResourceLocation(tag.getString("block_id"))).getDefaultState();
		ListNBT prop = tag.getList("prop", Constants.NBT.TAG_COMPOUND);
		for(INBT nbt : prop) {
			CompoundNBT val = (CompoundNBT)nbt;
			String name = val.getString("name");
			String value = val.getString("value");
			for(IProperty<?> iprop : state.getProperties()) {
				if(iprop.getName().contentEquals(name)) {
					state = IStateHolder.func_215671_a(state, iprop, "", "", value);
				}
			}
		}
		return state;
	}
	
	public static void teleportEntities(Entity e, ServerWorld world, double x, double y, double z, float yaw, float pitch) {
		if(e instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)e;
			if(player.world == world) {
				player.connection.setPlayerLocation(x, y, z, yaw, pitch);
			}
			else player.teleport(world, x, y, z, yaw, pitch);
		}
		else {
			//In in same dimension
			if(e.world == world) {
				e.setPosition(x, y, z);
				e.rotationYaw = yaw;
				e.rotationPitch = pitch;
			}
			else {
				//If interdimensional
				e.dimension = world.getDimension().getType();
				Entity old = e;
				Entity newE = e.getType().create(world);
				
				if(old.world instanceof ServerWorld)
					((ServerWorld)old.world).removeEntity(old);
				
				newE.copyDataFromOld(old);
				newE.setPosition(x, y, z);
				world.func_217460_e(newE);
			}
		}
	}


    public static void dropEntityLoot(Entity target, PlayerEntity attacker) {
    	LivingEntity targeted = (LivingEntity) target;
    	  ResourceLocation resourcelocation = targeted.getLootTableResourceLocation();
          LootTable loottable = target.world.getServer().getLootTableManager().getLootTableFromLocation(resourcelocation);
        LootContext.Builder lootcontext$builder = getLootContextBuilder(true, DamageSource.GENERIC, targeted, attacker);
          loottable.generate(lootcontext$builder.build(LootParameterSets.ENTITY), target::entityDropItem);
       
    }

    public static LootContext.Builder getLootContextBuilder(boolean p_213363_1_, DamageSource damageSourceIn, LivingEntity entity, PlayerEntity attacker) {
        LootContext.Builder lootcontext$builder = (new LootContext.Builder((ServerWorld) entity.world)).withRandom(rand).withParameter(LootParameters.THIS_ENTITY, entity).withParameter(LootParameters.POSITION, new BlockPos(entity)).withParameter(LootParameters.DAMAGE_SOURCE, damageSourceIn).withNullableParameter(LootParameters.KILLER_ENTITY, damageSourceIn.getTrueSource()).withNullableParameter(LootParameters.DIRECT_KILLER_ENTITY, damageSourceIn.getImmediateSource());
         if (p_213363_1_ && entity.getAttackingEntity() != null) {
        	 attacker = (PlayerEntity) entity.getAttackingEntity();
            lootcontext$builder = lootcontext$builder.withParameter(LootParameters.LAST_DAMAGE_PLAYER, attacker).withLuck(attacker.getLuck());
         }
         return lootcontext$builder;
    }
    
    public static void doIfAdvancementPresent(String name, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(Tardis.MODID, name));
    	if(adv != null)
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    }

	public static boolean unlockInterior(ConsoleTile console, @Nullable ServerPlayerEntity player, ConsoleRoom room) {
		if(!console.getUnlockedInteriors().contains(room)) {
			console.getUnlockedInteriors().add(room);
			if(player != null) {
				player.sendStatusMessage(new TranslationTextComponent(Translations.UNLOCKED_INTERIOR, room.getDisplayName()), true);
			}
			return true;
		}
		return false;
	}
	
	public static boolean unlockExterior(ConsoleTile console, @Nullable ServerPlayerEntity player, IExterior ext) {
		if(!console.getExteriors().contains(ext)) {
			console.getExteriors().add(ext);
			if(player != null) {
				player.sendStatusMessage(new TranslationTextComponent("status.tardis.exterior.unlock", ext.getDisplayName().getFormattedText()), true);
			}
			return true;
		}
		return false;
	}
}
