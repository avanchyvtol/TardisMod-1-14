package net.tardis.mod.helper;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.tileentities.ConsoleTile;

public class LandingSystem {
	
	
	public static boolean isSafe(World world, BlockPos pos) {
		return world.getBlockState(pos).getMaterial().isReplaceable();
	}
	
	public static boolean canLand(World world, BlockPos pos, IExterior exterior, ConsoleTile console) {
		BlockPos up = new BlockPos(exterior.getWidth(console), exterior.getHeight(console), exterior.getWidth(console));
		BlockPos down = new BlockPos(exterior.getWidth(console), 0, exterior.getWidth(console));
		for(BlockPos check : BlockPos.getAllInBoxMutable(pos.subtract(down), pos.add(up))){
			if(!isSafe(world, check))
				return false;
		}
		return world.getBlockState(pos.down()).isSolid();
	}
	
	public static BlockPos findVaildLandSpot(World world, BlockPos dest, EnumLandType type, IExterior ext, ConsoleTile console) {
		
		if(canLand(world, dest, ext, console))
			return dest;
		
		if(type == EnumLandType.UP) {
			return getLandSpotUp(world, dest, ext, console);
		}
		
		if(type == EnumLandType.DOWN) {
			return getLandSpotDown(world, dest, ext, console);
		}
		
		return BlockPos.ZERO;
		
	}
	
	public static BlockPos getLandSpotUp(World world, BlockPos dest, IExterior ext, ConsoleTile console) {
		for(int y = dest.getY(); y < world.getActualHeight(); ++y) {
			BlockPos test = new BlockPos(dest.getX(), y, dest.getZ());
			if(canLand(world, test, ext, console))
				return test;
		}
		return BlockPos.ZERO;
	}
	
	public static BlockPos getLandSpotDown(World world, BlockPos dest, IExterior ext, ConsoleTile console) {
		for(int y = dest.getY(); y > 0; --y) {
			BlockPos test = new BlockPos(dest.getX(), y, dest.getZ());
			if(canLand(world, test, ext, console))
				return test;
		}
		return BlockPos.ZERO;
	}
	
	/**
	 * Searches for a suitable position that isn't above the Nether Roof
	 * @implNote This is better than using world heightmap, because on worlds with a bedrock roof, it will allow you to get past the roof
	 * @param world
	 * @param pos
	 * @return
	 */
	public static BlockPos getTopBlock(World world, BlockPos pos) {
		for(int y = world.getActualHeight(); y > 0; --y) {
			BlockPos newPos = new BlockPos(pos.getX(), y, pos.getZ());
			BlockState state = world.getBlockState(newPos);
			BlockState underState = world.getBlockState(newPos.down());
			
			if(!state.causesSuffocation(world, newPos) && underState.isSolid() && !isPosBelowOrAboveWorld(world.getDimension().getType(),newPos.getY())) {
				return newPos;
			}
		}
		return pos;
	}
	
	public static boolean isBlockBehindWorldBorder(World world, BlockPos pos) {
		return world.getWorldBorder().contains(pos);
	}
	
	public static boolean isBlockBehindWorldBorder(World world, int x, int y, int z) {
		return world.getWorldBorder().contains(new BlockPos(x,y,z));
	}
	
	public static boolean isPosBelowOrAboveWorld(int y) {
		return y <= 0 || y >= 256;
	}
	
	public static BlockPos getLand(World world, BlockPos dest, EnumLandType type, ConsoleTile console) {
		BlockPos pos = findVaildLandSpot(world, dest, type, console.getExterior(), console);
		if(pos.equals(BlockPos.ZERO))
			pos = findVaildLandSpot(world, dest, type == EnumLandType.DOWN ? EnumLandType.UP : EnumLandType.DOWN, console.getExterior(), console);
		return pos;
	}
	/**
	 * Dimension Specific version of the check, prevents users from teleporting to the Nether roof
	 * @implNote If returns false, will search for safe spot that is not above the Nether roof
	 * @param dim
	 * @param y
	 * @return
	 */
	
	public static boolean isPosBelowOrAboveWorld(DimensionType dim, int y) {
		if (dim.equals(DimensionType.THE_NETHER)) {
			return y <= 0 || y >= 126;
		}
		return y <= 0 || y >= 256;
	}


}
