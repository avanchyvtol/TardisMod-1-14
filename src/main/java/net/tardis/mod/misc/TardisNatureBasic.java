package net.tardis.mod.misc;

import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class TardisNatureBasic extends TardisNature {

	@Override
	public int modFlight(ConsoleTile console, int mood) {
		if(console.getControl(StabilizerControl.class).isStabilized())
			console.getEmotionHandler().addLoyalty(1);
		if(mood < EnumHappyState.ESTATIC.getTreshold() * 1.5)
			return mood + (console.getControl(StabilizerControl.class).isStabilized() ? 1 : 2);
		return mood;
	}

	@Override
	public int modLanded(ConsoleTile console, int mood) {
		return mood;
	}

	@Override
	public int modEmptyTick(ConsoleTile console, int mood) {
		return mood > EnumHappyState.SAD.getTreshold() ? mood - 1 : 0;
	}

}
