package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.sounds.TSounds;



public class TeleportFunction implements IFunction{
	
	@Override
	public void onActivated(World world, PlayerEntity player){
		if (world.isRemote && Helper.canVMTravelToDimension(world.getDimension().getType())) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_TELE, null);
				world.playSound(player, player.getPosition(), TSounds.VM_BUTTON, SoundCategory.PLAYERS, 0.5F, 1F);
		}
		else {
			player.sendStatusMessage(new TranslationTextComponent("message.vm.forbidden"), false);	
		}
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("function.vm.teleport").getFormattedText();
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}
}
