package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public interface IFunction {
	
	void onActivated(World world,PlayerEntity player);

	String getNameKey();
	Boolean stateComplete();
}
