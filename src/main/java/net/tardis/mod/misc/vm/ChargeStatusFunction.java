package net.tardis.mod.misc.vm;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.sounds.TSounds;

public class ChargeStatusFunction implements IFunction{
	
	/*TODO:
	 * Will display amount of charge left
	 * Will take more charge to jump longer distances
	 * Hook into Tardis Fuel Handler and refuel VM from Tardis engine via some sort of charger block/item
	 *  
	 */
	
	@Override
	public void onActivated(World world, PlayerEntity player){
		if(world.isRemote) {
			Tardis.proxy.openGUI(Constants.Gui.VORTEX_STATUS, null);
			world.playSound(player, player.getPosition(), TSounds.VM_BUTTON, SoundCategory.PLAYERS, 0.5F, 1F);
		}
	}

	@Override
	public String getNameKey() {
		return new TranslationTextComponent("function.vm.charge_status").getFormattedText();
	}

	@Override
	public Boolean stateComplete() {
		return true;
	}
}
