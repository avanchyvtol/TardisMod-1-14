package net.tardis.mod.misc;

import net.minecraft.item.Item;

public class TardisLikes {

	private int loyaltyMod = 0;
	private Item item;
	
	public TardisLikes(Item item, int amount) {
		this.loyaltyMod = amount;
		this.item = item;
	}
	
	public Item getItem() {
		return item;
	}
	
	public int getLoyaltyMod() {
		return this.loyaltyMod;
	}
}
