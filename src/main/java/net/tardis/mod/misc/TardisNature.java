package net.tardis.mod.misc;

import net.tardis.mod.tileentities.ConsoleTile;

/*
 * Natures are singletons, so beware
 */
public abstract class TardisNature {
	
	public abstract int modFlight(ConsoleTile console, int mood);
	public abstract int modLanded(ConsoleTile console, int mood);
	public abstract int modEmptyTick(ConsoleTile console, int mood);

}
