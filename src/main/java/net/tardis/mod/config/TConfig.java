package net.tardis.mod.config;


import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.google.common.collect.Lists;

import net.minecraftforge.common.ForgeConfigSpec;

/**
 * Created by Swirtzly
 * on 24/03/2020 @ 21:39
 */


//Making a global config
public class TConfig {

    public static final TConfig CONFIG;
    public static final ForgeConfigSpec CONFIG_SPEC;

    static {
        final Pair<TConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(TConfig::new);
        CONFIG = specPair.getLeft();
        CONFIG_SPEC = specPair.getRight();
    }
    
    //Tardis Specific
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> entitiesToKill;
    
    //VM Specific
    public ForgeConfigSpec.BooleanValue toggleVMWhitelistDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> whitelistedVMDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedVMDims;
    public ForgeConfigSpec.ConfigValue<List<? extends String>> blacklistedVMDimTypes;

    
    //Entity
    public ForgeConfigSpec.BooleanValue detonateCreeper;
    public ForgeConfigSpec.BooleanValue shearSheep;
    public ForgeConfigSpec.BooleanValue dismantleSkeleton;
    public ForgeConfigSpec.BooleanValue inkSquid;

    //Block
    public ForgeConfigSpec.BooleanValue detonateTnt;
    public ForgeConfigSpec.BooleanValue redstoneLamps;
    public ForgeConfigSpec.BooleanValue openDoors;
    public ForgeConfigSpec.BooleanValue openTrapDoors;
    public ForgeConfigSpec.BooleanValue toggleRedstone;

    //Tardis
    public ForgeConfigSpec.BooleanValue coordinateTardis;

    //Laser
    public ForgeConfigSpec.BooleanValue laserFire;


    public TConfig(ForgeConfigSpec.Builder builder) {
    	
    	builder.push("Tardis Configurations");
    	blacklistedDims = builder.translation("config.tardis.blacklistedDims")
    			.comment("List of dimensions Tardis cannot travel to, everything else is allowed","Seperate every entry except the last one with commas")
    			.defineList("blacklistedDims", Lists.newArrayList(), String.class::isInstance);  			
    	builder.pop();
    	
    	builder.push("Entities to Kill when changing TARDIS interior");
    	this.entitiesToKill = builder.translation("config.tardis.entities_to_kill")
    			.comment("")
    			.defineList("things_to_kill", Lists.newArrayList(""), String.class::isInstance);
    	builder.pop();
    	
    	builder.push("Vortex Manipulator (VM) Configurations");
    	toggleVMWhitelistDims = builder.translation("config.tardis.vm.toggleVMDimWhitelist")
		.comment("Toggle whether to use the Whitelist. By default, uses the Blacklist")
		.define("toggleVMWhitelistDims", false);
    	whitelistedVMDims = builder.translation("config.tardis.whitelistedVMDims")
    			.comment("List of dimensions the VM can travel to, anything else is not allowed","Seperate every entry except the last one with commas")
    			.defineList("whitelistedVMDims", Lists.newArrayList("minecraft:overworld","minecraft:nether","minecraft:the_end"), String.class::isInstance);  			
    	blacklistedVMDims = builder.translation("config.tardis.blacklistedVMDims")
    			.comment("List of dimensions the VM cannot travel to, everything else is allowed","Seperate every entry except the last one with commas")
    			.defineList("blacklistedVMDims", Lists.newArrayList("tardis"), String.class::isInstance);  			
    	builder.pop();
    	
        
    	builder.push("Sonic Screwdriver");
        builder.push("Block Interactions");
        detonateTnt = builder.translation("config.tardis.detonate_tnt").comment("Toggle whether sonics can detonate TNT").define("detonate_tnt", true);
        redstoneLamps = builder.translation("config.tardis.redstone_lamps").comment("Toggle whether sonics can toggle Lamps").define("redstone_lamps", true);
        openDoors = builder.translation("config.tardis.open_doors").comment("Toggle whether sonics can open doors").define("open_doors", true);
        openTrapDoors = builder.translation("config.tardis.open_trapdoors").comment("Toggle whether sonics can open Trap doors").define("open_trapdoors", true);
        toggleRedstone = builder.translation("config.tardis.toggle_redstone").comment("Toggle whether enable/disable redstone").define("redstone", true);
        builder.pop();

        builder.push("Entity Interactions");
        detonateCreeper = builder.translation("config.tardis.detonate_creeper").comment("Toggle whether sonics can detonate Creepers").define("detonate_creeper", true);
        shearSheep = builder.translation("config.tardis.shear_sheep").comment("Toggle whether sonics can Shear Sheep").define("shear_sheep", true);
        dismantleSkeleton = builder.translation("config.tardis.dismantle_skeleton").comment("Toggle whether sonics can dismantle Skeleton like entities").define("dismantle_skeletons", true);
        inkSquid = builder.translation("config.tardis.ink_squid").comment("Toggle whether sonics can ink Squids").define("ink_squid", true);
        builder.pop();

        builder.push("Tardis");
        coordinateTardis = builder.translation("config.tardis.coordinate_tardis").comment("Toggle whether sonics can tell the Tardis where to land").define("coordinate_tardis", true);
        builder.pop();

        builder.push("Laser");
        laserFire = builder.translation("config.tardis.laser_fire").comment("Toggle where the Laser setting burns things").define("laser_fire", true);
        builder.pop();

        builder.pop();

    }


}
