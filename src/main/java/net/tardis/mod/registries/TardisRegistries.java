package net.tardis.mod.registries;

import java.util.ArrayList;
import java.util.List;

import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.animation.IExteriorAnimation.ExteriorAnimationEntry;
import net.tardis.mod.entity.ai.dalek.types.DalekType;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.consoles.Console;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.sounds.InteriorHum;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.upgrades.UpgradeEntry;

/**
 * 
 * @author Spectre0987
 * 
 * Register things during {@link FMLCommonSetupEvent}
 *
 */
@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TardisRegistries {

	public static Registry<DalekType> DALEK_TYPE = new Registry<DalekType>();;
	public static Registry<Protocol> PROTOCOL_REGISTRY = new Registry<Protocol>();
	public static Registry<Console> CONSOLE_REGISTRY = new Registry<Console>();
	public static Registry<SubsystemEntry<?>> SUBSYSTEM_REGISTRY = new Registry<SubsystemEntry<?>>();
	public static Registry<UpgradeEntry<?>> UPGRADES = new Registry<UpgradeEntry<?>>();
	public static Registry<ExteriorAnimationEntry<?>> EXTERIOR_ANIMATIONS = new Registry<ExteriorAnimationEntry<?>>();
	public static Registry<InteriorHum> HUM_REGISTRY = new Registry<InteriorHum>();
    public static Registry<FlightEvent> FLIGHT_EVENT = new Registry<FlightEvent>();
    public static Registry<Schematic> SCHEMATICS = new Registry<Schematic>();


	private static List<Runnable> REGISTRIES = new ArrayList<Runnable>();

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public static void init(FMLCommonSetupEvent event) {
		doRegisters();
	}
	
	public static void doRegisters() {
		for (Runnable run : REGISTRIES) {
			run.run();
		}
	}
	
	/**
	 * 
	 * @param run - the runnable to execute after all registries are set up
	 */
	public static void registerRegisters(Runnable run) {
		REGISTRIES.add(run);
	}
}
