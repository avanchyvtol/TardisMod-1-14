package net.tardis.mod.entity.ai.dalek;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.resources.IResource;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.Tardis;

@OnlyIn(Dist.CLIENT)
public class SpeachManager {

	private static HashMap<ResourceLocation, SpeachLight> REGISTRY;
	
	public static void setup() {
		try {
			List<IResource> speaches = Minecraft.getInstance().getResourceManager().getAllResources(new ResourceLocation(Tardis.MODID, ""));
			for(IResource res : speaches) {
				//TODO: Figure out speach format (probably json, thinking an array of ticks to hold the value + the value)
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
