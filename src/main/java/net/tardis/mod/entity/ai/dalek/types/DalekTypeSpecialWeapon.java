package net.tardis.mod.entity.ai.dalek.types;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.*;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.sounds.TSounds;

/**
 * Created by Swirtzly
 * on 21/10/2019 @ 22:53
 */
public class DalekTypeSpecialWeapon extends DalekBaseType {

    private static IDalekWeapon WEAPON_INSTANCE = new SWDalekWeapon();
    private Vec3d laser_color = new Vec3d(1, 1, 0);

    public DalekTypeSpecialWeapon(String name) {
        super(name);
    }

    @Override
    public boolean canFly() {
        return false;
    }

    @Override
    public SoundEvent getLaserSound(DalekEntity dalek) {
        return TSounds.DALEK_SW_FIRE;
    }

    @Override
    public IDalekWeapon getDalekWeapon(DalekEntity dalek) {
        return WEAPON_INSTANCE;
    }

    @Override
    public void onAttack(DalekEntity dalek, LivingEntity target, float distanceFactor) {
        dalek.playSound(TSounds.DALEK_SW_AIM, 0.75F, 1);
        super.onAttack(dalek, target, distanceFactor);
    }

    @Override
    public float getLaserScale() {
        return 2.5F;
    }

    @Override
    public Vec3d getLaserColor(DalekEntity dalek) {
        return laser_color;
    }

    //SWD Dalek Instance
    public static class SWDalekWeapon implements IDalekWeapon {

        @Override
        public void onHit(DalekEntity dalek, World world, RayTraceResult rayTraceResult) {

            BlockPos explodePos = null;

            //I should make a util for this if I ever need to use it again
            if (rayTraceResult.getType() == RayTraceResult.Type.BLOCK) {
                BlockRayTraceResult blockResult = (BlockRayTraceResult) rayTraceResult;
                explodePos = blockResult.getPos();
            }

            if (rayTraceResult.getType() == RayTraceResult.Type.ENTITY) {
                EntityRayTraceResult entityHitResult = ((EntityRayTraceResult) rayTraceResult);
                explodePos = entityHitResult.getEntity().getPosition();
            }
            //

            if (!world.isRemote && explodePos != null) {
                Explosion.Mode explosion$mode = ForgeEventFactory.getMobGriefingEvent(world, dalek) ? Explosion.Mode.DESTROY : Explosion.Mode.NONE;
                world.createExplosion(dalek, explodePos.getX(), explodePos.getY(), explodePos.getZ(), 3F, explosion$mode);
                dalek.playSound(TSounds.DALEK_SW_HIT_EXPLODE, 1, 1);
            }
        }
    }


}
