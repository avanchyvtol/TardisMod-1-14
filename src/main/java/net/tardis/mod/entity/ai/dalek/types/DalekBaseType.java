package net.tardis.mod.entity.ai.dalek.types;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.TDamage;
import net.tardis.mod.sounds.TSounds;

import java.util.Random;

/**
 * Created by Swirtzly
 * on 20/10/2019 @ 18:28
 */
public class DalekBaseType implements DalekType {

    private final TranslationTextComponent translation;
    private ResourceLocation texture = null;
    private Vec3d laser_color = new Vec3d(0, 0, 1);
    private ResourceLocation registryName = null;
    private SoundEvent[] ambientSounds = new SoundEvent[]{};
    private SoundEvent[] attackSounds = new SoundEvent[]{TSounds.DALEK_EXTERMINATE};
    private boolean canFly = true;
    private IDalekWeapon dalekWeapon = null;
    private float attackDamage = 4F;
    private float laserWidth = 0.5F;
    private Item spawnItem = TItems.SPAWN_EGG_DALEK_TIMEWAR;

    public DalekBaseType(String name) {
        this.translation = new TranslationTextComponent("dalek.tardis." + name);
        texture = new ResourceLocation(Tardis.MODID, "textures/entity/dalek/" + name + ".png");

        //TODO This is a temp measure, without doing this, the regname seems to return null?
        registryName = new ResourceLocation(Tardis.MODID, name);
    }

    @Override
    public boolean canFly() {
        return canFly;
    }

    @Override
    public DalekType setCanFly(boolean canFly) {
        this.canFly = canFly;
        return this;
    }

    @Override
    public DalekType setLaserColor(Vec3d color) {
        this.laser_color = color;
        return this;
    }

    @Override
    public float getLaserScale() {
        return laserWidth;
    }

    @Override
    public DalekType setLaserScale(float laserWidth) {
        this.laserWidth = laserWidth;
        return this;
    }

    @Override
    public void specialUpdate(DalekEntity dalek) {

    }

    @Override
    public Vec3d getLaserColor(DalekEntity dalek) {
        return laser_color;
    }

    @Override
    public IDalekWeapon getDalekWeapon(DalekEntity dalek) {
        return dalekWeapon;
    }

    @Override
    public ResourceLocation getTexture() {
        return texture;
    }

    @Override
    public DalekType setRegistryName(ResourceLocation regName) {
        registryName = regName;
        return this;
    }

    @Override
    public ResourceLocation getRegistryName() {
        return registryName;
    }

    @Override
    public void onAttack(DalekEntity dalek, LivingEntity target, float distanceFactor) {
        World world = dalek.world;
        if (target.getDistance(dalek) < 15) {
            if (world.rand.nextBoolean() && world.rand.nextInt(5) % 2 == 0) {
                dalek.playSound(getAttackSounds(dalek), 0.8F, 1);
            }
            LaserEntity laser = new LaserEntity(TEntities.LASER, dalek, world);
            laser.setColor(getLaserColor(dalek));
            laser.setDamage(getAttackDamage(dalek));
            laser.setScale(getLaserScale());
            laser.setWeaponType(getDalekWeapon(dalek));
            laser.setSource(TDamage.DALEK);
            double d0 = target.posX - dalek.posX;
            double d1 = target.getBoundingBox().minY + (double) (target.getHeight() / 3.0F) - laser.posY;
            double d2 = target.posZ - dalek.posZ;
            double d3 = (double) MathHelper.sqrt(d0 * d0 + d2 * d2);
            laser.shoot(d0, d1 + d3 * (double) 0.2F, d2, 1.6F, (float) (14 - dalek.world.getDifficulty().getId() * 4));
            dalek.playSound(getLaserSound(dalek), 1F, 1);
            dalek.world.addEntity(laser);
        }
    }

    @Override
    public boolean isPowered(DalekEntity dalek, BlockPos pos) {
        return false;
    }

    @Override
    public Item getSpawnItem() {
        return spawnItem;
    }

    @Override
    public DalekType setSpawnItem(Item spawnTime) {
        this.spawnItem = spawnTime;
        return this;
    }

    @Override
    public SoundEvent getAttackSounds(DalekEntity dalek) {
        Random rand = dalek.world.rand;
        return attackSounds[rand.nextInt(attackSounds.length)];
    }

    @Override
    public SoundEvent getAmbientSounds(DalekEntity dalek) {
        return ambientSounds.length > 0 ? ambientSounds[dalek.world.rand.nextInt(ambientSounds.length)] : null;
    }

    @Override
    public SoundEvent getDeathSound(DalekEntity dalek) {
        return TSounds.DALEK_DEATH;
    }

    @Override
    public SoundEvent getLaserSound(DalekEntity dalek) {
        return TSounds.DALEK_FIRE;
    }

    @Override
    public ITextComponent getName() {
        return translation;
    }

    @Override
    public DalekType setDalekWeapon(IDalekWeapon dalekWeapon) {
        this.dalekWeapon = dalekWeapon;
        return this;
    }

    @Override
    public DalekType setAttackDamage(float damage) {
        this.attackDamage = damage;
        return this;
    }

    @Override
    public float getAttackDamage(DalekEntity dalek) {
        return attackDamage;
    }
}
