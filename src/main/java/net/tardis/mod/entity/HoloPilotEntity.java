package net.tardis.mod.entity;

import java.util.ArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.network.IPacket;
import net.minecraft.util.HandSide;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.items.TItems;

public class HoloPilotEntity extends LivingEntity{

	private int timeToLive = 100;
	
	public HoloPilotEntity(EntityType<? extends LivingEntity> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
	}
	
	public HoloPilotEntity(World worldIn) {
		this(TEntities.HOLO_PILOT, worldIn);
	}

	@Override
	public void tick() {
		super.tick();
		--this.timeToLive;
		if(this.timeToLive <= 0) {
			this.remove();
		}
		this.rotationPitch = 45;
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public Iterable<ItemStack> getArmorInventoryList() {
		return new ArrayList<ItemStack>();
	}

	@Override
	public ItemStack getItemStackFromSlot(EquipmentSlotType slotIn) {
		return new ItemStack(TItems.BATTERY);
	}

	@Override
	public void setItemStackToSlot(EquipmentSlotType slotIn, ItemStack stack) {}

	@Override
	public HandSide getPrimaryHand() {
		return HandSide.RIGHT;
	}

	@Override
	public boolean canAttack(LivingEntity target) {
		return false;
	}

	@Override
	public boolean canBeCollidedWith() {
		return false;
	}

	@Override
	public boolean canBePushed() {
		return false;
	}

	@Override
	public boolean canBeHitWithPotion() {
		return false;
	}

	@Override
	protected boolean canBeRidden(Entity entityIn) {
		return false;
	}

	@Override
	public boolean canBeAttackedWithItem() {
		return false;
	}

	@Override
	public boolean canRenderOnFire() {
		return false;
	}

}
