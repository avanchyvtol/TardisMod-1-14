package net.tardis.mod.datagen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.ChairBlock;
import net.tardis.mod.blocks.LightBlock;
import net.tardis.mod.blocks.RoundelBlock;
import net.tardis.mod.blocks.TardisEngineBlock;

public class TagGen<T> {

	private static Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	private ArrayList<String> resourceLocations = new ArrayList<String>();
	private Tag<T> tag;
	
	public TagGen(Tag<T> tag) {
		this.tag = tag;
	}
	
	public void create(DataGenerator gen) {
		String type = "blocks/";
		File file = gen.getOutputFolder().resolve("data/" + tag.getId().getNamespace() + "/tags/" + type + tag.getId().getPath() + ".json").toFile();
		
		System.out.println("TARDIS Tag gen created file at " + file.getAbsolutePath());
		
		try {
			//Make directories leading to this path
			file.getParentFile().mkdirs();
			
			if(file.exists())
				file.delete();
			file.createNewFile();
			
			JsonWriter writer = GSON.newJsonWriter(new FileWriter(file));
			
			//Start Document
			writer.beginObject();
			writer.name("replace").value(false);
			
			//Write values array
			writer.name("values");
			writer.beginArray();
			for(Block block : ForgeRegistries.BLOCKS) {
				if(this.isARSBlock(block)) {
					writer.value(block.getRegistryName().toString());
					System.out.println(block.getRegistryName() + " added to ARS");
				}
			}
			writer.endArray();
			
			writer.endObject();
			
			writer.flush();
			writer.close();
			System.out.println("ARS Writer closed");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public boolean isARSBlock(Block block) {
		return block instanceof RoundelBlock ||
				block instanceof LightBlock ||
				block instanceof ChairBlock||
				block instanceof TardisEngineBlock ||
				block instanceof IARS ||
				resourceLocations.contains(block.getRegistryName().toString());
	}
	
	public void addToTag(ResourceLocation key) {
		resourceLocations.add(key.toString());
	}
}
