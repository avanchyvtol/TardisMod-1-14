package net.tardis.mod.tags;

import net.minecraft.item.Item;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;

public class TardisItemTags {
	
	public static final Tag<Item> CINNABAR = new ItemTags.Wrapper(new ResourceLocation("forge", "dusts/cinnabar"));

}
