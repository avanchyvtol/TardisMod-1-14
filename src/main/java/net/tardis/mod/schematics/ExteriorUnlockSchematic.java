package net.tardis.mod.schematics;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.tileentities.ConsoleTile;

public class ExteriorUnlockSchematic extends Schematic{

	private Supplier<IExterior> ext;
	
	public ExteriorUnlockSchematic(Supplier<IExterior> ext) {
		this.ext = ext;
	}
	
	@Override
	public void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity entity) {
		IExterior ext = this.ext.get();
		if(!tile.getExteriors().contains(ext)) {
			tile.getExteriors().add(ext);
			entity.sendStatusMessage(new TranslationTextComponent("status.tardis.exterior.unlock", ext.getDisplayName().getFormattedText()), true);
		}
	}

}
