package net.tardis.mod.sounds;

import net.minecraft.util.ResourceLocation;

public abstract class SoundSchemeBase implements ISoundScheme {

	private ResourceLocation registryName;

	@Override
	public ISoundScheme setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	
}
