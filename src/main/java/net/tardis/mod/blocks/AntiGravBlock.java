package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.AntiGravityTile;

/**
 * Created by Swirtzly
 * on 06/04/2020 @ 22:08
 */
public class AntiGravBlock extends TileBlock {

    public static final BooleanProperty ACTIVATED = BooleanProperty.create("activated");

    public AntiGravBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
        this.setDefaultState(this.getDefaultState().with(ACTIVATED, Boolean.TRUE));
    }

    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (player.isSneaking()) {
            AntiGravityTile gravityTile = (AntiGravityTile) worldIn.getTileEntity(pos);
            if (gravityTile != null) {
                gravityTile.setRange(gravityTile.getRange() + 1);
            }
        } else {
            worldIn.setBlockState(pos, state.cycle(ACTIVATED), 4);
        }
        return true;
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(ACTIVATED);
        super.fillStateContainer(builder);
    }

    @Override
    public BlockState getStateForPlacement(BlockState state, Direction facing, BlockState state2, IWorld world, BlockPos pos1, BlockPos pos2, Hand hand) {
        return this.getDefaultState().with(ACTIVATED, true);
    }

}
