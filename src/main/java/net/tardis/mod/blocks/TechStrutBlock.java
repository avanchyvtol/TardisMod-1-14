package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.fluid.IFluidState;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;
import net.tardis.mod.helper.VoxelShapeUtils;
import net.tardis.mod.properties.Prop;

public class TechStrutBlock extends Block {

	public static final IntegerProperty TYPE = IntegerProperty.create("type", 0, 2);
	public static final VoxelShape NORTH = createVoxelShape();
	public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
	public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
	public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);
	
	public TechStrutBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}
	
	@Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.calcState(context.getWorld(), context.getPos()).with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
	}

	@Override
	public void updateNeighbors(BlockState stateIn, IWorld worldIn, BlockPos pos, int flags) {
		if(!worldIn.isRemote()) {
			//up
			BlockPos newPos = pos.offset(Direction.UP);
			int type = this.calcState(worldIn, newPos).get(TYPE);
			if(worldIn.getBlockState(newPos).has(TYPE) && worldIn.getBlockState(newPos).get(TYPE) != type) {
				worldIn.setBlockState(newPos, stateIn.with(TYPE, type), 3);
			}
			
			//Down
			newPos = pos.offset(Direction.DOWN);
			type = this.calcState(worldIn, newPos).get(TYPE);
			if(worldIn.getBlockState(newPos).has(TYPE) && worldIn.getBlockState(newPos).get(TYPE) != type) {
				worldIn.setBlockState(newPos, stateIn.with(TYPE, type), 3);
			}
		}
		super.updateNeighbors(stateIn, worldIn, pos, flags);
	}

	@Override
	public IFluidState getFluidState(BlockState state) {
		// TODO Auto-generated method stub
		return super.getFluidState(state);
	}

	@Override
	protected void fillStateContainer(Builder<Block, BlockState> builder) {
		builder.add(BlockStateProperties.HORIZONTAL_FACING);
		builder.add(TYPE);
	}
	
	public BlockState calcState(IWorldReader world, BlockPos pos) {
		if(world.getBlockState(pos.down()).getBlock() != TBlocks.tech_strut)
			return this.getDefaultState().with(TYPE, 0);
		if(world.getBlockState(pos.up()).getBlock() != TBlocks.tech_strut)
			return this.getDefaultState().with(TYPE, 2);
		
		return this.getDefaultState().with(TYPE, 1);
	}

	@Override
	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos) {
		return false;
	}

	@Override
	public boolean isSolid(BlockState state) {
		return false;
	}
	
	public static VoxelShape createVoxelShape() {
		VoxelShape shape = VoxelShapes.create(0.390625, 0.0, 0.59375, 0.640625, 1.0, 0.84375);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.453125, 0.0, 0.8125, 0.578125, 1.0, 1.0), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.453125, 0.125, 0.578125, 0.578125, 0.25, 0.703125), IBooleanFunction.OR);
		shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.453125, 0.75, 0.578125, 0.578125, 0.875, 0.703125), IBooleanFunction.OR);
		return shape;
	}

	@Override
	public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
		switch(state.get(BlockStateProperties.HORIZONTAL_FACING)) {
			case EAST: return EAST;
			case SOUTH: return SOUTH;
			case WEST: return WEST;
			default: return NORTH;
		}
	}
	
}
