package net.tardis.mod.events;


import java.util.Random;

import net.minecraft.block.BedBlock;
import net.minecraft.block.BellBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.merchant.villager.VillagerTrades.ITrade;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.FilledMapItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.MerchantOffer;
import net.minecraft.nbt.INBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.storage.MapData;
import net.minecraft.world.storage.MapDecoration.Type;
import net.minecraftforge.common.DimensionManager;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerRespawnEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.RegisterDimensionsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ChunkLoaderCapability;
import net.tardis.mod.cap.IChunkLoader;
import net.tardis.mod.cap.ILightCap;
import net.tardis.mod.cap.ITardisWorldData.TardisWorldProvider;
import net.tardis.mod.cap.LightCapability;
import net.tardis.mod.cap.entity.IPlayerData;
import net.tardis.mod.cap.entity.PlayerDataCapability;
import net.tardis.mod.cap.items.IRemote;
import net.tardis.mod.cap.items.IWatch;
import net.tardis.mod.cap.items.RemoteCapability;
import net.tardis.mod.cap.items.WatchCapability;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.dimensions.TardisDimension;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.sonic.SonicManager;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.trades.ItemTrade;
import net.tardis.mod.trades.Villager;
import net.tardis.mod.world.WorldGen;

@Mod.EventBusSubscriber(modid = Tardis.MODID)
public class CommonEvents {
	
	public static final ResourceLocation LIGHT_CAP = new ResourceLocation(Tardis.MODID, "light");
	public static final ResourceLocation CHUNK_CAP = new ResourceLocation(Tardis.MODID, "loader");
	public static final ResourceLocation TARDIS_CAP = new ResourceLocation(Tardis.MODID, "tardis_data");
	public static final ResourceLocation WATCH_CAP = new ResourceLocation(Tardis.MODID, "watch");
    public static final ResourceLocation PLAYER_DATA_CAP = new ResourceLocation(Tardis.MODID, "player_data");
    public static final ResourceLocation REMOTE_CAP = new ResourceLocation(Tardis.MODID, "remote");

	@SubscribeEvent
	public static void attachChunkCaps(AttachCapabilitiesEvent<Chunk> event) {
		if(event.getObject().getWorld().getDimension().getType().getModType() == TDimensions.TARDIS) {
			event.addCapability(LIGHT_CAP, new ILightCap.LightProvider(new LightCapability(event.getObject())));
		}
	}
	
	@SubscribeEvent
	public static void attachItemStackCap(AttachCapabilitiesEvent<ItemStack> event) {
		if(event.getObject().getItem() == TItems.POCKET_WATCH)
			event.addCapability(WATCH_CAP, new IWatch.Provider(new WatchCapability()));
		if(event.getObject().getItem() == TItems.STATTENHEIM_REMOTE)
			event.addCapability(REMOTE_CAP, new IRemote.Provider(new RemoteCapability(event.getObject())));
	}

    @SubscribeEvent
    public static void attachPlayerCap(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity)
            event.addCapability(PLAYER_DATA_CAP, new IPlayerData.Provider(new PlayerDataCapability((PlayerEntity) event.getObject())));
    }
	
    @SubscribeEvent
	public static void registerTrades(VillagerTradesEvent event) {
		
		if(event.getType() == Villager.STORY_TELLER) {
			event.getTrades().get(2).add(new ITrade() {

				@Override
				public MerchantOffer getOffer(Entity trader, Random rand) {
					BlockPos pos = WorldGen.getClosestBrokenExterior((int)trader.posX, (int)trader.posZ);
					if(pos != null && trader != null && trader.world != null) {
						ItemStack map = FilledMapItem.setupNewMap(trader.world, pos.getX(), pos.getZ(), (byte)1, true, true);
						MapData.addTargetDecoration(map, pos, "Artefact", Type.RED_X);
						map.setDisplayName(new StringTextComponent("Ancient Atrifact Map"));
						if (map != null) {
							FilledMapItem.renderBiomePreviewMap(trader.world, map);
							return new MerchantOffer(new ItemStack(Items.EMERALD, 10), map, 2, 1, 0);
						}
					}
					return null;
				}});
			event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.PAPER, 24), new ItemStack(Items.EMERALD), 3, 3));
			event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.GLASS_PANE, 10), new ItemStack(Items.EMERALD), 3, 3));
			ItemStack manual = new ItemStack(TItems.MANUAL);
			manual.setDisplayName(new StringTextComponent("Strange Journal"));
			event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.EMERALD, 3), manual, 5, 3));
		}

		
	}
    
	@SubscribeEvent
	public static void attachChunkLoadCaps(AttachCapabilitiesEvent<World> event) {
		if (event.getObject().isRemote) return;
		
		if(event.getObject().getDimension() instanceof TardisDimension)
			event.addCapability(TARDIS_CAP, new TardisWorldProvider(event.getObject()));
		
		 final LazyOptional<IChunkLoader> inst = LazyOptional.of(() -> new ChunkLoaderCapability((ServerWorld)event.getObject()));
	        final ICapabilitySerializable<INBT> provider = new ICapabilitySerializable<INBT>() {
	            @Override
	            public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
	                return Capabilities.CHUNK_LOADER.orEmpty(cap, inst);
	            }

	            @Override
	            public INBT serializeNBT() {
	                return Capabilities.CHUNK_LOADER.writeNBT(inst.orElse(null), null);
	            }

	            @Override
	            public void deserializeNBT(INBT nbt) {
	            	Capabilities.CHUNK_LOADER.readNBT(inst.orElse(null), null, nbt);
	            }
	        };
        event.addCapability(CHUNK_CAP, provider);
        event.addListener(() -> inst.invalidate());
	}
	
	@SubscribeEvent
	public static void onChunkLoad(ChunkEvent.Load event) {
		
	}


	@SubscribeEvent
	public static void onBlockBreak(BlockEvent.BreakEvent event) {
		if(event.getState().getBlock() instanceof IDontBreak) {
			event.setCanceled(true);
		}
	}
	
	@SubscribeEvent
	public static void onBlockClicked(PlayerInteractEvent.RightClickBlock event) {
		if(event.getWorld().getBlockState(event.getPos()).getBlock() instanceof BellBlock) {
			ChunkPos pos = event.getWorld().getChunk(event.getEntity().getPosition()).getPos();
			for(int x = -3; x < 3; ++x) {
				for(int z = -3; z < 3; ++z) {
					for(TileEntity te : event.getWorld().getChunk(pos.x + x, pos.z + z).getTileEntityMap().values()) {
						if(te instanceof ExteriorTile || te instanceof BrokenExteriorTile) {
							BlockPos soundPos = te.getPos().subtract(event.getEntity().getPosition());
							Vec3d vec = new Vec3d(soundPos.getX(), soundPos.getY(), soundPos.getZ()).normalize().scale(8);
							event.getWorld().playSound(null, event.getEntity().getPosition().add(new BlockPos(vec.x, vec.y, vec.z)), TSounds.SINGLE_CLOISTER, SoundCategory.BLOCKS, 1F, 0.5F);
						}
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onWorldTick(WorldTickEvent event) {
		//Set Phase to Start to ensure our event is only called once
		if(event.phase == Phase.START)
			event.world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> cap.tick());
			event.world.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> cap.tick());
	}


	@SubscribeEvent
	public static void onEntityJoin(EntityJoinWorldEvent event){
		if(event.getWorld().getDimension().getType().getModType() == TDimensions.TARDIS && !event.getWorld().isRemote) {

            //Ring alarm for naughty mobs
			if(event.getEntity() instanceof IMob) {
				TileEntity te = event.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
				if(te instanceof ConsoleTile)
					((ConsoleTile)te).getInteriorManager().setAlarmOn(true);
			}
			
			//If a player enters the TARDIS
			if(event.getEntity() instanceof ServerPlayerEntity) {
				ServerPlayerEntity player = (ServerPlayerEntity)event.getEntity();
				
				if(TardisHelper.isInOwnedTardis(player)){
					//Unlock things based on Achievement
					Helper.doIfAdvancementPresent("fortune_exterior", player, () -> {
						TileEntity te = player.getServerWorld().getTileEntity(TardisHelper.TARDIS_POS);
						if(te instanceof ConsoleTile) {
							IExterior ext = ExteriorRegistry.FORTUNE;
							ConsoleTile console = (ConsoleTile)te;
							if(!console.getExteriors().contains(ext)) {
								console.getExteriors().add(ext);
								player.sendStatusMessage(new TranslationTextComponent("status.tardis.exterior.unlock", ext.getDisplayName().getFormattedText()), true);
							}
						}
					});
					
					Helper.doIfAdvancementPresent("nemo_interior", player, () -> {
						ConsoleTile console = TardisHelper.getConsole(player.server, player.dimension);
						if(console != null) {
							Helper.unlockInterior(console, player, ConsoleRoom.NAUTILUS);
						}
					});
					
					Helper.doIfAdvancementPresent("trunk_exterior", player, () -> {
						ConsoleTile tile = TardisHelper.getConsole(player.server, player.dimension);
						if(tile != null)
							Helper.unlockExterior(tile, player, ExteriorRegistry.TRUNK);
					});
				}
			}
		}
	}
	
	//Only works in player's own tardis at the moment
	@SubscribeEvent
	public static void onSleep(PlayerSleepInBedEvent event) {
		//Checks if player have a Tardis
		if (TardisHelper.hasTARDIS(event.getPlayer().getServer(), event.getPlayer().getUniqueID())) {
		
			ConsoleTile console = TardisHelper.getConsole(event.getEntityPlayer().getServer(), event.getPlayer().getUniqueID());
			if(console instanceof ConsoleTile) {
				if(TardisHelper.isInATardis(event.getPlayer())) {
					console.addOrUpdateBedLoc(event.getPlayer(), event.getPos()); //Sets player spawn to Tardis dim
				}
				else if (!TardisHelper.isInOwnedTardis(event.getEntityPlayer())){
					//If the player sleeps successfully in a non Tardis dimension, remove the bed pos so vanilla will overwrite the tardis one
					//Prevents you from always spawning in the tardis even if you sleep in another dimension
					console.removePlayerBedLoc(event.getPlayer()); 
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void onPlayerRespawn(PlayerRespawnEvent event) {
		if (!event.getEntityPlayer().world.isRemote && TardisHelper.hasTARDIS(event.getPlayer().getServer(), event.getPlayer().getUniqueID())) {
			ConsoleTile console = TardisHelper.getConsole(event.getEntityPlayer().getServer(), event.getEntityPlayer().getUniqueID());
			if(console != null) {
				if (console.getBedPosForPlayer(event.getPlayer()) != null) {
					BlockPos pos = console.getBedPosForPlayer(event.getPlayer());
					if(!(console.getWorld().getBlockState(pos).getBlock() instanceof BedBlock) || console.getBedPosForPlayer(event.getPlayer()).equals(null)) {
							return; //should respawn them at overworld if they break bed in tardis and die
					}
					event.getPlayer().setPosition(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
					if (!TardisHelper.isInATardis(event.getPlayer())) {
						Helper.teleportEntities(event.getPlayer(), (ServerWorld)console.getWorld(), pos.getX()  + 0.5, pos.getY() + 1, pos.getZ() + 0.5, event.getPlayer().rotationYaw, event.getPlayer().rotationPitch);
					}
				}
			}
		}
	}
	
	@SubscribeEvent
	public static void registerPremDimensions(RegisterDimensionsEvent event) {
		TDimensions.VORTEX_TYPE = DimensionManager.registerOrGetDimension(new ResourceLocation(Tardis.MODID, "vortex"), TDimensions.VORTEX, null, false);
	}

    @SubscribeEvent
    public static void onLivingTick(LivingEvent.LivingUpdateEvent event) {
        if (event.getEntityLiving() instanceof PlayerEntity)
            event.getEntityLiving().getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> cap.tick());
    }


    //=== This is where I will handle special cases for blocks and entities that do something other than the intended sonic result
    @SubscribeEvent
    public static void sonicOnEntity(PlayerInteractEvent.EntityInteract event) {
        if (event.getItemStack().getItem() instanceof SonicItem) {
            ItemStack stack = event.getItemStack();
            SonicManager.ISonicMode mode = SonicItem.getCurrentMode(stack);
            mode.processSpecialEntity(event);
        }
    }

    @SubscribeEvent
    public static void sonicOnSpecialBlock(PlayerInteractEvent.RightClickBlock event) {
        if (event.getItemStack().getItem() instanceof SonicItem) {
            ItemStack stack = event.getItemStack();
            SonicManager.ISonicMode mode = SonicItem.getCurrentMode(stack);
            mode.processSpecialBlocks(event);
        }
    }
    
    @SubscribeEvent
    public static void onHurt(LivingHurtEvent event) {
    	if(event.getEntity() instanceof PlayerEntity && !event.getEntity().world.isRemote) {
    		PlayerEntity player = (PlayerEntity)event.getEntity();
    		if(TardisHelper.hasTARDIS(player.world.getServer(), player.getUniqueID()) && player.getHealth() < 6) {
    			if(Helper.canTravelToDimension(player.dimension)) {
    				ConsoleTile tile = TardisHelper.getConsole(player.world.getServer(), player.getUniqueID());
        			if(tile != null){
        				HandbrakeControl brake = tile.getControl(HandbrakeControl.class);
        				if(tile.getEmotionHandler().getLoyalty() > 100 && brake != null && brake.isFree()) {
        					tile.getExterior().remove(tile);
        					tile.setLocation(player.dimension, player.getPosition());
        					tile.getExterior().place(tile, player.dimension, player.getPosition());
        					ExteriorTile ext = tile.getExterior().getExterior(tile);
        					if(ext != null) {
        						ext.remat();
        					}
        				}
        			}
    			}
    		}
    	}
    }
}
