package net.tardis.mod.cap.entity;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;

public class PlayerDataCapability implements IPlayerData {

    private PlayerEntity player;
    private SpaceTimeCoord coord = SpaceTimeCoord.UNIVERAL_CENTER;

    public PlayerDataCapability(PlayerEntity ent) {
        this.player = ent;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.put("coord", this.coord.serialize());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if (nbt.contains("coord"))
            this.coord = SpaceTimeCoord.deserialize(nbt.getCompound("coord"));
    }

    @Override
    public SpaceTimeCoord getDestination() {
        return coord;
    }

    @Override
    public void setDestination(SpaceTimeCoord coord) {
        this.coord = coord;
    }

    @Override
    public void tick() {
        if (player instanceof ServerPlayerEntity) {
            if (player.dimension.getModType() == TDimensions.VORTEX) {
                if (player.posY < 0) {
                    ServerWorld world = player.world.getServer().getWorld(DimensionType.byName(coord.getDimType()));
                    if (world != null) {
                        BlockPos pos = coord.getPos();
                        player.fallDistance = 0;
                        ((ServerPlayerEntity) player).teleport(world, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 0);
                        ((ServerPlayerEntity) player).getServerWorld().playSound(null, player.getPosition(), TSounds.VM_TELEPORT_DEST, SoundCategory.BLOCKS, 0.25F, 1F);
                    }
                }
            }
        }
    }

}
