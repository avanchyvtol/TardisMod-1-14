package net.tardis.mod.cap.items;

import net.minecraft.entity.Entity;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class WatchCapability implements IWatch{

	private static final BlockPos RADIUS = new BlockPos(25, 25, 25); 
	private BrokenExteriorTile tile;
	
	public WatchCapability() {}
	
	@Override
	public void tick(World world, Entity ent) {
		if(tile == null || tile.isRemoved() || !tile.getPos().withinDistance(ent.getPosition(), 25)) {
			for(BlockPos pos : BlockPos.getAllInBoxMutable(ent.getPosition().subtract(RADIUS), ent.getPosition().add(RADIUS))) {
				if(world.getTileEntity(pos) instanceof BrokenExteriorTile) {
					tile = (BrokenExteriorTile)world.getTileEntity(pos);
					break;
				}
			}
		}
	}
	
	@Override
	public boolean shouldSpin(Entity ent) {
		return tile != null && !tile.isRemoved() && tile.getPos().withinDistance(ent.getPosition(), 25);
	}

}
