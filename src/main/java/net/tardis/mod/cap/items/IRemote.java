package net.tardis.mod.cap.items;

import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;

public interface IRemote extends INBTSerializable<CompoundNBT>{

	SpaceTimeCoord getExteriorLocation();
	void setExteriorLocation(SpaceTimeCoord coord);
	int getTimeToArrival();
	void setTimeToArrival(int time);
	UUID getOwner();
	void setOwner(UUID id);
	BlockPos getExteriorPos();
	DimensionType getExteriorDim();
	
	void onClick(World world, PlayerEntity player, BlockPos pos);
	void tick(World world, Entity ent);
	
	public static class Storage implements Capability.IStorage<IRemote>{

		@Override
		public INBT writeNBT(Capability<IRemote> capability, IRemote instance, Direction side) {
			return instance.serializeNBT();
		}

		@Override
		public void readNBT(Capability<IRemote> capability, IRemote instance, Direction side, INBT nbt) {
			if(nbt instanceof CompoundNBT)
				instance.deserializeNBT((CompoundNBT)nbt);
		}
		
	}
	
	public static class Provider implements ICapabilitySerializable<CompoundNBT>{

		IRemote remote;
		
		public Provider(IRemote rem) {
			this.remote = rem;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == Capabilities.REMOTE_CAP ? (LazyOptional<T>)LazyOptional.of(() -> remote) : LazyOptional.empty();
		}

		@Override
		public CompoundNBT serializeNBT() {
			return remote.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundNBT nbt) {
			remote.deserializeNBT(nbt);
		}
		
	}
}
