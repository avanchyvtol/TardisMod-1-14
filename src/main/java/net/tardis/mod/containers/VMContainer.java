package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

/**
 * Created by 50ap5ud5
 * on 22 Apr 2020 @ 8:12:47 pm
 */
public class VMContainer extends Container{
   private final IInventory lowerChestInventory;
   private final int numRows;
	
	public VMContainer(ContainerType<?> type, int id, PlayerInventory playerInventoryIn, PacketBuffer buf,int rows) {
		 super(type,id);
		 this.lowerChestInventory = playerInventoryIn;
		 this.numRows = rows;
	}

	public VMContainer(ContainerType<?> type, int id, PlayerInventory playerInventoryIn, int rows) {
		 this(type, id, playerInventoryIn, new Inventory(3 * rows), rows);
	}
	
	public static VMContainer createGeneric3x1(int id, PlayerInventory player, IInventory inv) {
		return new VMContainer(TContainers.VORTEX_M_BATTERY, id, player, inv, 1);
	}
	
	public static VMContainer createGeneric3x1New(int id, PlayerInventory player) {
		return new VMContainer(TContainers.VORTEX_M_BATTERY, id, player, 1);
	}

	public VMContainer(ContainerType<?> type, int id, PlayerInventory playerInventoryIn, IInventory inventory, int rows) {
		super(type,id);
		this.lowerChestInventory = inventory;
		this.numRows = rows;
		inventory.openInventory(playerInventoryIn.player);
	      int i = (this.numRows - 4) * 18;
	      
	      //The actual Container Slots
	      for(int j = 0; j < this.numRows; ++j) {
	         for(int k = 0; k < 3; ++k) {
	            this.addSlot(new Slot(inventory, k + j * 3, 8 + k * 18, 18 + j * 18));
	         }
	      }
	      
	      //Player inventory
	      for(int l = 0; l < 3; ++l) {
	         for(int j1 = 0; j1 < 9; ++j1) {
	            this.addSlot(new Slot(playerInventoryIn, j1 + l * 9 + 9, 8 + j1 * 18, 103 + l * 18 + i));
	         }
	      }
	      
	      //Player Hotbar
	      for(int i1 = 0; i1 < 9; ++i1) {
	         this.addSlot(new Slot(playerInventoryIn, i1, 8 + i1 * 18, 161 + i));
	      }
	}
	

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		Slot slot = this.inventorySlots.get(index);
		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();
			if (index < this.numRows * 3) {
				if (!this.mergeItemStack(itemstack1, this.numRows * 3, this.inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.mergeItemStack(itemstack1, 0, this.numRows * 3, false)) {
				return ItemStack.EMPTY;
			}
			
			if (itemstack1.isEmpty()) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
		}

		return itemstack;
	}
	
	public IInventory getLowerChestInventory() {
	      return this.lowerChestInventory;
	   }

   @OnlyIn(Dist.CLIENT)
   public int getNumRows() {
      return this.numRows;
   }

}
