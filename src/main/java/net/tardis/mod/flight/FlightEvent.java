package net.tardis.mod.flight;

import com.google.common.collect.Lists;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.controls.*;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.registries.TardisRegistries;

import java.util.ArrayList;
import java.util.List;

public class FlightEvent implements IRegisterable<FlightEvent> {

    public static FlightEvent SCRAP;

    private List<Class<? extends IControl>> controlsToHit = new ArrayList<Class<? extends IControl>>();
    private ResourceLocation name;
    private TranslationTextComponent translation;

    @SafeVarargs
    public FlightEvent(String name, Class<? extends IControl>... controls) {
        this.translation = new TranslationTextComponent("flight.tardis." + name);
        this.controlsToHit = Lists.newArrayList(controls);
    }

    public static void registerAll() {
        TardisRegistries.FLIGHT_EVENT.register("scrap", SCRAP = new FlightEvent("scrap", FacingControl.class, RandomiserControl.class));
        TardisRegistries.FLIGHT_EVENT.register("time_wind", new FlightEvent("time_wind", ThrottleControl.class));
        TardisRegistries.FLIGHT_EVENT.register("x", new FlightEvent("x", XControl.class));
        TardisRegistries.FLIGHT_EVENT.register("y", new FlightEvent("y", YControl.class));
        TardisRegistries.FLIGHT_EVENT.register("z", new FlightEvent("z", ZControl.class));
        TardisRegistries.FLIGHT_EVENT.register("refueler", new FlightEvent("refueler", RefuelerControl.class));
    }

    @Override
    public ResourceLocation getRegistryName() {
        return this.name;
    }

    @Override
    public FlightEvent setRegistryName(ResourceLocation regName) {
        this.name = regName;
        return this;
    }

    public List<Class<? extends IControl>> getControls() {
        return this.controlsToHit;
    }

    public TranslationTextComponent getTranslation() {
        return this.translation;
    }
}
