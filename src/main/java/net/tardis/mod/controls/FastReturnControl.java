package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class FastReturnControl extends BaseControl{

	public FastReturnControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.099999994F, 0.099999994F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.25F, 0.25F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);

		return EntitySize.flexible(0.125F, 0.125F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			SpaceTimeCoord coord = console.getReturnLocation();
			console.setDestination(DimensionType.byName(coord.getDimType()), coord.getPos());
			console.setDirection(coord.getFacing());
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(9 / 16.0, 7 / 16.0, 10.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.26433123161703187, 0.45625, -0.8173110443482853);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.2420538492516182, 0.28125, -1.3057008551153237);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.7572450871689476, 0.53125, -0.12846421576058586);

		return new Vec3d(16.0 / 16.0, 7.0 / 16.0, 2.5 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE;
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

    @Override
    public int getAnimationTicks() {
        return 20;
    }
}
