package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;

public class DoorControl extends BaseControl{

	public DoorControl(ConsoleTile console) {
		super(console);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		return EntitySize.flexible(0.1625F, 0.1625F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		boolean lock = player.isSneaking();
		if(!console.getWorld().isRemote) {
			for(DoorEntity ent : console.getWorld().getEntitiesWithinAABB(DoorEntity.class, new AxisAlignedBB(console.getPos()).grow(25))) {
				if(lock) {
					ent.setLocked(!ent.isLocked());
					ent.playSound(ent.isLocked() ? TSounds.DOOR_LOCK : TSounds.DOOR_UNLOCK, 1F, 1F);
					player.sendStatusMessage(ent.isLocked() ? ExteriorBlock.LOCKED : ExteriorBlock.UNLOCKED, true);
					ent.setOpenState(EnumDoorState.CLOSED);
				}
				else if(!ent.isLocked()){
					if(ent.getOpenState() == EnumDoorState.CLOSED)
						ent.setOpenState(EnumDoorState.BOTH);
					else ent.setOpenState(EnumDoorState.CLOSED);
					ent.playSound(ent.getOpenState() == EnumDoorState.CLOSED ? TSounds.DOOR_CLOSE : TSounds.DOOR_OPEN, 1F, 1F);
				}
			}
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(0.6213499136129208, 0.42499999701976776, -0.36428324173310445);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.7811600989048273, 0.46875, -0.1477678772167177);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.8847060867080625, 0.4375, 0.16935628475360354);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.004641167746829211, 0.5625, -0.9150439261422297);

		return new Vec3d(-0.7238774917092281, 0.5, 0.8087972034724291);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_TWO;
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

}
