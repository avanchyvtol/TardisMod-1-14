package net.tardis.mod.controls;

import java.util.HashMap;
import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;

public class TardisControls {

	private static HashMap<ResourceLocation, Supplier<IControl>> REGISTRY = new HashMap<ResourceLocation, Supplier<IControl>>();
	
	/*
	 * Register any controls anytime during loading, should be done on both sides
	 */
	public static void register(ResourceLocation loc, Supplier<IControl> sup) {
		REGISTRY.put(loc, sup);
	}
	
	public static void register(String name, Supplier<IControl> sup) {
		register(new ResourceLocation(Tardis.MODID, name), sup);
	}
	
	public static Supplier<IControl> getControl(ResourceLocation key) {
		try {
			if(!REGISTRY.containsKey(key))
				throw new Exception("No such control");
			return REGISTRY.get(key);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
