package net.tardis.mod.sonic;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.sonic.interactions.SonicBlockInteraction;
import net.tardis.mod.sonic.interactions.SonicEntityInteraction;
import net.tardis.mod.sonic.interactions.SonicTardisDestinationInteraction;

import java.util.ArrayList;
import java.util.function.Supplier;

/**
 * Created by Swirtzly
 * on 22/08/2019 @ 19:50
 */

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class SonicManager {


    public static final SonicManager.SonicMode BLOCK_INTERACT = new SonicManager.SonicMode(SonicBlockInteraction::new);
    public static final SonicManager.SonicMode ENTITY_INTERACT = new SonicManager.SonicMode(SonicEntityInteraction::new);
    public static final SonicManager.SonicMode TARDIS_DESTINATION = new SonicManager.SonicMode(SonicTardisDestinationInteraction::new);
    //public static final SonicManager.SonicMode LASER = new SonicManager.SonicMode(SonicLaserInteraction::new);

    public static SonicMode[] SONIC_TYPES_ARRAY;


    //Registry creation
    public static IForgeRegistry<SonicMode> REGISTRY;

    @SubscribeEvent
    public static void registerSonicModes(RegistryEvent.Register<SonicMode> event){
        event.getRegistry().registerAll(BLOCK_INTERACT, ENTITY_INTERACT, TARDIS_DESTINATION);
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, TConfig.CONFIG_SPEC);
        SONIC_TYPES_ARRAY = REGISTRY.getValues().toArray(new SonicMode[0]); //TODO move this
    }

    @SubscribeEvent
    public static void onRegisterNewRegistries(RegistryEvent.NewRegistry e) {
        REGISTRY = new RegistryBuilder<SonicMode>().setName(new ResourceLocation(Tardis.MODID, "sonic_modes")).setType(SonicMode.class).setIDRange(0, 2048).create();
    }


    public interface ISonicMode {
        boolean processBlock(PlayerEntity player, BlockState blockState, ItemStack sonic, BlockPos pos);

        boolean processEntity(PlayerEntity user, Entity targeted, ItemStack sonic);

        ResourceLocation getRegistryName();

        default String getLangKey() {
            return "sonic.mode." + getRegistryName().getPath();
        }

        default String getDescriptionLangKey() {
            return "sonic.mode." + getRegistryName().getPath() + ".desc";
        }

        default ArrayList<TranslationTextComponent> getAdditionalInfo() {
            return new ArrayList<>();
        }

        boolean hasAdditionalInfo();

        void updateHeld(PlayerEntity playerEntity, ItemStack stack);

        default boolean consumeCharge(float amount, ItemStack stack) {
            return true;
        }

        default double getReachDistance(){
            return 30D;
        }

        default void processSpecialBlocks(PlayerInteractEvent.RightClickBlock event) {

        }

        default void processSpecialEntity(PlayerInteractEvent.EntityInteract event) {

        }
    }

    public static class SonicMode extends ForgeRegistryEntry<SonicMode> {
        private Supplier<ISonicMode> supplier;

        public SonicMode(Supplier<ISonicMode> supplier) {
            this.supplier = supplier;
            setRegistryName(supplier.get().getRegistryName());
        }

        public SonicMode(Supplier<ISonicMode> supplier, String modid, String name) {
            this.supplier = supplier;
            this.setRegistryName(modid, name);
        }

        public SonicMode(Supplier<ISonicMode> supplier, ResourceLocation resourceLocation) {
            this.supplier = supplier;
            this.setRegistryName(resourceLocation);
        }

        public ISonicMode getSonicType() {
            return this.supplier.get();
        }
    }


}
