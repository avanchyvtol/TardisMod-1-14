package net.tardis.mod.sonic;

import net.minecraft.item.Item;
import net.tardis.mod.items.TItems;

/**
 * Created by Swirtzly
 * on 21/03/2020 @ 19:16
 */
public interface ISonicPart {

    SonicPart getSonicPart();
    void update(); //May not be needed

    enum SonicPart{
        EMITTER(0), ACTIVATOR(1), HANDLE(2), END(3);

        private final int invID;

        SonicPart(int invId) {
            this.invID = invId;
        }
        
        public int getInvID() {
            return invID;
        }
        
        public static SonicPart getFromItem(Item item) {
        	if(item == TItems.SONIC_EMITTER)
        		return SonicPart.EMITTER;
        	if(item == TItems.SONIC_ACTIVATOR)
        		return SonicPart.ACTIVATOR;
        	if(item == TItems.SONIC_END)
        		return SonicPart.END;
        	return SonicPart.HANDLE;
        }
    }

}
