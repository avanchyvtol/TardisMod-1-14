package net.tardis.mod.exterior;

import javax.annotation.Nullable;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public interface IExterior {
	
	void demat(ConsoleTile console);
	
	void remat(ConsoleTile console);
	
	ExteriorTile getExterior(ConsoleTile console);
	
	ResourceLocation getRegistryName();
	IExterior setRegistryName(ResourceLocation name);
	
	void remove(ConsoleTile tile);
	void place(ConsoleTile tile, DimensionType type, BlockPos pos);
	
	/*
	 * Whether this is added to the chameleon circuit by default
	 */
	boolean isDefault();
	
	int getWidth(ConsoleTile console);
	int getHeight(ConsoleTile console);
	
	ResourceLocation getBlueprintTexture();
	
	IDoorType getDoorType();
	
	TranslationTextComponent getDisplayName();
	
	@Nullable
	TexVariant[] getVariants();
}
