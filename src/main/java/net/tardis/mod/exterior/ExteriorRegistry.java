package net.tardis.mod.exterior;

import java.util.ArrayList;
import java.util.HashMap;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.texturevariants.TextureVariants;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class ExteriorRegistry {

	private static HashMap<ResourceLocation, IExterior> REGISTRY = new HashMap<ResourceLocation, IExterior>();
	
	public static TwoBlockBasicExterior STEAMPUNK;
	public static TwoBlockBasicExterior TRUNK;
	public static TwoBlockBasicExterior RED;
	public static TwoBlockBasicExterior POLICE_BOX;
	public static TwoBlockBasicExterior FORTUNE;
	
	public static <T extends IExterior> T register(ResourceLocation key, T ext) {
		ext.setRegistryName(key);
		REGISTRY.put(key, ext);
		return ext;
	}
	
	public static IExterior getExterior(ResourceLocation key){
		return REGISTRY.get(key);
	}
	
	public static ArrayList<IExterior> getDefaultExteriors() {
		ArrayList<IExterior> list = new ArrayList<IExterior>();
		for(IExterior ext : REGISTRY.values()) {
			if(ext.isDefault())
				list.add(ext);
		}
		return list;
	}
	
	@SubscribeEvent
	public static void registerExteriors(FMLCommonSetupEvent event) {
		STEAMPUNK = register(new ResourceLocation(Tardis.MODID, "steampunk"), new TwoBlockBasicExterior(() -> TBlocks.exterior_steampunk.getDefaultState(), true, EnumDoorType.STEAM, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/steam.png")));
		TRUNK = register(new ResourceLocation(Tardis.MODID, "trunk"), new TwoBlockBasicExterior(() -> TBlocks.exterior_trunk.getDefaultState(), false, EnumDoorType.TRUNK, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/trunk.png"), TextureVariants.TRUNK));
		RED = register(new ResourceLocation(Tardis.MODID, "red"), new TwoBlockBasicExterior(() -> TBlocks.exterior_red.getDefaultState(), true, EnumDoorType.RED, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/red.png")));
		POLICE_BOX = register(new ResourceLocation(Tardis.MODID, "police_box"), new TwoBlockBasicExterior(() -> TBlocks.exterior_police_box.getDefaultState(), false, EnumDoorType.POLICE_BOX, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/police_box.png")));
		FORTUNE = register(new ResourceLocation(Tardis.MODID, "fortune"), new TwoBlockBasicExterior(() -> TBlocks.exterior_fortune.getDefaultState(), false, EnumDoorType.FORTUNE, new ResourceLocation(Tardis.MODID, "textures/gui/exteriors/fortune.png")));
	}
}
