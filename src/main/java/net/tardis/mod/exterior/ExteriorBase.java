package net.tardis.mod.exterior;

import net.minecraft.util.ResourceLocation;

public abstract class ExteriorBase implements IExterior{

	private ResourceLocation registryName;

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}

	@Override
	public IExterior setRegistryName(ResourceLocation name) {
		this.registryName = name;
		return this;
	}

}
